import os
from daiquiri.resources.utils import ConfigDict

COMMENTED_YAML = """value: true
# a comment
somelist:
  - one
  - two

adict:
  akey: avalue
  bkey: bvalue # inline comment

keywithinclude:
  # should leave this entry alone
  - !include include.yml
"""


def test_load_modify_dump(mock_resource_provider):
    mock_resource_provider.create_resource("config", "include.yml", "included: true")
    mock_resource_provider.create_resource("config", "comments.yml", COMMENTED_YAML)

    conf = ConfigDict("comments.yml")
    conf["adict"]["akey"] = "cvalue"
    conf.save()

    path = os.path.join(
        mock_resource_provider.root, conf._resource_type, conf._resource_name
    )
    with open(path) as file:
        contents = file.read()
        assert contents == COMMENTED_YAML.replace("avalue", "cvalue")
