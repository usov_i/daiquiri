import pytest

from daiquiri.core.exceptions import SyntaxErrorYAML
from daiquiri.resources import utils


def test_include_partial(mock_resource_provider):
    """Test to include partial as one liner syntax"""
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include partials/2dview.yml"
    )

    contents = utils.YamlDict("layout", "test.yml")
    assert isinstance(contents["value"], dict)


def test_include_partial_with_default_variables(mock_resource_provider):
    """Test to include a real production partial"""
    mock_resource_provider.create_resource(
        "layout", "test.yml", "value: !include partials/2dview.yml"
    )

    contents = utils.YamlDict("layout", "test.yml")
    assert isinstance(contents["value"], dict)


def test_include_partial_variable(mock_resource_provider):
    """
    Include a partial containing default variables.

    The layout must have interpolated the layout with the variable defined at the include.
    """
    mock_resource_provider.create_resource(
        "layout", "include.yml", "variable: ${variable}"
    )
    mock_resource_provider.create_resource(
        "layout",
        "test.yml",
        """
value: !include
      file: include.yml
      variables:
        variable: 2
""",
    )

    contents = utils.YamlDict("layout", "test.yml")
    assert contents["value"]["variable"] == 2
    assert "default_variables" not in contents["value"]


def test_include_partial_default_variables(mock_resource_provider):
    """
    Include a partial containing default variables.

    The layout must have interpolated the layout with the default variables.
    """
    mock_resource_provider.create_resource(
        "layout",
        "include.yml",
        """
variable: ${variable}
default_variables:
    variable: 1
""",
    )
    mock_resource_provider.create_resource(
        "layout",
        "test.yml",
        """
value: !include
      file: include.yml
      variables:
        variable: 2
""",
    )

    contents = utils.YamlDict("layout", "test.yml")
    assert contents["value"]["variable"] == 2
    assert "default_variables" not in contents["value"]


def test_include_partial_override_default_variables(mock_resource_provider):
    """
    Include a partial containing default variables.

    The layout must have interpolated the layout with the variable defined at the include.
    Default variables are ignored in this case.
    """
    mock_resource_provider.create_resource(
        "layout",
        "include.yml",
        """
variable: ${variable}
default_variables:
    variable: 1
""",
    )
    mock_resource_provider.create_resource(
        "layout",
        "test.yml",
        """
value: !include
      file: include.yml
""",
    )

    contents = utils.YamlDict("layout", "test.yml")
    assert contents["value"]["variable"] == 1


def test_include_partial_invalid(mock_resource_provider):
    mock_resource_provider.create_resource(
        "layout",
        "test.yml",
        """
value: !include
  nofile: partials/2dview.yml
""",
    )

    with pytest.raises(SyntaxErrorYAML) as e:
        utils.YamlDict("layout", "test.yml")
    assert "must include a `file`" in e.value.args[0]["error"]
