import os
import pytest
from daiquiri.resources.utils import add_resource_root
from daiquiri.resources.utils import RESOURCE_PROVIDER


class MockResourceProvider:
    def __init__(self, root):
        self.__root = root

    def abs_path(self, resourcetype: str, resourcename: str):
        filepath = os.path.join(self.__root, resourcetype, resourcename)
        return os.path.abspath(filepath)

    @property
    def root(self):
        return self.__root

    def create_resource(self, resourcetype: str, resourcename: str, content: str):
        resource_dir = os.path.join(self.__root, resourcetype)
        if not os.path.exists(resource_dir):
            os.mkdir(resource_dir)
        file = os.path.join(self.__root, resourcetype, resourcename)
        with open(file, "w") as f:
            f.write(content)


@pytest.fixture
def mock_resource_provider(tmpdir_factory):
    global RESOURCE_PROVIDER
    mock_resources = tmpdir_factory.mktemp("resources")
    os.mkdir(mock_resources.join("config"))

    # reset the global allocated providers
    RESOURCE_PROVIDER = None

    root = str(mock_resources)
    add_resource_root(root)
    yield MockResourceProvider(root)
