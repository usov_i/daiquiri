import subprocess
import os
import sys
import socket
import shutil
import time
import atexit
from collections import namedtuple
from contextlib import contextmanager

import ruamel.yaml
import gevent
import pytest
import redis

from bliss.config import static
from bliss.config.conductor import client, connection
from bliss.tango.clients.utils import wait_tango_device, wait_tango_db
from bliss.testutils.process_utils import start_tango_server, wait_terminate

import daiquiri
from daiquiri.app import init_server


_HERE = os.path.dirname(__file__)
DAIQUIRI_DOC = os.path.realpath(os.path.join(_HERE, "..", "doc"))
"""Location of the project documentation"""

DAIQUIRI_ROOT = os.path.dirname(daiquiri.__file__)
"""Location of the package data"""


def pytest_configure(config):
    sys._running_pytest = True


def pytest_addoption(parser):
    parser.addoption(
        "--apispec",
        action="store_true",
        help="specific test to generate and check apispec",
    )


def pytest_runtest_setup(item):
    if "apispec" in item.keywords and not item.config.getoption("--apispec"):
        pytest.skip("need --apispec option to run this test")


def get_open_ports(n):
    sockets = [socket.socket() for _ in range(n)]
    try:
        for s in sockets:
            s.bind(("", 0))
        return [s.getsockname()[1] for s in sockets]
    finally:
        for s in sockets:
            s.close()


def wait_for(stream, target):
    def do_wait_for(stream, target, data=b""):
        target = target.encode()
        while target not in data:
            char = stream.read(1)
            if not char:
                raise RuntimeError(
                    "Target {!r} not found in the following stream:\n{}".format(
                        target, data.decode()
                    )
                )
            data += char

    return do_wait_for(stream, target)


BLISS = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BEACON = [sys.executable, "-m", "bliss.config.conductor.server"]
BEACON_DB_PATH = os.path.join(BLISS, "tests", "bliss_configuration")


@pytest.fixture(scope="session")
def beacon_directory(tmpdir_factory):
    tmpdir = str(tmpdir_factory.mktemp("beacon"))
    beacon_dir = os.path.join(tmpdir, "bliss_configuration")
    shutil.copytree(BEACON_DB_PATH, beacon_dir)
    yield beacon_dir


@pytest.fixture(scope="session")
def ports(beacon_directory, tmpdir_factory):
    redis_uds = os.path.join(beacon_directory, "redis.sock")
    # redis_data_uds = os.path.join(beacon_directory, "redis_data.sock")

    port_names = [
        "redis_port",
        "redis_data_port",
        "tango_port",
        "beacon_port",
        "cfgapp_port",
        "logserver_port",
        "homepage_port",
    ]

    ports = namedtuple("Ports", " ".join(port_names))(*get_open_ports(7))
    args = [
        "--port=%d" % ports.beacon_port,
        "--redis_port=%d" % ports.redis_port,
        "--redis_socket=" + redis_uds,
        # "--redis-data-port=%d" % ports.redis_data_port,
        # "--redis-data-socket=" + redis_data_uds,
        "--db_path=%s" % beacon_directory,
        "--tango_port=%d" % ports.tango_port,
        "--webapp_port=%d" % ports.cfgapp_port,
        "--homepage-port=%d" % ports.homepage_port,
        "--log_server_port=%d" % ports.logserver_port,
        # "--log_output_folder=%s" % log_directory,
        "--log-level=WARN",
        "--tango_debug_level=0",
    ]
    proc = subprocess.Popen(BEACON + args)
    wait_ports(ports)

    # disable .rdb files saving (redis persistence)
    r = redis.Redis(host="localhost", port=ports.redis_port)
    r.config_set("SAVE", "")
    del r

    os.environ["TANGO_HOST"] = "localhost:%d" % ports.tango_port
    os.environ["BEACON_HOST"] = "localhost:%d" % ports.beacon_port

    tmpdir = tmpdir_factory.mktemp("celery")
    os.environ["CELERY_BROKER_URL"] = "memory://"
    os.environ["CELERY_BACKEND_URL"] = f"db+sqlite:///{tmpdir / 'celery_results.db'}"

    yield ports

    atexit._run_exitfuncs()
    wait_terminate(proc)


def wait_ports(ports, timeout=10):
    with gevent.Timeout(timeout):
        wait_tcp_online("localhost", ports.beacon_port)
        wait_tango_db(port=ports.tango_port, db=2)


@pytest.fixture(scope="session")
def beacon(ports):
    redis_db = redis.Redis(port=ports.redis_port)
    redis_db.flushall()
    # redis_data_db = redis.Redis(port=ports.redis_data_port)
    # redis_data_db.flushall()
    static.Config.instance = None
    client._default_connection = connection.Connection("localhost", ports.beacon_port)
    config = static.get_config()
    yield config
    config.close()
    client._default_connection.close()
    # Ensure no connections are created due to garbage collection:
    client._default_connection = None


def wait_tcp_online(host, port, timeout=10):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        with gevent.Timeout(timeout):
            while True:
                try:
                    sock.connect((host, port))
                    break
                except ConnectionError:
                    pass
                gevent.sleep(0.1)
    finally:
        sock.close()


@contextmanager
def lima_simulator_context(personal_name, device_name):
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/LimaCCDs/{personal_name}"

    conda_env = os.environ.get("LIMA_SIMULATOR_CONDA_ENV")
    if not conda_env:
        conda_env = None
    if conda_env is not None:
        runner = ["conda", "run", "-n", conda_env, "--no-capture-output", "LimaCCDs"]
    else:
        runner = ["LimaCCDs"]

    extra_kwargs = {}
    if conda_env is not None:
        extra_kwargs["check_children"] = True

    with start_tango_server(
        *runner,
        personal_name,
        # "-v4",               # to enable debug
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=None,
        **extra_kwargs,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


_DEVICE_PROXY_HOLDER = []
"""Holder to avoid release of device proxy.

Workaround for https://gitlab.com/tango-controls/pytango/-/issues/315#note_972732865
"""


@pytest.fixture
def lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy
    _DEVICE_PROXY_HOLDER.append(fqdn_proxy[1])


@pytest.fixture(scope="module")
def module_lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture(scope="session")
def app(beacon):
    resource_folders = [os.path.join(os.path.dirname(__file__), "resources")]
    app, socketio = init_server(resource_folders=resource_folders, testing=True)
    app.socketio = socketio
    return app


@pytest.fixture(scope="session")
def write_spec(beacon):
    resource_folders = [os.path.join(os.path.dirname(__file__), "resources")]
    try:
        app, _socketio = init_server(
            resource_folders=resource_folders,
            testing=True,
            save_spec_file=f"{DAIQUIRI_DOC}/api/spec.json",
        )
    except SystemExit:
        pass


@pytest.fixture
def resource_folder():
    yield os.path.join(os.path.dirname(__file__), "resources")


@pytest.fixture
def tomo_installed():
    try:
        import tomo  # noqa: F401
    except ImportError:
        pytest.skip("ebs-tomo is not installed")


@pytest.fixture(scope="session")
def celery_config():
    return {
        "broker_url": os.environ["CELERY_BROKER_URL"],
        "result_backend": os.environ["CELERY_BACKEND_URL"],
        "worker_send_task_events": True,
        "database_engine_options": {"isolation_level": "READ UNCOMMITTED"},
    }


@pytest.fixture
def with_celery_task(celery_app):
    @celery_app.task(name="sleep")
    def sleep(sleep_time: int = 0.1, raise_exception: bool = False, **kwargs):
        print("sleeping")
        if raise_exception:
            raise RuntimeError("Task raising")
        time.sleep(sleep_time)
        print("sleep done")

        return True

    yield sleep


@pytest.fixture
def reloaded_celery_worker(with_celery_task, celery_worker):
    # TODO: https://github.com/celery/celery/issues/3642
    celery_worker.reload()
    yield celery_worker
