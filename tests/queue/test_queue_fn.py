import time


def test_queue(empty_queue, create_actor):
    actor = create_actor(runtime=3)
    empty_queue.push(actor)

    status = empty_queue.status()
    assert len(status["stack"]) == 1
    act = status["stack"][0]
    assert act["uid"] == actor.uid


def test_queue_run(empty_queue, create_actor):
    actor = create_actor(runtime=3)
    empty_queue.push(actor)
    empty_queue.start()

    status = empty_queue.status()
    while not status["current"]:
        status = empty_queue.status()
        time.sleep(0.1)
    assert status["current"]["uid"] == actor.uid


def test_queue_kill(empty_queue, create_actor):
    actor = create_actor(runtime=10)
    empty_queue.push(actor)
    empty_queue.start()

    while actor.execstate != "running":
        time.sleep(0.1)
    empty_queue.kill()
    while actor.execstate == "running":
        time.sleep(0.1)
    assert actor.execstate == "killed"


def test_queue_move(empty_queue, create_actor):
    actors = [create_actor(runtime=3) for i in range(3)]

    for a in actors:
        empty_queue.push(a)

    empty_queue.move_item(actors[0].uid, 2)

    status = empty_queue.status()
    assert status["stack"][2]["uid"] == actors[0].uid


def test_queue_remove(empty_queue, create_actor):
    actor = create_actor(runtime=3)
    empty_queue.push(actor)

    empty_queue.remove(actor.uid)
    status = empty_queue.status()

    assert len(status["stack"]) == 0


def test_queue_clear(empty_queue, create_actor):
    actor = create_actor(runtime=3)
    empty_queue.push(actor)

    empty_queue.clear()
    status = empty_queue.status()

    assert len(status["stack"]) == 0
