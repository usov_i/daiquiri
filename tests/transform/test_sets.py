import pytest
import numpy
import random
from daiquiri.core.transform import sets


def test_realvectorspace():
    for _ in range(100):
        ndim = random.randint(1, 5)
        ncoord = random.randint(1, 3)
        Rn = sets.RealVectorSpace(ndim=ndim)
        assert Rn.isVectorSpace
        assert_space_infinite(Rn)
        assert_space_selfcmp(Rn)
        coord = Rn.random_uniform(ncoord)
        Rn.raiseIfNotIn(coord)

    # Check specific sub/super spaces
    R3 = sets.RealVectorSpace(ndim=3)
    R2 = sets.RealVectorSpace(ndim=2)
    R3.raiseIfNotIn(R2)
    assert_proper_subset(R3, R2)
    assert_proper_superset(R2, R3)


def test_realinterval():
    # Random coordinate generation and validation
    for _ in range(100):
        ndim = random.randint(1, 5)
        ncoord = random.randint(1, 3)
        borders = numpy.random.uniform(-10, 10, size=(ndim, 2))
        closed = numpy.random.choice(a=[False, True], size=(ndim, 2))
        limits = numpy.concatenate([borders, closed], axis=1)
        superinterval = sets.RealVectorSpace(ndim=ndim + 1)
        interval = sets.RealVectorSpace(ndim=ndim)
        assert_space_infinite(interval)
        assert_space_selfcmp(interval)
        assert_proper_subset(superinterval, interval)
        assert_proper_superset(interval, superinterval)

        # Explicit open/closed intervals
        subinterval = sets.RealInterval(limits=limits)
        assert_space_finite(subinterval)
        assert_space_selfcmp(subinterval)
        assert_proper_subset(interval, subinterval)
        assert_proper_superset(subinterval, interval)
        assert_proper_subset(superinterval, subinterval)
        assert_proper_superset(subinterval, superinterval)
        orgborders = borders.copy()
        borders.sort(axis=1)
        flip = (borders != orgborders).any(axis=1)
        closed[flip] = closed[flip, ::-1]
        numpy.testing.assert_array_equal(subinterval.limits.low, borders[:, 0])
        numpy.testing.assert_array_equal(subinterval.limits.high, borders[:, 1])
        numpy.testing.assert_array_equal(subinterval.limits.clow, closed[:, 0])
        numpy.testing.assert_array_equal(subinterval.limits.chigh, closed[:, 1])
        coord = subinterval.random_uniform(ncoord)
        subinterval.raiseIfNotIn(coord)

        # Implicit closed intervals
        subinterval = sets.RealInterval(limits=borders)
        assert_space_finite(subinterval)
        assert_space_selfcmp(subinterval)
        assert_proper_subset(interval, subinterval)
        assert_proper_superset(subinterval, interval)
        assert_proper_subset(superinterval, subinterval)
        assert_proper_superset(subinterval, superinterval)
        closed[:] = True
        numpy.testing.assert_array_equal(subinterval.limits.low, borders[:, 0])
        numpy.testing.assert_array_equal(subinterval.limits.high, borders[:, 1])
        numpy.testing.assert_array_equal(subinterval.limits.clow, closed[:, 0])
        numpy.testing.assert_array_equal(subinterval.limits.chigh, closed[:, 1])
        coord = subinterval.random_uniform(ncoord)
        subinterval.raiseIfNotIn(coord)

        # No boundaries
        limits = [[-numpy.inf, numpy.inf]] * ndim
        subinterval = sets.RealInterval(limits=limits)
        assert subinterval.isVectorSpace
        assert_space_infinite(subinterval)
        assert_space_selfcmp(subinterval)
        assert_subset(interval, subinterval)
        assert_superset(subinterval, interval)
        assert_proper_subset(superinterval, subinterval)
        assert_proper_superset(subinterval, superinterval)
        coord = numpy.random.uniform(-100, 100, (ncoord, ndim))
        interval.raiseIfNotIn(coord)

    # Check specific sub/super spaces
    interval = sets.RealInterval(limits=[[0, 1, True, True], [0, 1, True, True]])
    subinterval = sets.RealInterval(limits=[[0.1, 1, True, True], [0, 1, True, True]])
    assert subinterval in interval

    interval = sets.RealInterval(limits=[[0, 1, True, False], [0, 1, False, True]])
    subinterval = sets.RealInterval(limits=[[0.1, 1, True, False], [0, 1, False, True]])
    assert subinterval in interval

    interval = sets.RealInterval(limits=[[0, 1, False, True], [0, 1, True, True]])
    subinterval = sets.RealInterval(limits=[[0, 1, True, True], [0, 1, True, True]])
    with pytest.raises(AssertionError):
        assert subinterval in interval

    interval = sets.RealInterval(limits=[[0, 1, True, True], [0, 1, True, True]])
    subinterval = sets.RealInterval(limits=[0, 1, True, True])
    assert subinterval in interval

    interval = sets.RealInterval(limits=[[0, 1, True, True], [0, 1, True, True]])
    subinterval = sets.RealInterval(limits=[-1, 1, True, True])
    with pytest.raises(AssertionError):
        assert subinterval in interval

    interval = sets.RealInterval(
        limits=[[0, 1, True, True], [-numpy.inf, numpy.inf, True, True]]
    )
    subinterval = sets.RealInterval(
        limits=[[0, 1, True, True], [0, numpy.inf, True, True]]
    )
    assert subinterval in interval

    interval = sets.RealInterval(
        limits=[[0, 1, True, True], [-numpy.inf, numpy.inf, True, False]]
    )
    subinterval = sets.RealInterval(
        limits=[[0, 1, True, True], [0, numpy.inf, True, True]]
    )
    with pytest.raises(AssertionError):
        assert subinterval in interval

    # Border cases
    superinterval = sets.RealVectorSpace(ndim=2)
    subinterval = sets.RealInterval(limits=[0, 0, False, False])
    assert_proper_superset(subinterval, superinterval)
    assert 0 not in subinterval
    subinterval = sets.RealInterval(limits=[0, 0, True, False])
    assert_proper_superset(subinterval, superinterval)
    assert 0 not in subinterval
    subinterval = sets.RealInterval(limits=[0, 0, False, True])
    assert_proper_superset(subinterval, superinterval)
    assert 0 not in subinterval
    superinterval = sets.RealInterval(limits=[0, 0, True, False])
    subinterval = sets.RealInterval(limits=[0, 0, False, False])
    with pytest.raises(AssertionError):
        assert_proper_superset(subinterval, superinterval)
    superinterval = sets.RealInterval(limits=[0, 0, True, True])
    subinterval = sets.RealInterval(limits=[0, 0, False, False])
    assert_proper_superset(subinterval, superinterval)


def test_realintervalcomposite():
    interval1 = sets.RealInterval(
        limits=[[0, 1, True, True], [-numpy.inf, numpy.inf, True, False]]
    )
    interval2 = sets.RealInterval(
        limits=[[0, numpy.inf, False, True], [-10, 5, True, True]]
    )
    interval = sets.RealIntervalComposite([interval1, interval2])
    assert interval.ndim == 4
    coord = interval.random_uniform(1000)
    assert coord in interval
    assert [0.5, 0, 10, 0] in interval
    assert [0.5, 0, 0, 0] not in interval


def assert_space_selfcmp(_set):
    assert_equal_space(_set, _set)
    assert_subset(_set, _set)


def assert_space_finite(_set):
    assert _set.isFinite
    assert not _set.isInfinite


def assert_space_infinite(_set):
    assert not _set.isFinite
    assert _set.isInfinite


def assert_equal_space(set1, set2):
    assert set1 == set2
    assert not (set1 != set2)
    assert set1 >= set2
    assert not (set1 > set2)
    assert set1 <= set2
    assert not (set1 < set2)


def assert_subset(_set, subset):
    assert subset in _set
    assert subset <= _set
    assert not (subset > _set)


def assert_proper_subset(_set, subset):
    assert_subset(_set, subset)
    assert subset < _set
    assert not (subset >= _set)
    assert subset != _set


def assert_superset(_set, superset):
    assert superset >= _set
    assert not (superset < _set)


def assert_proper_superset(_set, superset):
    assert_superset(_set, superset)
    assert superset > _set
    assert not (superset <= _set)
    assert superset != _set
