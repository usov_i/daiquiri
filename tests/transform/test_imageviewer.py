import pytest
import numpy
import logging
from daiquiri.core.transform import imageviewer
from daiquiri.core.transform import simgui


# TODO: Need someone who understands this
@pytest.mark.skip(reason="need someone that understands this")
def test_imageviewer_motors_to_world(motors):
    motors = (
        motors["simy"],
        motors["simpy"],
        motors["simz"],
        motors["simpz"],
        motors["simx"],
    )
    trn = imageviewer.MotorsToWorld(*motors)

    trn.domain.move(trn.domain.center)
    numpy.testing.assert_array_equal(trn.domain.current_position, [1, 50, 2, 50, 0.5])

    # Check limits
    trn.domain.move([1, 30, 2.5, 10, -0.5])
    calc = trn.motor_world_limits("simy")
    expected = -4e6 + 30e3, 6e6 + 30e3
    numpy.testing.assert_allclose(calc, expected)
    calc = trn.motor_world_limits("simpy")
    expected = 1e6 + 0e3, 1e6 + 100e3
    numpy.testing.assert_allclose(calc, expected)

    calc = trn.motor_world_limits("simz")
    expected = -1e6 + 10e3, 5e6 + 10e3
    numpy.testing.assert_allclose(calc, expected)
    calc = trn.motor_world_limits("simpz")
    expected = 2.5e6 + 0e3, 2.5e6 + 100e3
    numpy.testing.assert_allclose(calc, expected)

    calc = trn.motor_world_limits("simx")
    expected = -2e6, 3e6
    numpy.testing.assert_allclose(calc, expected)

    assert trn.codomain == trn.image
    limits = trn.image.limits
    calc = limits.low
    expected = [-4e6, -1e6, -2e6, -numpy.inf]
    numpy.testing.assert_allclose(calc, expected)
    calc = limits.high
    expected = [6e6 + 100e3, 5e6 + 100e3, 3e6, numpy.inf]
    numpy.testing.assert_allclose(calc, expected)


def test_imageviewer_vlm_to_world():
    trn = imageviewer.VLMToWorld(focaloffset=[10, 100, 1000])

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=1e6)
    coord2 = trn.inverse(trn.forward(coord))
    numpy.testing.assert_allclose(coord, coord2)
    assert coord2 in trn.domain
    icoord = trn.codomain.random_uniform(10000, maxnum=1e6)
    icoord2 = trn.forward(trn.inverse(icoord))
    numpy.testing.assert_allclose(icoord, icoord2)
    assert icoord2 in trn.codomain


def test_imageviewer_world_to_sample(motors):
    motors = (
        motors["simy"],
        motors["simpy"],
        motors["simz"],
        motors["simpz"],
        motors["simx"],
    )
    motormap = imageviewer.MotorsToWorld(*motors)
    trn = imageviewer.WorldToSample(sampleoffset=[10, 100, 1000], motormap=motormap)

    # Round-trip
    # TODO: why the rounding errors? (it's a simple translation ...)
    coord = trn.domain.random_uniform(10000, maxnum=1e6)
    coord2 = trn.inverse(trn.forward(coord))
    numpy.testing.assert_allclose(coord, coord2, rtol=1e-4, atol=1e-3)
    assert coord2 in trn.domain
    icoord = trn.codomain.random_uniform(10000, maxnum=1e6)
    icoord2 = trn.forward(trn.inverse(icoord))
    numpy.testing.assert_allclose(icoord, icoord2, rtol=1e-4, atol=1e-3)
    assert icoord2 in trn.codomain


@pytest.mark.flaky(reruns=3)
def test_imageviewer_beam_to_world():
    trn = imageviewer.BeamToWorld(beamangle=28, beamoffset=[10, 100, 1000])

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=1e6)
    coord2 = trn.inverse(trn.forward(coord))
    numpy.testing.assert_allclose(coord, coord2)
    assert coord2 in trn.domain
    icoord = trn.codomain.random_uniform(10000, maxnum=1e6)
    icoord2 = trn.forward(trn.inverse(icoord))
    numpy.testing.assert_allclose(icoord, icoord2)
    assert icoord2 in trn.codomain


def test_imageviewer_vlm_to_vi():
    zoominfo = {"x12": {"pixelsize": [1e3, 1.2e3], "focalpoint": [20, -43.2]}}
    trn = imageviewer.VLMToVI(
        imageshape=(1024, 845), zoomlevel="x12", zoominfo=zoominfo
    )

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=1e6)
    coord2 = trn.inverse(trn.forward(coord))
    coord[:, 2] = 0
    numpy.testing.assert_allclose(coord, coord2, rtol=1e-4, atol=1e-4)
    assert coord2 in trn.domain
    icoord = trn.codomain.random_uniform(10000, maxnum=1e6)
    icoord2 = trn.forward(trn.inverse(icoord))
    numpy.testing.assert_allclose(icoord, icoord2, rtol=1e-4, atol=1e-3)
    assert icoord2 in trn.codomain


def test_imageviewer_sample_to_canvas():
    trn = imageviewer.SampleToCanvas()

    # Round-trip
    coord = trn.domain.random_uniform(10000, maxnum=1e6)
    coord2 = trn.inverse(trn.forward(coord))
    coord[:, 2] = 0
    numpy.testing.assert_allclose(coord, coord2)
    assert coord2 in trn.domain
    icoord = trn.codomain.random_uniform(10000, maxnum=1e6)
    icoord2 = trn.forward(trn.inverse(icoord))
    numpy.testing.assert_allclose(icoord, icoord2)
    assert icoord2 in trn.codomain


def test_imageviewer_canvas(motors):
    motors = {
        "x": motors["simy"],
        "x_fine": motors["simpy"],
        "y": motors["simz"],
        "y_fine": motors["simpz"],
        "z": motors["simx"],
    }
    sx = 1e4
    sy = 1e4
    ox = 3
    oy = -2
    zoominfo = {
        "x12": {"pixelsize": [sx, sy], "focalpoint": [ox, oy]},
        "x6": {"pixelsize": [sx * 6, sy * 6], "focalpoint": [ox, oy]},
        "x1": {"pixelsize": [sx * 12, sy * 12], "focalpoint": [ox, oy]},
    }
    vlmimageshape = (40, 30)
    zoomlevel = "x6"
    sampleoffset = [0, 0, 0]
    beamoffset = [0, 0, 0]
    focaloffset = [0, 0, 0]
    units = "nm"
    downstream = True
    canvas = imageviewer.ImageViewerCanvas(
        motors,
        units=units,
        sampleoffset=sampleoffset,
        beamoffset=beamoffset,
        focaloffset=focaloffset,
        vlmimageshape=vlmimageshape,
        zoomlevel=zoomlevel,
        zoominfo=zoominfo,
        downstream=downstream,
    )

    # Center fine axes while staying on the same World position
    canvas.motors_to_world.domain.move([0, 0, 0, 0, 1e-3])
    wpos1 = canvas.motors_to_world.current_world_position
    canvas.center_fine()
    wpos2 = canvas.motors_to_world.current_world_position
    numpy.testing.assert_allclose(wpos1, wpos2, atol=1e-7)
    current_position = canvas.motors_to_world.domain.current_position
    numpy.testing.assert_array_equal(current_position, [-50e-3, 50, -50e-3, 50, 1e-3])

    # Manual testing
    try:
        gui = simgui.SimGUI(canvas)
        gui.start()
    except Exception:
        logging.warning("Error while displaying GUI", exc_info=True)
