import pytest
import json


class AuthClient:
    def __init__(self, client):
        self._client = client

    def login(self, username="abcd", password="abcd"):
        res = self._client.post(
            "/api/authenticator/login",
            json={"username": username, "password": password},
        )
        self._token = res.json["token"]
        return res

    def logout(self):
        return self._client.delete("/api/authenticator/login")

    def client(self, method, url, *args, **kwargs):
        headers = {"Authorization": f"Bearer {self._token}"}
        return getattr(self._client, method)(
            url, json=kwargs.get("payload"), headers=headers
        )

    @property
    def token(self):
        return self._token

    def get(self, *args, **kwargs):
        return self.client("get", *args, **kwargs)

    def post(self, *args, **kwargs):
        return self.client("post", *args, **kwargs)

    def put(self, *args, **kwargs):
        return self.client("put", *args, **kwargs)

    def patch(self, *args, **kwargs):
        return self.client("patch", *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self.client("delete", *args, **kwargs)


@pytest.fixture
def get_client(request):
    return request.getfixturevalue(request.param)


@pytest.fixture
def get_with_control(request):
    return request.getfixturevalue(request.param)


@pytest.fixture
def get_with_session(request):
    return request.getfixturevalue(request.param)


@pytest.fixture
def to_json():
    """Decode json from response"""

    def to_json(response):
        return json.loads(response.data.decode("utf8"))

    return to_json


@pytest.fixture
def auth_client(client):
    auth = AuthClient(client)
    auth.login()
    yield auth
    auth.logout()


@pytest.fixture
def sioclient(request):
    app = request.getfixturevalue("app")
    client = request.getfixturevalue("client")

    yield app.socketio.test_client(
        app, namespace=request.param, flask_test_client=client
    )


@pytest.fixture
def auth_sioclient(request):
    app = request.getfixturevalue("app")
    auth_client = request.getfixturevalue("auth_client")

    yield app.socketio.test_client(
        app,
        namespace=request.param,
        flask_test_client=auth_client._client,
        query_string=f"?token={auth_client.token}",
    )


@pytest.fixture
def with_session(auth_client):
    session = "blc00001-1"
    auth_client.post("/api/metadata/sessions/select", payload={"session": session})
    yield session


@pytest.fixture
def with_control(auth_client):
    auth_client.post("/api/session/current/control")
    has_control = True
    yield has_control
    auth_client.delete("/api/session/current/control")


@pytest.fixture
def auth_client_admin(client):
    auth = AuthClient(client)
    auth.login(username="efgh")
    yield auth
    auth.logout()


@pytest.fixture
def with_control_admin(auth_client_admin):
    auth_client_admin.post("/api/session/current/control")
    has_control = True
    yield has_control
    auth_client_admin.delete("/api/session/current/control")


@pytest.fixture
def with_session_admin(auth_client_admin):
    session = "blc00001-1"
    auth_client_admin.post(
        "/api/metadata/sessions/select", payload={"session": session}
    )
    yield session


@pytest.fixture
def omega(app):
    omega = app.hardware.get_object("omega")
    pos = omega.get("position")

    omega.move(0)
    omega.wait()

    yield omega

    omega.move(pos)
    omega.wait()


@pytest.fixture
def short_session(app):
    old_session_length = app.session._config["session_length"]

    new_session_length = 3
    app.session._config["session_length"] = new_session_length

    try:
        yield new_session_length
    finally:
        app.session._config["session_length"] = old_session_length
