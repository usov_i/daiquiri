import os
import time
import pytest


@pytest.mark.skip()
def test_parameteriser_clean(auth_client, with_session):
    res = auth_client.get("/api/parameteriser?type=sample")
    parameters = res.json["rows"]
    for parameter in parameters:
        os.unlink(os.path.join(parameter["directory"], parameter["file_name"]))


def test_parameteriser_scircle(auth_client, with_session):
    data = {
        "center_horizontal": 5,
        "center_vertical": 10,
        "diameter": 15,
        "name": "test1",
    }

    res = auth_client.post("/api/parameteriser/scircle", payload=data)
    assert res.status_code == 200

    time.sleep(1)

    res = auth_client.get("/api/parameteriser?type=sample")

    assert res.json["total"] == 1
    parameters = res.json["rows"]
    parameter = parameters[0]

    for key, value in data.items():
        assert parameter["parameters"][key] == value

    os.unlink(os.path.join(parameter["directory"], parameter["file_name"]))

    res = auth_client.get("/api/parameteriser?type=sample")
    assert res.json["total"] == 0


def test_parameteriser_scircle_modify_without_overwrite(auth_client, with_session):
    data = {
        "center_horizontal": 5,
        "center_vertical": 10,
        "diameter": 15,
        "name": "test1",
    }

    res = auth_client.post("/api/parameteriser/scircle", payload=data)
    assert res.status_code == 200

    time.sleep(1)

    data_new = data.copy()
    data_new["diameter"] = 20

    res = auth_client.post("/api/parameteriser/scircle", payload=data_new)
    assert res.status_code == 200

    time.sleep(1)

    res = auth_client.get("/api/parameteriser?type=sample")

    assert res.json["total"] == 1
    parameters = res.json["rows"]
    parameter = parameters[0]

    for key, value in data.items():
        assert parameter["parameters"][key] == value

    os.unlink(os.path.join(parameter["directory"], parameter["file_name"]))

    res = auth_client.get("/api/parameteriser?type=sample")
    assert res.json["total"] == 0


def test_parameteriser_scircle_modify(auth_client, with_session):
    data = {
        "center_horizontal": 5,
        "center_vertical": 10,
        "diameter": 15,
        "name": "test1",
    }

    res = auth_client.post("/api/parameteriser/scircle", payload=data)
    assert res.status_code == 200

    time.sleep(1)

    data_new = data.copy()
    data_new["diameter"] = 20

    res = auth_client.post(
        "/api/parameteriser/scircle", payload={**data_new, "overwrite": True}
    )
    assert res.status_code == 200

    time.sleep(1)

    res = auth_client.get("/api/parameteriser?type=sample")

    assert res.json["total"] == 1
    parameters = res.json["rows"]
    parameter = parameters[0]

    for key, value in data_new.items():
        assert parameter["parameters"][key] == value

    os.unlink(os.path.join(parameter["directory"], parameter["file_name"]))

    res = auth_client.get("/api/parameteriser?type=sample")
    assert res.json["total"] == 0
