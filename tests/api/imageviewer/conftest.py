import pytest
import time


@pytest.fixture
def wait_for_lima_video_frame(app, lima_simulator):
    device_fqdn, proxy = lima_simulator
    proxy.video_live = True

    lima = app.hardware.get_object("lima_simulator")

    # Wait for daiquiri to get object
    for _ in range(60):
        if lima.online():
            break
        print("waiting for online...")
        time.sleep(1)
    else:
        raise RuntimeError("Still waiting for online lima")

    # Wait until a frame is available
    for _ in range(60):
        if lima.frame() is not None:
            break
        print("waiting for a frame...")
        time.sleep(1)
    else:
        raise RuntimeError("Still waiting for a lima video frame")

    try:
        yield lima
    finally:
        # Speed up the disconnection
        lima._disconnect()


@pytest.fixture
def translation(app):
    m1 = app.hardware.get_object("m1")
    m2 = app.hardware.get_object("m2")

    yield m1, m2
