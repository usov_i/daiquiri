def test_login(client):
    res = client.post(
        "/api/authenticator/login", json={"username": "abcd", "password": "abcd"}
    )
    assert res.status_code == 201


def test_login_fail(client):
    res = client.post(
        "/api/authenticator/login", json={"username": "abce", "password": "abcd"}
    )
    assert res.status_code == 401


def test_app(client):
    res = client.get("/api/components/config")
    assert res.status_code == 200
