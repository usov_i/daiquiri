import time


def test_start_queue(auth_client, with_session, with_control):
    res = auth_client.put("/api/queue", payload={"state": True})

    assert res.status_code == 200


def test_start_queue_without_control(auth_client, with_session):
    res = auth_client.put("/api/queue", payload={"state": True})

    assert res.status_code == 400


def test_queue_monitor(app, auth_client, with_session):
    res = auth_client.get("/api/queue")

    omega = app.hardware.get_object("omega")
    omega.move(80)
    omega.wait()

    assert res.status_code == 200
    assert res.json["ready"] is True

    omega.move(90)
    omega.wait()

    # Wait for next queue check
    time.sleep(5)

    res = auth_client.get("/api/queue")

    assert res.status_code == 200
    assert res.json["ready"] is False
