import pytest


def test_api_available(auth_client, with_session):
    """
    Configuration file was properly setup

    Make sure an API is available for this device
    """
    res = auth_client.get("/api/hardware/lima_simulator__acq_nb_frames")
    assert res.status_code == 200


@pytest.mark.skip(reason="flakey test, cant connect to device?")
def test_api_content(lima_simulator, app, auth_client, with_session, with_control):
    """
    Make sure the expected content is provided by the API
    """
    tangoattr = app.hardware.get_object("lima_simulator__acq_nb_frames")

    tangoattr.set_online(True)  # <---- FIXME: IS IT REALLY NEEDED?

    res = auth_client.get("/api/hardware/lima_simulator__acq_nb_frames")
    assert res.status_code == 200
    assert res.json["properties"]["value"] == 1, res.json
    assert res.json["properties"]["state"] == "ON", res.json


@pytest.mark.skip(reason="flakey test, cant connect to device?")
def test_updating_hardware_updates_api(
    lima_simulator, app, auth_client, with_session, with_control
):
    """
    Update an attribute of the hardware device

    Make sure the expected content is provided by the API
    """
    tangoattr = app.hardware.get_object("lima_simulator__acq_nb_frames")

    tangoattr.set_online(True)  # <---- FIXME: IS IT REALLY NEEDED?

    # Update the tango attribute
    tangoattr._object.acq_nb_frames = 2

    res = auth_client.get("/api/hardware/lima_simulator__acq_nb_frames")
    assert res.status_code == 200
    assert res.json["properties"]["value"] == 2, res.json


@pytest.mark.skip(reason="flakey test, cant connect to device?")
def test_updating_hardware_config_updates_api(
    lima_simulator, app, auth_client, with_session, with_control
):
    """
    Update the configuration field of the hardware device

    Make sure the expected content is provided by the API
    """
    tangoattr = app.hardware.get_object("lima_simulator__acq_nb_frames")

    tangoattr.set_online(True)  # <---- FIXME: IS IT REALLY NEEDED?

    # Update the tango attribute config
    info = tangoattr._object.get_attribute_config("acq_nb_frames")
    info.display_unit = "toto"
    info = tangoattr._object.set_attribute_config(info)
    # Make sure it was updated (cause you could have pbm)
    assert (
        tangoattr._object.get_attribute_config("acq_nb_frames").display_unit == "toto"
    )

    res = auth_client.get("/api/hardware/lima_simulator__acq_nb_frames")
    assert res.status_code == 200
    assert res.json["properties"]["display_unit"] == "toto", res.json
