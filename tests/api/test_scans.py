import numpy
import pytest
from flask import Flask
from daiquiri.core.components.scans import ScanDataBinaryResource


class MockedScanDataBinaryResource(ScanDataBinaryResource):
    @property
    def _parent(self):
        class Parent:
            def get_scan_data(self, scalars, *args, **kwargs):
                data = numpy.array([1, 2, 3, 4, 5], dtype=numpy.int8)
                return {"data": {"foo": {"data": data, "size": data.size}}}

        return Parent()


@pytest.fixture
def flask_app(mocker):
    app = Flask(__name__)
    yield app


@pytest.fixture
def api(flask_app):
    view = MockedScanDataBinaryResource.as_view("test")
    flask_app.add_url_rule("/<int:scanid>", view_func=view, methods=["GET"])
    with flask_app.test_client() as c:
        yield c


def test_raw(api):
    assert (
        api.get("/1?channel=foo").data[10:]
        == b"cdbfa\x05\x00\xf4\x99\x0bG\x05\x00\x00\x00"
    )


def test_selection(api):
    assert (
        api.get("/1?channel=foo&selection=4:").data[10:]
        == b"c\x05\x00\x02\x1bh\xa2\x01\x00\x00\x00"
    )


def test_dtype(api):
    assert (
        api.get("/1?channel=foo&dtype=int16").data[10:]
        == b"cd`b`f`a`e\x00\x00\x89\x15\xec:\n\x00\x00\x00"
    )


def test_safe_dtype(api):
    assert (
        api.get("/1?channel=foo&dtype=safe").data[10:]
        == b"cdbfa\x05\x00\xf4\x99\x0bG\x05\x00\x00\x00"
    )
