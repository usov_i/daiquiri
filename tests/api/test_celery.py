import time
from celery.result import AsyncResult


def test_celery_api_tasks(auth_client, with_session):
    response = auth_client.get("/api/celery/tasks")
    assert response.status_code == 200

    rows = response.json["rows"]
    assert rows


def test_celery_api_sleep(
    celery_app, reloaded_celery_worker, auth_client, with_session
):
    response = auth_client.post("/api/celery/tasks/sleep")
    assert response.status_code == 200

    task_id = response.json["result"]["task_id"]
    future = AsyncResult(task_id, app=celery_app)
    result = future.get(timeout=2)

    assert result is True

    # TODO: horrible synchronisation
    # wait for sqlite to catch-up
    time.sleep(2)

    response = auth_client.get(f"/api/celery/tasks/executed/{task_id}")
    assert response.status_code == 200


def test_celery_api_sleep_revoke(
    celery_app, reloaded_celery_worker, auth_client, with_session
):
    response = auth_client.post("/api/celery/tasks/sleep", payload={"sleep_time": 5})
    assert response.status_code == 200

    task_id = response.json["result"]["task_id"]

    response = auth_client.delete(f"/api/celery/revoke/{task_id}")
    assert response.status_code == 200


def test_celery_api_workers(
    celery_app, reloaded_celery_worker, auth_client, with_session
):
    response = auth_client.get("/api/celery/workers")
    assert response.status_code == 200

    rows = response.json["rows"]
    assert rows
