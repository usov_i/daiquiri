from marshmallow import fields
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.bliss.object import BlissObject


class FakePropertiesSchema(HardwareSchema):
    status = fields.Str()
    led = fields.Str()
    translated = fields.Str()


class AbstractFakeController(HardwareObject):
    _type = "fakecontroller"
    _properties = FakePropertiesSchema()


class FakeHardwareProperty(HardwareProperty):
    def translate_from(self, value):
        return value + "_from"

    def translate_to(self, value):
        return value + "_to"


class FakeController(AbstractFakeController):
    def _get_status(self):
        return "foobar"

    property_map = {
        "led": HardwareProperty("myled"),
        "status": HardwareProperty("foo", getter=_get_status),
        "translated": FakeHardwareProperty("foo", getter=_get_status),
    }

    def _call_led(self, value, **kwargs):
        if value:
            self._object.led_on()
        else:
            self._object.led_off()

    def _call_foil(self, value, **kwargs):
        if value:
            self._object.foil_in()
        else:
            self._object.foil_out()

    def _call_screen(self, value, **kwargs):
        if value:
            self._object.screen_in()
        else:
            self._object.screen_out()

    def _call_current(self, value, **kwargs):
        return self._object.current


class BlissFakeController(BlissObject, FakeController):
    property_map = FakeController.property_map


def test_get_property(mocker):
    obj = mocker.Mock()
    c = BlissFakeController(obj=obj)
    c._do_get = mocker.Mock(return_value="foobar1")
    mocker.patch.object(c, "_do_get", return_value="foobar_led")
    assert c.get("led") == "foobar_led"


def test_set_property(mocker):
    obj = mocker.Mock()
    c = BlissFakeController(obj=obj)
    c._do_get = mocker.Mock(return_value="foobar1")
    mocked = mocker.patch.object(c, "_do_set")
    c.set("led", "ON")
    assert mocked.call_count == 1
    assert mocked.call_args[0][1] == "ON"


def test_get_property_with_getter(mocker):
    obj = mocker.Mock()
    c = BlissFakeController(obj=obj)
    assert c.get("status") == "foobar"


def test_get_property_with_translator(mocker):
    obj = mocker.Mock()
    c = BlissFakeController(obj=obj)
    assert c.get("translated") == "foobar_from"


def test_set_property_with_translator(mocker):
    obj = mocker.Mock()
    c = BlissFakeController(obj=obj)
    mocked = mocker.patch.object(c, "_do_set")
    c.set("translated", "foobar")
    assert mocked.call_count == 1
    assert mocked.call_args[0][1] == "foobar_to"
