import time
from bliss.common.scans import ascan


def test_bliss_scan(bliss_omega, bliss_diode, bliss_mca, scans):
    scan_list = scans.get_scans()
    assert not scan_list["rows"]

    points = 5
    ascan(bliss_omega, 0, 20, points, 0.1, bliss_diode, bliss_mca)

    scan_list = scans.get_scans()
    assert len(scan_list["rows"]) == 1

    last_scan = scan_list["rows"][0]

    data = scans.get_scan_data(last_scan["scanid"])
    xs = data["axes"]["xs"]

    # intervals vs points
    assert len(data["data"][xs[0]]["data"]) == points + 1

    spectra = scans.get_scan_spectra(last_scan["scanid"], point=0)

    assert spectra["npoints"] == points + 1

    ys = data["axes"]["ys"]["spectra"]
    spectrum = spectra["data"][ys[0]]

    assert spectrum["name"] == ys[0]
    assert len(spectrum["data"][0]) == 1024


def test_bliss_scan_watch(bliss_omega, bliss_diode, scans):
    class ScanWatch:
        def __init__(self):
            self._new_scan = False
            self._new_data = False
            self._end_scan = False

        def new_scan(self, *args, **kwargs):
            self._new_scan = True

        def new_data(self, *args, **kwargs):
            self._new_data = True

        def end_scan(self, *args, **kwargs):
            self._end_scan = True

        @property
        def all(self):
            return [self._new_scan, self._new_data, self._end_scan]

    sw = ScanWatch()

    src = scans._scan_sources[0]
    src.watch_new_scan(sw.new_scan)
    src.watch_end_scan(sw.end_scan)
    src.watch_new_data(sw.new_data)

    ascan(bliss_omega, 0, 20, 5, 0.1, bliss_diode)

    # TODO: Rubbish synchronisation :(
    # Message is sometimes emitted after scan finishes
    time.sleep(1)

    assert all(sw.all)
