import pytest
from bliss.config.static import get_config


@pytest.fixture
def bliss_omega(app):
    return get_config().get("omega")


@pytest.fixture
def bliss_objectref1(app):
    return get_config().get("objectref1")


@pytest.fixture
def bliss_diode(app):
    return get_config().get("diode")


@pytest.fixture
def bliss_tomo_detectors(app):
    return get_config().get("tomo_detectors")


@pytest.fixture
def bliss_tomo_detector(app):
    return get_config().get("tomo_detector")


@pytest.fixture
def bliss_shutter1(app):
    shutter = get_config().get("shutter1")
    # reach the attribute so what it is disabled
    # FIXME: This is a hack to use an abstract BLISS shutter as a mock
    try:
        shutter.mode = None
    except Exception:
        pass
    return shutter


@pytest.fixture
def bliss_mca(app):
    return get_config().get("simu1")


@pytest.fixture
def scans(app):
    yield app.components.get_component("scans")
