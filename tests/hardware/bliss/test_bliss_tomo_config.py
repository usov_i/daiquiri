def test_tomo_config(tomo_installed, tomo_config):
    """Get a tomo config.

    Make sure the relationships are there
    """
    assert tomo_config.get("detectors") == "hardware:tomo_detectors"
    assert tomo_config.get("sx") == "hardware:tomo_config_xr"
    assert tomo_config.get("somega") == "hardware:tomo_config_srot"
