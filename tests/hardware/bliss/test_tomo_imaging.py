def test_tomo_imaging(tomo_installed, tomo_imaging):
    """Get a tomo_detectors.

    Make sure the relationships are there
    """
    assert tomo_imaging.get("update_on_move") is False
    assert tomo_imaging.get("exposure_time") == 0.5
