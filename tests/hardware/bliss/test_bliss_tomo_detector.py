def test_tomo_detector_with_optic(tomo_installed, tomo_detector):
    """Get a tomo_detector.

    Make sure the relationships are there
    """
    assert tomo_detector.get("optic") == "hardware:tomo_detector__optic"
    assert tomo_detector.get("detector") == "hardware:lima_simulator"


def test_tomo_detector_without_optic(tomo_installed, tomo_detector_without_optic):
    """Get a tomo_detector.

    Make sure the relationships are there
    """
    tomo_detector = tomo_detector_without_optic
    assert tomo_detector.get("optic") == "hardware:"
    assert tomo_detector.get("detector") == "hardware:lima_simulator"
