import gevent
from daiquiri.core.utils import loggingutils
from bliss.common.shutter import BaseShutterState
from bliss.controllers.tango_shutter import TangoShutterState


def _set_shutter_state(shutter, state):
    """
    Internally set a BLISS shutter state.

    FIXME: Implement a mocked shutter in BLISS (it's part of BLISS 1.11)
    """
    if hasattr(shutter, "_Shutter__state"):
        # BLISS <= 1.10
        shutter._Shutter__state.value = state
    else:
        # BLISS >= 1.11
        # With this version a mocker shutter is provided
        shutter._AxisWithExtTriggerShutter__state.value = state


def test_base_state__closed(shutter1, bliss_shutter1):
    """Set CLOSED to the BLISS shutter

    Check that the Daiquiri state representation is CLOSED
    """
    _set_shutter_state(bliss_shutter1, BaseShutterState.CLOSED)
    gevent.sleep(0.1)
    assert shutter1.get("state") == "CLOSED"


def test_base_state__unexpected_value(shutter1, bliss_shutter1):
    """Set an unexpected value to the BLISS shutter

    Check that the Daiquiri state representation is UNKNOWN
    """
    _set_shutter_state(bliss_shutter1, object())
    gevent.sleep(0.1)
    assert shutter1.get("state") == "UNKNOWN"


def test_tango_state__running(shutter1, bliss_shutter1):
    """Set RUNNING tango state to the BLISS shutter

    Check that the Daiquiri representation is OPEN
    """
    _set_shutter_state(bliss_shutter1, TangoShutterState.RUNNING)
    gevent.sleep(0.1)
    assert shutter1.get("state") == "OPEN"


def test_tango_state__moving(shutter1, bliss_shutter1):
    """Set MOVING tango state to the BLISS shutter

    Check that the Daiquiri representation is MOVING
    """
    _set_shutter_state(bliss_shutter1, TangoShutterState.MOVING)
    gevent.sleep(0.1)
    assert shutter1.get("state") == "MOVING"


def test_string_state__closed(shutter1, bliss_shutter1):
    """Check that some controller using string in state instead of enum still work."""
    _set_shutter_state(bliss_shutter1, "CLOSED")
    gevent.sleep(0.1)
    assert shutter1.get("state") == "CLOSED"


def test_string_state__wrong(shutter1, bliss_shutter1):
    """Check that some controller using string in state instead of enum still work."""
    listener = loggingutils.LoggingListener("daiquiri.core.hardware.bliss.shutter")
    with listener:
        _set_shutter_state(bliss_shutter1, "FOO")
        gevent.sleep(0.1)
        assert shutter1.get("state") == "UNKNOWN"
    assert len(listener.records) == 1
    assert "FOO" in listener.records[0].msg
