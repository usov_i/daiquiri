import pytest
import logging
import time
import gevent

_logger = logging.getLogger(__name__)


@pytest.fixture
def daiquiri_lima_simulator(app, lima_simulator):
    """Returns a Daiquiri object of kind tango lima"""
    lima = app.hardware.get_object("lima_simulator")

    for _ in range(60):
        if lima.online():
            break
        print("waiting for online...")
        time.sleep(1)
    else:
        raise TimeoutError(60)

    try:
        yield lima
    finally:
        # Speed up the disconnection
        lima._disconnect()


@pytest.fixture
def daiquiri_lima_simulator_exposure(daiquiri_lima_simulator):
    """Returns the history of the exposure property of the tango lima object"""

    class Listener:
        def __init__(self):
            self._result = []
            self._expected = []
            self._wait = gevent.event.Event()

        def wait_for(self, value, timeout=None):
            self._expected = value
            if self._expected == self._result:
                return
            self._wait.clear()
            self._wait.wait(timeout)

        def received(self, obj, prop_name, value):
            self._result.append(value)
            if self._expected == self._result:
                self._wait.set()

    listener = Listener()
    daiquiri_lima_simulator.subscribe("exposure", listener.received)
    try:
        yield listener
    finally:
        daiquiri_lima_simulator.unsubscribe("exposure", listener.received)


def test_attribute_subscription(
    daiquiri_lima_simulator, daiquiri_lima_simulator_exposure, lima_simulator
):
    """
    Update a tango device attribute.

    Make sure the Daiquiri object is properly synchronized after some times.
    Without explicit request.
    """
    _device_fqdn, proxy = lima_simulator
    proxy.video_exposure = 1
    daiquiri_lima_simulator_exposure.wait_for([1], timeout=20)
    proxy.video_exposure = 2
    daiquiri_lima_simulator_exposure.wait_for([1, 2], timeout=20)
