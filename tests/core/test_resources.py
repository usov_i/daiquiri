import pytest
import os
from daiquiri.core.resources.file_resource_provider import FileResourceProvider

RESOURCES_ROOT = os.path.join(os.path.dirname(__file__), "..", "resources")


def test_file_resource__get():
    provider = FileResourceProvider(RESOURCES_ROOT)

    path = provider.get_resource_path("config", "app.yml")
    assert path is not None

    resource = provider.resource("config", "app.yml")
    assert resource is not None
    with provider.open_resource(resource) as f:
        content = f.read()
    assert "meta_beamline" in content


def test_file_resource__composite_resource_type():
    provider = FileResourceProvider(RESOURCES_ROOT)
    path = provider.get_resource_path("layout.partials", "something.yml")
    assert path is not None


def test_file_resource__list():
    provider = FileResourceProvider(RESOURCES_ROOT)

    result = provider.list_resource_names("config", "app.yml")
    assert len(result) == 1
    resource = provider.resource("config", result[0])
    assert resource is not None
    with provider.open_resource(resource) as f:
        content = f.read()
    assert "meta_beamline" in content


def test_file_resource__get__root_with_trailing_slash():
    provider = FileResourceProvider(RESOURCES_ROOT + "/")

    path = provider.get_resource_path("config", "app.yml")
    assert path is not None

    resource = provider.resource("config", "app.yml")
    assert resource is not None
    with provider.open_resource(resource) as f:
        content = f.read()
    assert "meta_beamline" in content


def test_file_resource__list__root_with_trailing_slash():
    provider = FileResourceProvider(RESOURCES_ROOT + "/")

    result = provider.list_resource_names("config", "app.yml")
    assert len(result) == 1
    resource = provider.resource("config", result[0])
    assert resource is not None
    with provider.open_resource(resource) as f:
        content = f.read()
    assert "meta_beamline" in content


def test_file_resource__get_abs_path():
    provider = FileResourceProvider(RESOURCES_ROOT + "/")

    with pytest.raises(ValueError):
        provider.resource("config", "/app.yml")
