from daiquiri.core.schema.validators import SanitizedHTML
from marshmallow import Schema


def test_sanitize_html():
    class SanitizedHTMLSchema(Schema):
        html = SanitizedHTML()

    data = {
        "input": {"html": "<script>alert()</script>"},
        "output": {"html": "&lt;script&gt;alert()&lt;/script&gt;"},
    }

    schema = SanitizedHTMLSchema()
    output = schema.load(data["input"])

    assert output == data["output"]


def test_sanitised_html_strip():
    class SanitizedHTMLSchema(Schema):
        html = SanitizedHTML(strip=True)

    data = {
        "input": {"html": "<script>alert()</script>test"},
        "output": {"html": "alert()test"},
    }

    schema = SanitizedHTMLSchema()
    output = schema.load(data["input"])

    assert output == data["output"]
