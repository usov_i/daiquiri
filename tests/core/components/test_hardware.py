import numpy
from daiquiri.core.components.hardware import Hardware


class HardwareMock(Hardware):
    def __init__(self, mocker):
        self._hardware = mocker.Mock()
        self._hardware.get_objects = lambda: []
        self._session = mocker.Mock()
        app = mocker.Mock()
        socketio = mocker.Mock()
        docs = mocker.Mock()
        schema = mocker.Mock()
        super(HardwareMock, self).__init__(
            {}, app=app, socketio=socketio, docs=docs, schema=schema
        )


def test_obj_param_change__scalar(mocker):
    component = HardwareMock(mocker)
    obj = mocker.Mock()
    obj.id = lambda: "obj1"
    component._obj_param_change(obj, "foo", 10)
    assert component._last_value["obj1"]["foo"] == 10


def test_obj_param_change__numpy_array(mocker):
    component = HardwareMock(mocker)
    obj = mocker.Mock()
    obj.id = lambda: "obj1"
    component._obj_param_change(obj, "foo", numpy.array([1, 2]))
    assert component._last_value["obj1"]["foo"] == [1, 2]


def test_obj_param_change__big_numpy_array(mocker):
    component = HardwareMock(mocker)
    obj = mocker.Mock()
    obj.id = lambda: "obj1"
    component._obj_param_change(obj, "foo", numpy.arange(100))
    assert component._last_value["obj1"]["foo"] == numpy.arange(100).tolist()
