import uuid
from daiquiri.core.components import ComponentActor


def test_saving_bliss_basic(app):
    datacollectionid = 1
    sampleid = 1
    sample = app.metadata.get_samples(sampleid=sampleid, no_context=True)

    class TestActor(ComponentActor):
        saving_args = {"data_filename": "{sampleid.name}_{datacollectionid}"}

        def method(self, **kwargs):
            pass

    actor = TestActor(
        uid=str(uuid.uuid4()),
    )
    actor.prepare(sessionid=1, sampleid=sampleid, datacollectionid=datacollectionid)

    app.saving.set_filename(extra_saving_args=actor.saving_args, **actor.all_data)

    assert (
        app.saving.scan_saving.data_filename == f"{sample['name']}_{datacollectionid}"
    )


def test_saving_metadata(app):
    definition = "Test definition"
    sampleid = 1
    sample = app.metadata.get_samples(sampleid=sampleid, no_context=True)

    class TestActor(ComponentActor):
        additional_metadata = {"definition": definition}

        def method(self, **kwargs):
            pass

    actor = TestActor(
        uid=str(uuid.uuid4()),
    )
    actor.prepare(sessionid=1, sampleid=sampleid)

    saving_metadata = {
        "sample_name": "{sampleid.name}",
        "sample_description": "{componentid.description}",
        "sample_composition": "{componentid.sequence}",
        "dataset_definition": "{metadata_definition}",
    }

    metadata = app.saving.set_metadata(
        saving_arguments=saving_metadata, warn_on_missing=False, **actor.all_data
    )

    assert metadata["dataset_definition"] == definition
    assert metadata["sample_name"] == sample["name"]
