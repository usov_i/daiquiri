# -*- coding: utf-8 -*-
"""daiquiri main package

.. autosummary::
    :toctree:

    core
"""

try:
    # This is an auto generated file from setuptools_scm
    from . import _version  # noqa

    __version__ = _version.version
    version_info = _version.version_tuple
except ImportError:
    __version__ = "0.0.0"
    version_info = tuple([0, 0, 0])
