#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    import bliss  # noqa F401

    # Importing bliss patches gevent which in turn patches python
except ImportError:
    from gevent.monkey import patch_all

    patch_all(thread=False)

# TODO: Horrible hack to avoid:
#   ImportError: dlopen: cannot load any more object with static TLS
#   https://github.com/pytorch/pytorch/issues/2575
#   https://github.com/scikit-learn/scikit-learn/issues/14485
from silx.math import colormap  # noqa F401

import time
import os
import json
import argparse
from ruamel.yaml import YAML
from flask import Flask, jsonify
from flask_socketio import SocketIO
from flask_apispec import FlaskApiSpec
from flask_restful import abort
from webargs.flaskparser import parser
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

import daiquiri
from daiquiri.core.authenticator import Authenticator
from daiquiri.core.session import Session
from daiquiri.core.queue import Queue
from daiquiri.core.metadata import MetaData
from daiquiri.core.components import Components
from daiquiri.core.hardware import Hardware
from daiquiri.core.schema import Schema
from daiquiri.core.layout import Layout
from daiquiri.core.saving import Saving
from daiquiri.core.stomp import Stomp
from daiquiri.resources import utils
from daiquiri.core.logging import log
from daiquiri.core.responses import nocache

import logging

logger = logging.getLogger(__name__)
DAIQUIRI_ROOT = os.path.dirname(daiquiri.__file__)


def init_server(
    resource_folders=tuple(),
    static_folder="static.default",
    hardware_folder=None,
    testing=False,
    save_spec_file=None,
    implementors=None,
    **kwargs,
):
    """Instantiate a Flask application

    :param list(str) resource_folders: REST server resource root directories
    :param str static_folder: server side resources for the client
    :param str hardware_folder: ???
    :param bool testing: Attaches core components to the `app` so that can be retrieved in tests
    :param str save_spec_file: Location to generate an API description as JSON file
    :param kwargs: arguments for Flask instantiation
    :returns Flask, SocketIO:
    """
    start = time.time()
    for resource_folder in reversed(resource_folders):
        if not resource_folder:
            continue
        if not os.path.isdir(resource_folder):
            raise ValueError(f"Resource folder '{resource_folder}' does not exist")
        utils.add_resource_root(resource_folder)
        logger.info("Added resource folder: %s", resource_folder)
    if static_folder.startswith("static."):
        provider = utils.get_resource_provider()
        try:
            static_folder = provider.get_resource_path(static_folder, "")
        except utils.ResourceNotAvailable:
            raise ValueError(
                f"Static resource '{static_folder}' does not exist"
            ) from None
    if not os.path.isdir(static_folder):
        raise ValueError(f"Static folder '{static_folder}' does not exist")
    if hardware_folder:
        if not os.path.isdir(hardware_folder):
            raise ValueError(f"Hardware folder '{hardware_folder}' does not exist")
        os.environ["HWR_ROOT"] = hardware_folder

    config = utils.ConfigDict("app.yml")

    if not config.get("versions"):
        config["versions"] = []

    # Allow CLI to override implementors
    if implementors:
        config["implementors"] = implementors

    log.start(config=config)
    logger.info(f"Starting daiquiri version: {daiquiri.__version__}")

    app = Flask(__name__, static_folder=static_folder, **kwargs)

    app.config["APISPEC_FORMAT_RESPONSE"] = None
    app.config["APISPEC_TITLE"] = "daiquiri"

    security_definitions = {
        "bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}
    }

    app.config["APISPEC_SPEC"] = APISpec(
        title="daiquiri",
        version="v1",
        openapi_version="2.0",
        plugins=[MarshmallowPlugin()],
        securityDefinitions=security_definitions,
    )

    if not config.get("swagger"):
        app.config["APISPEC_SWAGGER_URL"] = None
        app.config["APISPEC_SWAGGER_UI_URL"] = None

    docs = FlaskApiSpec(app)

    sio_kws = {}
    if config["cors"]:
        from flask_cors import CORS

        sio_kws["cors_allowed_origins"] = "*"
        CORS(app)

    app.config["SECRET_KEY"] = config["iosecret"]
    socketio = SocketIO(app, **sio_kws)

    log.init_sio(socketio)

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify(error=str(e)), 404

    @app.errorhandler(405)
    def method_not_allowed(e):
        return jsonify(message="The method is not allowed for the requested URL."), 405

    # @app.errorhandler(422)
    # def unprocessable(e):
    #     return jsonify(error=str(e)), 422

    @parser.error_handler
    def handle_request_parsing_error(
        err, req, schema, error_status_code, error_headers
    ):
        abort(422, description=err.messages)

    if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        schema = Schema(app=app, docs=docs, socketio=socketio)
        ses = Session(
            config=config, app=app, docs=docs, socketio=socketio, schema=schema
        )
        schema.set_session(ses)

        Authenticator(config=config, app=app, session=ses, docs=docs, schema=schema)

        # TODO
        # This is BLISS specific, move to where bliss is handled ?
        if config.get("controls_session_type", None) == "bliss":
            # This is not very elegant but a solution until this section
            # have been moved to an appropriate place
            from daiquiri.core.hardware.bliss.session import BlissSession

            BlissSession(config["controls_session_name"])

        hardware = Hardware(
            base_config=config, app=app, socketio=socketio, docs=docs, schema=schema
        )

        queue = Queue(
            config=config,
            app=app,
            session=ses,
            docs=docs,
            socketio=socketio,
            schema=schema,
        )

        Layout(
            config, app=app, session=ses, docs=docs, socketio=socketio, schema=schema
        )

        metadata = MetaData(
            config, app=app, session=ses, docs=docs, socketio=socketio, schema=schema
        ).init()
        ses.set_metadata(metadata)

        saving = Saving(
            config,
            app=app,
            session=ses,
            docs=docs,
            socketio=socketio,
            schema=schema,
            metadata=metadata,
        ).init()

        stomp = None
        if config.get("stomp_host"):
            stomp = Stomp(
                config=config,
                app=app,
                session=ses,
                docs=docs,
                socketio=socketio,
                schema=schema,
            )

        components = Components(
            base_config=config,
            app=app,
            socketio=socketio,
            docs=docs,
            schema=schema,
            hardware=hardware,
            session=ses,
            metadata=metadata,
            queue=queue,
            saving=saving,
            stomp=stomp,
        )

    if save_spec_file is not None:
        logger.info("Writing API spec and exiting")
        save_spec_dir = os.path.dirname(save_spec_file)
        if not os.path.exists(save_spec_dir):
            os.mkdir(save_spec_dir)
        with open(save_spec_file, "w") as spec:
            json.dump(docs.spec.to_dict(), spec)

        exit()

    if config["debug"] is True:
        app.debug = True

    if testing:
        app.hardware = hardware
        app.queue = queue
        app.metadata = metadata
        app.components = components
        app.session = ses
        app.socketio = socketio
        app.saving = saving

    @app.route("/manifest.json")
    def manifest():
        return app.send_static_file("manifest.json")

    @app.route("/meta.json")
    @nocache
    def meta():
        return app.send_static_file("meta.json")

    @app.route("/favicon.ico")
    def favicon():
        return app.send_static_file("favicon.ico")

    @app.route("/", defaults={"path": ""})
    @app.route("/<string:path>")
    @app.route("/<path:path>")
    @nocache
    def index(path):
        return app.send_static_file("index.html")

    took = round(time.time() - start, 2)
    logger.info(f"Server ready, startup took {took}s", extra={"startup_time": took})

    return app, socketio


def get_certs_file_path(resource):
    """Get a file path from a resource path

    This can be:
    - foobar.txt  # read file from resource provides "certs"
    - /etc/foobar.txt  # read file from absolute path
    """
    if resource.startswith("/"):
        return os.path.abspath(resource)
    provider = utils.get_resource_provider()
    return provider.get_resource_path("certs", resource)


def get_ssl_context():
    """Build an ssl context for serving over HTTPS"""
    config = utils.ConfigDict("app.yml")

    if not config.get("ssl", False):
        return None

    import ssl

    crt = get_certs_file_path(config["ssl_cert"])
    key = get_certs_file_path(config["ssl_key"])

    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    try:
        context.load_cert_chain(crt, key)
    except Exception:
        logger.exception("Could not load certificate chain", exc_info=True)
        raise

    return context


def run_server(port=8080, **kwargs):
    """Runs REST server

    :param int port:
    :param kwargs: see `init_server`
    """
    app, socketio = init_server(**kwargs)

    server_args = {}
    context = get_ssl_context()
    if context:
        server_args["ssl_context"] = context

    socketio.run(app, host="0.0.0.0", port=port, **server_args)  # nosec


def parse_args():
    """Parse command line args"""

    def dir_list(string):
        """Returns a list of directories from a comma separater list"""
        folders = string.split(",")
        folders = [s.strip() for s in folders]
        return folders

    parser = argparse.ArgumentParser(description="REST server for a beamline GUI")
    parser.add_argument(
        "--resource-folders",
        type=dir_list,
        default=None,
        dest="resource_folders",
        help="Server resources directories (first has priority)",
    )
    parser.add_argument(
        "--implementors",
        default=None,
        dest="implementors",
        help="Actor implementors module",
    )
    parser.add_argument(
        "--static-folder",
        dest="static_folder",
        default=None,
        help="Web server static folder (static.* refers to server resource)",
    )
    parser.add_argument(
        "--hardware-folder",
        dest="hardware_folder",
        default=None,
        help="Hardware folder",
    )
    parser.add_argument(
        "-p", "--port", dest="port", default=None, help="Web server port", type=int
    )
    parser.add_argument(
        "-s",
        "--save-spec",
        nargs="?",
        default=None,
        const="spec.json",
        dest="save_spec_file",
        help="If defined, save the current API spec into the specified file, and exit",
    )
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default=None,
        help="File/resource which is used to setup the server (instead of the command line argument)",
    )
    cmd_args = parser.parse_args()

    # Default args
    args = {
        "resource_folders": [],
        "static_folder": "static.default",
        "hardware_folder": "",
        "port": 8080,
        "save_spec_file": None,
        "implementors": None,
    }

    # Override with config
    if cmd_args.config:
        logging.info("Read config file: %s", cmd_args.config)
        yaml = YAML(typ="safe")
        with open(cmd_args.config, mode="rt") as f:
            config_args = yaml.load(f)
        args.update(config_args)

    # Override from command lines
    cmd_dict = {}
    for k in dir(cmd_args):
        if k.startswith("_"):
            continue
        if k == "config":
            continue
        v = getattr(cmd_args, k)
        if v is not None:
            cmd_dict[k] = v
    args.update(cmd_dict)

    class Options(dict):
        def __init__(self, *args, **kwargs):
            super(Options, self).__init__(*args, **kwargs)
            self.__dict__ = self

    return Options(args)


def main():
    """Runs REST server with CLI configuration"""
    args = parse_args()
    run_server(
        resource_folders=args.resource_folders,
        static_folder=args.static_folder,
        hardware_folder=args.hardware_folder,
        port=args.port,
        save_spec_file=args.save_spec_file,
        implementors=args.implementors,
        static_url_path="/",
    )


if __name__ == "__main__":
    main()
