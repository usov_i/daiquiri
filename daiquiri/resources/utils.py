import os
import logging
import re

import ruamel.yaml

from daiquiri.core.exceptions import SyntaxErrorYAML
from daiquiri.core.resources.multi_resource_provider import MultiResourceProvider
from daiquiri.core.resources.file_resource_provider import FileResourceProvider
from daiquiri.core.resources.resource_provider import ResourceNotAvailable
from daiquiri.core.utils.dictutils import prudent_update


logger = logging.getLogger(__name__)

RESOURCE_PROVIDER = None

VAR_MATCHER = re.compile(r"(\${([\w_]+)})")


def _interpolate(value, args):
    if isinstance(value, dict):
        return interpolate(value, args)
    elif isinstance(value, list):
        return [_interpolate(i, args) for i in value]
    elif isinstance(value, str):
        match = VAR_MATCHER.search(value)
        if match:
            # Full variable replacement (allowing complex types)
            if match[1] == value:
                return args.get(match[2])

            # String replace
            else:

                def repl_var(mat):
                    return str(args.get(mat[2]))

                value = VAR_MATCHER.sub(repl_var, value)

        return value

    return value


def interpolate(layout, variables={}):
    """Recursively interpolate variables in a nested dict"""
    if isinstance(layout, dict):
        new_layout = {}
        for k, v in layout.items():
            new_layout[k] = _interpolate(v, variables)

        return new_layout

    elif isinstance(layout, list):
        new_layout = []
        for v in layout:
            new_layout.append(_interpolate(v, variables))

        return new_layout


class CustomYamlLoader(ruamel.yaml.Loader):
    pass


def _construct_include(loader: CustomYamlLoader, node: ruamel.yaml.Node):
    """Include file referenced at node."""
    if isinstance(node.value, list):
        args = loader.construct_mapping(node, deep=True)
        if not args.get("file"):
            raise SyntaxErrorYAML(
                {"error": "Include definition must include a `file` key"}
            )
        parts = args.get("file").split("#")
        variables = args.get("variables", {})

    elif isinstance(node.value, str):
        parts = node.value.split("#")
        variables = {}

    else:
        raise SyntaxErrorYAML({"error": "Unknown include configuration"})

    resourcename = parts[0]
    ref = parts[1] if len(parts) > 1 else None

    # assume the name is in form resourcetype/resourcename.yml
    # FIXME: This have to be handled properly
    root = loader.stream.name
    paths = os.path.split(root)
    roottype = os.path.split(paths[-2])[-1]

    reference = f"in !include '{root}', line {node.start_mark.line}, column {node.start_mark.column}"
    extension = os.path.splitext(resourcename)[1][1:]
    if extension not in ("yaml", "yml"):
        raise SyntaxErrorYAML(
            {
                "error": f"!include file must have extension 'yml' or 'yaml'\n  extension was {extension}\n{reference}"
            }
        )

    try:
        provider = get_resource_provider()
        resource = provider.resource(roottype, resourcename)
        with provider.open_resource(resource, mode="t") as stream:
            loader = CustomYamlLoader(stream)
            contents = loader.get_single_data()
        if isinstance(contents, (dict, list)):
            if isinstance(contents, dict):
                defaults = contents.pop("default_variables", {})
            else:
                defaults = {}
            defaults.update(variables)
            contents = interpolate(contents, defaults)

    except ResourceNotAvailable:
        raise SyntaxErrorYAML(
            {
                "error": f"can't find included resource {roottype}/{resourcename}\n{reference}"
            }
        )

    except ruamel.yaml.YAMLError as ex:
        raise SyntaxErrorYAML({"error": ex}) from None

    else:
        if ref is not None:
            try:
                if not isinstance(contents, dict):
                    raise SyntaxErrorYAML(
                        {
                            "error": f"!include has #{ref}, but contents of {roottype}/{resourcename} is not an object\n{reference}"
                        }
                    )
                return contents[ref]
            except KeyError:
                raise SyntaxErrorYAML(
                    {
                        "error": f"no such key '{ref}' in '{roottype}/{resourcename}'\n{reference}"
                    }
                )

        else:
            return contents


ruamel.yaml.add_constructor("!include", _construct_include, CustomYamlLoader)


def get_resource_provider() -> MultiResourceProvider:
    """Get the resource provider"""
    global RESOURCE_PROVIDER

    if RESOURCE_PROVIDER is None:
        RESOURCE_PROVIDER = MultiResourceProvider()
        root = os.path.join(os.path.dirname(__file__))
        mainprovider = FileResourceProvider(root)
        RESOURCE_PROVIDER.prepend_provider(mainprovider)

    return RESOURCE_PROVIDER


def add_resource_root(path: str):
    """The new resource root directory will be used
    before the existing ones
    """
    file_provider = FileResourceProvider(path)
    provider = get_resource_provider()
    provider.prepend_provider(file_provider)


class YamlDict(dict):
    """
    Create a dictionary from resource.

    Provides a dictionary API and an extra `reload` method to reread the
    resource file.

    Arguments:
        resourcetype: subdirectory
        resourcename: filename
    """

    def __init__(self, resourcetype: str, resourcename: str):
        dict.__init__(self)
        self._resource_type = resourcetype
        self._resource_name = resourcename
        self.__update()

    @property
    def resource(self):
        return f"{self._resource_type}/{self._resource_name}"

    def __update(self):
        provider = get_resource_provider()
        resource = provider.resource(
            resourcetype=self._resource_type, resourcename=self._resource_name
        )
        self.clear()
        with provider.open_resource(resource) as stream:
            try:
                loader = CustomYamlLoader(stream)
                self.update(loader.get_single_data())
            except ruamel.yaml.YAMLError as ex:
                raise SyntaxErrorYAML({"error": ex}) from None

    def save(self):
        """Save an updated yaml file"""
        provider = get_resource_provider()
        resource = provider.resource(
            resourcetype=self._resource_type, resourcename=self._resource_name
        )

        yaml = ruamel.yaml.YAML()
        yaml.allow_duplicate_keys = True
        yaml.default_flow_style = False
        yaml.indent(mapping=2, sequence=4, offset=2)

        yaml_contents = None
        with provider.open_resource(resource) as stream:
            # The default loader in ruamel is RoundTrip which inherits from SafeLoader
            # https://sourceforge.net/p/ruamel-yaml/code/ci/default/tree/constructor.py#l1011
            yaml_contents = yaml.load(stream)  # nosec

        prudent_update(yaml_contents, self, ignore_unknown=True)

        logger.info(f"Saving {self._resource_type}/{self._resource_name}")
        with provider.open_resource(resource, "wt") as stream:
            yaml.dump(yaml_contents, stream=stream)

    def reload(self):
        """Reload the resource name"""
        logger.info(f"Reloading {self._resource_type}/{self._resource_name}")
        self.__update()


class ConfigDict(YamlDict):
    """
    Create a dictionary from resource name from the config.

    Provides a dictionary API and an extra `reload` method to reread the
    resource file.

    Arguments:
        resourcename: filename
    """

    def __init__(self, resourcename):
        YamlDict.__init__(self, "config", resourcename)
