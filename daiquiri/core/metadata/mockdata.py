import datetime
from .mockxrfdata import MAP_DATA

USERS = [
    {
        "givenname": "User",
        "familyname": "Name",
        "fullname": "User Name",
        "personid": 1,
        "login": "abcd",
        "groups": [],
        "permissions": [],
    },
    {
        "givenname": "Another",
        "familyname": "User",
        "fullname": "Another User",
        "personid": 2,
        "login": "efgh",
        "groups": ["bl_staff"],
        "permissions": ["bl_staff"],
    },
]

PROPOSALS = [
    {
        "proposalid": 1,
        "proposalcode": "blc",
        "proposalnumber": "00001",
        "proposal": "blc00001",
    },
]


today = datetime.date.today()

SESSIONS = [
    {
        "sessionid": 1,
        "proposalid": 1,
        "proposal": "blc00001",
        "visit_number": 1,
        "session": "blc00001-1",
        "startdate": datetime.datetime.combine(today, datetime.datetime.min.time()),
        "enddate": datetime.datetime.combine(today, datetime.datetime.max.time()),
    },
]

COMPONENTS = [
    {
        "proposalid": 1,
        "componentid": 1,
        "acronym": "comp1",
        "name": "component 1",
        "samples": 3,
        "datacollections": 0,
    },
    {
        "proposalid": 1,
        "componentid": 2,
        "acronym": "comp2",
        "name": "component 2",
        "samples": 0,
        "datacollections": 0,
    },
]

SAMPLES = [
    {
        "proposalid": 1,
        "sampleid": 1,
        "component": "comp1",
        "componentid": 1,
        "name": "test1",
        "offsetx": 0,
        "offsety": 0,
        "subsamples": 0,
        "datacollections": 0,
    },
    {
        "proposalid": 1,
        "component": "comp1",
        "componentid": 1,
        "sampleid": 2,
        "name": "test2",
        "offsetx": 100,
        "offsety": 200,
        "subsamples": 0,
        "datacollections": 0,
    },
]

SUBSAMPLES = [
    {
        "sampleid": 2,
        "x": 40,
        "y": 40,
        "x2": 14640,
        "y2": 17040,
        "type": "roi",
        "subsampleid": 1,
        "datacollections": 1,
        "positions": {"z": 4},
    },
    {
        "sampleid": 2,
        "x": 120,
        "y": 200,
        "type": "poi",
        "subsampleid": 2,
        "datacollections": 0,
        "positions": {},
    },
    {
        "sampleid": 2,
        "x": 40000,
        "y": 40000,
        "x2": 40071,
        "y2": 40071,
        "type": "roi",
        "subsampleid": 3,
        "datacollections": 1,
        "positions": {"z": 3},
    },
    {
        "sampleid": 2,
        "x": 600,
        "y": 600,
        "type": "poi",
        "subsampleid": 4,
        "datacollections": 1,
        "positions": {},
    },
    {
        "sampleid": 1,
        "x": 40000,
        "y": 50000,
        "x2": 54600,
        "y2": 60000,
        "type": "roi",
        "subsampleid": 5,
        "datacollections": 0,
        "positions": {},
    },
    {
        "sampleid": 1,
        "x": 1000,
        "y": 2000,
        "type": "poi",
        "subsampleid": 6,
        "datacollections": 0,
        "positions": {"z": 2.5},
    },
]

SAMPLEIMAGES = [
    {
        "sampleid": 2,
        "sampleimageid": 1,
        "name": "mos1",
        "file": "xtal.png",
        "scalex": 100,
        "scaley": 100,
        "rotation": -25,
        "offsetx": -102400,
        "offsety": -76800,
    },
    {
        "sampleid": 2,
        "sampleimageid": 2,
        "name": "mos2",
        "file": "xtal.png",
        "scalex": 50,
        "scaley": 51,
        "offsetx": 102200,
        "offsety": 200,
    },
]

SAMPLEACTIONS = []

DATACOLLECTIONS = [
    {
        "datacollectionid": 1,
        "sessionid": 1,
        "sampleid": 2,
        "subsampleid": 1,
        "starttime": datetime.datetime.now() - datetime.timedelta(seconds=600),
        "endtime": datetime.datetime.now() - datetime.timedelta(seconds=400),
        "experimenttype": "XRF map",
        "runstatus": "Successful",
        "datacollectionnumber": 2,
        "snapshots": {1: False, 2: False, 3: False, 4: False},
    }
]

DATACOLLECTIONATTACHMENTS = []


# XRF
XRF_MAPS = [
    {
        "sampleid": 2,
        "mapid": 1,
        "subsampleid": 3,
        "datacollectionid": 1,
        "maproiid": 2,
        "element": "Cu",
        "w": 71,
        "h": 71,
        "snaked": True,
        "data": MAP_DATA[1],
        "opacity": 1,
        "url": "/metadata/xrf/maps/1",
    },
    {
        "sampleid": 2,
        "mapid": 2,
        "subsampleid": 1,
        "datacollectionid": 1,
        "maproiid": 2,
        "element": "Cu",
        "edge": "Ka1",
        "w": 102,
        "h": 119,
        "snaked": False,
        "data": MAP_DATA[2],
        "opacity": 1,
        "composites": 1,
        "url": "/metadata/xrf/maps/2",
    },
    {
        "sampleid": 2,
        "mapid": 3,
        "subsampleid": 1,
        "datacollectionid": 1,
        "maproiid": 3,
        "element": "Ni",
        "edge": "Ka1",
        "w": 102,
        "h": 119,
        "snaked": False,
        "data": MAP_DATA[3],
        "opacity": 1,
        "composites": 1,
        "url": "/metadata/xrf/maps/3",
    },
    {
        "sampleid": 2,
        "mapid": 4,
        "subsampleid": 1,
        "datacollectionid": 1,
        "maproiid": 1,
        "element": "Fe",
        "edge": "Ka1",
        "w": 102,
        "h": 119,
        "snaked": True,
        "data": MAP_DATA[0],
        "opacity": 0.7,
        "composites": 1,
        "url": "/metadata/xrf/maps/4",
    },
]

XRF_MAP_ROIS = [
    {
        "maproiid": 1,
        "element": "Fe",
        "edge": "Ka1",
        "start": 6200,
        "end": 6600,
        "maps": 1,
    }
]

XRF_COMPOSITES = [
    {
        "compositeid": 1,
        "sampleid": 2,
        "subsampleid": 1,
        "r": 2,
        "rroi": "Cu",
        "g": 3,
        "groi": "Ni",
        "b": 4,
        "broi": "Fe",
        "ropacity": 1,
        "gopacity": 1,
        "bopacity": 1,
        "opacity": 1,
        "url": "/metadata/xrf/maps/composite/1",
    }
]
