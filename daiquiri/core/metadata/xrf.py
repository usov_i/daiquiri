# -*- coding: utf-8 -*-
import io
import gzip
import json

import matplotlib as mpl
import matplotlib.cm as cm
import numpy as np
from PIL import Image

from daiquiri.core.utils import worker


def shape_map(map_):
    """Shapes a 1d XRF map array into the correct 2d image

    Reorders the data if needbe for snaked collection
    Reshapes if the data was collected vertically

    Args:
        map_ (dict): An XRF map from the metadata handler

    Returns:
        data (ndarray): The XRF map data

    """
    data = np.array(map_["data"])

    # TODO: Catch raise
    if map_.get("orientation") == "vertical":
        data = data.reshape(int(map_["w"]), int(map_["h"]))
        data = np.rot90(data)
        data = np.flipud(data)
    else:
        data = data.reshape(int(map_["h"]), int(map_["w"]))

    # For snaked collection every other row is reversed
    if map_.get("snaked"):
        data[1::2, :] = data[1::2, ::-1]

    return data


MAP_SCALINGS = {"linear": mpl.colors.Normalize, "logarithmic": mpl.colors.LogNorm}


def generate_map_image(map_):
    """Generates a PIL Image from an XRF map

    -1 placeholder values are converted to a transparent pixel

    Args:
        map_ (dict): An XRF map from the metadata handler

    Returns:
        image (Image): A PIL image
    """

    def generate():
        data = shape_map(map_)

        norm_alg = MAP_SCALINGS.get(map_.get("scale", "linear"), MAP_SCALINGS["linear"])
        norm = norm_alg(vmin=map_.get("min"), vmax=map_.get("max"))

        colourmap = map_.get("colourmap") or "viridis"
        if not hasattr(cm, colourmap):
            colourmap = "viridis"

        cmap = getattr(cm, colourmap or "viridis")

        m = cm.ScalarMappable(norm=norm, cmap=cmap)
        img_data = m.to_rgba(data, bytes=True, alpha=map_["opacity"])

        mask = data == -1
        img_data[mask, :] = [255, 255, 255, 0]

        return Image.fromarray(img_data, "RGBA")

    return worker(generate)


def generate_composite_image(comp, maps):
    """Generates a PIL Image from an XRF composite map

    Args:
        comp (dict): An XRF composite map from the metadata handler
        maps (list(dict)): A list of XRF maps from the metadata handler

    Returns:
        image (Image): A PIL image
    """

    def generate():
        layers = []
        for col in ["r", "g", "b"]:
            map_ = maps[col]

            data = shape_map(map_)

            norm = mpl.colors.Normalize(vmin=map_.get("min"), vmax=map_.get("max"))
            map_["norm"] = norm(data) * 255 * comp[f"{col}opacity"]

            layers.append(map_["norm"])

        layers.append(
            np.full((int(map_["h"]), int(map_["w"])), round(comp["opacity"] * 255))
        )

        img_data = np.dstack(layers).astype(np.uint8)
        return Image.fromarray(img_data, "RGBA")

    return worker(generate)


def generate_histogram(data):
    """Generates a histogram of map data

    Args:
        map_(dict): An XRF map from the metadata handler

    Returns:
        data: (dict(list)): The histogram, bins, and widths
    """

    def generate():
        ndata = np.array(data)
        rdata = np.where(ndata == -1, 0, ndata)

        try:
            hist, bins = np.histogram(rdata, bins=50)
            center = (bins[:-1] + bins[1:]) / 2
            width = np.diff(bins)

        # TODO: This should not happen
        except (OverflowError, ValueError):
            hist = []
            center = []
            width = []

        return {"hist": hist, "bins": center, "width": width}

    return worker(generate)


def gunzip_json(bytes_obj):
    """Un gzips a bytes object and load into json

    Args:
        bytes_obj(BytesIO): The compressed data

    Returns:
        data(dict): The decoded json as a python object
    """
    if not bytes_obj:
        return []

    in_ = io.BytesIO()
    in_.write(bytes_obj)
    in_.seek(0)
    with gzip.GzipFile(fileobj=in_, mode="rb") as fo:
        gunzipped_bytes_obj = fo.read()

    return json.loads(gunzipped_bytes_obj.decode())


def gzip_json(obj):
    """Gzips a json dump of a python object

    Args:
        obj(dict): An object

    Returns:
        data(BytesIO): The binary compressed data
    """
    json_str = json.dumps(obj)

    out = io.BytesIO()
    with gzip.GzipFile(fileobj=out, mode="w") as fo:
        fo.write(json_str.encode())

    return out.getvalue()
