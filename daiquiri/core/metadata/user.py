class User(dict):
    is_staff = False

    def permission(self, permission):
        if "permissions" in self:
            return permission in self["permissions"]

    def group(self, group):
        if "groups" in self:
            return group in self["groups"]

    def staff(self):
        return self["is_staff"] is True
