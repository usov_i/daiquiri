# -*- coding: utf-8 -*-
import logging

from flask import g

from daiquiri.core.metadata import MetaDataHandler
from daiquiri.core.metadata.user import User
from daiquiri.core.metadata.mockdata import (
    USERS,
    PROPOSALS,
    SESSIONS,
    COMPONENTS,
    SAMPLES,
    SUBSAMPLES,
    SAMPLEIMAGES,
    SAMPLEACTIONS,
    DATACOLLECTIONS,
    DATACOLLECTIONATTACHMENTS,
    XRF_MAPS,
    XRF_MAP_ROIS,
    XRF_COMPOSITES,
)
from daiquiri.core.metadata.xrf import generate_histogram

logger = logging.getLogger(__name__)


class MockMetaDataHandler(MetaDataHandler):
    _componentid = len(COMPONENTS) + 1
    _sampleid = len(SAMPLES) + 1
    _sampleimageid = len(SAMPLEIMAGES) + 1
    _subsampleid = len(SUBSAMPLES) + 1
    _datacollectionid = len(DATACOLLECTIONS) + 1
    _datacollectionattachmentid = len(DATACOLLECTIONATTACHMENTS) + 1
    _sampleactionid = len(SAMPLEACTIONS) + 1

    _xrf_mapid = len(XRF_MAPS) + 1
    _xrf_map_roiid = len(XRF_MAP_ROIS) + 1
    _xrf_compositeid = len(XRF_COMPOSITES) + 1

    def get_user(self, **kwargs):
        for u in USERS:
            if u["login"] == g.login:
                user = User(**u)
                user["is_staff"] = user.permission(self._config["meta_staff"])

                return user

    def set_session(self, session):
        if self.verify_session(session):
            self._session.update({"blsession": session})
            return True

    def verify_session(self, session):
        for s in SESSIONS:
            if s["session"] == session:
                return s

        return None

    def get_proposals(self, proposal=None, **kwargs):
        if proposal:
            for p in PROPOSALS:
                if p["proposal"] == proposal:
                    return p

            return None

        return {"total": len(PROPOSALS), "rows": PROPOSALS}

    def get_sessions(self, session=None, **kwargs):
        if session:
            for s in SESSIONS:
                if s["session"] == session:
                    return s

            return None

        if kwargs.get("sessionid"):
            for s in SESSIONS:
                if s["sessionid"] == kwargs["sessionid"]:
                    return s

            return None

        return {"total": len(SESSIONS), "rows": SESSIONS}

    def get_components(self, componentid=None, **kwargs):
        if componentid:
            for c in COMPONENTS:
                if c["componentid"] == componentid:
                    return c

            return None

        return {"total": len(COMPONENTS), "rows": COMPONENTS}

    def add_component(self, **kwargs):
        kwargs["componentid"] = self._componentid
        self._componentid += 1

        COMPONENTS.append(kwargs)
        return kwargs

    def update_component(self, componentid, **kwargs):
        print("update_component", componentid, kwargs)
        componentid = self.get_components(componentid)

        if componentid:
            for k, v in kwargs.items():
                componentid[k] = v

            return componentid

    def get_samples(self, sampleid=None, **kwargs):
        if sampleid:
            for s in SAMPLES:
                if s["sampleid"] == sampleid:
                    return s

            return None

        return {"total": len(SAMPLES), "rows": SAMPLES}

    def add_sample(self, **kwargs):
        kwargs["sampleid"] = self._sampleid
        kwargs["datacollections"] = 0
        kwargs["subsamples"] = 0
        self._sampleid += 1

        SAMPLES.append(kwargs)
        return kwargs

    def remove_sample(self, sampleid):
        sample = self.get_samples(sampleid)
        if sample:
            SAMPLES.remove(sample)
            return True

    def update_sample(self, sampleid, **kwargs):
        print("update_sample", sampleid, kwargs)
        sample = self.get_samples(sampleid)

        if sample:
            for k, v in kwargs.items():
                sample[k] = v

            return sample

    def queue_sample(self, sampleid, **kwargs):
        queue = 1
        if "unqueue" in kwargs:
            queue = 0

        sample = self.update_sample(sampleid, queued=queue)
        if sample:
            return 1, 1

    def unqueue_sample(self, sampleid, **kwargs):
        self.queue_sample(sampleid, unqueue=True)

    def get_subsamples(self, subsampleid=None, **kwargs):
        if subsampleid:
            for s in SUBSAMPLES:
                if s["subsampleid"] == subsampleid:
                    return s

            return None

        if "sampleid" in kwargs:
            return [s for s in SUBSAMPLES if s["sampleid"] == kwargs["sampleid"]]

        return SUBSAMPLES

    def add_subsample(self, **kwargs):
        kwargs["subsampleid"] = self._subsampleid
        kwargs["datacollections"] = 0
        self._subsampleid += 1

        sample = self.get_samples(kwargs["sampleid"])
        sample["subsamples"] += 1

        SUBSAMPLES.append(kwargs)
        return kwargs

    def update_subsample(self, subsampleid, **kwargs):
        print("update_subsample", subsampleid, kwargs)
        subsample = self.get_subsamples(subsampleid)

        # if subsample["datacollections"] > 0:
        #     return

        if subsample:
            for k, v in kwargs.items():
                subsample[k] = v

            return subsample

    def remove_subsample(self, subsampleid):
        subsample = self.get_subsamples(subsampleid)

        if subsample:
            SUBSAMPLES.remove(subsample)
            return True

    def queue_subsample(self, subsampleid, **kwargs):
        queue = 1
        if "unqueue" in kwargs:
            queue = 0

        subsample = self.update_subsample(subsampleid, queued=queue)
        if subsample:
            print("queue_subsample after", subsample)
            return 1, 1

    def unqueue_subsample(self, subsampleid, **kwargs):
        self.queue_subsample(subsampleid, unqueue=True)

    def get_sampleimages(self, sampleimageid=None, **kwargs):
        for i in SAMPLEIMAGES:
            i["url"] = f"/{self._base_url}/samples/images/{i['sampleimageid']}"

        if sampleimageid:
            for s in SAMPLEIMAGES:
                if s["sampleimageid"] == sampleimageid:
                    return s

            return None

        if "sampleid" in kwargs:
            return [s for s in SAMPLEIMAGES if s["sampleid"] == kwargs["sampleid"]]

        return SAMPLEIMAGES

    def add_sampleimage(self, **kwargs):
        kwargs["sampleimageid"] = self._sampleimageid
        self._sampleimageid += 1

        SAMPLEIMAGES.append(kwargs)
        return self.get_sampleimages(sampleimageid=kwargs["sampleimageid"])

    def get_sampleactions(self, sampleactionid=None, **kwargs):
        if sampleactionid:
            for a in SAMPLEACTIONS:
                if a["sampleactionid"] == sampleactionid:
                    return a

            return None

        if "sampleid" in kwargs:
            return [a for a in SAMPLEACTIONS if a["sampleid"] == kwargs["sampleid"]]

        return SAMPLEACTIONS

    def add_sampleaction(self, **kwargs):
        kwargs["sampleactionid"] = self._sampleactionid
        self._sampleactionid += 1

        SAMPLEACTIONS.append(kwargs)
        return kwargs

    def update_sampleaction(self, sampleactionid, **kwargs):
        print("update_sampleaction", sampleactionid, kwargs)
        sampleaction = self.get_sampleactions(sampleactionid)

        if sampleaction:
            for k, v in kwargs.items():
                sampleaction[k] = v

            return sampleaction

    def get_datacollections(self, datacollectionid=None, **kwargs):
        print("get_datacollections", datacollectionid, kwargs)
        datacollections_copy = DATACOLLECTIONS.copy()
        for d in datacollections_copy:
            d = self._check_snapshots(d)

        if datacollectionid:
            for i, d in enumerate(datacollections_copy):
                if d["datacollectionid"] == datacollectionid:
                    return DATACOLLECTIONS[i] if kwargs.get("original") else d

            return None

        if "sampleid" in kwargs:
            dcs = [
                d for d in datacollections_copy if d["sampleid"] == kwargs["sampleid"]
            ]
            return {"total": len(dcs), "rows": dcs}

        if "subsampleid" in kwargs:
            dcs = [
                d
                for d in datacollections_copy
                if d["subsampleid"] == kwargs["subsampleid"]
            ]
            return {"total": len(dcs), "rows": dcs}

        dcs = [d for d in datacollections_copy]
        return {"total": len(dcs), "rows": dcs}

    def add_datacollection(self, **kwargs):
        kwargs["datacollectionid"] = self._datacollectionid
        kwargs["datacollectiongroupid"] = self._datacollectionid
        self._datacollectionid += 1

        if "subsampleid" in kwargs:
            subsample = self.get_subsamples(kwargs["subsampleid"])
            subsample["datacollections"] += 1

        if "sampleid" in kwargs:
            sample = self.get_samples(kwargs["sampleid"])
            sample["datacollections"] += 1

        DATACOLLECTIONS.append(kwargs)

        print("add datacollection", kwargs)
        return kwargs.copy()

    def update_datacollection(self, datacollectionid=None, **kwargs):
        dc = self.get_datacollections(datacollectionid, original=True)
        if dc:
            for kw, val in kwargs.items():
                if kw == "runstatus" and dc.get("subsampleid"):
                    self.unqueue_subsample(dc["subsampleid"])

                dc[kw] = val

            print("update_datacollection after", dc)
            return dc.copy()

    def get_datacollection_attachments(
        self, datacollectionfileattachmentid=None, **kwargs
    ):
        if datacollectionfileattachmentid:
            for a in DATACOLLECTIONATTACHMENTS:
                if (
                    a["datacollectionfileattachmentid"]
                    == datacollectionfileattachmentid
                ):
                    return a

            return None

        if "datacollectionid" in kwargs:
            return [
                a
                for a in DATACOLLECTIONATTACHMENTS
                if a["datacollectionid"] == kwargs["datacollectionid"]
            ]

        return DATACOLLECTIONATTACHMENTS

    def add_datacollection_attachment(self, **kwargs):
        kwargs["datacollectionfileattachmentid"] = self._datacollectionattachmentid
        self._datacollectionattachmentid += 1

        DATACOLLECTIONATTACHMENTS.append(kwargs)
        return self.get_datacollection_attachments(
            datacollectionattachmentid=kwargs["datacollectionfileattachmentid"]
        )

    def add_datacollectionplan(self, **kwargs):
        pass

    def get_xrf_maps(self, mapid=None, **kwargs):
        xrf_maps_copy = XRF_MAPS.copy()
        if mapid is not None:
            for m in xrf_maps_copy:
                if m["mapid"] == mapid:
                    m["histogram"] = generate_histogram(m["data"])
                    return m

            return None

        if kwargs.get("sampleid"):
            return [m for m in XRF_MAPS if m["sampleid"] == kwargs.get("sampleid")]

        if kwargs.get("subsampleid"):
            return [
                m for m in XRF_MAPS if m["subsampleid"] == kwargs.get("subsampleid")
            ]

        for m in xrf_maps_copy:
            m["histogram"] = generate_histogram(m["data"])

        return xrf_maps_copy

    def add_xrf_map(self, **kwargs):
        print("add map", kwargs)

        dc = self.get_datacollections(datacollectionid=kwargs["datacollectionid"])
        roi = self.get_xrf_map_rois(maproiid=kwargs["maproiid"])

        if roi.get("scalar"):
            kwargs["scalar"] = roi["scalar"]
        else:
            kwargs["element"] = roi["element"]
            kwargs["edge"] = roi["edge"]

        for k in ["sampleid", "subsampleid"]:
            kwargs[k] = dc[k]

        kwargs["w"] = dc["steps_x"]
        kwargs["h"] = dc["steps_y"]

        kwargs["snaked"] = False
        kwargs["opacity"] = 1

        kwargs["mapid"] = self._xrf_mapid
        self._xrf_mapid += 1

        XRF_MAPS.append(kwargs)

        return kwargs

    def update_xrf_map(self, mapid, **kwargs):
        m = self.get_xrf_maps(mapid=mapid)
        if m:
            print("update map")
            for k, v in kwargs.items():
                m[k] = v

            return m

    def remove_xrf_map(self, mapid):
        m = self.get_xrf_maps(mapid=mapid)
        if m:
            XRF_MAPS.remove(m)
            return True

    def get_xrf_map_rois(self, maproiid=None, **kwargs):
        if maproiid is not None:
            for m in XRF_MAP_ROIS:
                if m["maproiid"] == maproiid:
                    return m

            return None

        return XRF_MAP_ROIS

    def add_xrf_map_roi(self, **kwargs):
        kwargs["maproiid"] = self._xrf_map_roiid
        self._xrf_map_roiid += 1

        kwargs["maps"] = 0

        XRF_MAP_ROIS.append(kwargs)

        return kwargs

    def add_xrf_map_roi_scalar(self, **kwargs):
        kwargs["maproiid"] = self._xrf_map_roiid
        self._xrf_map_roiid += 1

        kwargs["maps"] = 0
        kwargs["startenergy"] = 0
        kwargs["endenergy"] = 0

        XRF_MAP_ROIS.append(kwargs)

        return kwargs

    def update_xrf_map_roi(self, maproiid, **kwargs):
        roi = self.get_xrf_map_rois(maproiid=maproiid)
        if roi:
            for k, v in kwargs.items():
                roi[k] = v

            return roi

    def remove_xrf_map_roi(self, maproiid, **kwargs):
        roi = self.get_xrf_map_rois(maproiid=maproiid)
        if roi:
            XRF_MAP_ROIS.remove(roi)
            return True

    def get_xrf_composites(self, compositeid=None, sampleid=None):
        if compositeid is not None:
            for c in XRF_COMPOSITES:
                if c["compositeid"] == compositeid:
                    return c

            return None

        if sampleid is not None:
            return [m for m in XRF_COMPOSITES if m["sampleid"] == sampleid]

        return XRF_COMPOSITES

    def add_xrf_composite(self, **kwargs):
        subsample = self.get_subsamples(subsampleid=kwargs["subsampleid"])

        if not subsample:
            return

        exists = []
        for mid in ["r", "g", "b"]:
            for m in XRF_MAPS:
                if kwargs[mid] == m["mapid"]:
                    exists.append(True)

        if len(exists) != 3:
            return

        kwargs["sampleid"] = subsample["sampleid"]
        kwargs["opacity"] = 1

        kwargs["compositeid"] = self._xrf_compositeid
        self._xrf_compositeid += 1

        XRF_COMPOSITES.append(kwargs)

        return kwargs

    def update_xrf_composite(self, compositeid, **kwargs):
        composite = self.get_xrf_composites(compositeid=compositeid)
        if composite:
            for k, v in kwargs.items():
                composite[k] = v

            return composite

    def remove_xrf_composite(self, compositeid, **kwargs):
        comp = self.get_xrf_composites(compositeid=compositeid)
        if comp:
            XRF_COMPOSITES.remove(comp)
            return True
