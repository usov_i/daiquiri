# -*- coding: utf-8 -*-
import os
import json

from flask import g

import sqlalchemy
from sqlalchemy import func, or_
from sqlalchemy.sql.expression import cast

from daiquiri.core.metadata.ispyalchemy.handler import IspyalchemyHandler
from daiquiri.core.metadata.ispyalchemy.utils import page, order


class DCHandler(IspyalchemyHandler):
    exported = [
        "get_datacollections",
        "add_datacollection",
        "update_datacollection",
        "get_datacollection_attachments",
        "add_datacollection_attachment",
        "add_sampleaction",
        "update_sampleaction",
        "get_sampleactions",
        "get_sampleaction_positions",
        "add_sampleaction_position",
        "remove_sampleaction_position",
        "get_scanqualityindicators",
        "add_scanqualityindicators",
        "get_datacollectionplans",
        "add_datacollectionplan",
        "update_datacollectionplan",
        "remove_datacollectionplan",
        "_decode_scanparameters",
    ]

    def get_datacollections(self, datacollectionid=None, **kwargs):
        with self.session_scope() as ses:
            duration = func.time_to_sec(
                func.timediff(
                    self.DataCollection.endtime,
                    self.DataCollection.starttime,
                )
            )
            dcid = self.DataCollection.datacollectionid
            starttime = self.DataCollection.starttime
            endtime = self.DataCollection.endtime

            if not kwargs.get("datacollectiongroupid") and not kwargs.get("ungroup"):
                duration = func.sum(duration)
                dcid = func.min(self.DataCollection.datacollectionid).label(
                    "datacollectionid"
                )
                starttime = func.min(self.DataCollection.starttime).label("starttime")
                endtime = func.max(self.DataCollection.endtime).label("endtime")

            datacollections = (
                ses.query(
                    dcid,
                    self.DataCollectionGroup.datacollectiongroupid,
                    self.DataCollectionGroup.blsampleid.label("sampleid"),
                    self.BLSample.name.label("sample"),
                    self.DataCollection.blsubsampleid.label("subsampleid"),
                    self.DataCollection.datacollectionnumber,
                    self.DataCollection.comments,
                    starttime,
                    endtime,
                    self.DataCollection.xtalsnapshotfullpath1,
                    self.DataCollection.xtalsnapshotfullpath2,
                    self.DataCollection.xtalsnapshotfullpath3,
                    self.DataCollection.xtalsnapshotfullpath4,
                    self.DataCollection.filetemplate,
                    self.DataCollection.imagedirectory,
                    duration.label("duration"),
                    self.DataCollection.runstatus,
                    self.DataCollectionGroup.experimenttype,
                    self.DataCollection.exposuretime,
                    self.DataCollection.numberofimages,
                    self.DataCollection.numberofpasses,
                    self.DataCollection.wavelength,
                    self.DataCollection.xbeam,
                    self.DataCollection.ybeam,
                    self.DataCollection.beamsizeatsamplex,
                    self.DataCollection.beamsizeatsampley,
                    cast(self.GridInfo.steps_x, sqlalchemy.Integer).label("steps_x"),
                    cast(self.GridInfo.steps_y, sqlalchemy.Integer).label("steps_y"),
                    self.GridInfo.patchesx,
                    self.GridInfo.patchesy,
                    self.GridInfo.dx_mm.label("dx_mm"),
                    self.GridInfo.dy_mm.label("dy_mm"),
                    self.DataCollection.datacollectionplanid,
                )
                .join(
                    self.DataCollectionGroup,
                    self.DataCollectionGroup.datacollectiongroupid
                    == self.DataCollection.datacollectiongroupid,
                )
                .outerjoin(
                    self.BLSample,
                    self.BLSample.blsampleid == self.DataCollectionGroup.blsampleid,
                )
                .join(self.BLSession)
                .outerjoin(
                    self.GridInfo,
                    self.GridInfo.datacollectionid
                    == self.DataCollection.datacollectionid,
                )
            )

            if kwargs.get("datacollectiongroupid"):
                datacollections = datacollections.filter(
                    self.DataCollection.datacollectiongroupid
                    == kwargs["datacollectiongroupid"]
                )
            elif kwargs.get("ungroup") is not None:
                pass
            else:
                datacollections = datacollections.add_columns(
                    func.count(
                        func.distinct(self.DataCollection.datacollectionid)
                    ).label("datacollections")
                )
                datacollections = datacollections.group_by(
                    self.DataCollection.datacollectiongroupid
                )

            if not kwargs.get("no_context"):
                datacollections = datacollections.filter(
                    self.BLSession.sessionid == g.blsession.get("sessionid")
                )

                if not g.user.staff():
                    datacollections = datacollections.join(
                        self.Session_has_Person
                    ).filter(self.Session_has_Person.personid == g.user.get("personid"))

            if kwargs.get("sampleid"):
                datacollections = datacollections.filter(
                    self.DataCollectionGroup.blsampleid == kwargs.get("sampleid")
                )

            if kwargs.get("subsampleid"):
                datacollections = datacollections.filter(
                    self.DataCollection.blsubsampleid == kwargs.get("subsampleid")
                )

            if kwargs.get("status"):
                datacollections = datacollections.filter(
                    self.DataCollection.runstatus == kwargs.get("status")
                )

            if datacollectionid:
                datacollections = datacollections.filter(
                    self.DataCollection.datacollectionid == datacollectionid
                )
                datacollection = datacollections.first()
                if datacollection:
                    datacollection = datacollection._asdict()
                    return self._check_snapshots(datacollection)

            else:
                total = datacollections.count()
                datacollections = order(
                    datacollections,
                    {
                        "id": self.DataCollection.datacollectionid,
                        "starttime": self.DataCollection.starttime,
                        "runstatus": self.DataCollection.runstatus,
                        "experimenttype": self.DataCollectionGroup.experimenttype,
                        "duration": duration,
                    },
                    default=["id", "desc"],
                    **kwargs,
                )
                datacollections = page(datacollections, **kwargs)

                dcs = [r._asdict() for r in datacollections.all()]
                for dc in dcs:
                    dc = self._check_snapshots(dc)

                return {"total": total, "rows": dcs}

    def add_datacollection(self, **kwargs):
        with self.session_scope() as ses:
            dcg_id = kwargs.get("datacollectiongroupid")
            if dcg_id is None:
                dcg = self.DataCollectionGroup(
                    sessionid=kwargs.get("sessionid"),
                    # New location of sampleid
                    blsampleid=kwargs.get("sampleid"),
                    experimenttype=kwargs.get("experimenttype"),
                    scanparameters=kwargs.get("scanparameters"),
                )

                ses.add(dcg)
                ses.commit()

                dcg_id = dcg.datacollectiongroupid

            dc = self.DataCollection(
                datacollectiongroupid=dcg_id,
                filetemplate=kwargs.get("filetemplate"),
                imagedirectory=kwargs.get("imagedirectory"),
                starttime=kwargs.get("starttime"),
                # This is set in dcg, not dc
                # experimenttype=kwargs.get("experimenttype"),
                datacollectionplanid=kwargs.get("datacollectionplanid"),
                # Legacy, but still used in synchweb, should be in dcg above
                blsampleid=kwargs.get("sampleid"),
                blsubsampleid=kwargs.get("subsampleid"),
                datacollectionnumber=kwargs.get("datacollectionnumber", None),
                exposuretime=kwargs.get("exposuretime", None),
                numberofimages=kwargs.get("numberofimages", None),
                xtalsnapshotfullpath1=kwargs.get("xtalsnapshotfullpath1"),
                beamsizeatsamplex=kwargs.get("beamsizeatsamplex"),
                beamsizeatsampley=kwargs.get("beamsizeatsampley"),
            )

            ses.add(dc)
            ses.commit()

            if kwargs.get("steps_x") or kwargs.get("steps_y"):
                grid = self.GridInfo(
                    datacollectionid=dc.datacollectionid,
                    steps_x=kwargs.get("steps_x"),
                    steps_y=kwargs.get("steps_y"),
                    patchesx=kwargs.get("patchesx"),
                    patchesy=kwargs.get("patchesy"),
                    dx_mm=kwargs.get("dx_mm"),
                    dy_mm=kwargs.get("dy_mm"),
                    pixelspermicronx=kwargs.get("pixelspermicronx"),
                    pixelspermicrony=kwargs.get("pixelspermicrony"),
                    snapshot_offsetxpixel=kwargs.get("snapshot_offsetxpixel"),
                    snapshot_offsetypixel=kwargs.get("snapshot_offsetypixel"),
                )

                ses.add(grid)
                ses.commit()

            return self.get_datacollections(
                datacollectionid=dc.datacollectionid, no_context=True
            )

    def update_datacollection(self, datacollectionid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_datacollections(
                datacollectionid=datacollectionid, no_context=kwargs.get("no_context")
            )
            if chk:
                dc = (
                    ses.query(self.DataCollection)
                    .filter(self.DataCollection.datacollectionid == datacollectionid)
                    .first()
                )

                updatable = [
                    "runstatus",
                    "endtime",
                    "comments",
                    "datacollectionnumber",
                    "numberofimages",
                    "numberofpasses",
                    "xtalsnapshotfullpath1",
                    "xtalsnapshotfullpath2",
                    "xtalsnapshotfullpath3",
                    "xtalsnapshotfullpath4",
                    "wavelength",
                    "transmission",
                    "xbeam",
                    "ybeam",
                    "beamsizeatsamplex",
                    "beamsizeatsampley",
                    "imageprefix",
                    "imagedirectory",
                    "imagesuffix",
                    "filetemplate",
                    "imagecontainersubpath",
                    "detectordistance",
                    "detectorid",
                    "flux",
                    "exposuretime",
                ]
                for kw in kwargs:
                    if kw in updatable:
                        setattr(dc, kw, kwargs[kw])

                gridinfo = (
                    ses.query(self.GridInfo)
                    .filter(self.GridInfo.datacollectionid == dc.datacollectionid)
                    .first()
                )

                gr_updatable = [
                    "orientation",
                    "steps_x",
                    "steps_y",
                    "dx_mm",
                    "dy_mm",
                    "pixelspermicronx",
                    "pixelspermicrony",
                    "snapshot_offsetxpixel",
                    "snapshot_offsetypixel",
                ]
                update_grid = False
                for kw in kwargs:
                    if kw in gr_updatable:
                        update_grid = True

                if update_grid:
                    if not gridinfo:
                        gridinfo = self.GridInfo(datacollectionid=dc.datacollectionid)
                        ses.add(gridinfo)
                        ses.commit()

                    for kw in kwargs:
                        if kw in gr_updatable:
                            setattr(gridinfo, kw, kwargs[kw])

                ses.commit()

                return self.get_datacollections(
                    datacollectionid=datacollectionid,
                    no_context=kwargs.get("no_context"),
                )

    def get_datacollection_attachments(self, datacollectionattachmentid=None, **kwargs):
        with self.session_scope() as ses:
            attachments = (
                ses.query(
                    self.DataCollectionFileAttachment.datacollectionfileattachmentid,
                    self.DataCollectionFileAttachment.filefullpath,
                    self.DataCollectionFileAttachment.filetype,
                )
                .join(self.DataCollection)
                .join(self.DataCollectionGroup)
            )

            if not kwargs.get("no_context"):
                attachments = attachments.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("datacollectionid"):
                attachments = attachments.filter(
                    self.DataCollection.datacollectionid == kwargs["datacollectionid"]
                )

            if kwargs.get("filetype"):
                attachments = attachments.filter(
                    self.DataCollectionFileAttachment.filetype == kwargs["filetype"]
                )

            if datacollectionattachmentid:
                attachments = attachments.filter(
                    self.DataCollectionFileAttachment.datacollectionfileattachmentid
                    == datacollectionattachmentid
                )
                attachment = attachments.first()
                if attachment:
                    return self._add_file_to_attachment(attachment._asdict())

            else:
                attachments = [
                    self._add_file_to_attachment(r._asdict()) for r in attachments.all()
                ]
                return {"total": len(attachments), "rows": attachments}

    def _add_file_to_attachment(self, attachment):
        attachment["filename"] = os.path.basename(attachment["filefullpath"])
        attachment["extension"] = (
            os.path.splitext(attachment["filename"])[1][1:].strip().lower()
        )
        return attachment

    def add_datacollection_attachment(self, **kwargs):
        dc = self.get_datacollections(
            datacollectionid=kwargs.get("datacollectionid"),
            no_context=kwargs.get("no_context"),
        )

        if dc:
            with self.session_scope() as ses:
                attachment = self.DataCollectionFileAttachment(
                    datacollectionid=kwargs.get("datacollectionid"),
                    filetype=kwargs.get("filetype"),
                    filefullpath=kwargs.get("filepath"),
                )

                ses.add(attachment)
                ses.commit()

                return self.get_datacollection_attachments(
                    attachment.datacollectionfileattachmentid,
                    no_context=kwargs.get("no_context"),
                )

    def get_sampleactions(self, sampleactionid=None, **kwargs):
        with self.session_scope() as ses:
            sampleactions = ses.query(
                self.RobotAction.robotactionid.label("sampleactionid"),
                self.RobotAction.blsessionid.label("sessionid"),
                self.RobotAction.blsampleid.label("sampleid"),
                self.RobotAction.actiontype,
                self.RobotAction.starttimestamp,
                self.RobotAction.endtimestamp,
                self.RobotAction.status,
                self.RobotAction.message,
                self.RobotAction.xtalsnapshotbefore,
                self.RobotAction.xtalsnapshotafter,
                self.RobotAction.resultfilepath,
            )

            if not kwargs.get("no_context"):
                sampleactions = sampleactions.filter(
                    self.RobotAction.blsessionid == g.blsession.get("sessionid")
                )

            if kwargs.get("actiontype"):
                sampleactions = sampleactions.filter(
                    self.RobotAction.actiontype == kwargs["actiontype"]
                )

            if kwargs.get("sampleid"):
                sampleactions = sampleactions.filter(
                    self.RobotAction.blsampleid == kwargs["sampleid"]
                )

            if sampleactionid:
                sampleactions = sampleactions.filter(
                    self.RobotAction.robotactionid == sampleactionid
                )
                sampleaction = sampleactions.first()
                if sampleaction:
                    sampleaction = sampleaction._asdict()
                    sampleaction = self._check_result(sampleaction)
                    return sampleaction

            else:
                sampleactions = [r._asdict() for r in sampleactions.all()]
                for sampleaction in sampleactions:
                    sampleaction = self._check_result(sampleaction)
                return {"total": len(sampleactions), "rows": sampleactions}

    def _check_result(self, sampleaction):
        sampleaction["has_result"] = (
            os.path.exists(sampleaction["resultfilepath"])
            if sampleaction["resultfilepath"]
            else False
        )
        return sampleaction

    def add_sampleaction(self, **kwargs):
        with self.session_scope() as ses:
            sample = self.get_samples(
                sampleid=kwargs["sampleid"], no_context=kwargs.get("no_context")
            )

            if sample:
                sampleaction = self.RobotAction(
                    blsessionid=kwargs.get("sessionid"),
                    blsampleid=kwargs.get("sampleid"),
                    actiontype=kwargs.get("actiontype"),
                    starttimestamp=kwargs.get("starttime"),
                    status=kwargs.get("status"),
                    message=kwargs.get("message"),
                    xtalsnapshotbefore=kwargs.get("xtalsnapshotbefore"),
                    xtalsnapshotafter=kwargs.get("xtalsnapshotafter"),
                )

                ses.add(sampleaction)
                ses.commit()

                return self.get_sampleactions(
                    sampleactionid=sampleaction.robotactionid,
                    no_context=kwargs.get("no_context"),
                )

    def update_sampleaction(self, sampleactionid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_sampleactions(
                sampleactionid=sampleactionid, no_context=kwargs.get("no_context")
            )
            if chk:
                sampleaction = (
                    ses.query(self.RobotAction)
                    .filter(self.RobotAction.robotactionid == sampleactionid)
                    .first()
                )

                updatable = [
                    "starttimestamp",
                    "endtimestamp",
                    "status",
                    "message",
                    "xtalsnapshotbefore",
                    "xtalsnapshotafter",
                    "resultfilepath",
                ]
                for kw in kwargs:
                    if kw in updatable:
                        val = kwargs[kw]

                        if kw == "message":
                            val = val[:255]

                        setattr(sampleaction, kw, val)

                ses.commit()

                return self.get_sampleactions(
                    sampleactionid=sampleactionid, no_context=kwargs.get("no_context")
                )

    def get_sampleaction_positions(
        self,
        sampleactionpositionid=None,
        sampleactionid=None,
        type=None,
        no_context=None,
    ):
        with self.session_scope() as ses:
            sampleactionspositions = (
                ses.query(
                    self.RobotActionPosition.robotactionpositionid.label(
                        "sampleactionpositionid"
                    ),
                    self.RobotActionPosition.type,
                    self.RobotActionPosition.id,
                    self.RobotActionPosition.posx,
                    self.RobotActionPosition.posy,
                )
                .join(
                    self.RobotAction,
                    self.RobotAction.robotactionid
                    == self.RobotActionPosition.robotactionid,
                )
                .group_by(self.RobotActionPosition.robotactionpositionid)
            )

            if not no_context:
                sampleactionspositions = sampleactionspositions.filter(
                    self.RobotAction.blsessionid == g.blsession.get("sessionid")
                )

            if sampleactionid:
                sampleactionspositions = sampleactionspositions.filter(
                    self.RobotActionPosition.robotactionid == sampleactionid
                )

            if type:
                sampleactionspositions = sampleactionspositions.filter(
                    self.RobotActionPosition.type == type
                )

            if sampleactionpositionid:
                sampleactionspositions = sampleactionspositions.filter(
                    self.RobotActionPosition.robotactionpositionid
                    == sampleactionpositionid
                )
                sampleactionspositions = sampleactionspositions.first()
                if sampleactionspositions:
                    return sampleactionspositions._asdict()

            else:
                sampleactionspositions = [
                    r._asdict() for r in sampleactionspositions.all()
                ]
                return {
                    "total": len(sampleactionspositions),
                    "rows": sampleactionspositions,
                }

    def add_sampleaction_position(self, *, sampleactionid, posx, posy, type, id):
        sampleaction = self.get_sampleactions(sampleactionid=sampleactionid)
        if not sampleaction:
            return

        with self.session_scope() as ses:
            sampleactionposition = self.RobotActionPosition(
                robotactionid=sampleactionid,
                posx=posx,
                posy=posy,
                type=type,
                id=id,
            )
            ses.add(sampleactionposition)
            ses.commit()

            return self.get_sampleaction_positions(
                sampleactionpositionid=sampleactionposition.robotactionpositionid
            )

    def remove_sampleaction_position(self, sampleactionpositionid, **kargs) -> bool:
        with self.session_scope() as ses:
            chk = self.get_sampleaction_positions(
                sampleactionpositionid=sampleactionpositionid
            )
            if chk:
                sampleactionposition = (
                    ses.query(self.RobotActionPosition)
                    .filter(
                        self.RobotActionPosition.robotactionpositionid
                        == sampleactionpositionid
                    )
                    .first()
                )

                ses.delete(sampleactionposition)
                ses.commit()

                return True

        return False

    def get_scanqualityindicators(self, **kwargs):
        with self.session_scope() as ses:
            sqis = (
                ses.query(
                    self.ImageQualityIndicators.imagenumber.label("point"),
                    self.ImageQualityIndicators.totalintegratedsignal.label("total"),
                    self.ImageQualityIndicators.spottotal.label("spots"),
                    self.ImageQualityIndicators.autoprocprogramid,
                )
                .join(
                    self.DataCollection,
                    self.DataCollection.datacollectionid
                    == self.ImageQualityIndicators.datacollectionid,
                )
                .join(self.DataCollectionGroup)
            )

            if not kwargs.get("no_context"):
                sqis = sqis.filter(
                    self.DataCollectionGroup.sessionid == g.blsession.get("sessionid")
                )

            sqis = sqis.filter(
                self.DataCollection.datacollectionid == kwargs.get("datacollectionid")
            )

            sqis = sqis.order_by(self.ImageQualityIndicators.imagenumber)

            data = {}
            for sqi in sqis.all():
                row = sqi._asdict()
                for k in ["point", "total", "spots", "autoprocprogramid"]:
                    if k not in data:
                        data[k] = []

                    data[k].append(row[k])

            return data

    def add_scanqualityindicators(self, **kwargs):
        with self.session_scope() as ses:
            dc = self.get_datacollections(
                datacollectionid=kwargs["datacollectionid"],
                no_context=kwargs.get("no_context"),
            )

            if dc:
                args = {}
                for key in ["total", "spots"]:
                    args[key] = None
                    if kwargs.get(key) is not None:
                        args[key] = float(kwargs[key])

                sqi = self.ImageQualityIndicators(
                    datacollectionid=kwargs["datacollectionid"],
                    imagenumber=kwargs["point"],
                    totalintegratedsignal=args["total"],
                    spottotal=args["spots"],
                )

                ses.add(sqi)
                ses.commit()

                return f"{sqi.datacollectionid}-{sqi.imagenumber}"

    def get_datacollectionplans(
        self,
        datacollectionplanid=None,
        blsampleid=None,
        status=None,
        queued=None,
        no_context=False,
        **kwargs,
    ):
        with self.session_scope() as ses:
            datacollectionplans = (
                ses.query(
                    self.DiffractionPlan.diffractionplanid.label(
                        "datacollectionplanid"
                    ),
                    self.DiffractionPlan.scanparameters,
                    self.DiffractionPlan.experimentkind,
                    self.DiffractionPlan.recordtimestamp.label("timestamp"),
                    self.DiffractionPlan.energy,
                    self.DiffractionPlan.exposuretime,
                    self.BLSample.blsampleid.label("sampleid"),
                    self.BLSubSample.blsubsampleid.label("subsampleid"),
                    self.BLSample.name.label("sample"),
                    self.Protein.acronym.label("component"),
                    self.BLSample_has_DataCollectionPlan.planorder,
                    self.DataCollection.datacollectionid,
                    self.ContainerQueueSample.containerqueuesampleid,
                    func.IF(
                        self.DataCollection.datacollectionid, "executed", "pending"
                    ).label("status"),
                    func.IF(
                        self.ContainerQueueSample.containerqueuesampleid, True, False
                    ).label("queued"),
                    func.IF(
                        self.BLSample_has_DataCollectionPlan.blsampleid,
                        True,
                        False,
                    ).label("linked"),
                )
                .outerjoin(
                    self.BLSample_has_DataCollectionPlan,
                    self.DiffractionPlan.diffractionplanid
                    == self.BLSample_has_DataCollectionPlan.datacollectionplanid,
                )
                .outerjoin(
                    self.ContainerQueueSample,
                    self.ContainerQueueSample.datacollectionplanid
                    == self.DiffractionPlan.diffractionplanid,
                )
                .outerjoin(
                    self.BLSubSample,
                    self.BLSubSample.blsubsampleid
                    == self.ContainerQueueSample.blsubsampleid,
                )
                .outerjoin(
                    self.BLSample,
                    or_(
                        self.BLSample.blsampleid
                        == self.BLSample_has_DataCollectionPlan.blsampleid,
                        self.BLSample.blsampleid
                        == self.ContainerQueueSample.blsampleid,
                        self.BLSample.blsampleid == self.BLSubSample.blsampleid,
                    ),
                )
                .outerjoin(
                    self.Crystal, self.Crystal.crystalid == self.BLSample.crystalid
                )
                .outerjoin(
                    self.Protein, self.Crystal.proteinid == self.Protein.proteinid
                )
                .outerjoin(
                    self.DataCollection,
                    self.DataCollection.datacollectionplanid
                    == self.DiffractionPlan.diffractionplanid,
                )
            )

            if not no_context:
                datacollectionplans = datacollectionplans.filter(
                    self.Protein.proposalid == g.blsession.get("proposalid")
                )

            if blsampleid:
                datacollectionplans = datacollectionplans.filter(
                    self.BLSample_has_DataCollectionPlan.blsampleid == blsampleid
                )

            if status:
                if status == "executed":
                    datacollectionplans = datacollectionplans.filter(
                        self.DataCollection.datacollectionid != None  # noqa: E711
                    )
                else:
                    datacollectionplans = datacollectionplans.filter(
                        self.DataCollection.datacollectionid == None  # noqa: E711
                    )

            if queued:
                datacollectionplans = datacollectionplans.filter(
                    self.ContainerQueueSample.containerqueuesampleid
                    != None  # noqa: E711
                )

            if datacollectionplanid:
                datacollectionplans = datacollectionplans.filter(
                    self.DiffractionPlan.diffractionplanid == datacollectionplanid
                )
                datacollectionplan = datacollectionplans.first()
                if datacollectionplan:
                    return self._decode_scanparameters(datacollectionplan._asdict())

            else:
                datacollectionplans = order(
                    datacollectionplans,
                    {
                        "sampleid": self.BLSample.blsampleid,
                        "planorder": self.BLSample_has_DataCollectionPlan.planorder,
                        "datacollectionplanid": self.DiffractionPlan.diffractionplanid,
                    },
                    default=["datacollectionplanid", "desc"],
                    **kwargs,
                )

                datacollectionplans = [
                    self._decode_scanparameters(r._asdict())
                    for r in datacollectionplans.all()
                ]
                return {"total": len(datacollectionplans), "rows": datacollectionplans}

    def _decode_scanparameters(self, datacollectionplan):
        if datacollectionplan["scanparameters"]:
            datacollectionplan["scanparameters"] = json.loads(
                datacollectionplan["scanparameters"]
            )
        return datacollectionplan

    def add_datacollectionplan(
        self,
        *,
        scanparameters,
        sampleid=None,
        energy=None,
        exposuretime=None,
        experimentkind=None,
        planorder=None,
        no_context=False,
    ):
        with self.session_scope() as ses:
            datacollectionplan = self.DiffractionPlan(
                scanparameters=json.dumps(scanparameters),
                experimentkind=experimentkind,
                energy=energy,
                exposuretime=exposuretime,
            )

            ses.add(datacollectionplan)
            ses.commit()

            if sampleid:
                sample_has_datacollectionplan = self.BLSample_has_DataCollectionPlan(
                    blsampleid=sampleid,
                    datacollectionplanid=datacollectionplan.diffractionplanid,
                    planorder=planorder,
                )

                ses.add(sample_has_datacollectionplan)
                ses.commit()

            return self.get_datacollectionplans(
                datacollectionplanid=datacollectionplan.diffractionplanid,
                no_context=no_context,
            )

    def update_datacollectionplan(
        self, datacollectionplanid, no_context=False, **kwargs
    ):
        with self.session_scope() as ses:
            chk = self.get_datacollectionplans(
                datacollectionplanid=datacollectionplanid,
                no_context=kwargs.get("no_context"),
            )
            if chk:
                datacollectionplan = (
                    ses.query(self.DiffractionPlan)
                    .filter(
                        self.DiffractionPlan.diffractionplanid == datacollectionplanid
                    )
                    .first()
                )

                updatable = [
                    "scanparameters",
                    "experimentkind",
                    "exposuretime",
                    "energy",
                ]
                for kw in kwargs:
                    if kw in updatable:
                        val = kwargs[kw]

                        if kw == "scanparameters":
                            val = json.dumps(val)

                        setattr(datacollectionplan, kw, val)

                sample_has_datacollectionplan = (
                    ses.query(self.BLSample_has_DataCollectionPlan)
                    .filter(
                        self.BLSample_has_DataCollectionPlan.datacollectionplanid
                        == datacollectionplanid,
                    )
                    .first()
                )

                updatable_in_sample = ["planorder"]
                for kw in kwargs:
                    if kw in updatable_in_sample:
                        val = kwargs[kw]

                        setattr(sample_has_datacollectionplan, kw, val)

                ses.commit()

                return self.get_datacollectionplans(
                    datacollectionplanid=datacollectionplanid, no_context=no_context
                )

    def remove_datacollectionplan(self, datacollectionplanid, **kwargs):
        with self.session_scope() as ses:
            chk = self.get_datacollectionplans(
                datacollectionplanid=datacollectionplanid,
                no_context=kwargs.get("no_context"),
            )

            if chk:
                if chk["datacollectionid"]:
                    raise RuntimeError(
                        "Data collection plan has been executed, it cannot be deleted"
                    )

                if chk["queued"]:
                    raise RuntimeError(
                        "Data collection plan has been queued, it cannot be deleted without being unqueued"
                    )

                dp = (
                    ses.query(
                        self.DiffractionPlan,
                    )
                    .filter(
                        self.DiffractionPlan.diffractionplanid == datacollectionplanid,
                    )
                    .first()
                )

                sample_has_datacollectionplan = (
                    ses.query(self.BLSample_has_DataCollectionPlan)
                    .filter(
                        self.BLSample_has_DataCollectionPlan.datacollectionplanid
                        == datacollectionplanid
                    )
                    .first()
                )

                if sample_has_datacollectionplan:
                    ses.delete(sample_has_datacollectionplan)
                    ses.commit()

                ses.delete(dp)
                ses.commit()

                return True
