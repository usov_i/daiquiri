#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import importlib
import logging
import traceback
import sys
import json
import threading
from functools import partial

import graypy
import graypy.handler
from flask import g

import daiquiri
from daiquiri.core.exceptions import PrettyException

logger = logging.getLogger(__name__)

for k in [
    "engineio",
    "engineio.server",
    "socketio.server",
    "werkzeug",
    "geventwebsocket.handler",
]:
    log = logging.getLogger(k)
    log.setLevel(logging.ERROR)


class LogWrapper:
    """LogWrapper wrappers .info etc with an extra dict"""

    _valid_types = set(["app", "hardware", "scan", "queue", "session", "actor"])

    _methods = ["info", "debug", "warning", "error", "exception"]

    def __init__(self, logger):
        self.logger = logger
        for k in self._methods:
            setattr(self, k, partial(self.meth, k))

    def inst(self):
        return self.logger

    def meth(self, meth, *args, type=None, state=None, **kwargs):
        extra = {}
        if state:
            extra["state"] = state
        if type:
            if type in self._valid_types:
                extra["type"] = type
            else:
                logger.warning("type '%s' unsupported", type)

        return getattr(self.logger, meth)(*args, extra=extra, **kwargs)


class GrayPyFilter(logging.Filter):
    """Inject some useful metadata into graylog records"""

    def __init__(self, config):
        self._config = config

    def filter(self, record):
        record.daiquiri_version = daiquiri.__version__
        # TODO: Why is this MainProcess by default?
        record.process_name = "daiquiri"
        record.beamlinename = self._config.get("meta_beamline")

        for library in self._config["versions"]:
            try:
                mod = importlib.import_module(library)
                setattr(record, f"{library}_version", mod.__version__)
            except (AttributeError, ModuleNotFoundError):
                pass

        return True


def handle_unhandled_exception(exc_type, exc_value, exc_traceback):
    """Handler for unhandled exceptions that will write to the logs

    See https://www.scrygroup.com/tutorial/2018-02-06/python-excepthook-logging/
    """
    if isinstance(exc_value, PrettyException):
        logger.exception(exc_value, exc_info=(exc_type, exc_value, exc_traceback))
        messages = logging.getLogger("daiquiri.messages")
        messages.error("\n" + exc_value.pretty())
        return
    if issubclass(exc_type, KeyboardInterrupt):
        # call the default excepthook saved at __excepthook__
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logger.critical(
        "Unhandled exception", exc_info=(exc_type, exc_value, exc_traceback)
    )


def patch_threading_excepthook():
    """Installs logging exception handler into the `threading` modules
    `Thread` object.

    Inspired by https://bugs.python.org/issue1230540
    """
    old_init = threading.Thread.__init__

    def new_init(self, *args, **kwargs):
        old_init(self, *args, **kwargs)
        old_run = self.run

        def run_with_our_excepthook(*args, **kwargs):
            try:
                old_run(*args, **kwargs)
            except (KeyboardInterrupt, SystemExit):
                raise
            except Exception as e:
                sys.excepthook(type(e), e, e.__traceback__)

        self.run = run_with_our_excepthook

    threading.Thread.__init__ = new_init


class Logging:
    _levels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
    }

    _logs = ["user", "ui"]

    def __init__(self):
        self.log_formatter = logging.Formatter(
            "{asctime}\t{name}\t{levelname}\t{message}", style="{"
        )

    def start(self, config):
        sys.excepthook = handle_unhandled_exception
        patch_threading_excepthook()

        self._level = self._levels[config.get("log_level", "debug")]
        _log_root = config.get("log_root", os.path.join("~", "logs", "daquiri"))
        self._log_root = os.path.abspath(os.path.expanduser(_log_root))

        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(self.log_formatter)

        os.makedirs(self._log_root, exist_ok=True)

        file_handler = logging.handlers.TimedRotatingFileHandler(
            f"{self._log_root}/daiquiri.log", when="d", interval=1, backupCount=5
        )
        file_handler.setFormatter(self.log_formatter)

        logging.captureWarnings(True)
        logging.getLogger().addHandler(stdout_handler)
        logging.getLogger().addHandler(file_handler)
        logging.getLogger().setLevel(self._level)

        if config.get("graylog_host") and config.get("graylog_port"):
            self.init_graylog(config)

    def init_sio(self, socketio):
        socket_handler = SocketIOLogHandler(socketio)
        socket_handler.setLevel(self._level)

        for ty in self._logs:
            file_handler = logging.handlers.TimedRotatingFileHandler(
                f"{self._log_root}/{ty}_json.log", when="d", interval=1, backupCount=5
            )
            file_handler.setFormatter(JSONFormatter())
            log = self.get(ty).inst()
            log.addHandler(socket_handler)
            log.addHandler(file_handler)

    def get(self, name):
        if name in self._logs:
            return LogWrapper(logging.getLogger(name))

        else:
            logger.error(f"No such logger {name}")

    def list_files(self):
        return {ty: f"{self._log_root}/{ty}_json.log" for ty in self._logs}

    def init_graylog(self, config):
        # https://github.com/DiamondLightSource/python-zocalo/blob/main/zocalo/__init__.py
        class PythonLevelToSyslogConverter:
            @staticmethod
            def get(level, _):
                if level < 20:
                    return 7  # DEBUG
                elif level < 25:
                    return 6  # INFO
                elif level < 30:
                    return 5  # NOTICE
                elif level < 40:
                    return 4  # WARNING
                elif level < 50:
                    return 3  # ERROR
                elif level < 60:
                    return 2  # CRITICAL
                else:
                    return 1  # ALERT

        graypy.handler.SYSLOG_LEVELS = PythonLevelToSyslogConverter()

        handler = graypy.GELFUDPHandler
        graylog = handler(
            config["graylog_host"], config["graylog_port"], level_names=True
        )
        graylog.addFilter(GrayPyFilter(config))
        logging.getLogger().addHandler(graylog)


def format_record(record):
    if record.exc_info:
        if isinstance(record.exc_info[1], PrettyException):
            stack_trace = record.exc_info[1].pretty()
        else:
            stack_trace = "".join(traceback.format_exception(*record.exc_info))
    else:
        stack_trace = ""
    try:
        record.asctime
    except AttributeError:
        record.asctime = logging._defaultFormatter.formatTime(record)

    try:
        record.type
    except AttributeError:
        record.type = None

    try:
        record.state
    except AttributeError:
        record.state = None

    session = None
    try:
        session = g.sessionid
    except (AttributeError, RuntimeError):
        pass

    return {
        "message": record.getMessage(),
        "level": record.levelname,
        "timestamp": record.asctime,
        "epoch": record.created,
        "logger": record.name,
        "stack_trace": stack_trace,
        "type": record.type,
        "sessionid": session,
        "state": record.state,
    }


class JSONFormatter(logging.Formatter):
    def format(self, record):
        return json.dumps(format_record(record))


class SocketIOLogHandler(logging.Handler):
    """SocketIO Log Handler
    Emits log_records to /logging
    """

    def __init__(self, socketio):
        super().__init__()
        self._socketio = socketio

    def emit(self, record):
        self._socketio.emit("log_record", format_record(record), namespace="/app")


log = Logging()
