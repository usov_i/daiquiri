# -*- coding: utf-8 -*-
"""
Coordinate transformations on any domain (unbound or bound)
can be derived from one of these base classes:

    Map:    f: D -> C
            D is the domain to the map
            C is the codomain to the map
            f(D) ⊆ C is the image of the map
    Injection:  inverse f^(-1) not defined on the entire codomain
    Surjection: inverse f^(-1) not unique (needs boundary conditions)
    Bijection:  inverse f^(-1) is defined on the entire codomain and unique
    LinearMap:  coordinate transformation can be represented
                by a matrix M (m x n) multiplication
    Isomorphism: linear and bijective, matrix M (n x n) is invertible
    LinearCombination: linear and surjective with matrix M (1 x n)
    AxisLinearCombination: linear combination with specific inverse

An example

    .. code-block:: python

        class StepperPiezo(AxisLinearCombination):
            \"""y(nm) = samy(mm) + sampy(um) + off(nm)
            \"""

            def __init__(self, offset=0):
                coefficients = [1e6, 1e3]
                limits = [[-1, 1], [0, 100]]
                super().__init__(coefficients=coefficients, offset=offset, limits=limits)
"""

from abc import ABC, abstractmethod, abstractproperty
import numpy
import warnings
from daiquiri.core.transform import sets


class TransformError(ValueError):
    pass


class DimensionError(TransformError):
    pass


class Map(ABC):
    """f: D -> C: a==b then f(a)==f(b)"""

    def __init__(self, domain=None, codomain=None, **kwargs):
        """
        :param Set domain:
        :param Set codomain:
        """
        self._domain = domain
        self._codomain = codomain

    @property
    def domain(self):
        """Instance of Set"""
        return self._domain

    @property
    def codomain(self):
        """Instance of Set"""
        return self._codomain

    @property
    def ndomain(self):
        return self.domain.ndim

    @property
    def ncodomain(self):
        return self.codomain.ndim

    @abstractproperty
    def image(self):
        """Subspace of the codomain reached by this map

        :returns Set:
        """
        pass

    def __repr__(self):
        return "{}:{}->{}".format(
            self.__class__.__name__, str(self.domain), str(self.codomain)
        )

    def __call__(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        return self.forward(elements)

    def forward(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        elements = self.domain.as_sequence(elements)
        self.domain.raiseIfNotIsElement(elements)
        return self._forward(elements)

    @abstractmethod
    def _forward(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        pass

    def inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        elements = self.codomain.as_sequence(elements)
        self.image.raiseIfNotIsElement(elements)
        return self._inverse(elements)

    @abstractmethod
    def _inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        pass


class Surjection(Map):
    """f: D -> C: x->f(x) and forall y in C there is
    one (or more) x in R^n for which f(x) == y.

    The inverse is not unique and needs boundary conditions.
    """

    @property
    def image(self):
        """Subspace of the codomain reached by this map.
        For a surjection, image and codomain are identical.

        :returns Set:
        """
        return self.codomain

    def inverse(self, elements, **boundary_conditions):
        """
        :param num or sequence elements:
        :param boundary_conditions: needed to select one solution
        :returns sequence:
        """
        elements = self.codomain.as_sequence(elements)
        self.codomain.raiseIfNotIsElement(elements)
        return self._inverse(elements, **boundary_conditions)

    def _inverse(self, elements, **boundary_conditions):
        """
        :param num or sequence elements:
        param boundary_conditions: needed to select one solution
        :returns sequence:
        """
        return self._right_inverse(elements, **boundary_conditions)

    @abstractmethod
    def _right_inverse(self, elements, **boundary_conditions):
        """
        :param num or sequence elements:
        :param boundary_conditions: needed to select one solution
        :returns sequence:
        """
        pass


class Injection(Map):
    """f: D -> C : x->f(x) and f(a) != f(b)

    The inverse is not defined on the entire codomain.
    """

    def _inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        return self._left_inverse(elements)

    @abstractmethod
    def _left_inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        pass


class Bijection(Surjection, Injection):
    """f: D ⊆ R^n -> D ⊆ R^n: x->f(x) and the inverse exists and is unique"""

    @property
    def image(self):
        return self.codomain

    @abstractmethod
    def _inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        pass

    def _right_inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        return self._inverse(elements)

    def _left_inverse(self, elements):
        """
        :param num or sequence elements:
        :returns sequence:
        """
        return self._inverse(elements)


class LinearMap(Map):
    """This is an abstract class because the inverse transformation
    does not always exist.
    """

    def __init__(self, matrix=None, **kwargs):
        """
        :param array-like matrix: shape (ncodomain, ndomain)
        """
        super().__init__(**kwargs)
        self._matrix = self._parse_matrix(matrix)

    @property
    def matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        return self._matrix

    def _parse_matrix(self, value):
        """
        :param array-like matrix: shape (ncodomain, ndomain)
        :returns array: shape (ncodomain, ndomain)
        """
        value = numpy.atleast_2d(value)
        shape = self.ncodomain, self.ndomain
        if value.shape != shape:
            raise DimensionError(
                "Transformation matrix should have shape {}".format(shape)
            )
        return value

    def _forward(self, coord):
        """
        :param array coord: shape is (npoints, ndomain)
        :returns array: shape is (npoints, ncodomain)
        """
        return self.matrix.dot(coord.T).T


class InvertibleLinearMap(LinearMap):
    """Linear map which has an invertible matrix (not necessarily bijective)"""

    def _parse_matrix(self, value):
        """
        :param array-like matrix: shape (ncodomain, ndomain)
        :returns array: shape (ncodomain, ndomain)
        """
        value = numpy.atleast_2d(value)
        # Raises exception when not invertible
        self.__imatrix = numpy.linalg.inv(value)
        return value

    @property
    def inverse_matrix(self):
        """
        :returns array: shape is (ncodomain, ndomain)
        """
        return self.__imatrix

    def _inverse(self, icoord):
        """
        :param num or array-like icoord: shape is (npoints, ncodomain)
        :returns array-like: shape is (npoints, ndomain)
        """
        return self.inverse_matrix.dot(icoord.T).T


class Isomorphism(InvertibleLinearMap, Bijection):
    """f: D ⊆ R^n -> D ⊆ R^n: x->f(x) and f bijective and linear"""

    pass


class LinearInvertibleInjection(InvertibleLinearMap, Injection):
    """f: D ⊂ R^n -> C ⊂ R^n: x->f(x) and f injective and linear"""

    def _left_inverse(self, icoord):
        """
        :param num or array-like icoord: shape is (npoints, ncodomain)
        :returns array-like: shape is (npoints, ndomain)
        """
        # TODO: This can never have worked
        # return self._inverse(elements)

    @property
    def image(self):
        # TODO: image of linear transformation is not a RealInterval
        warnings.warn(
            "Image is not implemented: use codomain (allows inverse transformation for coordinates outside the image)"
        )
        return self.codomain


class LinearCombination(LinearMap, Surjection):
    """f: D ⊆ R^n -> C ⊆ R: x->c[0]*x[0] + ... + c[-1]*x[-1] + offset"""

    def __init__(self, coefficients=None, offset=0, **kwargs):
        """
        :param array-like coefficients: ndomain
        :param num offset:
        """
        self.offset = offset
        super().__init__(codomain=None, matrix=coefficients, **kwargs)

    @property
    def ncodomain(self):
        return 1

    @property
    def codomain(self):
        if isinstance(self.domain, sets.RealInterval):
            coord = self.domain.border_inlim
            icoord = self.forward(coord)
            limits = [icoord.min(), icoord.max()]
            return sets.RealInterval(ndim=1, limits=limits)
        else:
            warnings.warn(
                "Codomain is not implemented: use R (allows inverse transformation for coordinates outside the image)"
            )
            return sets.RealVectorSpace(ndim=1)

    def _forward(self, coord):
        """
        :param array coord: shape is (npoints, ndomain)
        :returns array: shape is (npoints, ncodomain)
        """
        return super()._forward(coord) + self.offset

    @property
    def coefficients(self):
        """
        :returns array: ndomain
        """
        return self.matrix[0]

    def _right_inverse(self, icoord, **boundary_conditions):
        """
        :param array icoord: shape is (npoints, 1)
        :param ctype: boundary condition type
        :param boundary_conditions: needed to select one solution
        :returns array-like: shape is (npoints, ndomain)
        """
        icoord2 = icoord - self.offset
        if self.ndomain == 1:
            # This is actually a bijection
            return icoord2 / self.coefficients[0]
        else:
            return self._lc_inverse(icoord2, **boundary_conditions)

    @abstractmethod
    def _lc_inverse(self, icoord, **boundary_conditions):
        """Offset is already subtracted

        :param array icoord: shape is (npoints, 1)
        :param ctype: boundary condition type
        :param boundary_conditions: needed to select one solution
        :returns array-like: shape is (npoints, ndomain)
        """
        pass


class AxisLinearCombination(LinearCombination):
    """A linear combination with sets.RealInterval as domain and
    specific boundary conditions for the inverse transformation
    appropriate for parallel axes with different travel ranges.

    For example:
        y(nm) = samy(mm) + sampy(um) + offset(nm)
    """

    def __init__(self, coefficients=None, offset=0, limits=None):
        super().__init__(
            domain=sets.RealInterval(limits=limits),
            coefficients=coefficients,
            offset=offset,
        )

    def codomain_limits(self, axis, reference=None):
        """Axis limits projected on the codomain

        :param num axis:
        :param array reference: shape is (1, ndomain)
        :returns array-like: shape is (2,)
        """
        reference = self._prepare_reference(reference)
        low = self.domain.limits[axis].low
        high = self.domain.limits[axis].high
        pts = numpy.tile(reference, (2, 1))
        pts[:, axis] = [low, high]
        icoord = self.forward(pts)[:, 0]
        icoord.sort()
        return icoord

    def _lc_inverse(self, icoord, ctype="single", **boundary_conditions):
        """Offset is already subtracted

        :param array icoord: shape is (npoints, 1)
        :param ctype: boundary condition type
        :param boundary_conditions: needed to select one solution
        :returns array-like: shape is (npoints, ndomain)
        """
        if ctype == "single":
            return self._inverse_solution_single(icoord, **boundary_conditions)
        else:
            raise ValueError("Unknown boundary condition")

    @property
    def default_reference(self):
        """Default reference position to calculate the inverse"""
        return self.domain.center

    def _prepare_reference(self, reference=None):
        """
        :param array reference: shape is (1, ndomain)
        :returns array-like: shape is (1, ndomain)
        """
        if reference is None:
            reference = self.default_reference
        reference = self.domain.as_sequence(reference).copy()
        # fixed = self.domain.length == 0
        # if fixed.any():
        #    reference[0][fixed] = self.domain.center[fixed]
        return reference

    def _inverse_solution_single(self, icoord, reference=None):
        """Try to reach all points with and axis with the smallest
        possible range. Center axes with a lower range and modify
        axes with higher range when needed.

        :param array icoord: shape is (npoints, 1)
        :param array reference: shape is (1, ndomain)
        :returns array-like: shape is (npoints, ndomain)
        """
        reference = self._prepare_reference(reference=reference)
        # Range in the domain reached by each axis
        ranges = self.domain.length_inlim
        # Range in the domain covered by the points
        coeff = self.coefficients
        pcoord = icoord / coeff
        pranges = pcoord.max(axis=0) - pcoord.min(axis=0)
        # Can all points be reached by one axis?
        inrange = pranges <= ranges
        if not inrange.any():
            raise ValueError("Points are not within the limits of a single motor")
        # Loop over all axes (smallest range first)
        center = self.domain.center
        idx = numpy.argsort(self.domain.length_inlim * coeff)
        for i, axis in enumerate(idx):
            # Center the axis
            reference[0][axis] = center[axis]
            if inrange[axis]:
                # Try to reach all points with one axis at the
                # current reference position
                coord = self._calc_inverse_single_axis(icoord, axis, reference, coeff)
                if coord in self.domain:
                    return coord
                # Try to reach the centroid of all points by moving
                # the reference position. Only move axes with a longer range
                # than the current axis.
                self._calc_inverse_single_point(
                    icoord.mean(), idx[i + 1 :], reference, coeff, center
                )
                # Try to reach all points by moving one axis at the
                # current reference position
                coord = self._calc_inverse_single_axis(icoord, axis, reference, coeff)
                if coord in self.domain:
                    return coord
        raise ValueError("Cannot reach all points with the given boundary conditions")

    def _calc_inverse_single_axis(self, y, j, x, coeff):
        """Solve y = sum_i(ci.xi) by fixing all but xj and calculate xj

        :param array y: shape is (npoints, 1)
        :param num axis:
        :param array x: shape is (1, ndomain)
        :param array coeff: shape is (ndomain, )
        :returns array: shape is (npoints, ndomain)
        """
        x = numpy.tile(x, (y.size, 1))
        yarr = x[0] * coeff
        yarr[j] = 0
        x[:, j] = (y[:, 0] - yarr.sum()) / coeff[j]
        return x

    def _calc_inverse_single_point(self, y, idx, x, coeff, center):
        """Solve y = sum_i(ci.xi) by moving xj with j in `idx`

        :param num y:
        :param array idx:
        :param array x: shape is (1, ndomain)
        :param array coeff: shape is (ndomain, )
        :param array center: shape is (ndomain, )
        """
        # TODO: this is not enough
        for j in idx:
            yarr = x[0] * coeff
            yarr[j] = 0
            x[0, j] = (y - yarr.sum()) / coeff[j]
            if x in self.domain:
                break
            x[0, j] = center[j]
