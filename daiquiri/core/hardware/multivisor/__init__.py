#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import ValidationError, fields, validates_schema, post_load

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.utils import loader
from ._httpclient import Multivisor

import logging

logger = logging.getLogger(__name__)


class MultivisorHOConfigSchema(HOConfigSchema):
    """Define the way Multivisor hardware object config is parsed"""

    address = fields.Str(metadata={"description": "UID from Multivisor"})

    @validates_schema
    def schema_validate(self, data, **kwargs):
        if not (data.get("address") or data.get("url")):
            raise ValidationError(
                "Object must have either an `address` or `url` defined"
            )

    @post_load
    def populate(self, data, **kwargs):
        """Generate the device address from its url"""
        if data.get("url"):
            _, address = data["url"].split("://")
            data["address"] = address
        if "type" not in data:
            data["type"] = "service"

        return data


class MultivisorHandler(ProtocolHandler):
    """Daiquiri implementation of the Multivisor protocol.

    This class define the protocol and hold the access to the Multivisor API.

    To use such protocol, the address of the Multivisor service have to be
    configured the following way.

    .. code-block: yaml

        protocols:
          - id:  multivisor
            type: multivisor
            address: http://localhost:22000
    """

    library = "multivisor"

    def __init__(self, *args, id, address, **kwargs):
        logger.info(
            "Connect Multivisor service (url: %s) as protocol name: '%s'", address, id
        )
        self._multivisor = Multivisor(address)
        ProtocolHandler.__init__(self, *args, **kwargs)

    def disconnect(self):
        self._multivisor.disconnect()

    def get(self, **kwargs):
        try:
            kwargs = MultivisorHOConfigSchema().load(kwargs)
        except ValidationError as err:
            raise InvalidYAML(
                {
                    "message": "Multivisor hardware object definition is invalid",
                    "file": "hardware.yml",
                    "obj": kwargs,
                    "errors": err.messages,
                }
            ) from err

        try:
            return loader(
                "daiquiri.core.hardware.multivisor",
                "",
                kwargs.get("type"),
                app=self._app,
                multivisor=self._multivisor,
                **kwargs,
            )
        except ModuleNotFoundError:
            logger.error(
                "Could not find class for multivisor object %s", kwargs.get("type")
            )
            raise
