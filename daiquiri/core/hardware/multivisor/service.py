#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.service import Service as AbstractService
from daiquiri.core.hardware.multivisor.object import MultivisorObject

import logging

logger = logging.getLogger(__name__)


class Service(MultivisorObject, AbstractService):
    property_map = {
        "state": HardwareProperty("state"),
    }
    callable_map = {"start": "start", "stop": "stop", "restart": "restart"}
