#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import gevent

import tango
from tango.gevent import DeviceProxy

from daiquiri.core.hardware.abstract import MappedHardwareObject, HardwareProperty

import logging

logger = logging.getLogger(__name__)


def is_int_str(v) -> bool:
    try:
        int(v)
    except ValueError:
        return False
    return True


def is_float_str(v) -> bool:
    try:
        float(v)
    except ValueError:
        return False
    return True


class TangoStateProperty(HardwareProperty):
    def translate_from(self, value):
        return value.name

    def translate_to(self, value):
        state = tango.DevState.__members__.get(value)
        if state is None:
            return tango.DevState.UNKNOWN
        return state


class TangoAttrProperty(HardwareProperty):
    def __init__(self, name: str, attrname: callable):
        """
        Attribute
            name: Name of the attribute property
            attrname: Callable returning the attribute name
        """
        HardwareProperty.__init__(self, name=name)
        self._attrname = attrname

    def translate_from(self, value):
        if self.name == "quality":
            return value.name
        if self.name == "data_type":
            return value.name
        if self.name == "display_unit":
            if value is None or value == "No display unit":
                # This values are part of the Tango specification
                return ""
        return value

    def translate_to(self, value):
        return HardwareProperty.translate_from(self, value)

    def get_attrname(self, obj) -> str:
        """Returns the attrname used from a mapping object"""
        return self._attrname(obj)


class TangoObject(MappedHardwareObject):
    _protocol = "tango"

    def __init__(self, app, tango_url, **kwargs):
        self._app = app
        self._subscriptions = {}
        self._tango_url = tango_url
        self._object = DeviceProxy(self._tango_url)
        super().__init__(**kwargs)

        self._watchdog = gevent.spawn(self.try_device)
        self._watchdog.name = f"{__name__}.{tango_url}"

        # TODO: Temporary fix for
        # https://github.com/tango-controls/TangoTickets/issues/34
        time.sleep(1)

    def __repr__(self) -> str:
        return f"<Tango: {self._tango_url} ({self.__class__.__name__})>"

    def _connect(self):
        """Try to connect this daiquiri hardware to the remote tango hardware

        raises:
            tango.ConnectionFailed: Connection have failed
            tango.DevFailed: Another tango thing have failed
            Exception: Another thing have failed
        """
        logger.info(f"Trying to connect to {self._tango_url}")
        self._object.ping()
        self._subscribe_device()
        self.set_online(True)
        try:
            self._broadcast()
        except Exception:
            self.set_online(False)
            raise

    def _disconnect(self):
        """Disconnect this daiquiri hardware from the remote tango hardware"""
        self.set_online(False)
        self._unsubscribe_device()

    def try_device(self):
        """Watch dog trying to reconnect to the remote hardware if it was
        disconnected"""
        while True:
            log_failure = logger.exception if self._app.debug else logger.info
            if not self._online:
                try:
                    self._connect()
                except Exception:
                    log_failure(
                        f"Could not connect to {self._tango_url}. Retrying in 10s"
                    )
            gevent.sleep(10)

    def _broadcast(self):
        """Read the connected hardware properties from the proxy and update this
        daiquiri hardware"""
        for name, prop in self.property_map.items():
            self._update(name, prop, self._do_get(prop))

    def _force_subscribe_event(self, tango_attr_name, event_type, callback):
        """Force polling a tango attribute.

        Trying first to subscribe.

        If it fails, setup the server side events.

        raises:
            tango.DevFailed: If the subscription failed
            RuntimeError: If the event configuration have failed
        """
        obj = self._object
        try:
            return self._object.subscribe_event(
                tango_attr_name, event_type, callback, green_mode=tango.GreenMode.Gevent
            )
        except tango.DevFailed as e:
            if hasattr(e, "reason"):
                if e.reason not in [
                    "API_EventPropertiesNotSet",
                    "API_AttributePollingNotStarted",
                ]:
                    raise
            else:
                raise

        info: tango.AttributeInfoEx = obj.get_attribute_config(tango_attr_name)
        changes = []
        valid_period = is_int_str(info.events.per_event.period)
        if not valid_period:
            changes += ["polling period"]
            info.events.per_event.period = "1000"
        valid_rel_change = is_float_str(info.events.ch_event.rel_change)
        valid_abs_change = is_float_str(info.events.ch_event.abs_change)
        if not valid_abs_change and not valid_rel_change:
            changes += ["rel_change"]
            info.events.ch_event.rel_change = "0.001"

        if changes != []:
            msg = " + ".join(changes)
            logger.info(f"Active {msg} for {self._tango_url} {tango_attr_name}")
            try:
                info.name = tango_attr_name
                obj.set_attribute_config(info)
            except tango.DevFailed:
                raise RuntimeError(
                    f"Failed to configure events {self._tango_url} {tango_attr_name}"
                )

        logger.info(f"Try using event for {self._tango_url} {tango_attr_name}")
        return self._object.subscribe_event(
            tango_attr_name, event_type, callback, green_mode=tango.GreenMode.Gevent
        )

    def _subscribe_device(self):
        """Subscribe events for this hardware"""
        log_failure = logger.exception if self._app.debug else logger.info
        for prop in self.property_map.values():
            if isinstance(prop, TangoAttrProperty):
                # FIXME: This have to be properly implemented
                continue
            try:
                tango_attr_name = prop.name
                event_id = self._force_subscribe_event(
                    prop.name, tango.EventType.CHANGE_EVENT, self._push_event
                )
            except Exception:
                log_failure(
                    f"Could not subscribe to property {self._tango_url} {prop.name}"
                )
            else:
                self._subscriptions[tango_attr_name] = event_id

    def _unsubscribe_device(self):
        """Unsubscribe registred events for this daiquiri hardware"""
        log_failure = logger.exception if self._app.debug else logger.info
        for tango_attr_name, eid in self._subscriptions.items():
            try:
                self._object.unsubscribe_event(eid)
            except Exception:
                log_failure(f"Couldnt unsubscribe from {tango_attr_name}")

        self._subscriptions = {}

    def _push_event(self, event: tango.EventData):
        """Callback triggered when the remote tango hardware fire an event"""
        try:
            self._protected_push_event(event)
        except Exception:
            logger.error("Error while processing push_event", exc_info=True)

    def _protected_push_event(self, event: tango.EventData):
        """Callback triggered when the remote tango hardware fire an event

        Any exceptions raised are catched by `_push_event`.
        """
        log_failure = logger.exception if self._app.debug else logger.info

        if not self._online:
            return

        if event.errors:
            error = event.errors[0]
            log_failure(f"Error in push_event for {event.attr_name}: {error.desc}")
            # if error.reason == 'API_EventTimeout':
            self._disconnect()
            return

        if event.attr_value is not None:
            for name, prop in self.property_map.items():
                if event.attr_value.name == prop.name:
                    self._update(name, prop, event.attr_value.value)
                    break

    def _do_set(self, prop: HardwareProperty, value):
        obj = self._object
        if isinstance(prop, TangoAttrProperty):
            if prop.name != "value":
                raise RuntimeError(
                    f"Setter for '{prop.name}' attribute config is not implemented"
                )
            attrname = prop.get_attrname(self)
            return obj.write_attribute(attrname, value)
        else:
            return obj.write_attribute(prop.name, value)

    def _do_get(self, prop: HardwareProperty):
        obj = self._object
        if isinstance(prop, TangoAttrProperty):
            attrname = prop.get_attrname(self)
            if prop.name == "name":
                return attrname
            if prop.name == "value":
                return obj.read_attribute(attrname).value
            elif prop.name == "data_type":
                return obj.read_attribute(attrname).type
            elif prop.name == "quality":
                return obj.read_attribute(attrname).quality
            elif prop.name == "unit":
                return obj.get_attribute_config(attrname).unit
            elif prop.name == "format":
                return obj.get_attribute_config(attrname).format
            elif prop.name == "display_unit":
                return obj.get_attribute_config(attrname).display_unit
            elif prop.name == "label":
                return obj.get_attribute_config(attrname).label
            elif prop.name == "description":
                return obj.get_attribute_config(attrname).description
            else:
                raise RuntimeError(f"Unsupported {prop.name} attribute")

        getter = prop.getter
        if getter is not None:
            return getter(self)
        return obj.read_attribute(prop.name).value

    def _do_call(self, function, value, **kwargs):
        return self._object.command_inout(function, value, **kwargs)
