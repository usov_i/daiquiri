#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.valve import Valve as AbstractValve
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class ValveStateProperty(HardwareProperty):
    def translate_from(self, value):
        val_map = {
            DevState.OPEN: "OPEN",
            DevState.CLOSE: "CLOSED",
            DevState.UNKNOWN: "UNKNOWN",
            DevState.FAULT: "FAULT",
        }
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Valve(TangoObject, AbstractValve):
    property_map = {
        "state": ValveStateProperty("state"),
        "status": HardwareProperty("status"),
    }

    callable_map = {"open": "open", "close": "close", "reset": "reset"}
