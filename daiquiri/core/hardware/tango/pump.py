#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.pump import Pump as AbstractPump
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class PumpStateProperty(HardwareProperty):
    def translate_from(self, value):
        val_map = {DevState.ON: "ON", DevState.OFF: "OFF", DevState.UNKNOWN: "UNKNOWN"}
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Pump(TangoObject, AbstractPump):

    property_map = {
        "state": PumpStateProperty("state"),
        "status": HardwareProperty("status"),
        "pressure": HardwareProperty("pressure"),
        "voltage": HardwareProperty("voltage"),
        "current": HardwareProperty("current"),
    }

    callable_map = {"on": "on", "off": "off"}
