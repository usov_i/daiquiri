#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.shutter import Shutter as AbstractShutter
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class ShutterStateProperty(HardwareProperty):
    def translate_from(self, value):
        val_map = {
            DevState.CLOSE: "CLOSED",
            DevState.DISABLE: "DISABLED",
            DevState.OPEN: "OPEN",
            DevState.FAULT: "FAULT",
            DevState.UNKNOWN: "UNKNOWN",
            DevState.RUNNING: "OPEN",  # The RUNNING state is not very useful in user point of view
            DevState.STANDBY: "STANDBY",
            DevState.MOVING: "MOVING",
        }
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Shutter(TangoObject, AbstractShutter):
    callable_map = {"open": "Open", "close": "Close", "reset": "Reset"}

    def _get_open_text(self):
        return ""

    def _get_closed_text(self):
        return ""

    def _get_valid(self):
        return True

    property_map = {
        "state": ShutterStateProperty("state"),
        "status": HardwareProperty("status"),
        "open_text": HardwareProperty("open_text", getter=_get_open_text),
        "closed_text": HardwareProperty("closed_text", getter=_get_closed_text),
        "valid": HardwareProperty("valid", getter=_get_valid),
    }
