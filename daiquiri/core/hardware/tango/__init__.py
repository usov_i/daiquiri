#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import ValidationError, fields, validates_schema, post_load

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.utils import loader

import logging

logger = logging.getLogger(__name__)


class TangoHOConfig(HOConfigSchema):
    """The Tango Hardware Object Config Schema"""

    type = fields.Str(required=True, metadata={"description": "The object type"})
    tango_url = fields.Str(metadata={"description": "URL to tango device/attribute"})
    attrname = fields.Str(
        required=False, metadata={"description": "Used by TangoAttr object"}
    )

    @validates_schema
    def schema_validate(self, data, **kwargs):
        if not (data.get("tango_url") or data.get("url")):
            raise ValidationError(
                "Object must have either a `tango_url` or `url` defined"
            )

    @post_load
    def populate(self, data, **kwargs):
        """Maintain backwards compatibility with `tango_url`

        TODO: Remove me
        """
        url = data.get("url")
        if url is not None:
            # Normalize URL to a valid tango URL
            if url.startswith("tango:///"):
                # short schema without host and port
                url = url[len("tango:///") :]
            data["tango_url"] = url

        return data


class TangoHandler(ProtocolHandler):
    library = "tango"

    def get(self, **kwargs):
        # Discover tangoattr using from url
        if "url" in kwargs:
            url = kwargs["url"]

            if "type" not in kwargs or kwargs["type"] == "tangoattr":
                if url.count("/") == 6:
                    url, attrname = url.rsplit("/", 1)
                    kwargs["url"] = url
                    kwargs["attrname"] = attrname
                    if "type" not in kwargs:
                        kwargs["type"] = "tangoattr"

        try:
            kwargs = TangoHOConfig().load(kwargs)
        except ValidationError as err:
            raise InvalidYAML(
                {
                    "message": "Tango hardware object definition is invalid",
                    "file": "hardware.yml",
                    "obj": kwargs,
                    "errors": err.messages,
                }
            ) from err

        try:
            return loader(
                "daiquiri.core.hardware.tango",
                "",
                kwargs.get("type"),
                app=self._app,
                **kwargs
            )
        except ModuleNotFoundError:
            logger.error(
                "Could not find class for tango object {type}".format(
                    type=kwargs.get("type")
                )
            )
            raise
