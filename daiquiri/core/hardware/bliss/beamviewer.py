#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.beamviewer import Beamviewer as AbstractBeamviewer
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Beamviewer(BlissObject, AbstractBeamviewer):
    def _get_state(self):
        return self._object._bpm._cam_proxy.state()

    property_map = {
        "led": HardwareProperty("led_status"),
        "foil": HardwareProperty("foil_status"),
        "screen": HardwareProperty("screen_status"),
        "diode_range": HardwareProperty("diode_range"),
        "diode_ranges": HardwareProperty("diode_range_available"),
        "state": HardwareProperty("state", getter=_get_state),
    }

    def _call_led(self, value, **kwargs):
        if value:
            self._object.led_on()
        else:
            self._object.led_off()

    def _call_foil(self, value, **kwargs):
        if value:
            self._object.foil_in()
        else:
            self._object.foil_out()

    def _call_screen(self, value, **kwargs):
        if value:
            self._object.screen_in()
        else:
            self._object.screen_out()

    def _call_current(self, value, **kwargs):
        return self._object.current
