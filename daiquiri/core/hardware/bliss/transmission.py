#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.transmission import (
    Transmission as AbstractTransmission,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Transmission(BlissObject, AbstractTransmission):
    property_map = {
        "datafile": HardwareProperty("datafile"),
        "transmission_factor": HardwareProperty("get"),
    }

    callable_map = {"get": "get", "set": "set"}
