#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomoconfig import TomoConfig as AbstractTomoConfig
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.objectref import ObjectRefProperty
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class TomoConfig(BlissObject, AbstractTomoConfig):
    property_map = {
        "sx": ObjectRefProperty("x_axis", compose=True),
        "sy": ObjectRefProperty("y_axis", compose=True),
        "sz": ObjectRefProperty("z_axis", compose=True),
        "somega": ObjectRefProperty("rotation_axis", compose=True),
        "sampx": ObjectRefProperty("sample_x_axis", compose=True),
        "sampy": ObjectRefProperty("sample_y_axis", compose=True),
        "sampu": ObjectRefProperty("sample_u_axis", compose=True),
        "sampv": ObjectRefProperty("sample_v_axis", compose=True),
        "detectors": ObjectRefProperty("detectors", compose=True),
        "energy": HardwareProperty("energy"),
    }


Default = TomoConfig
