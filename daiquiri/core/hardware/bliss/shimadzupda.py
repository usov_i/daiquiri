# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.shimadzupda import (
    ShimadzuPDA as AbstractShimadzuPDA,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Shimadzupda(BlissObject, AbstractShimadzuPDA):
    callable_map = {
        "connect_pda": "connect_pda",
        "disconnect_pda": "disconnect_pda",
        "read_wl": "read_wl",
        "read_all": "read_all",
        "start_read": "start_read",
        "stop_read": "stop_read",
    }

    property_map = {
        "data": HardwareProperty("data"),
    }
