#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.laser import Laser as AbstractLaser
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Laser(BlissObject, AbstractLaser):
    property_map = {
        "power": HardwareProperty("power"),
        "state": HardwareProperty("state"),
    }

    callable_map = {"on": "on", "off": "off"}
