#!/usr/bin/env python
# -*- coding: utf-8 -*-

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.tango_attr_as_counter import (
    Tango_Attr_As_Counter as AbstractTango_Attr_As_Counter,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class Tango_Attr_As_Counter(BlissObject, AbstractTango_Attr_As_Counter):
    property_map = {
        "name": HardwareProperty("name"),
        "attribute": HardwareProperty("attribute"),
        "value": HardwareProperty("value"),
        "unit": HardwareProperty("unit"),
    }

    callable_map = {
        "get_metadata": "get_metadata",
    }
