#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bliss.config.static import get_config

import logging

logger = logging.getLogger(__name__)


class BlissSession:
    def __init__(self, name):
        cfg = get_config()
        self._session = cfg.get(name)

        try:
            self._session.setup()
        except Exception:
            logger.exception("Warning: Errors occurred during session initialisation")

        for k, v in self._session.env_dict.items():
            globals()[k] = v
