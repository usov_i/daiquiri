#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.laserheating import (
    Laserheating as AbstractLaserheating,
    LaserheatingStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Laserheating(BlissObject, AbstractLaserheating):
    def _get_state(self):
        return LaserheatingStates[0]

    property_map = {
        "state": HardwareProperty("state", getter=_get_state),
        "exposure_time": HardwareProperty("exposure_time"),
        "background_mode": HardwareProperty("background_mode"),
        "fit_wavelength": HardwareProperty("fit_wavelength"),
        "current_calibration": HardwareProperty("current_calibration"),
    }

    callable_map = {"measure": "measure"}
