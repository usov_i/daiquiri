#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.multiposition import (
    Multiposition as AbstractMultiposition,
    MultipositionStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class PositionsProperty(HardwareProperty):
    def translate_from(self, value):
        positions = []
        for line in value:
            target = [
                {
                    "object": t["axis"].name,
                    "destination": t["destination"],
                    "tolerance": t["tolerance"],
                }
                for t in line["target"]
            ]
            p = {
                "position": line["label"],
                "description": line["description"],
                "target": target,
            }
            positions.append(p)

        return positions


class StateProperty(HardwareProperty):
    def translate_from(self, value):
        for s in MultipositionStates:
            if value == s:
                return s


class Multiposition(BlissObject, AbstractMultiposition):
    property_map = {
        "position": HardwareProperty("position"),
        "positions": PositionsProperty("positions_list"),
        "state": StateProperty("state"),
    }

    callable_map = {"stop": "stop"}

    def _call_move(self, value):
        logger.debug(f"_call_move multiposition {value}")
        self._object.move(value, wait=False)
