#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from bliss.config.static import get_config
from tomo.tomo_detectors import TomoDetectorsState
from daiquiri.core.hardware.abstract.tomodetectors import (
    TomoDetectors as AbstractTomoDetectors,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import ObjectRefProperty
from daiquiri.core.hardware.bliss.object import ObjectRefListProperty
from daiquiri.core.hardware.bliss.object import EnumProperty


logger = logging.getLogger(__name__)


class TomoDetectors(BlissObject, AbstractTomoDetectors):
    property_map = {
        "state": EnumProperty("state", enum_type=TomoDetectorsState),
        "active_detector": ObjectRefProperty("active_detector"),
        "detectors": ObjectRefListProperty("detectors", compose=True),
    }

    # Call moves with wait=False
    def _call_mount(self, value, **kwargs):
        logger.debug(f"_call_mount {self.name()} {value} {kwargs}")

        if value.startswith("hardware:"):
            device_name = value[9:]
        else:
            device_name = value

        config = get_config()
        device = config.get(device_name)
        if device is not None:
            self._object.mount(device, wait=False)


Default = TomoDetectors
