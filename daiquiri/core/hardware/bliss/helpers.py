import logging
import typing
import numpy
import math

from bliss.flint.helper import scan_info_helper

from daiquiri.core.utils import worker

_logger = logging.getLogger(__name__)

try:
    from silx.io.h5py_utils import retry
    from silx.io.h5py_utils import File
except ImportError:
    # For older silx version
    _logger.warning("Fallback to HDF5 reader without retry")

    def retry(retry_timeout):
        def same(f):
            return f

        return same

    from h5py import File


@retry(retry_timeout=2)
def get_data_from_file(
    scan_node_name: str, scan_info, type="scalar"
) -> typing.Dict[str, numpy.ndarray]:
    """Read channel data from HDF5, and referenced by this scan_info"""
    # Load it locally in case there is setup
    from nexus_writer_service.subscribers.devices import device_info
    from nexus_writer_service.subscribers.dataset_proxy import normalize_nexus_name

    types = {
        "scalar": 0,
        "spectrum": 1,
    }

    if type not in types:
        raise KeyError(
            f"Unknown data type {type} requested, available types are {types.keys()}"
        )

    channels = list(scan_info_helper.iter_channels(scan_info))
    channel_names = set(
        [c.name for c in channels if c.info.get("dim", 0) == types[type]]
    )
    scan_nb = scan_info["scan_nb"]

    if "nexus" not in scan_info["data_writer"]:
        raise EnvironmentError("nexuswriter was not enabled for this scan")

    def read_hdf5():
        points = math.inf
        result = {}

        with File(scan_info["filename"]) as h5:
            devices = scan_info["nexuswriter"]["devices"]
            devices = device_info(devices, scan_info)
            for subscan_id, (_subscan, devices) in enumerate(devices.items(), 1):
                for channel_name, device in devices.items():
                    if channel_name not in channel_names:
                        continue
                    grpname = normalize_nexus_name(device["device_name"])
                    dsetname = normalize_nexus_name(device["data_name"])
                    path = f"/{scan_nb}.{subscan_id}/instrument/{grpname}/{dsetname}"
                    try:
                        # Create a memory copy of the data
                        data = h5[path][()]
                    except Exception:
                        _logger.debug("Backtrace", exc_info=True)
                        _logger.info(
                            "Data from channel %s is not reachable", channel_name
                        )
                    else:
                        if len(data) < points:
                            points = len(data)

                        result[channel_name] = data

            if points == math.inf:
                points = 0
        return result, points

    res = worker(read_hdf5)
    return res[0], res[1]
