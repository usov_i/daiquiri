#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.motor import Motor as AbstractMotor, MotorStates
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import CouldBeNotImplementedProperty

import logging

logger = logging.getLogger(__name__)


class LimitsProperty(HardwareProperty):
    def translate_from(self, value):
        if value is None:
            return [None, None]

        value = list(value)
        for i, v in enumerate(value):
            if math.isinf(v):
                if v < 0:
                    value[i] = -999999999
                else:
                    value[i] = 999999999

        return value


class StateProperty(HardwareProperty):
    def translate_from(self, value):
        if value is None:
            return ["UNKNOWN"]

        states = []
        for s in MotorStates:
            if s in value:
                states.append(s)

        if len(states):
            return states

        return ["UNKNOWN"]


class PositionProperty(HardwareProperty):
    def translate_from(self, value):
        if value is None:
            return None

        if math.isnan(value):
            return None

        return round(value, 4)


class NoneIfNotImplementedProperty(CouldBeNotImplementedProperty):
    """Acceleration and velocity can not be exposed in case of a
    CalcController"""

    def notImplementedValue(self):
        """Returned if the property is not implemented in the hardware"""
        return None


class Motor(BlissObject, AbstractMotor):
    property_map = {
        "position": PositionProperty("position"),
        "target": PositionProperty("_set_position"),
        "tolerance": HardwareProperty("tolerance"),
        "acceleration": NoneIfNotImplementedProperty("acceleration"),
        "velocity": NoneIfNotImplementedProperty("velocity"),
        "limits": LimitsProperty("limits"),
        "state": StateProperty("state"),
        "unit": HardwareProperty("unit"),
        "offset": HardwareProperty("offset"),
        "sign": HardwareProperty("sign"),
    }

    callable_map = {"stop": "stop", "wait": "wait_move"}

    # Call moves with wait=False
    def _call_move(self, value, **kwargs):
        logger.debug(f"_call_move {self.name()} {value} {kwargs}")

        current = self.get("position")
        tolerance = self.get("tolerance")

        if abs(value - current) > tolerance:
            self._object.move(value, wait=False)
        else:
            logger.debug(
                f"Demand position ({value}) is within tolerance ({tolerance}) of current ({current})"
            )

    def _call_rmove(self, value, **kwargs):
        logger.debug(f"_call_rmove {self.name()} {value} {kwargs}")
        self._object.move(value, wait=False, relative=True)
