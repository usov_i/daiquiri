#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.objectref import Objectref as AbstractObjectref
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import ObjectRefProperty
import logging

try:
    from tomo.globals import ACTIVE_TOMOCONFIG
except ImportError:
    ACTIVE_TOMOCONFIG = None

logger = logging.getLogger(__name__)


class _ActiveTomoConfigRefProperty(ObjectRefProperty):
    def __init__(self):
        ObjectRefProperty.__init__(self, "ACTIVE_TOMOCONFIG", compose=True)

    @property
    def getter(self):
        return _ActiveTomoConfigRefProperty._get_ref

    def _get_ref(self):
        try:
            active_obj = ACTIVE_TOMOCONFIG.deref_active_object()
        except Exception:
            return None
        if active_obj is None:
            return None
        return active_obj.name


class Activetomoconfig(BlissObject, AbstractObjectref):
    property_map = {"ref": _ActiveTomoConfigRefProperty()}
