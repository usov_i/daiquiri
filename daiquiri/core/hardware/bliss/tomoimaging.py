#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.tomoimaging import (
    TomoImaging as AbstractTomoImaging,
)
from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import EnumProperty
from tomo.tomo_imaging import TomoImagingState

import logging

logger = logging.getLogger(__name__)


class TomoImaging(BlissObject, AbstractTomoImaging):
    property_map = {
        "state": EnumProperty("state", enum_type=TomoImagingState),
        "update_on_move": HardwareProperty("update_on_move"),
        "exposure_time": HardwareProperty("exposure_time"),
        "stabilization_time": HardwareProperty("sleep_time"),
    }

    callable_map = {
        "take_proj": "take_proj",
        "take_dark": "take_dark",
        "take_flat": "take_flat",
    }


Default = TomoImaging
