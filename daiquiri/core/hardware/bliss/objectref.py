#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.objectref import Objectref as AbstractObjectref
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.bliss.object import ObjectRefProperty

import logging

logger = logging.getLogger(__name__)


class Objectref(BlissObject, AbstractObjectref):
    property_map = {"ref": ObjectRefProperty("ref")}
