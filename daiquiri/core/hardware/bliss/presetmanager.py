#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract.presetmanager import (
    Presetmanager as AbstractPresetmanager,
    PresetmanagerStates,
)
from daiquiri.core.hardware.bliss.object import BlissObject
from daiquiri.core.hardware.abstract import HardwareProperty

import logging

logger = logging.getLogger(__name__)


class Presetmanager(BlissObject, AbstractPresetmanager):
    def _get_state(self):
        return PresetmanagerStates[0]

    property_map = {
        "presets": HardwareProperty("presets"),
        "state": HardwareProperty("state", getter=_get_state),
    }

    callable_map = {"apply": "apply"}
