#!/usr/bin/env python
# -*- coding: utf-8 -*-
from io import BytesIO
from typing import Optional

import requests
from requests.auth import HTTPDigestAuth
from PIL import Image, UnidentifiedImageError
import numpy

from daiquiri.core.hardware.abstract import HardwareProperty
from daiquiri.core.hardware.abstract.camera import Camera as AbstractCamera
from daiquiri.core.hardware.daiquiri.object import DaiquiriObject

import logging

logger = logging.getLogger(__name__)


# Loader only uppercases first letter to create classname, so cant use proper camel casing here
# TODO: Make loader more explicit
class Httpcamera(DaiquiriObject, AbstractCamera):
    """A camera implementation that returns frames from a url

    ```yaml
      - id: jpegcamera
        protocol: daiquiri
        type: httpcamera
        camera_url: http://mycamera/jpg/image.jpg
        # optionally
        username: user
        password: pass
    ```

    """

    property_map = {
        "state": HardwareProperty("acq_status"),
        "live": HardwareProperty("video_live"),
        "height": HardwareProperty("image_height"),
        "width": HardwareProperty("image_width"),
        "exposure": HardwareProperty("video_exposure"),
        "gain": HardwareProperty("video_gain"),
        "mode": HardwareProperty("video_mode"),
    }

    acq_status = "RUNNING"
    video_live = True
    video_exposure = 1
    video_gain = 1
    video_mode = "RGB24"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._auth = None
        if "username" in self._config and "password" in self._config:
            self._auth = HTTPDigestAuth(
                self._config["username"], self._config["password"]
            )

    @property
    def image_width(self) -> Optional[int]:
        image = self._get_frame()
        if image:
            return image.size[0]

    @property
    def image_height(self) -> Optional[int]:
        image = self._get_frame()
        if image:
            return image.size[1]

    def _get_frame(self) -> Optional[Image.Image]:
        response = requests.get(self._config["camera_url"], auth=self._auth, timeout=10)
        if response.status_code == 200:
            try:
                image = Image.open(BytesIO(response.content))
                if image.mode != "RGB":
                    image = image.convert("RGB")
                return image
            except UnidentifiedImageError:
                logger.error("Could not parse image with PIL")
        else:
            logger.warning(
                f"{self._config['camera_url']} returned {response.status_code}"
            )

    def frame(self) -> Optional[numpy.ndarray]:
        image = self._get_frame()
        if image:
            return numpy.array(image)
