#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from typing import Callable

from daiquiri.core.hardware.abstract import (
    MappedHardwareObject,
    HardwareProperty,
)
from daiquiri.core.utils import get_nested_attr


logger = logging.getLogger(__name__)


class DaiquiriObject(MappedHardwareObject):
    """The daiquiri hardware object

    This is simple object that just maps properties onto the hardware
    objects properties.

    It allows arbitrary config options to be passed and interpreted by the
    targeted hardware object.
    """

    _protocol = "daiquiri"
    _online = True

    def __init__(self, *args, **kwargs):
        self._config = kwargs
        super().__init__(*args, **kwargs)

    def __repr__(self) -> str:
        return f"<Daiquiri: {self.name()} ({self.__class__.__name__}/{self._object.__class__.__name__})>"

    def _do_set(self, prop: HardwareProperty, value):
        return setattr(self, prop.name, value)

    def _do_get(self, prop: HardwareProperty):
        getter = prop.getter
        if getter is not None:
            return getter(self)
        try:
            return get_nested_attr(self, prop.name)
        except RuntimeError:
            logger.info(
                f"Could not get property {prop.name} from {self.name()}", exc_info=True
            )
            return None

    def _do_call(self, function: Callable, value, **kwargs):
        fn = getattr(self, self.callable_map[function])
        if value is None:
            return fn(**kwargs)
        else:
            return fn(value, **kwargs)
