#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import traceback
from marshmallow import ValidationError, INCLUDE

from daiquiri.core import CoreBase
from daiquiri.core.schema.hardware import HOConfigSchema, HardwareGroupSchema
from daiquiri.core.utils import loader
from daiquiri.resources.utils import ConfigDict
from daiquiri.core.exceptions import InvalidYAML

import logging
from daiquiri.core.hardware.abstract import HardwareObject

logger = logging.getLogger(__name__)


class Hardware(CoreBase):
    """Core Hardware Control

    This initialises all hardware defined in hardware.yml, first loading the protocol
    handlers, then initialising each object with the relevant handler. A list of groups
    is built from the yaml, as well as a list of object types along with their schema

    """

    _blueprint = False

    def __init__(self, *args, **kwargs):
        self._handlers = {}
        self._objects = []
        self._types = {}
        self._groups = []
        CoreBase.__init__(self, *args, **kwargs)

    def setup(self):
        yml = "hardware.yml"
        try:
            self._config = ConfigDict(yml)
        except (IOError, RuntimeError):
            logger.warning(
                "Configuration file missing, hardware component not configured"
            )
        else:
            self.read_config(self._config, yml)

    def read_config(self, config, filename=None):
        self._handlers["daiquiri"] = loader(
            "daiquiri.core.hardware",
            "Handler",
            "daiquiri",
            app=self._app,
        )

        for h in config["protocols"]:
            logger.debug("Loading handler {hand}".format(hand=h))
            self._handlers[h["id"]] = loader(
                "daiquiri.core.hardware", "Handler", h["type"], app=self._app, **h
            )

            library = self._handlers[h["id"]].library
            if library and library not in self._base_config["versions"]:
                self._base_config["versions"].append(library)

        hoschema = HOConfigSchema()
        ids = []
        subconfigs = []
        for obj in config["objects"]:
            try:
                obj = hoschema.load(obj, unknown=INCLUDE)
            except ValidationError as err:
                raise InvalidYAML(
                    {
                        "message": "Hardware object definition is invalid",
                        "file": filename,
                        "obj": obj,
                        "errors": err.messages,
                    }
                )

            if obj["protocol"] in self._handlers:
                if obj["id"] in ids:
                    auto_message = (
                        obj.get("auto_id", "")
                        and " This id was generated automatically from its `url`, please specify an `id` property."
                    )
                    raise InvalidYAML(
                        {
                            "message": f"Hardware object id: {obj['id']} is not unique."
                            + auto_message,
                            "file": filename,
                            "obj": obj,
                        }
                    )
                ids.append(obj["id"])

                if self.get_object(obj["id"]):
                    continue

                o = self.register_object_from_config(obj, register_sub_objects=False)
                # Store the sub object configs to load them later
                subconfigs.extend(o.get_subobject_configs())
            else:
                raise Exception(
                    f'No protocol handler {obj["protocol"]} for {obj["name"]}'
                )

        # Load the sub object configs
        for config in subconfigs:
            self.register_object_from_config(config)

        hgschema = HardwareGroupSchema()
        groups = config.get("groups", None)
        self._groups = list(groups) if groups else []

        for g in self._groups:
            try:
                hgschema.load(g, unknown=INCLUDE)
            except ValidationError as err:
                raise InvalidYAML(
                    {
                        "message": "Hardware group definition is invalid",
                        "file": filename,
                        "obj": g,
                        "errors": err.messages,
                    }
                )

            for oid in g["objects"]:
                obj = self.get_object(oid)
                if obj is None:
                    raise InvalidYAML(
                        {
                            "message": f'Could not find object: {oid} for group: {g["name"]}',
                            "file": filename,
                            "obj": g,
                        }
                    )

    def register_object(self, obj: HardwareObject, register_sub_objects=True):
        """Register a new object to this hardware repository

        Attributes:
            obj: Object to register
            register_sub_objects: If true (default), sub components are loaded
                                  and registered
        """
        logger.info(f"Register {obj._protocol}://{obj.id()}")
        self._objects.append(obj)
        if not (obj.type() in self._types):
            self._types[obj.type()] = obj.schema().__class__.__name__
        self._schema.register(obj.schema())

        if register_sub_objects:
            subconfigs = obj.get_subobject_configs()
            for config in subconfigs:
                self.register_object_from_config(config)

    def register_object_from_config(self, config: dict, register_sub_objects=True):
        """
        Register an object from its config.

        If the object id was already registered, the object is skipped.

        It have to contains a `protocol` and an `id` key.

        Attributes:
            config: Object to register
            register_sub_objects: If true (default), sub components are loaded
                                  and registered
        """
        objectid = config["id"]
        obj = self.get_object(objectid)
        if obj:
            logger.debug(
                "Object id %s already registered. COnfiguration skipped", objectid
            )
            return

        obj = self._handlers[config["protocol"]].get(**config)
        self.register_object(obj, register_sub_objects=register_sub_objects)
        return obj

    def remove_object(self, obj: HardwareObject):
        """Remove an object from this hardware repository"""
        logger.info(f"Removing {obj.id()}")
        self._objects.remove(obj)

    def reload(self):
        self.setup()
        ids = [o["id"] for o in self._config["objects"]]
        removed = [o for o in self.get_objects() if o.id() not in ids]
        for obj in removed:
            self.remove_object(obj)

    def disconnect(self):
        for h in self._handlers.values():
            h.disconnect()

    def get_objects(self, *args, **kwargs):
        """Get a list of hardware objects

        Kwargs:
            type (str): Filter the list of objects by a type (i.e. motor)
            group (int): Filter the objects by a group id

        Returns:
            A list of objects
        """
        objs = self._objects

        ty = kwargs.get("type", None)
        if ty:
            objs = filter(lambda obj: obj.type() == ty, objs)

        group = kwargs.get("group", None)
        if group:
            group = self.get_group(group)
            objs = filter(lambda obj: obj.id() in group["objects"], objs)

        return objs

    def get_object(self, objectid):
        """Get a specific object

        Args:
            objectid (str): The object id

        Returns
            The object
        """
        for o in self._objects:
            if o.id() == objectid:
                return o
        return None

    def get_types(self):
        """Return the dict of object types loaded and their schema"""
        return self._types

    def get_groups(self, groupid=None, objectid=None):
        """Get the list of groups loaded"""
        groups = self._groups
        ret = []
        for g in groups:
            if groupid:
                if groupid == g["groupid"]:
                    return g

            if objectid:
                if objectid in g["objects"]:
                    ret.append(g)
            else:
                ret.append(g)

        logger.debug(
            "get_groups groupid=%s, objectid=%s to_update=%s", groupid, objectid, ret
        )
        # logger.warning("Call stack:\n%s", "".join(traceback.format_stack()))
        for r in ret:
            r["state"] = True
            for o in r["objects"]:
                obj = self.get_object(o)
                r["state"] = r["state"] and obj.online() and obj.state_ok()

        return ret

    def get_group(self, groupid):
        """Get a specific group

        Args:
            groupid (int): The groupid to return details for

        Returns:
            The group for the groupid
        """
        for g in self._groups:
            if g["groupid"] == groupid:
                return g
