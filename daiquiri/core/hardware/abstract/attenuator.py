#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

AttenuatorStates = ["ON", "ERROR"]


class AttenuatorPropertiesSchema(HardwareSchema):
    state = OneOf(AttenuatorStates, metadata={"readOnly": True})
    factor = fields.Int(metadata={"readOnly": True})
    thickness = fields.Float(metadata={"readOnly": True})


class Attenuator(HardwareObject):
    _type = "attenuator"
    _state_ok = [AttenuatorStates[0]]
    _properties = AttenuatorPropertiesSchema()
