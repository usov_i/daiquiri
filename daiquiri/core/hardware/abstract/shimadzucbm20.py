# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)


class VolumeParameters(Schema):
    volume = fields.Float()
    rate = fields.Float()


class PressureParameters(Schema):
    pmin = fields.Float()
    pmax = fields.Float()


class VialParameters(Schema):
    vial = fields.Float()
    vol = fields.Float()
    runtime = fields.Int()


class ShimadzuCBM20PumpSchema(Schema):
    on = fields.Boolean()
    flow_rate = fields.Float()
    min_pressure = fields.Float()
    max_pressure = fields.Float()
    port = fields.String()


class ShimadzuCBM20AutoSamplerSchema(Schema):
    on = fields.Boolean()
    temperature = fields.Float()
    temperature_setpoint = fields.Float()


class ShimadzuCBM20AutoPurgeSchema(Schema):
    on = fields.Boolean()


class ShimadzuCBM20StateSchema(Schema):
    pump = fields.Nested(ShimadzuCBM20PumpSchema, metadata={"readOnly": True})
    auto_purge = fields.Nested(
        ShimadzuCBM20AutoPurgeSchema, metadata={"readOnly": True}
    )
    auto_sampler = fields.Nested(
        ShimadzuCBM20AutoSamplerSchema, metadata={"readOnly": True}
    )
    inject_from_vial = fields.Nested(
        VialParameters, metadata={"readOnly": True, "many": False}
    )
    system_state = fields.Int(metadata={"readOnly": True})


class ShimadzuCBM20CallablesSchema(HardwareSchema):
    connect_cbm20 = RequireEmpty(metadata={"readOnly": True})
    disconnect_cbm20 = RequireEmpty(metadata={"readOnly": True})
    start_pump = RequireEmpty(metadata={"readOnly": True})
    stop_pump = RequireEmpty(metadata={"readOnly": True})
    set_pump_max_pressure = fields.Float(metadata={"readOnly": True})
    set_pump_min_pressure = fields.Float(metadata={"readOnly": True})
    set_pump_flow = fields.Float(metadata={"readOnly": True})
    set_pump_flow_threshold = fields.Float(metadata={"readOnly": True})

    set_flow_mode = fields.String(metadata={"readOnly": True})
    select_solenoid_valve = fields.String(metadata={"readOnly": True})

    start_auto_purge = RequireEmpty(metadata={"readOnly": True})
    stop_auto_purge = RequireEmpty(metadata={"readOnly": True})
    inject_from_vial = fields.Nested(
        VialParameters, metadata={"readOnly": True, "many": False}
    )
    stop_inject = RequireEmpty(metadata={"readOnly": True})
    set_auto_sampler_temp = fields.Float(metadata={"readOnly": True})
    pump_from_port = fields.String(metadata={"readOnly": True})
    enable_auto_sampler = RequireEmpty(metadata={"readOnly": True})
    disable_auto_sampler = RequireEmpty(metadata={"readOnly": True})


class DataValueSchema(Schema):
    xdata = fields.List(fields.Float(), metadata={"many": True})
    ydata = fields.List(fields.Float(), metadata={"many": True})


class ShimadzuCBM20DataSchema(Schema):
    channels = fields.List(fields.String(), metadata={"many": True})
    pressure = fields.Nested(DataValueSchema, metadata={"many": False})
    flow_rate = fields.Nested(DataValueSchema, metadata={"many": False})


class ShimadzuCBM20PropertiesSchema(HardwareSchema):
    state = fields.Nested(ShimadzuCBM20StateSchema, metadata={"readOnly": True})
    data = fields.Nested(ShimadzuCBM20DataSchema, metadata={"readOnly": True})


class ShimadzuCBM20(HardwareObject):
    _type = "shimadzucbm20"

    _callables = ShimadzuCBM20CallablesSchema()
    _properties = ShimadzuCBM20PropertiesSchema()
