#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

ValveStates = ["OPEN", "CLOSED", "UNKNOWN", "FAULT"]


class ValvePropertiesSchema(HardwareSchema):
    state = OneOf(ValveStates, metadata={"readOnly": True})
    status = fields.Str(metadata={"readOnly": True})


class ValveCallablesSchema(HardwareSchema):
    open = RequireEmpty()
    close = RequireEmpty()
    reset = RequireEmpty()


class Valve(HardwareObject):
    _type = "valve"
    _state_ok = [ValveStates[0], ValveStates[1]]

    _properties = ValvePropertiesSchema()
    _callables = ValveCallablesSchema()
