#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

TangoDeviceStates = [
    "ON",
    "OFF",
    "CLOSE",
    "OPEN",
    "INSERT",
    "EXTRACT",
    "MOVING",
    "STANDBY",
    "FAULT",
    "INIT",
    "RUNNING",
    "ALARM",
    "DISABLE",
    "UNKNOWN",
]

TangoQualities = [
    "VALID",
    "INVALID",
    "ALARM",
    "CHANGING",
    "WARNING",
]

TangoDataTypes = [
    "DevVoid",
    "DevBoolean",
    "DevShort",
    "DevLong",
    "DevLong64",
    "DevUChar",
    "DevUShort",
    "DevULong",
    "DevULong64",
    "DevFloat",
    "DevDouble",
    "DevString",
    "DevVarBooleanArray",
    "DevVarDoubleArray",
    "DevVarFloatArray",
    "DevVarShortArray",
    "DevVarLongArray",
    "DevVarLong64Array",
    "DevVarCharArray",
    "DevVarStringArray",
    "DevVarUShortArray",
    "DevVarULongArray",
    "DevVarULong64Array",
    "DevEncoded",
    "DevVarEncodedArray",
    "DevVarLongStringArray",
    "DevVarDoubleStringArray",
    "DevState",
    "DevVarStateArray",
    "DevEnum",
    "DevPipeBlob",
    "DevFailed",
]


class TangoAttrPropertiesSchema(HardwareSchema):
    # Device state

    state = OneOf(TangoDeviceStates)
    status = fields.Str(metadata={"readOnly": True})

    # Attribute properties

    name = fields.Str(metadata={"readOnly": True})
    value = fields.Raw()
    # read_value
    # read_dim_x
    # read_dim_y
    # set_value
    # write_dim_x
    # write_dim_y
    quality = OneOf(TangoQualities)
    data_type = OneOf(TangoDataTypes)
    # data_format

    # Attribute config

    description = fields.Str()
    label = fields.Str()
    unit = fields.Str()
    # standard_unit = fields.Str()
    display_unit = fields.Str()
    format = fields.Str()
    # min_value = fields.Number()
    # max_value = fields.Number()
    # min_alarm = fields.Number()
    # max_alarm = fields.Number()
    # min_warning = fields.Number()
    # max_warning = fields.Number()
    # delta_val = fields.Number()
    # delta_t = fields.Number()
    # rel_change = fields.Number()
    # abs_change = fields.Number()
    # archive_rel_change = fields.Number()
    # archive_abs_change = fields.Number()
    # period = fields.Number()
    # archive_period = fields.Number()
    # writable
    # display_level


class TangoAttr(HardwareObject):
    _type = "tangoattr"
    _state_ok = [
        "ON",
        "RUNNING",
        "STANDBY",
        "MOVING",
        "CLOSE",
        "OPEN",
        "INSERT",
        "EXTRACT",
    ]

    _properties = TangoAttrPropertiesSchema()
