# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty

import logging

logger = logging.getLogger(__name__)


class PDAValueUnitSchema(Schema):
    values = fields.Float(metadata={"readOnly": True})
    unit = fields.String(metadata={"readOnly": True})


# class PDAValuesSchema(Schema):
#     channel = fields.String(metadata={"readOnly": True})
#     wavelength = fields.Nested(PDAValueUnitSchema, many=False, metadata={"readOnly": True})
#     absorbance = fields.Nested(PDAValueUnitSchema, many=False, metadata={"readOnly": True})
#     bdanwidth = fields.Nested(PDAValueUnitSchema, many=False, metadata={"readOnly": True})
#     range = fields.Float(metadata={"readOnly": True})
#     polarity = fields.String(metadata={"readOnly": True})


class PDADataSchema(Schema):
    xdata = fields.List(fields.Float(), metadata={"many": True})
    ydata = fields.List(fields.Float(), metadata={"many": True})


class PDAValuesSchema(Schema):
    channels = fields.List(fields.String(), metadata={"many": True})
    w0 = fields.Nested(PDADataSchema, many=False)
    w1 = fields.Nested(PDADataSchema, many=False)
    w2 = fields.Nested(PDADataSchema, many=False)
    w3 = fields.Nested(PDADataSchema, many=False)


class ShimadzuPDACallablesSchema(HardwareSchema):
    connect_pda = RequireEmpty(metadata={"readOnly": True})
    disconnect_pda = RequireEmpty(metadata={"readOnly": True})
    read_wl = RequireEmpty(metadata={"readOnly": True})
    read_all = RequireEmpty(metadata={"readOnly": True})
    start_read = RequireEmpty(metadata={"readOnly": True})
    stop_read = RequireEmpty(metadata={"readOnly": True})


class ShimadzuPDAPropertiesSchema(HardwareSchema):
    data = fields.Nested(PDAValuesSchema, metadata={"readOnly": True})


class ShimadzuPDA(HardwareObject):
    _type = "shimadzupda"

    _properties = ShimadzuPDAPropertiesSchema()
    _callables = ShimadzuPDACallablesSchema()
