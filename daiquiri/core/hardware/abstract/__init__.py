#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from marshmallow import fields
import traceback
import typing

from daiquiri.core.schema.hardware import HardwareSchema, HardwareObjectBaseSchema


import logging

logger = logging.getLogger(__name__)


class ProtocolHandler:
    """A protocol handler instantiates a hardware object from a specific protocol

    i.e. initialise a motor object from bliss, or a shutter from tango
    """

    library = None

    def __init__(self, *args, **kwargs):
        self._app = kwargs.get("app")

    def disconnect(self):
        """Called at the termination of the handler.

        Implement it if the protocol hold resources.
        """
        pass

    @abstractmethod
    def get(self, *args, **kwargs):
        """Get the specific object from the protocol handler.

        This function checks that kwargs conforms to Schema defined for the
        protocol handler (see core/hardware/bliss/__init__.py for a concrete example)

        Returns:
            The hardware object instance for the specific protocol

        """
        pass


class HardwareProperty:
    """Describe a property from the device controls system.

    Provides translation function between the controls system specific
    nomenclature and the abstract one. This can be overwritten.

    Arguments:
        name: Name used by the controls system
    """

    def __init__(self, name: str, getter: callable = None):
        self._name = name
        self._getter = getter

    @property
    def name(self) -> str:
        return self._name

    @property
    def getter(self) -> typing.Optional[callable]:
        return self._getter

    def translate_from(self, value):
        """Translate the value from the controls layer to the abstraction layer
        i.e for getters

        Arguments:
            value: The property value to translate.

        Returns:
            The translated value
        """
        return value

    def translate_to(self, value):
        """Translate the value to the controls layer from the abstraction layer
        i.e for setters

        Arguments:
            value: The property value to translate.

        Returns:
            The translated value
        """
        return value


class HardwareObject(ABC):
    """Base HardwareObject from which all inherit

    The base hardware object defines the objects procotol, type, its properties
    and callables schema, and mechanisms to subscribe to property changes

    Attributes:
        _object (obj): The instance of the control system object.
        _protocol (str): The protocol for this object, i.e. bliss
        _type (str): The object type, i.e. motor, shutter

    """

    _object = None
    _online = False
    _state_ok = []

    _protocol = None
    _type = None

    _properties = HardwareSchema()
    _callables = HardwareSchema()

    def schema_name(self):
        ty = self._type
        return ty[0].upper() + ty[1:]

    def __init__(self, *args, **kwargs):
        self._callbacks = {}
        self._online_callbacks = []

        self._id = kwargs.get("id", None)
        self._name = kwargs.get("name", "")
        self._require_staff = kwargs.get("require_staff", False)
        self._alias = None

    def get(self, prop: str):
        """Get a property from a hardware object

        First checks the property is defined in the objects
        property schema, then delegates to the local getter
        implementation _get

        Arguments:
            prop: The property to retreive.

        Returns:
            The property value if the property exists, otherwise
            rasises an exception

        """
        if not self._online:
            return

        if prop in self._properties:
            value = self._get(prop)
            return value
        else:
            raise KeyError("Unknown property {p}".format(p=prop))

    def set(self, prop: str, value):
        """Set a property on a hardware object

        First checks the property is defined in the objects
        property schema, then delegates to the local setter
        implementation _set

        Arguments:
            prop: The property to set.
            value: The property to set.

        Returns:
            The the result from the object setter if the property exists
            otherwise raises an exception

        """
        if not self._online:
            return

        if prop in self._properties:
            if self._properties.read_only(prop):
                raise AttributeError("Property {p} is read only".format(p=prop))

            value = self._properties.validate(prop, value)
            return self._set(prop, value)
        else:
            raise KeyError("Unknown property {p}".format(p=prop))

    def get_subobject_configs(self):
        """Returns a list of referenced objects own by this object."""
        return []

    def call(self, function, value, **kwargs):
        """Calls a function on a hardware object

        First checks the function is defined in the objects
        callables schema, then delegates to the local call
        implementation _call

        Args:
            function (str): The function to call.
            value: The value to call the function with.

        Returns:
            The the result from the object function if the function exists
            otherwise rasises an exception

        """
        if not self._online:
            return

        if function in self._callables:
            value = self._callables.validate(function, value)
            # try:
            ret = self._call(function, value, **kwargs)
            if ret:
                return ret
            else:
                return True
            # except Exception as e:
            #     return e
        else:
            raise KeyError("Unknown function {fn}".format(fn=function))

    @abstractmethod
    def _get(self, prop: str):
        """Local implementation of getter"""
        pass

    @abstractmethod
    def _set(self, prop: str, value):
        """Local implementation of setter"""
        pass

    @abstractmethod
    def _call(self, function, value):
        """Local implementation of call"""
        pass

    def state(self, **kwargs):
        """Gets the current state of a hardware object

        Builds a dictionary of the basic info of the object, plus its properties, and
        callables.

        Returns:
            A dict of the hardware object status

        """
        info = {
            k: getattr(self, "_" + k)
            for k in ["name", "type", "id", "protocol", "online"]
        }
        info["callables"] = [k for k in self._callables]
        info["errors"] = []
        properties = {}
        for p in self._properties:
            try:
                properties[p] = self.get(p)
            except Exception as e:
                properties[p] = None
                info["online"] = False
                info["errors"].append(
                    {
                        "property": p,
                        "exception": str(e),
                        "traceback": "".join(traceback.format_tb(e.__traceback__)),
                    }
                )
                logger.exception(f"Couldn't get property `{p}` for `{self.name()}`")

        try:
            info["properties"] = self._properties.dump(properties)
        except Exception:
            logger.error(
                "Error while serializing %s (%s)",
                self._name,
                properties,
                exc_info=True,
            )
            info["properties"] = {}
            info["online"] = False
        info["require_staff"] = self.require_staff()
        info["alias"] = self.alias()

        return info

    def schema(self):
        """Returns the schema for the current hardware object

        The hardware schema is built from the `HardwareObjectBaseSchema`
        and the object specific _property and _callable schema
        (both instances of `HardwareSchema`)

        Returns:
            An instance of a schema

        """
        schema = type(
            f"HW{self.schema_name()}Schema",
            (HardwareObjectBaseSchema,),
            {
                "properties": fields.Nested(self._properties.__class__),
                "callables": fields.Nested(self._callables.__class__),
            },
        )
        return schema()

    def set_online(self, state):
        """Set the online state of the device

        Sets the state and execute any registered callbacks

        Args:
            state (boolean): Set the online state
        """
        self._online = state

        for cb in self._online_callbacks:
            cb(self, self._online)

    def subscribe_online(self, fn):
        """Subscribe to the online state of the hardware object

        Add a function to a list of callbacks for when the online state of the object change

        Args:
            fn: (:callable) The function to call when this property changes.

        """
        if not callable(fn):
            raise AttributeError("Callback function must be callable")

        if not (fn in self._online_callbacks):
            self._online_callbacks.append(fn)

    def id(self):
        return self._id

    def name(self):
        return self._name

    def type(self):
        return self._type

    def object(self):
        return self._object

    def online(self):
        return self._online

    def alias(self) -> typing.Optional[str]:
        return self._alias

    def require_staff(self):
        return self._require_staff

    def state_ok(self):
        """Returns if the current object state is `ok`"""
        state = self.get("state")
        if isinstance(state, list):
            st = False
            for ok in self._state_ok:
                if ok in state:
                    st = True
            return st

        else:
            return state in self._state_ok

    def subscribe(self, prop: str, fn):
        """Subscribe to property changes on the hardware object

        Add a function to a list of callbacks for when properties on the object change

        Arguments:
            prop: The property to subscribe to. Can pass 'all' to subscribe to all changes
            fn: (:callable) The function to call when this property changes.

        """
        if not callable(fn):
            raise AttributeError("Callback function must be callable")

        if prop in self._properties or prop == "all":
            if not (prop in self._callbacks):
                self._callbacks[prop] = []

            if not (fn in self._callbacks[prop]):
                self._callbacks[prop].append(fn)

        else:
            raise KeyError(f"No such property: {prop}")

    def unsubscribe(self, prop: str, fn):
        """Unsubscribe from a property change on the hardware object

        Arguments:
            prop: The property to unsubscribe from.
            fn: (:callable) The function to unsubscribe

        """
        if prop in self._callbacks:
            if fn in self._callbacks[prop]:
                # logger.debug("Unsubscribe from property %s, %s", prop, fn)
                self._callbacks[prop].remove(fn)
                return True

        return False

    def _update(self, name: str, prop: HardwareProperty, value):
        """Internal function to call when a property has changed

        This delegates to all subscribes that a property has changed

        Arguments:
            name: Property name of the abstract device
            prop: Property of the control system hardware
            value: The new value (from the control system).

        """
        # logger.debug('{c}._update {n} - {p}: {v}'.format(c=self.__class__.__name__, n=self._address, p=prop, v=value))
        value = prop.translate_from(value)
        if name in self._properties:
            if name in self._callbacks:
                for cb in self._callbacks[name]:
                    cb(self, name, value)

            if "all" in self._callbacks:
                for cb in self._callbacks["all"]:
                    cb(self, name, value)


class MappedHardwareObject(HardwareObject):
    """Hardware object that maps properties via a simple dict

    HardwareObject that has a simple map between abstract properties and their
    actual properties on the object with fallback to a function on the parent
    """

    property_map: typing.Dict[str, HardwareProperty] = {}
    callable_map: typing.Dict[str, str] = {}

    def _set(self, prop: str, value):
        """Set a property on the child object

        First try from the simple property map which maps properties to attributes
        on the child object. Delegates to _do_set which locally implements the setter

        Second, if not in the map, try calling the function _get_<prop> on the parent

        Args:
            prop: The property to set.
            value: Its value.
        """
        hprop = self.property_map.get(prop)
        if hprop is not None:
            value = value = hprop.translate_to(value)
            self._do_set(hprop, value)
        else:
            raise KeyError(
                f"Couldnt find a setter for property `{prop}` on `{self.name()}`"
            )

    @abstractmethod
    def _do_set(self, prop, value):
        """Local implementation of how to set the property on the object"""
        pass

    def _get(self, prop: str):
        """Get a property from the child object

        First try from the simple property map which maps properties to attributes
        on the child object. Delegates to _do_get which locally implements the getter

        Second, if not in the map, try calling the function _get_<prop> on the parent

        Arguments:
            prop: The property to set.

        Returns:
            The property value
        """
        hprop = self.property_map.get(prop)
        if hprop is not None:
            try:
                hvalue = self._do_get(hprop)
                value = hprop.translate_from(hvalue)
                return value
            except NotImplementedError:
                logger.info("Object %s does not implement %s", self._id, prop)
        else:
            raise KeyError(
                f"Couldnt find a getter for property `{prop}` on `{self.name()}`"
            )

    @abstractmethod
    def _do_get(self, prop):
        """Local implementation of how to get the property from the object"""
        pass

    def _call(self, function, value, **kwargs):
        """Call a function on the child object

        First try from the simple function map which maps to function names
        on the child object. Delegates to _do_call which locally implements the getter

        Second, if not in the map, try calling the function _call_<fn> on the parent

        Args:
            function (str): The function to call.
            value: The value to call the function with

        Returns:
            True if function successfully called
        """
        if function in self.callable_map:
            ret = self._do_call(function, value, **kwargs)
        elif hasattr(self, "_call_{fn}".format(fn=function)):
            ret = getattr(self, "_call_{fn}".format(fn=function))(value, **kwargs)

        else:
            raise KeyError(
                f"Couldnt find a handler for function `{function}` on `{self.name()}`"
            )

        if ret is None:
            # When a function returns nothing
            ret = True

        return ret

    @abstractmethod
    def _do_call(self, function, value, **kwargs):
        """
        Local implementation of how to get the function from the object
        """
        pass
