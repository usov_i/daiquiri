#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    state = fields.Str(metadata={"readOnly": True})
    camera_pixel_size = fields.List(
        fields.Number(), metadata={"length": 2, "readOnly": True}
    )
    actual_size = fields.List(
        fields.Integer(), metadata={"length": 2, "readOnly": True}
    )
    sample_pixel_size_mode = fields.Str()
    user_sample_pixel_size = fields.Number()
    sample_pixel_size = fields.Number(metadata={"readOnly": True})
    field_of_view = fields.Number(metadata={"readOnly": True})
    detector = fields.Str(metadata={"readOnly": True})
    optic = fields.Str(metadata={"readOnly": True})


class TomoDetector(HardwareObject):
    _type = "tomodetector"
    _properties = _PropertiesSchema()
