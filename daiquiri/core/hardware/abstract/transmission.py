#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class TransmissionPropertiesSchema(HardwareSchema):
    datafile = fields.Str(metadata={"readOnly": True})
    transmission_factor = fields.Str(metadata={"readOnly": True})


class TransmissionCallablesSchema(HardwareSchema):
    set = fields.Float()
    get = fields.Float()


class Transmission(HardwareObject):
    _type = "transmission"

    _properties = TransmissionPropertiesSchema()
    _callables = TransmissionCallablesSchema()
