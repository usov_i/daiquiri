#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

BeamviewerStates = ["ON", "OFF", "UNKNOWN", "FAULT"]


class BeamviewerPropertiesSchema(HardwareSchema):
    state = OneOf(BeamviewerStates, metadata={"readOnly": True})
    foil = OneOf(["IN", "OUT", "UNKNOWN", "NONE"], metadata={"readOnly": True})
    led = OneOf(["ON", "OFF"], metadata={"readOnly": True})
    screen = OneOf(["IN", "OUT", "UNKNOWN"], metadata={"readOnly": True})
    diode_ranges = fields.List(fields.Str(), metadata={"readOnly": True})
    diode_range = fields.Str()


class BeamviewerCallablesSchema(HardwareSchema):
    led = fields.Bool()
    screen = fields.Bool()
    foil = fields.Bool()
    current = RequireEmpty()


class Beamviewer(HardwareObject):
    _type = "beamviewer"
    _state_ok = [BeamviewerStates[0], BeamviewerStates[1]]

    _properties = BeamviewerPropertiesSchema()
    _callables = BeamviewerCallablesSchema()
