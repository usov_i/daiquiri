#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class _PropertiesSchema(HardwareSchema):
    sx = fields.Str(metadata={"readOnly": True})
    sy = fields.Str(metadata={"readOnly": True})
    sz = fields.Str(metadata={"readOnly": True})
    somega = fields.Str(metadata={"readOnly": True})
    sampx = fields.Str(metadata={"readOnly": True})
    sampy = fields.Str(metadata={"readOnly": True})
    sampu = fields.Str(metadata={"readOnly": True})
    sampv = fields.Str(metadata={"readOnly": True})
    detectors = fields.Str(metadata={"readOnly": True})
    energy = fields.Float()


class _CallablesSchema(HardwareSchema):
    pass


class TomoConfig(HardwareObject):
    _type = "tomoconfig"
    _properties = _PropertiesSchema()
    _callables = _CallablesSchema()
