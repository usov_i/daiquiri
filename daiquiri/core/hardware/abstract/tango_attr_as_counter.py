#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)


class Tango_Attr_As_CounterPropertiesSchema(HardwareSchema):
    name = fields.Str(metadata={"readOnly": True})
    attribute = fields.Str(metadata={"readOnly": True})
    value = fields.Float()
    unit = fields.Str(metadata={"readOnly": True})


class Tango_Attr_As_CounterCallablesSchema(HardwareSchema):
    get_metadata = fields.Raw(required=True)


class Tango_Attr_As_Counter(HardwareObject):
    _type = "tango_attr_as_counter"

    _properties = Tango_Attr_As_CounterPropertiesSchema()
    _callables = Tango_Attr_As_CounterCallablesSchema()
