#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

LightStates = ["ON", "OFF", "STANDBY", "ERROR"]


class LightPropertiesSchema(HardwareSchema):
    state = OneOf(LightStates, metadata={"readOnly": True})
    temperature = fields.Float(metadata={"readOnly": True})
    intensity = fields.Float()


class Light(HardwareObject):
    _type = "light"
    _state_ok = [LightStates[0], LightStates[1], LightStates[2]]

    _properties = LightPropertiesSchema()
