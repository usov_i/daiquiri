#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields
import numpy as np
import cv2

from PIL import Image

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema

import logging

logger = logging.getLogger(__name__)

CameraStatuses = ["READY", "ACQUIRING", "UNKNOWN", "ERROR"]


class CameraPropertiesSchema(HardwareSchema):
    state = fields.Str()
    exposure = fields.Float()
    gain = fields.Float()
    mode = fields.Str(metadata={"readOnly": True})
    live = fields.Bool()
    width = fields.Int(metadata={"readOnly": True})
    height = fields.Int(metadata={"readOnly": True})


class CameraCallablesSchema(HardwareSchema):
    save = fields.Str()


class Camera(HardwareObject):
    _type = "camera"
    _state_ok = [CameraStatuses[0], CameraStatuses[1]]

    _properties = CameraPropertiesSchema()
    _callables = CameraCallablesSchema()

    def frame(self):
        """Return a frame from the camera"""
        return np.array([])

    def _call_save(self, path, type="PNG"):
        """Return a frame from the camera to disk"""
        frame = self.frame()
        if frame is None:
            raise RuntimeError("Could not get frame from camera")

        # Possible the frame will not be uint8, convert it (only if not grayscale)
        if frame.dtype != np.uint8 and len(frame.shape) > 2:
            frame = cv2.normalize(frame, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
            frame = frame.astype(np.uint8)

        im = Image.fromarray(frame)
        if im.mode != "RGB":
            im = im.convert("RGB")

        im.save(path, type, quality=100)
