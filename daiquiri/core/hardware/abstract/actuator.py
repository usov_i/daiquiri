#!/usr/bin/env python
# -*- coding: utf-8 -*-

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

ActuatorStatues = ["IN", "OUT", "UNKNOWN", "ERROR"]


class ActuatorPropertiesSchema(HardwareSchema):
    state = OneOf(ActuatorStatues, metadata={"readOnly": True})


class ActuatorCallablesSchema(HardwareSchema):
    move_in = RequireEmpty()
    move_out = RequireEmpty()
    toggle = RequireEmpty()


class Actuator(HardwareObject):
    _type = "actuator"
    _state_ok = [ActuatorStatues[0], ActuatorStatues[1]]

    _properties = ActuatorPropertiesSchema()
    _callables = ActuatorCallablesSchema()

    def move_in(self, **kwargs):
        self.call("ain", **kwargs)

    def move_out(self, **kwargs):
        self.call("aout", **kwargs)
