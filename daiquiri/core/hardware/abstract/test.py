#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import OneOf

import logging

logger = logging.getLogger(__name__)

TestStates = ["ON", "OFF", "UNKNOWN", "ERROR"]


class TestPropertiesSchema(HardwareSchema):
    state = OneOf(TestStates, metadata={"readOnly": True})
    number = fields.Int()
    string = fields.Str()
    option = OneOf(["one", "two", "three"])
    read_only = fields.Int(metadata={"readOnly": True})


class Test(HardwareObject):
    _type = "test"
    _state_ok = [TestStates[0], TestStates[1]]

    _properties = TestPropertiesSchema()
