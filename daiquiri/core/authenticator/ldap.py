#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ldap

from marshmallow import fields, Schema, EXCLUDE

from daiquiri.core.authenticator import AuthenticatorInterface

import logging

logger = logging.getLogger(__name__)


class LDAPConfigSchema(Schema):
    ldap_dn = fields.Str()


class LdapAuthenticator(AuthenticatorInterface):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logger.debug("Loaded: {c}".format(c=self.__class__.__name__))

        self._ldap_options = LDAPConfigSchema().load(
            args[0].get("auth_options", [{}])[0], unknown=EXCLUDE
        )

        self._ldap_dn = self._ldap_options.get("ldap_dn")

    def authenticate(self, username, password):
        res = False

        try:
            ldap_obj = ldap.initialize("ldap://" + self._config["auth_server"])
            ldap_obj.protocol_version = ldap.VERSION3
            user_dn = "uid=" + username + ",ou=people," + self._ldap_dn
            result = ldap_obj.bind_s(user_dn, password)
            res = True if (len(result) == 2 and result[0] == 97) else False
            ldap_obj.unbind_s()
        except ldap.LDAPError as e:
            logger.debug("LDAP Error: {e}".format(e=e))
            res = False

        return res
