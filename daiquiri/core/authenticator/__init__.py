#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from abc import ABC, abstractmethod
from flask import request
import gevent

from daiquiri.core import CoreBase, CoreResource, marshal
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.login import LoginSchema, LoginResponseSchema
from daiquiri.core.utils import loader

import logging

logger = logging.getLogger(__name__)


class LoginResource(CoreResource):
    @marshal(
        inp=LoginSchema,
        out=[
            [201, LoginResponseSchema(), "Session created"],
            [401, ErrorSchema(), "Invalid credentials"],
        ],
    )
    def post(self, **kwargs):
        """Login and obtain an authorisation token"""
        session = self._parent.authenticate(
            kwargs["username"], kwargs["password"], kwargs.pop("client", None)
        )
        if session:
            return session, 201
        else:
            return {"error": "Invalid Credentials"}, 401

    def delete(self):
        """Logout and delete the session"""
        logger.debug("Logout")
        status = self._session.remove()
        if status:
            return {}, 200
        else:
            return {}, 400


class Authenticator(CoreBase):
    """Authenticator

    Dynamically load the authenticator type from the config.yml auth_type property, and register
    the login an logout flask resources
    """

    def setup(self):
        self._fail_log = {}
        self._auth = loader(
            "daiquiri.core.authenticator",
            "Authenticator",
            self._config["auth_type"],
            self._config,
        )
        self.register_route(LoginResource, "/login")

        gevent.spawn(self.check_fail_log)

    def check_fail_log(self):
        while True:
            to_del = []
            now = time.time()
            for username, fail in self._fail_log.items():
                if fail["count"] > 2:
                    if now > fail["last"] + 5 ** (fail["count"] - 1):
                        to_del.append(username)

            for fail in to_del:
                logger.info(f"Removing {fail} from fail log")
                del self._fail_log[fail]

            time.sleep(5)

    def authenticate(self, username, password, client=None):
        """Authenticate

        Authenticate the credentials against the loaded authentication handler, and if valid
        create a session in the session handler

        Args:
            username (str): username to validate
            password (str): password to validate
            client (str): optional client name

        Returns:
            The session if credentials were valid, None if not

        """
        if username in self._fail_log:
            if self._fail_log[username]["count"] > 2:
                logger.warning(f"Login for {username} is blacklisted")
                return

        status = self._auth.authenticate(username, password)
        if status:
            if username in self._fail_log:
                del self._fail_log[username]

            logger.info(
                f"Successful login from {username} with ip {request.remote_addr}"
            )
        else:
            if username not in self._fail_log:
                self._fail_log[username] = {"count": 0}

            self._fail_log[username]["last"] = time.time()
            self._fail_log[username]["count"] += 1

            logger.warning(
                f"Failed login attempt from {username} with ip {request.remote_addr}"
            )

        if status:
            session = self._session.create({"username": username, "client": client})
            return session


class AuthenticatorInterface(ABC):
    """Autneticator Interface"""

    def __init__(self, config):
        self._config = config

    @abstractmethod
    def authenticate(self, username, password):
        """Autneticator Method

        Args:
            username (str): username to validate
            password (str): password to validate

        Returns:
            True if the credentials are valid, False if not
        """
        pass
