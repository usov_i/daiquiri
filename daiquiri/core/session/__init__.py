#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
import time
from functools import partial

from flask import request, g
from flask_socketio import disconnect, ConnectionRefusedError
import jwt
from marshmallow import fields

from daiquiri.core.logging import log
from daiquiri.core import (
    CoreBase,
    CoreResource,
    marshal,
    no_blsession_required,
    require_staff,
)
from daiquiri.core.schema.session import SessionListSchema, SignSchema
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.bewit import SignedUrl

# from daiquiri.core.user import User
from daiquiri.core.utils import loader

import logging

logger = logging.getLogger(__name__)


class SessionListResource(CoreResource):
    @marshal(
        out=[
            [200, paginated(SessionListSchema), "List of sessions"],
            [401, ErrorSchema(), "Invalid session"],
        ]
    )
    def get(self):
        """
        Get a list of current sessions
        """
        return self._parent.sessions(), 200


class SessionResource(CoreResource):
    @no_blsession_required
    @marshal(
        out=[
            [200, SessionListSchema(), "A single session"],
            [401, ErrorSchema(), "Invalid session"],
            [404, ErrorSchema(), "Session not found"],
        ]
    )
    def get(self):
        """
        Get the current session
        """
        ses = self._parent.session(g.sessionid)
        if ses:
            return ses, 200
        else:
            return {"error": "No such session"}, 404


class RequestControlResource(CoreResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Control granted"],
            [400, ErrorSchema(), "Could not get control"],
        ]
    )
    def post(self):
        """
        Request control for the current session
        """
        ses = self._parent.request_control(g.sessionid)
        if ses:
            return {"message": "Session now has control"}, 200
        else:
            return {"error": "Cant take control"}, 400

    @marshal(
        out=[
            [200, MessageSchema(), "Control granted"],
            [400, ErrorSchema(), "Could not get control"],
        ]
    )
    def delete(self):
        """
        Yield control for the current session
        """
        ses = self._parent.yield_control(g.sessionid)
        if ses:
            return {"message": "Session yielded control"}, 200
        else:
            return {"error": "Could not yield control"}, 400


class RespondRequestControlResource(CoreResource):
    @marshal(
        inp={"grant": fields.Bool(required=True)},
        out=[
            [200, MessageSchema(), "Response acknowledged"],
            [400, ErrorSchema(), "Could not acknowledge control request response"],
        ],
    )
    def post(self, **kwargs):
        """
        Respond to a request for control in the current session
        """
        ses = self._parent.respond_request_control(kwargs["grant"])
        if ses:
            return {"message": "Response acknowledged"}, 200
        else:
            return {"error": "Could not acknowledge response"}, 400


class SignResource(CoreResource):
    @no_blsession_required
    @marshal(
        inp=SignSchema,
        out=[
            [200, SignSchema(), "Url signed"],
            [400, ErrorSchema(), "Could not sign Url"],
        ],
    )
    def post(self, **kwargs):
        """Create a signed url

        This allows a resource to be accessed temporarily a single time without
        an access token. Can be used to downlad files, etc
        """
        bewit = self._parent.sign(kwargs["url"])
        if bewit:
            return {"url": kwargs["url"], "bewit": bewit}, 200
        else:
            return {"error": "Could not sign url"}, 400


class MirrorResource(CoreResource):
    @require_staff
    @marshal(
        inp={
            "sessionid": fields.Str(
                allow_none=True, metadata={"description": "The sessionid to mirror"}
            )
        },
        out=[
            [200, MessageSchema(), "Session mirroring enabled"],
            [400, ErrorSchema(), "Could not mirror session"],
        ],
    )
    def post(self, **kwargs):
        """Request mirroring of a session"""
        mirror = self._parent.request_mirror(**kwargs)
        if mirror:
            return {"message": "Mirroring enabled"}, 200
        else:
            return {"error": "Could not mirror session"}, 400


class Session(CoreBase):
    """Session Handler

    Loads the session storage mechanism dynamically from the config.yaml session_type.
    """

    _namespace = "session"

    _require_session = True
    _require_blsession = True
    _pending_control_request = None

    _session_timeout = {}
    _ui_state = {}

    def set_metadata(self, metadata):
        self._metadata = metadata

    def setup(self):
        self._sign = SignedUrl(self._config["url_secret"])

        self._session = self
        self._storage = loader(
            "daiquiri.core.session", "SessionStorage", self._config["session_type"]
        )
        self._storage.set_generate_token(self._generate_token)

        self.register_route(SessionListResource, "")
        self.register_route(SessionResource, "/current")
        self.register_route(RequestControlResource, "/current/control")
        self.register_route(RespondRequestControlResource, "/current/control/respond")
        self.register_route(SignResource, "/sign")
        self.register_route(MirrorResource, "/mirror")

        for namespace in [
            "app",
            "metadata",
            "persist",
            "queue",
            "session",
            "stomp",
        ]:
            self.register_namespace(namespace)

        self.setup_persistence()

        self.on("connect", namespace=None)(partial(self._on_socket_connect, ""))
        self.on("disconnect", namespace=None)(partial(self._on_socket_disconnect, ""))

    def register_namespace(self, namespace):
        self.on("connect", namespace=f"/{namespace}")(
            partial(self._on_socket_connect, namespace)
        )
        self.on("disconnect", namespace=f"/{namespace}")(
            partial(self._on_socket_disconnect, namespace)
        )

    def _on_socket_connect(self, namespace, *args, **kwargs):
        """Authenticate SocketIO Connection

        Makes sure a token is present on the socketio request and checks its validity in
        the current session list

        Returns:
            None if session is valid, else False which disconnects the SocketIO request
        """
        if "token" in request.args:
            sessionid = self._verify_token(request.args["token"])
            if isinstance(sessionid, str):
                if self._storage.exists(sessionid):
                    self._storage.update(sessionid, **{f"sio_{namespace}": request.sid})
                    self._stop_session_timeout(sessionid)
                    logger.debug(f"Got token for socketio login {sessionid}")
                    return True
                else:
                    logger.debug(
                        f"Invalid session for {sessionid}, token: {request.args['token']}"
                    )
            else:
                logger.debug("Invalid token")

        raise ConnectionRefusedError({"error": "Invalid Session"})

    def _on_socket_disconnect(self, namespace, *args, **kwargs):
        """SocketIO disconnect event

        Used to start session timeout
        """
        self._start_session_timeout(self._session_from_sio(namespace))

    def _start_session_timeout(self, sessionid):
        """Start session timeout

        Start a 10s timeout for the current session

        Args:
            sessionid(str): The sessionid
        """
        new_timeout = sessionid not in self._session_timeout
        self._session_timeout[sessionid] = 0

        if new_timeout:
            logger.info(f"Starting session timeout {sessionid}")
            gevent.spawn(self._session_timeout_process, sessionid)

    def _session_timeout_process(self, sessionid, sleep=5, timeout=30):
        """Session timeout process

        This greenlet will wait for 10 seconds, if the timeout has
        not been cancelled the session will be removed

        Args:
            sessionid(str): The sessionid
            sleep(int): Time to sleep
            timeout(int): Time in seconds after which to timeout
        """
        running = True
        while running:
            self._session_timeout[sessionid] += sleep
            time.sleep(sleep)

            delete = False
            if self._session_timeout[sessionid] == timeout:
                if self._storage.exists(sessionid):
                    self._storage.remove(sessionid)
                    log.get("user").info("Session disconnected", type="session")
                    self.emit(
                        "message",
                        {"type": "disconnect", "sessionid": sessionid},
                    )

                    delete = True

            if delete:
                running = False
                del self._session_timeout[sessionid]

                for session in self._storage.list():
                    if session["data"].get("mirror") == sessionid:
                        self.emit(
                            "message",
                            {"type": "mirror_disconnect", "sessionid": sessionid},
                            room=session["data"]["sio_session"],
                        )

                        del session["data"]["mirror"]

    def _stop_session_timeout(self, sessionid):
        """Cancel a session timeout

        Args:
            sessionid(str): The sessionid
        """
        logger.info(f"Cancelling session timeout {sessionid}")
        if sessionid in self._session_timeout:
            del self._session_timeout[sessionid]

    def sign(self, url):
        """Create a signed url

        Args:
            url(str): The url to sign
        Returns
            bewit(str): The bewit token
        """
        bewit = self._sign.sign(url, g.sessionid)
        if bewit:
            return bewit

    def create(self, data):
        """Create a new session

        Delegates to the session storage engine to create the session and emit a connect
        event

        Args:
            data (dict): Dictionary of session parameters to save

        Returns:
            The session id
        """
        session = self._storage.create(data)
        username = data.get("username", None)
        log.get("user").info(f"New session connected {username}", type="session")
        self.emit(
            "message",
            {
                "type": "connect",
                "sessionid": session["sessionid"],
                "username": username,
            },
        )

        if self._config.get("session_auto_control") and self._storage.no_control():
            logger.info(
                f"Automatically granting control for to {username} with session {session['sessionid']}"
            )
            self._do_grant_control(session["sessionid"])

        return session

    def remove(self):
        """Remove the current session

        Remove a session from the storage engine and emit a disconnect event

        Returns
            True is session successfully remove, False if not
        """
        invalid = self.require_valid_session()
        if not invalid:
            if self._storage.exists(g.sessionid):
                session = self._storage.get(g.sessionid)
                for key, value in session["data"].items():
                    if key.startswith("sio_"):
                        disconnect(sid=value, namespace=key.replace("sio_", ""))

                self._storage.remove(g.sessionid)
                log.get("user").info("Session disconnected", type="session")
                self.emit(
                    "message",
                    {"type": "disconnect", "sessionid": g.sessionid},
                )
                return True
            else:
                return False

    def sessions(self):
        """Return a list of sessions"""
        sessions = self._storage.list()
        for s in sessions:
            s["operator_pending"] = (
                True if s["sessionid"] == self._pending_control_request else False
            )

        return {"total": len(sessions), "rows": sessions}

    def session(self, sessionid):
        """Return a specific session

        Args:
            sessionid (uuid): The session to return

        Returns
            The session dict
        """
        session = self._storage.get(sessionid)

        if not session:
            return

        mirrored = False
        for ses in self._storage.list():
            if sessionid == ses["data"].get("mirror"):
                mirrored = True

        session["mirrored"] = mirrored

        return session

    def update(self, data):
        """Update data within the current session

        Kwargs:
            data (dict): Keywords to update
        """
        self._storage.update(g.sessionid, **data)

    def _generate_token(self, sessionid):
        """Generate a session token

        Generates a session token with similar properties to a JWT, storing the sessionid
        within the generated token. Token expirey defined in config session_length

        Args:
            sessionid (uuid): The sessionid

        Returns
            A session token used to validate all requests
        """
        encoded = jwt.encode(
            {
                "exp": time.time() + self._config["session_length"],
                "sessionid": sessionid,
            },
            self._config["secret"],
            algorithm="HS256",
        )
        return encoded

    def _verify_token(self, token):
        """Verify a token is valid

        Checks the signature of the token is valid and return the encoded sessionid

        Args:
            token (str): The token

        Returns:
            The sessionid from the token if valid, otherwise an error dict
        """
        try:
            data = jwt.decode(token, self._config["secret"], algorithms=["HS256"])
        except jwt.ExpiredSignatureError:
            logger.info("SignatureExpired {token}".format(token=token))
            sessionid = self._storage.remove(None, token=token)
            if sessionid:
                self.emit(
                    "message",
                    {"type": "expired", "sessionid": sessionid},
                )
                logger.info("Removing expired session {sid}".format(sid=sessionid))

            return {"error": "Token Expired"}

        except jwt.InvalidTokenError:
            return {"error": "Invalid Token"}

        return data["sessionid"]

    def request_control(self, sessionid):
        """Requst session control

        Request control for the sessionid and emit an event notifying other clients that
        sessionid is now in control.

        If the user is staff the request is automatically granted, otherwise the current
        user in control must respond

        Args:
            sessionid (uuid): The sessionid
        Returns:
            True if sessionid now in control
        """
        if g.user.staff() or self._storage.no_control():
            self._do_grant_control(sessionid)
            return True

        else:
            self._pending_control_request = sessionid
            self.start_control_request_timeout()
            log.get("user").info(
                f"Session {sessionid} requesting control", type="session"
            )
            self.emit(
                "message",
                {"type": "request_control", "sessionid": sessionid},
            )

    def start_control_request_timeout(self):
        """Start a greelet that counts down the control request timeout

        If the current operator does not respond within this timeframe
        the requester will automatically be granted control
        """
        if self._pending_control_request:
            self._pending_control_request_timeout = self._config[
                "session_control_timeout"
            ]
            self._greenlet = gevent.spawn(self._control_request_timeout)

    def _control_request_timeout(self):
        while self._pending_control_request:
            if self._pending_control_request_timeout == 0:
                self.emit(
                    "message",
                    {
                        "type": "timeout_control",
                        "sessionid": self._pending_control_request,
                    },
                )
                self._do_grant_control(self._pending_control_request)
                self._pending_control_request = None

            self._pending_control_request_timeout -= 1
            time.sleep(1)

    def respond_request_control(self, grant):
        """Respond to a request for control if not admin

        Args:
            grant (bool): Whether to grant control or not

        Returns:
            True if response acknowledged
        """
        if self._storage.has_control(g.sessionid):
            if grant:
                self._do_grant_control(self._pending_control_request)
            else:
                log.get("user").info(
                    f"Session {self._pending_control_request} denied control",
                    type="session",
                )
                self.emit(
                    "message",
                    {
                        "type": "deny_control",
                        "sessionid": self._pending_control_request,
                        "operator": False,
                    },
                )

            self._pending_control_request = None
            return True

    def _do_grant_control(self, sessionid):
        self._storage.take_control(sessionid)

        log.get("user").info("Session granted control", type="session")
        self.emit(
            "message",
            {"type": "control", "sessionid": sessionid, "operator": True},
        )
        return True

    def yield_control(self, sessionid):
        """Yield control for the current session

        Args:
            sessionid (uuid): Sessionid to yield

        Returns:
            True if session was in control and has now yielded
        """
        yielded = self._storage.yield_control(sessionid)
        if yielded:
            log.get("user").info("Session yielded control", type="session")
            self.emit(
                "message",
                {"type": "control", "sessionid": sessionid, "operator": True},
            )
            return True

    def require_valid_session(self, **kwargs):
        """Require a valid session for this request

        Require a valid session for a flask request.
        First checks if there is a bewit present and valid.
        Then parses the request header to retrieve a token, try to decode it, and set
        g.sessionid to the current sessionid. Return various error messages if the token
        is not present, valid, etc

        Kwargs:
            require_blsession (bool): Whether this resource requires a valid blsession

        Returns:
            error(dict): None if valid, otherwise an error dict
        """
        logger.debug("Session: require_valid_session")

        bewit = request.args.get("bewit")
        if bewit:
            url = f"http://localhost{request.path}?bewit={bewit}"
            session = self._sign.verify(url)
            if not isinstance(session, str):
                return session, 401
            else:
                return self._verify_session(session, **kwargs)

        auth_header = request.headers.get("Authorization")
        if auth_header:
            auth_token = auth_header.split(" ")[1]
        else:
            auth_token = None

        if auth_token is not None:
            resp = self._verify_token(auth_token)
            if not isinstance(resp, str):
                return resp, 401
            else:
                return self._verify_session(resp, **kwargs)
        else:
            return {"error": "No token provided"}, 401

    def _verify_session(self, session, **kwargs):
        if not self._storage.exists(session):
            return {"error": "That session is not valid"}, 401
        else:
            self._storage.update(session)

            g.sessionid = session

            ses = self._storage.get(session)

            g.login = ses["data"]["username"]
            g.user = self._metadata.get_user()
            if not g.user:
                return {"error": "The user does not exist in the database"}, 401

            g.blsession = None
            if kwargs.get("require_blsession"):
                valid = self._metadata.verify_session(
                    ses["data"].get("blsession", None)
                )
                if not valid:
                    return {"error": "No blsession selected"}, 403
                else:
                    g.blsession = valid

    def require_control(self):
        """Require control for this request

        Require that the current session has control to parse the current flask request.
        Use to block access to say moving a motor if the session is not in control.

        Returns:
            None if session is in control, otherwise an error dict

        """
        logger.debug("Session: require_control")

        if g.sessionid:
            if not self._storage.has_control(g.sessionid):
                return {"error": "That session does not have control"}
        else:
            return {"error": "No valid session"}

    def veto(self, state):
        """Veto all current sessions from control

        Remove control from all sessions, useful to veto all control when say a scan
        is in progress.

        Args:
            state (boolean): The veto state
        """
        pass

    # UI Persistence
    def _session_from_sio(self, namespace=""):
        """Get a sessionid from SocketIO sid"""
        if request.sid:
            for session in self._storage.list():
                if session["data"].get(f"sio_{namespace}") == request.sid:
                    return session["sessionid"]

    def request_mirror(self, **kwargs):
        """Request mirroring of another session"""
        if self._storage.exists(g.sessionid):
            self._storage.update(g.sessionid, mirror=kwargs.get("sessionid"))

            self.emit(
                "message",
                {
                    "type": "mirror",
                    "sessionid": g.sessionid,
                    "mirrorid": kwargs.get("sessionid"),
                },
            )

            return True

    def setup_persistence(self):
        """Setup UI persistence

        These methods are used by redux-persist in the UI to persist
        state via SocketIO to allow session mirroring
        """
        self.on("get_item", namespace="/persist")(self.ui_get_item)
        self.on("get_all_items", namespace="/persist")(self.ui_get_all_items)
        self.on("set_item", namespace="/persist")(self.ui_set_item)
        self.on("remove_item", namespace="/persist")(self.ui_remove_item)

    def _ensure_session(self):
        """Ensure session exists in _ui_state"""
        sessionid = self._session_from_sio("persist")
        if sessionid:
            if sessionid not in self._ui_state:
                self._ui_state[sessionid] = {}

            return sessionid

    def ui_get_item(self, payload):
        """Get an item from _ui_state"""
        sessionid = self._ensure_session()
        if not sessionid:
            logger.error(
                f"Requesting ui_get_item for unknown sessionid, sio {request.sid}"
            )
            return

        return self._ui_state[sessionid].get(payload)

    def ui_get_all_items(self, *args):
        """Get all keys from _ui_state"""
        sessionid = self._ensure_session()
        if not sessionid:
            logger.error(
                f"Requesting ui_get_all_items for unknown sessionid, sio {request.sid}"
            )
            return

        return self._ui_state[sessionid].keys()

    def ui_set_item(self, payload):
        """Set a key in _ui_state"""
        sessionid = self._ensure_session()
        if not sessionid:
            logger.error(
                f"Requesting ui_set_item for unknown sessionid, sio {request.sid}"
            )
            return

        key, val = payload
        self._ui_state[sessionid][key] = val

        for mirror in self._storage.list():
            if mirror["data"].get("mirror") == sessionid:
                if mirror["data"].get("sio_persist"):
                    self.emit(
                        "state_update",
                        {key: self._ui_state[sessionid][key]},
                        namespace="/persist",
                        room=mirror["data"]["sio_persist"],
                    )

    def ui_remove_item(self, payload):
        """Remove a key from _ui_state"""
        sessionid = self._ensure_session()
        if not sessionid:
            logger.error(
                f"Requesting ui_remove_item for unknown sessionid, sio {request.sid}"
            )
            return

        try:
            del self._ui_state[sessionid][payload]
        except KeyError:
            logger.warning(
                f"Trying to remove none existant key {payload} from _ui_state[{sessionid}]"
            )
