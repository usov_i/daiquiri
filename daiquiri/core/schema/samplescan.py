#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.schema.component import ComponentSchema


# Config Schema
class ScanSchema(Schema):
    actor = fields.Str(required=True)
    require_staff = fields.Bool()
    implementor = fields.Str()
    config = fields.Dict()
    tags = fields.List(fields.Str())


class ScanConfigSchema(ComponentSchema):
    scans = fields.Nested(ScanSchema, many=True)


# Endpoint Schema
class AvailableScansSchema(Schema):
    actor = fields.Str(required=True)
    tags = fields.List(fields.Str())
