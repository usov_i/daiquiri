#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

from marshmallow import Schema as MarshmallowSchema, fields, ValidationError, INCLUDE
from marshmallow_jsonschema import JSONSchema

from flask import g

from daiquiri.core import (
    CoreBase,
    CoreResource,
    marshal,
    require_control,
)
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.union import Union

logger = logging.getLogger(__name__)


class ErrorSchema(MarshmallowSchema):
    error = fields.Str()


class MessageSchema(MarshmallowSchema):
    message = fields.Str()


class ValidationErrorSchema(MarshmallowSchema):
    messages = fields.Dict()


class SchemasListSchema(MarshmallowSchema):
    name = fields.Str()


class SchemaSchema(MarshmallowSchema):
    definitions = fields.Dict()
    asyncValidate = fields.Bool(
        metadata={"description": "Whether the schema can be asynchronously validated"}
    )
    cache = fields.Bool(
        metadata={"description": "Whether values for this schema can be cached"}
    )
    method = fields.Str(metadata={"description": "Http method for this schema"})
    url = fields.Str(metadata={"description": "Url to submit this schema to"})
    uiorder = fields.List(fields.Str(), metadata={"description": "Field order"})
    uischema = fields.Dict(
        metadata={"description": "Override the default field widgets"}
    )
    uigroups = fields.List(
        Union(fields=[fields.Dict(), fields.Str()]),
        metadata={"description": "Field grouping"},
    )
    presets = fields.Dict(metadata={"description": "Presets for this schema"})
    save_presets = fields.Bool(
        metadata={"description": "Whether this schema can save presets"}
    )
    auto_load_preset = fields.Bool(
        metadata={"description": "Whether this schema will auto load the last preset"}
    )

    class Meta:
        additional = ["$ref", "$schema"]


class SchemasResource(CoreResource):
    def get(self):
        """All schemas in a JSON API spec format"""
        return self._parent.schemas(), 200


class SchemaListResource(CoreResource):
    @marshal(
        out=[[200, paginated(SchemasListSchema), "A list of available schema names"]]
    )
    def get(self):
        """List of schema names"""
        schemas = [{"name": n} for n in self._parent.list()]
        return {"total": len(schemas), "rows": schemas}


class SchemaResource(CoreResource):
    @marshal(
        out=[
            [200, SchemaSchema(), "The requested schema"],
            [404, ErrorSchema(), "No such schema"],
        ]
    )
    def get(self, name):
        """Get a single schema in JSON API Spec format"""
        schema = self._parent.get(name)
        if schema:
            return schema, 200
        else:
            return {"error": "No such schema"}, 404


class SchemaValidateResource(CoreResource):
    @require_control
    @marshal(
        inp={"data": fields.Dict(required=True), "name": fields.Str()},
        out=[[404, ErrorSchema(), "No such schema"]],
    )
    def post(self, name, **kwargs):
        """Validate and compute calculated parameters for a schema"""
        validated = self._parent.validate(name, kwargs["data"])
        if validated:
            return validated, 200
        else:
            return {"error": "No such schema"}, 404


class SavePresetResource(CoreResource):
    @require_control
    @marshal(
        inp={
            "data": fields.Dict(required=True),
            "name": fields.Str(),
            "preset": fields.Str(),
        },
        out=[
            [200, MessageSchema(), "Schema preset saved"],
            [400, ErrorSchema(), "Could not save schema preset"],
        ],
    )
    def post(self, name, **kwargs):
        """Save a schema preset"""
        try:
            self._parent.save_preset(name, kwargs["preset"], kwargs["data"])
            return {"message": f"Preset {kwargs['preset']} saved for {name}"}, 200
        except Exception as e:
            return {"error": f"Could not save preset, {str(e)}"}, 400
            logger.exception(f"Could not save preset {kwargs['preset']} for {name}")


class Schema(CoreBase):
    """The schema handler

    Allows schemas to be registered, and retrieved via a flask resource
    """

    _require_session = True
    _schemas = {}

    def setup(self):
        self._schema = self
        self.register_route(SchemasResource, "")
        self.register_route(SchemaListResource, "/list")
        self.register_route(SchemaResource, "/<string:name>")
        self.register_route(SchemaValidateResource, "/validate/<string:name>")
        self.register_route(SavePresetResource, "/preset/<string:name>")

    def set_session(self, session):
        self._session = session

    def register(self, schema, url=None, method=None):
        """Register a schema under its class name"""
        cls = schema.__class__.__name__
        if cls not in self._schemas:
            self._schemas[cls] = schema

        if url and method:
            self._schemas[cls].url = {"url": url, "method": method}

    def schemas(self):
        """Get all schemas"""
        return {key: self.get(key) for key in self._filter_schemas()}

    def list(self):
        """Get a list of registered schemas"""
        return self._filter_schemas()

    def _filter_schemas(self):
        keys = []
        for key, schema in self._schemas.items():
            if (hasattr(schema, "_require_staff") and g.user.staff()) or not hasattr(
                schema, "_require_staff"
            ):
                keys.append(key)
        return keys

    def iterate_schema(self, flds, root):
        for f, p in flds.items():
            if isinstance(p, fields.List):
                inner = p.inner

                if isinstance(inner, fields.Nested):
                    nested = inner.nested

                    if nested.Meta:
                        for k in [
                            "uiorder",
                            "uischema",
                            "presets",
                            "cache",
                            "uigroups",
                        ]:
                            if hasattr(nested.Meta, k) and nested.__name__ in root:
                                root[nested.__name__][k] = getattr(nested.Meta, k)

                    # print('iterate_schema  list nested', f, root)
                    if nested.__name__ in root:
                        self.iterate_schema(
                            nested._declared_fields, root[nested.__name__]
                        )

            if isinstance(p, fields.Nested):
                nested = p.nested

                if nested.Meta:
                    for k in ["uiorder", "uischema", "presets", "cache", "uigroups"]:
                        if hasattr(nested.Meta, k) and nested.__name__ in root:
                            root[nested.__name__][k] = getattr(nested.Meta, k)

                # print('iterate_schema nested', f, dir(nested))
                if nested.__name__ in root:
                    self.iterate_schema(nested._declared_fields, root[nested.__name__])

    def get(self, name):
        """Get a specific schema

        Args:
            name (str): The schema to retreive
        """
        if name in self.list():
            schema = self._schemas[name]

            json_schema = JSONSchema()
            json = json_schema.dump(schema)

            if hasattr(schema, "url"):
                json["url"] = schema.url["url"]
                json["method"] = schema.url["method"]

            #  Try to attach ui:schema,order to nested schemas
            self.iterate_schema(schema.fields, json["definitions"])

            if schema.Meta:
                for k in ["uiorder", "uischema", "uigroups", "presets", "cache"]:
                    if hasattr(schema.Meta, k):
                        json[k] = getattr(schema.Meta, k)

            asyncValidate = False
            for m in ["calculated", "schema_validate", "time_estimate"]:
                if hasattr(schema, m):
                    asyncValidate = True

            if hasattr(schema, "get_presets"):
                json["presets"] = schema.get_presets()

            json["save_presets"] = hasattr(schema, "save_preset")

            json["asyncValidate"] = asyncValidate

            return json

    def validate(self, name, data):
        """Async Schema Validation

        This allows for schemas to make asynchronous validation, i.e. to check
        combinations of beamline parameters.

        It will also compute any calculated parameters if the schema defines them
        and also a time estimate if defined

        Args:
            name (str): Schema name
            data (dict): Kwargs to validate

        Returns:
            errors (dict): Dictionary of errors
            warnings (dict): Dictionary of warnings
            calculated (dict): Dictionary of calculated params
            time_estimate (float): Time estimate for these params
        """
        if name in self._schemas:
            sch = self._schemas[name]
            validated = {
                "errors": {},
                "warnings": {},
                "calculated": {},
                "time_estimate": None,
            }

            try:
                sch.load(data, unknown=INCLUDE)
            except ValidationError as err:
                if "_schema" in err.messages:
                    validated["errors"] = {"schema": err.messages["_schema"]}
            else:
                for k in ["calculated", "time_estimate", "warnings"]:
                    if hasattr(sch, k):
                        try:
                            validated[k] = getattr(sch, k)(data)
                        except Exception as e:
                            logger.debug(f"{k} failed with: {e}", exc_info=True)

            return validated

    def save_preset(self, name, preset, data):
        """Save a preset for a schema

        Args:
            name (str): The schema name
            preset (str): The preset name
            data (dict): The preset data

        Returns:
            bool: True if successful
        """
        if name in self._schemas:
            sch = self._schemas[name]

            if hasattr(sch, "save_preset"):
                sch.save_preset(preset, data)
