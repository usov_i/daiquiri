from marshmallow import Schema, fields


class LayoutSchema(Schema):
    name = fields.Str(required=True, metadata={"description": "Layout name"})
    description = fields.Str(metadata={"description": "Description of layout"})
    acronym = fields.Str(metadata={"description": "Layout acronym, used in url slug"})
    icon = fields.Str(required=False)
    insidebar = fields.Bool(required=False)
    children = fields.List(fields.Dict(), required=True)
    error = fields.Str(
        metadata={"description": "Any errors found parsing the layout yaml"}
    )
