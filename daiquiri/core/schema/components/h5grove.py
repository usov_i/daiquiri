from marshmallow import Schema, fields, ValidationError


class DTypeField(fields.Field):
    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, str) or isinstance(value, dict):
            return value

        raise ValidationError("Dtype should be str or dict")


class ValueField(fields.Field):
    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, str) or isinstance(value, float) or isinstance(value, int):
            return value

        if isinstance(value, list):
            return [self._deserialize(v, attr, data, **kwargs) for v in value]

        raise ValidationError("Value should be str, number or list of those.")


class AttrMetaSchema(Schema):
    dtype = DTypeField(required=True)
    name = fields.Str(required=True)
    shape = fields.List(fields.Int(), required=True)


class BaseMetaSchema(Schema):
    name = fields.Str(required=True)
    type = fields.Str(required=True)


class ExternalLinkMetaSchema(BaseMetaSchema):
    target_file = fields.Str(required=True)
    target_path = fields.Str(required=True)


class SoftLinkMetaSchema(BaseMetaSchema):
    target_path = fields.Str(required=True)


class DatasetMetaSchema(BaseMetaSchema):
    attributes = fields.List(fields.Nested(AttrMetaSchema))
    chunks = fields.List(fields.Int(), allow_none=True)
    dtype = DTypeField(required=True)
    filters = fields.Dict(allow_none=True)
    shape = fields.List(fields.Int(), required=True)


class GroupMetaSchema(BaseMetaSchema):
    attributes = fields.List(fields.Nested(AttrMetaSchema), required=True)
    children = fields.Dict(fields.Nested(lambda: MetaSchema()), required=True)


class MetaSchema(BaseMetaSchema):
    external_link_schema = ExternalLinkMetaSchema()
    soft_link_schema = ExternalLinkMetaSchema()
    dataset_schema = DatasetMetaSchema()
    group_schema = GroupMetaSchema()

    def _get_data_schema(self, data):
        if data["type"] == "external_link":
            return self.external_link_schema
        if data["type"] == "soft_link":
            return self.soft_link_schema
        if data["type"] == "dataset":
            return self.dataset_schema
        if data["type"] == "group":
            return self.group_schema

        return super()

    def load(self, data, *args, **kwargs):
        data_schema = self._get_data_schema(data)
        return data_schema.load(data, *args, **kwargs)

    def dump(self, data, *args, **kwargs):
        data_schema = self._get_data_schema(data)
        return data_schema.dump(data, *args, **kwargs)

    def validate(self, data, *args, **kwargs):
        data_schema = self._get_data_schema(data)
        return data_schema.validate(data, *args, **kwargs)
