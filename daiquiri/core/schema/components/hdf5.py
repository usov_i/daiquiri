#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

import logging

logger = logging.getLogger(__name__)


class Hdf5GroupSchema(Schema):
    children = fields.Dict(values=fields.Nested(lambda: Hdf5Schema()))
    name = fields.Str()
    uri = fields.Str()
    attrs = fields.Dict()


class Hdf5DatasetSchema(Hdf5GroupSchema):
    type = fields.Str()
    data = fields.Dict()
    shape = fields.List(fields.Int())
    size = fields.Int()
    ndim = fields.Int()
    dtype = fields.Str()


class Hdf5Schema(Schema):
    children = fields.Dict(values=fields.Nested(lambda: Hdf5Schema()))
    name = fields.Str()
    uri = fields.Str()
    attrs = fields.Dict()
    type = fields.Str()
    data = fields.Dict()
    shape = fields.List(fields.Int())
    size = fields.Int()
    ndim = fields.Int()
    dtype = fields.Str()


class RootHdf5Schema(Hdf5Schema):
    file = fields.Str()

    class Meta:
        exclude = ("name", "uri", "type")
