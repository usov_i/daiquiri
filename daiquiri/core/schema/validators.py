#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import validate, fields, ValidationError
import bleach

"""Custom Schema Validators

    https://json-schema.org/understanding-json-schema/reference/string.html
"""


def ValidatedRegexp(pattern, **kwargs):
    """A regexp validated fields.Str

    Args:
        pattern: the pattern to use from the above dictionary
    Kwargs:
        passed to the `field`
    """

    expressions = {
        "word": "^[A-z0-9]+$",
        "word-dash": "^[A-z0-9-]+$",
        "word-dash-space": r"^[\w\s-]+$",
        "word-special": r"^(\w|\s|-|%|\(|\)|,|\.)+$",
        "saving": "^({(sample|component|subsampletype|subsampleid)}|[A-z0-9/-])+$",
        "smiles": r"^([^J][A-Za-z0-9@+\-\[\]\(\)\\\/%=#$]+)$",
        "path": "^/[A-z0-9-/]+$",
    }

    if pattern in expressions:
        validates = kwargs.pop("validate", None)
        if validates and not isinstance(validates, list):
            validates = [validates]

        metadata = kwargs.pop("metadata", {})
        metadata["pattern"] = expressions[pattern]

        return fields.Str(
            validate=[
                validate.Regexp(
                    expressions[pattern],
                    flags=0,
                    error="Input {input} does not match {regex}",
                )
            ]
            + (validates if validates else []),
            metadata=metadata,
            **kwargs
        )
    else:
        raise Exception("Cant find regular expression for {pat}".format(pat=pattern))


class Any(fields.Field):
    """Mixed field"""

    def _jsonschema_type_mapping(self):
        return {"type": "any", "title": self.name}


class NoneValidator(validate.Validator):
    """Validator that requires the field is None"""

    def __call__(self, val):
        if val is not None:
            raise ValidationError("Field must be empty")

        return val


class EmptyField(fields.Field):
    """Custom empty field type"""

    def _jsonschema_type_mapping(self):
        return {"type": "empty", "title": self.name}


def RequireEmpty(**kwargs):
    """A RequireEmpty field

    Requires that this particular field is empty by combining NoneValidator and EmptyField
    """
    return EmptyField(validate=NoneValidator(), allow_none=True, **kwargs)


def OneOf(choices, **kwargs):
    """Single enum type validator

    Args:
        choices (list): List of choices
    """
    metadata = kwargs.pop("metadata", {})
    metadata["enum"] = choices

    return fields.Str(metadata=metadata, validate=validate.OneOf(choices), **kwargs)


class SanitizedHTML(fields.Str):
    """A Sanitised HTML string

    bleach.cleaned HTML string field
    """

    def __init__(self, *args, **kwargs):
        self._strip = kwargs.pop("strip", None)
        self._load = kwargs.pop("load", None)
        self._linkify = kwargs.pop("linkify", None)

        super().__init__(*args, **kwargs)

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None

        if self._load:
            value = bleach.clean(value, strip=self._strip)

            if self._linkify:
                value = bleach.linkify(value)

        return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        value = bleach.clean(value, strip=self._strip)
        return super()._deserialize(value, attr, data, **kwargs)
