from marshmallow import Schema, fields, validate

from daiquiri.core.schema.validators import ValidatedRegexp, OneOf


# Helper to paginate a schema
def paginated(schema):
    class PaginatedSchema(Schema):
        total = fields.Int()
        rows = fields.Nested(schema, many=True)

    cls_name = f"Paginated<{schema.__name__}>"
    PaginatedSchema.__name__ = cls_name
    PaginatedSchema.__qualname__ = cls_name

    return PaginatedSchema


# Users
class UserSchema(Schema):
    user = fields.Str()
    personid = fields.Int()
    login = fields.Str()
    permissions = fields.List(fields.Str())
    groups = fields.List(fields.Str())
    fullname = fields.Str()
    givenname = fields.Str()
    familyname = fields.Str()
    is_staff = fields.Bool()


class UserCacheSchema(Schema):
    cache = fields.Dict()


# Proposals
class ProposalSchema(Schema):
    proposalid = fields.Int()
    proposal = fields.Str()
    start = fields.DateTime()
    title = fields.Str()
    visits = fields.Int()


# Visits
class SessionSchema(Schema):
    blsessionid = fields.Int()
    proposalid = fields.Int()
    session = fields.Str()
    startdate = fields.DateTime()
    enddate = fields.DateTime()
    proposal = fields.Str()
    beamlinename = fields.Str()
    active = fields.Bool()

    class Meta:
        datetimeformat = "%d-%m-%Y %H:%M"


# Components
class NewComponentSchema(Schema):
    name = ValidatedRegexp(
        "word-dash-space",
        metadata={"title": "Name"},
        validate=validate.Length(max=255),
        required=True,
    )
    acronym = ValidatedRegexp(
        "word-dash",
        metadata={"title": "Acronym"},
        validate=validate.Length(max=45),
        required=True,
    )
    molecularmass = fields.Float(metadata={"title": "Mass", "unit": "Da"})
    density = fields.Float(metadata={"title": "Density", "unit": "kg/m3"})
    sequence = ValidatedRegexp(
        "smiles", metadata={"title": "SMILES", "description": "Chemical composition"}
    )

    class Meta:
        uiorder = ["name", "acronym", "molecularmass", "density", "sequence"]


class ComponentSchema(NewComponentSchema):
    componentid = fields.Int()
    name = ValidatedRegexp("word-dash-space", metadata={"title": "Name"})
    acronym = ValidatedRegexp(
        "word-dash",
        metadata={"title": "Acronym", "description": "A short name for the component"},
    )
    samples = fields.Int()
    datacollections = fields.Int()


# Samples
class NewSampleSchema(Schema):
    name = ValidatedRegexp(
        "word-dash",
        metadata={"title": "Name"},
        validate=validate.Length(max=100),
        required=True,
    )
    # FIXME: Name length is not consistent with the size found in ispyb (100)
    offsetx = fields.Int(metadata={"title": "X Offset"}, dump_default=0)
    offsety = fields.Int(metadata={"title": "Y Offset"}, dump_default=0)
    positions = fields.Dict(keys=fields.Str(), values=fields.Float())
    componentid = fields.Int(metadata={"title": "Component"})
    comments = ValidatedRegexp(
        "word-special",
        metadata={"title": "Comments"},
        validate=validate.Length(max=1024),
        allow_none=True,
    )

    class Meta:
        uiorder = ["componentid", "name", "offsetx", "offsety", "comments", "positions"]
        uischema = {
            "positions": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "offsetx": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "offsety": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "componentid": {
                "ui:widget": "connectedSelect",
                "ui:options": {
                    "selector": "components",
                    "key": "acronym",
                    "value": "componentid",
                },
            },
        }


class SampleSchema(NewSampleSchema):
    name = ValidatedRegexp("word-dash", metadata={"title": "Name"})
    offsetx = fields.Int(metadata={"title": "X Offset"})
    offsety = fields.Int(metadata={"title": "Y Offset"})
    sampleid = fields.Int()
    component = fields.Str()
    subsamples = fields.Int()
    datacollections = fields.Int()
    queued = fields.Bool()

    class Meta:
        uischema = {
            "componentid": {
                "ui:widget": "connectedSelect",
                "ui:options": {
                    "selector": "components",
                    "key": "acronym",
                    "value": "componentid",
                },
            },
        }


class NewSubSampleSchema(Schema):
    sampleid = fields.Int(required=True)
    name = ValidatedRegexp("word-dash-space", metadata={"title": "Name"})
    type = OneOf(["roi", "poi", "loi"], metadata={"title": "Type"})
    source = OneOf(
        ["manual", "auto", "reference"],
        metadata={"title": "Source"},
        dump_default="manual",
    )
    comments = ValidatedRegexp(
        "word-special", metadata={"title": "Comments"}, allow_none=True
    )
    x = fields.Int()
    y = fields.Int()
    x2 = fields.Int()
    y2 = fields.Int()
    positions = fields.Dict(keys=fields.Str(), values=fields.Float())


class SubSampleSchema(NewSubSampleSchema):
    sampleid = fields.Int()
    sample = fields.Str()
    subsampleid = fields.Int()
    datacollections = fields.Int()
    queued = fields.Bool()


class NewSampleImageSchema(Schema):
    sampleid = fields.Int(required=True)
    offsetx = fields.Int()
    offsety = fields.Int()
    scalex = fields.Float()
    scaley = fields.Float()
    rotation = fields.Float()
    positions = fields.Dict(keys=fields.Str(), values=fields.Float())


class SampleImageSchema(NewSampleImageSchema):
    sampleimageid = fields.Int()
    sampleid = fields.Int()
    url = fields.Str()
    positions = fields.Dict(keys=fields.Str(), values=fields.Float())


class NewSampleActionPositionSchema(Schema):
    sampleactionid = fields.Int(required=True)
    posx = fields.Int(required=True)
    posy = fields.Int(required=True)
    type = OneOf(["reference", "real"], required=True)
    id = fields.Int(required=True)


class SampleActionPositionSchema(NewSampleActionPositionSchema):
    sampleactionpositionid = fields.Int()


class SampleActionSchema(Schema):
    sampleactionid = fields.Int()
    actiontype = fields.Str()
    status = fields.Str()
    message = fields.Str()
    positions = fields.List(fields.Nested(SampleActionPositionSchema))
    has_result = fields.Bool()


# Datacollections
class DataCollectionSchema(Schema):
    sessionid = fields.Int()
    datacollections = fields.Int()
    datacollectionid = fields.Int(required=True)
    datacollectiongroupid = fields.Int()
    sampleid = fields.Int()
    sample = fields.Str()
    subsampleid = fields.Int()
    starttime = fields.DateTime()
    datacollectionplanid = fields.Int()
    endtime = fields.DateTime()
    duration = fields.Int()
    experimenttype = fields.Str()
    datacollectionnumber = fields.Int()
    runstatus = fields.Str()

    # Paths
    filetemplate = fields.Str()
    imagedirectory = fields.Str()

    # DC Params
    exposuretime = fields.Float()
    wavelength = fields.Float()
    transmission = fields.Float()
    numberofimages = fields.Float()
    numberofpasses = fields.Int()

    # Beam position and size
    xbeam = fields.Float()
    ybeam = fields.Float()
    beamsizeatsamplex = fields.Float()
    beamsizeatsampley = fields.Float()

    # Gridinfo columns
    steps_x = fields.Int()
    steps_y = fields.Int()
    dx_mm = fields.Float()
    dy_mm = fields.Float()
    patchesx = fields.Int()
    patchesy = fields.Int()

    # Snapshots
    snapshots = fields.Dict(keys=fields.Str(), values=fields.Bool())

    comments = fields.Str(metadata={"title": "Comments"})

    class Meta:
        datetimeformat = "%d-%m-%Y %H:%M:%S"


class DataCollectionAttachmentSchema(Schema):
    datacollectionfileattachmentid = fields.Int()
    dataCollectionid = fields.Int()
    filename = fields.Str()
    filetype = fields.Str()


class NewDataCollectionPlanSchema(Schema):
    sampleid = fields.Int(required=True)
    experimentkind = fields.Str()
    energy = fields.Float()
    exposuretime = fields.Float()
    planorder = fields.Int()
    scanparameters = fields.Dict(required=True)


class DataCollectionPlanSchema(NewDataCollectionPlanSchema):
    datacollectionplanid = fields.Int()
    timestamp = fields.DateTime()
    sampleid = fields.Int()
    subsampleid = fields.Int()
    sample = fields.Str()
    component = fields.Str()
    subsampleid = fields.Int()
    datacollectionid = fields.Int()
    containerqueuesampleid = fields.Int()
    queued = fields.Bool()
    status = fields.Str()
    linked = fields.Bool()
    scanparameters = fields.Dict()


class ScanQualityIndicatorsSchema(Schema):
    point = fields.List(fields.Int(), metadata={"description": "Scan point"})
    total = fields.List(fields.Float(), metadata={"description": "Total signal"})
    spots = fields.List(fields.Int(), metadata={"description": "Number of spots"})
    autoprocprogramid = fields.List(fields.Int())


# XRF Maps
class NewXRFMapSchema(Schema):
    maproiid = fields.Int(required=True, metadata={"description": "The roiid"})
    subsampleid = fields.Int(
        required=True, metadata={"description": "Object this map relates to"}
    )
    datacollectionid = fields.Int(required=True)


class XRFMapHistogramSchema(Schema):
    bins = fields.List(fields.Float())
    hist = fields.List(fields.Float())
    width = fields.List(fields.Float())


class XRFMapSchema(NewXRFMapSchema):
    maproiid = fields.Int(metadata={"description": "The roiid"})
    subsampleid = fields.Int(metadata={"description": "Object this map relates to"})
    datacollectionid = fields.Int()
    datacollectiongroupid = fields.Int()
    datacollectionnumber = fields.Int()
    mapid = fields.Int(metadata={"description": "The id of the map"})
    sampleid = fields.Int()
    w = fields.Int(metadata={"description": "Width of the map"})
    h = fields.Int(metadata={"description": "Height of the map"})
    snaked = fields.Bool(
        metadata={
            "description": "Whether the map is collected interleaved left to right"
        }
    )
    orientation = fields.Str(
        metadata={
            "description": "Wehther the map is collected horizontally or vertically"
        }
    )
    element = fields.Str()
    edge = fields.Str()
    scalar = fields.Str()
    composites = fields.Int()

    points = fields.Int(metadata={"description": "Number of completed points"})

    opacity = fields.Float()
    min = fields.Int()
    max = fields.Int()
    colourmap = fields.String()
    scale = fields.String()

    url = fields.Str()


class XRFMapHistogramSchema(Schema):
    mapid = fields.Int()
    histogram = fields.Nested(
        XRFMapHistogramSchema, metadata={"description": "Histogram of the map points"}
    )


class XRFMapValueSchema(Schema):
    mapid = fields.Int()
    x = fields.Int()
    y = fields.Int()
    value = fields.Float()


# XRF Composite Maps
class NewXRFCompositeMapSchema(Schema):
    subsampleid = fields.Int(
        required=True, metadata={"description": "Object this map relates to"}
    )
    r = fields.Int(required=True)
    g = fields.Int(required=True)
    b = fields.Int(required=True)
    ropacity = fields.Float()
    gopacity = fields.Float()
    bopacity = fields.Float()


class XRFCompositeMapSchema(NewXRFCompositeMapSchema):
    compositeid = fields.Int(metadata={"description": "The id of the composite map"})
    subsampleid = fields.Int()
    sampleid = fields.Int()
    opacity = fields.Float()
    r = fields.Int()
    g = fields.Int()
    b = fields.Int()
    rroi = fields.Str()
    groi = fields.Str()
    broi = fields.Str()

    url = fields.Str()


# XRF Map ROIs
class NewXRFMapROISchema(Schema):
    sampleid = fields.Int(
        required=True, metadata={"title": "Sample ROI is associated to"}
    )
    element = ValidatedRegexp(
        "word-dash",
        required=True,
        metadata={"title": "Element"},
        validate=validate.Length(max=2),
    )
    edge = ValidatedRegexp(
        "word-dash",
        required=True,
        metadata={"title": "Edge"},
        validate=validate.Length(max=3),
    )
    start = fields.Float(
        required=True,
        metadata={"title": "Start", "description": "Start Energy of ROI", "unit": "eV"},
    )
    end = fields.Float(
        required=True,
        metadata={"title": "End", "description": "End Energy of ROI", "unit": "eV"},
    )

    class Meta:
        uiorder = ["element", "edge", "start", "end", "sampleid"]
        uischema = {"sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class XRFMapROISchema(NewXRFMapROISchema):
    maproiid = fields.Int()
    maps = fields.Int()

    class Meta:
        uiorder = ["maproiid", "element", "edge", "start", "end", "maps", "sampleid"]
        uischema = {
            "maproiid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "maps": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }


# Auto Processing
class AutoProcProgramSchema(Schema):
    autoprocprogramid = fields.Int(required=True)
    datacollectionid = fields.Int()
    status = fields.Int()
    message = fields.Str()
    starttime = fields.DateTime()
    endtime = fields.DateTime()
    duration = fields.Int()
    programs = fields.Str()
    automatic = fields.Int()
    warnings = fields.Int()
    errors = fields.Int()


class AutoProcProgramAttachmentSchema(Schema):
    autoprocprogramattachmentid = fields.Int(required=True)
    autoprocprogramid = fields.Int(required=True)
    filetype = fields.Str()
    filename = fields.Str()
    rank = fields.Int()


class AutoProcProgramMessageSchema(Schema):
    autoprocprogrammessageid = fields.Int(required=True)
    autoprocprogramid = fields.Int(required=True)
    timestamp = fields.DateTime()
    severity = fields.Str()
    message = fields.Str()
    description = fields.Str()
