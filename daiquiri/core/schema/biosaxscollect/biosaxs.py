# -*- coding: utf-8 -*-
from marshmallow import Schema, fields, validate
from marshmallow_objects import Model

import logging

logger = logging.getLogger(__name__)


class SampleIDSchema(Schema):
    id = fields.Str()


class SamplesDoneSchema(Schema):
    samples = fields.List(fields.Str())
    failed = fields.List(fields.Str())
    current = fields.Raw()
    collect_progress = fields.Float()
    job_output = fields.Raw()


class ProcessingParametersSchema(Schema):
    poni_file = fields.Str(required=True, metadata={"title": "Poni file"})
    mask_file = fields.Str(required=True, metadata={"title": "Mask file"})
    fidelity_abs = fields.Float(required=True, metadata={"title": "Fidelity absolute"})
    fidelity_rel = fields.Float(required=True, metadata={"title": "fidelity real"})
    normalization_factor = fields.Float(
        required=True, metadata={"title": "Normalisation factor"}
    )
    files_root = fields.Str(required=False, metadata={"title": "Files root"})


class BaseModel(Model, dict):
    def __init__(self, *args, **kwargs):
        Model.__init__(self, *args, **kwargs)
        dict.__init__(self, kwargs)

    # Remove special items from internal dict so that it can be serialized.
    def __plain_dict(self):
        return {k: v for k, v in self.__dict__.items() if not k.startswith("_")}

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def __getitem__(self, key):
        return self.__plain_dict[key]

    def __delitem__(self, key):
        del self.__dict__[key]

    def __missing__(self, key):
        # return self.__dict__.__missing__(key)
        return key

    def __contains__(self, item):
        return item in self.__dict__

    def __iter__(self):
        return iter(self.__plain_dict())

    def __len__(self):
        return len(self.__plain_dict())

    def keys(self):
        return self.__plain_dict().keys()

    def values(self):
        return self.__plain_dict().values()

    def items(self):
        return self.__plain_dict().items()

    def pop(self, *args):
        return self.__dict__.pop(*args)


class CollectOtherParams(BaseModel):
    directory = fields.Str(required=True, metadata={"title": "Directory"})
    prefix = fields.Str(required=True, metadata={"title": "Prefix"})
    run_number = fields.Int(required=True, metadata={"title": "Run Number"})
    exp_name = fields.Str(required=True, metadata={"title": "Experiment name"})
    experiment_type = fields.Str(
        required=True, metadata={"title": "Experiment type: SC OR HPLC"}
    )
    initial_cleaning = fields.Boolean(
        required=True, metadata={"title": "Cleaning sample changer (Before collect)"}
    )
    hplc_inject = fields.Boolean(
        required=False, metadata={"title": "Inject Control by  BSX3"}
    )
    hplc_injection_port = fields.Str(
        required=False, metadata={"title": "Injection Port Control by  BSX3"}
    )
    wait_for_beam = fields.Boolean(
        required=True, metadata={"title": "Check and wait for Beam (Before collect)"}
    )
    proposal = fields.Str(required=True, metadata={"title": "proposal"})


class SCIspybMetadata(BaseModel):
    SEUtemperature = fields.Float(metadata={"title": "SEU temperature in °C"})
    buffer = fields.Str(metadata={"title": "Sample ID"})
    buffername = fields.Str(metadata={"title": "Buffer name"})
    code = fields.Str(metadata={"title": "Name of Sample or Buffer"})
    comments = fields.Str(metadata={"title": "Comment"})
    concentration = fields.Float(
        validate=validate.Range(min=0, max=10000),
        metadata={"title": "Concentration mg/ml"},
    )
    enable = fields.Boolean(metadata={"title": "Enable"})
    flow = fields.Boolean(metadata={"title": "Flow"})
    recuperate = fields.Boolean(metadata={"title": "Recuperation"})
    title = fields.Str(metadata={"title": "Title"})
    transmission = fields.Float(metadata={"title": "Transmission"})
    macromolecule = fields.Str(metadata={"title": "Name of Sample or Buffer"})
    type = fields.Str(metadata={"title": "Type Buffer Or Sample"})
    typen: fields.Int(metadata={"title": "typen"})
    viscosity = fields.Str(metadata={"title": "Viscocity (low, medium,  high)"})
    volume = fields.Float(metadata={"title": "Volumn total in μl"})
    volumeToLoad = fields.Float(metadata={"title": "Volumn in μl"})
    waittime = fields.Float(metadata={"title": "Wait time in seconds"})
    plate = fields.Int(
        validate=validate.Range(min=1, max=3), metadata={"title": "Plate Number"}
    )
    row = fields.Int(metadata={"title": "Plate row"})
    well = fields.Int(metadata={"title": "Plate column"})


class SingleCollectSchema(BaseModel):
    id = fields.Str(metadata={"disable": True, "title": "Sample ID"})
    exp_name = fields.Str(
        required=True,
        metadata={
            "title": "Experiment Name",
            "value": "single_collect",
            "disable": True,
        },
    )
    experiment_type = fields.Str(
        metadata={"disable": True, "title": "Experiment type: SC OR HPLC"}
    )
    name = fields.Str(required=True, metadata={"title": "Sample Name"})
    number_frames = fields.Int(
        validate=validate.Range(min=1, max=10000),
        required=True,
        metadata={"title": "Number of Frames"},
    )
    exposure_time = fields.Float(
        validate=validate.Range(min=0.1, max=100),
        required=True,
        metadata={"title": "Exposure Time"},
    )
    transmission = fields.Float(
        validate=validate.Range(min=1, max=100),
        required=True,
        metadata={"title": "Transmission"},
    )
    proposal = fields.Str(
        required=False, metadata={"disable": True, "title": "User Proposal"}
    )
    collect_type = fields.Str(
        required=True, metadata={"title": "Collect As", "enum": ["SC", "HPLC"]}
    )


class ScanCollectSchema(BaseModel):
    number_frames = fields.Int(
        validate=validate.Range(min=1, max=10000),
        required=True,
        metadata={"title": "Number of Frames"},
    )
    exposure_time = fields.Float(
        validate=validate.Range(min=0.1, max=100),
        required=True,
        metadata={"title": "Exposure Time"},
    )
    collect = fields.Boolean(
        metadata={"disable": True, "title": "Collect"}, required=False
    )


class SCCollectSample(BaseModel):
    id = fields.Str(metadata={"title": "Sample ID"})
    num_frames = fields.Int(
        validate=validate.Range(min=1, max=10000),
        required=True,
        metadata={"title": "Number of frames"},
    )
    exposure_time = fields.Float(
        validate=validate.Range(min=0.01, max=999),
        required=True,
        metadata={"title": "Time(s) per frame"},
    )

    energy = fields.Float(metadata={"title": "Energy"})
    transmission = fields.Float(metadata={"title": "Transmission"})

    name = fields.Str(metadata={"title": "Sample Name"})
    file_name = fields.Str(metadata={"title": "File Name"}, required=False)
    plate = fields.Int(
        validate=validate.Range(min=1, max=3), metadata={"title": "Plate Number"}
    )
    row = fields.Str(metadata={"title": "Plate row"})
    column = fields.Int(metadata={"title": "Plate column"})
    comment = fields.Str(metadata={"title": "Comment"})

    flow = fields.Boolean(metadata={"title": "Flow"})
    extra_flow_time = fields.Float(metadata={"title": "Extra flow time in seconds"})
    seu_temperature = fields.Float(metadata={"title": "SEU temperature in °C"})
    storage_temperature = fields.Float(metadata={"title": "Storage temperature in °C"})
    recuperation = fields.Boolean(metadata={"title": "Recuperation"})
    wait = fields.Float(metadata={"title": "Wait time in seconds"})

    volume = fields.Float(metadata={"title": "Volumn in μl"})
    viscosity = fields.Str(metadata={"title": "Viscocity (low, medium,  high)"})
    concentration = fields.Float(
        validate=validate.Range(min=0, max=10000),
        metadata={"title": "Concentration mg/ml"},
    )

    buffer_name = fields.Str(metadata={"title": "Buffer name"})
    buffer_mode = fields.Str(
        metadata={"title": "Buffer mode (Before, After, Before & After, None)"}
    )


class SCCollectBuffer(BaseModel):
    id = fields.Str(metadata={"title": "Sample ID"})
    num_frames = fields.Int(
        validate=validate.Range(min=1, max=10000),
        required=True,
        metadata={"title": "Number of frames"},
    )
    exposure_time = fields.Float(
        validate=validate.Range(min=0.01, max=999),
        required=True,
        metadata={"title": "Time(s) per frame"},
    )

    energy = fields.Float(metadata={"title": "Energy"})
    transmission = fields.Float(metadata={"title": "Transmission"})

    name = fields.Str(metadata={"title": "Sample Name"})
    file_name = fields.Str(metadata={"title": "File Name"}, required=False)
    plate = fields.Int(
        validate=validate.Range(min=1, max=3), metadata={"title": "Plate Number"}
    )
    row = fields.Str(metadata={"title": "Plate row"})
    column = fields.Int(metadata={"title": "Plate column"})
    comment = fields.Str(metadata={"title": "Comment"})

    flow = fields.Boolean(metadata={"title": "Flow"})
    extra_flow_time = fields.Float(metadata={"title": "Extra flow time in seconds"})
    seu_temperature = fields.Float(metadata={"title": "SEU temperature in °C"})
    storage_temperature = fields.Float(metadata={"title": "Storage temperature in °C"})
    recuperation = fields.Boolean(metadata={"title": "Recuperation"})
    wait = fields.Float(metadata={"title": "Wait time in seconds"})

    volume = fields.Float(metadata={"title": "Volumn in μl"})
    viscosity = fields.Str(metadata={"title": "Viscocity (low, medium,  high)"})
    concentration = fields.Float(
        validate=validate.Range(min=0, max=10000),
        metadata={"title": "Concentration mg/ml"},
    )


class HPLCCollectSample(BaseModel):
    id = fields.Str()
    num_frames = fields.Int(
        validate=validate.Range(min=1, max=100000),
        required=True,
        metadata={"title": "Number of frames"},
    )
    exposure_time = fields.Float(
        validate=validate.Range(min=0.01, max=999),
        required=True,
        metadata={"title": "Exposure Time in s"},
    )

    energy = fields.Float(metadata={"title": "Energy"})
    transmission = fields.Float(metadata={"title": "Transmission"})
    seu_temperature = fields.Float(metadata={"title": "Exposure temperature in °C"})
    w1 = fields.Float(metadata={"title": "PDA wavelength w1"})
    w2 = fields.Float(metadata={"title": "PDA wavelength w2"})
    w3 = fields.Float(metadata={"title": "PDA wavelength w3"})
    w4 = fields.Float(metadata={"title": "PDA wavelength w4"})
    column_elution_time = fields.Float(
        metadata={"title": "Column Elution Time in minute"}
    )

    name = fields.Str(metadata={"title": "Sample Name"})
    plate = fields.Int(
        validate=validate.Range(min=1, max=3), metadata={"title": "Plate Number"}
    )
    vial = fields.Int(metadata={"title": "Vial"})
    comment = fields.Str(metadata={"title": "Comment"})
    wait = fields.Float(metadata={"title": "Wait time in seconds"})

    injection_volume = fields.Float(metadata={"title": "Injection volume in μl"})
    flow_rate = fields.Float(metadata={"title": "Flow rate mL/min"})
    concentration = fields.Float(
        validate=validate.Range(min=0, max=10000),
        metadata={"title": "Concentration mg/ml"},
    )


class HPLCEquilibrate(BaseModel):
    id = fields.Str()
    name = fields.Str(metadata={"title": "Name"})
    port = fields.Str(metadata={"title": "Port"})
    # flow_rate = fields.Float(metadata={"title": "Flow reate mL/min"})
    w1 = fields.Float(metadata={"title": "UV wavelength 1 in mm"})
    w2 = fields.Float(metadata={"title": "uv wavelength 2 in mm"})
    w3 = fields.Float(metadata={"title": "uv wavelength 3 in mm"})
    w4 = fields.Float(metadata={"title": "uv wavelength 4 in mm"})
    equilibrate_time = fields.Float(metadata={"title": "Time to Equilibrate in min"})
    comment = fields.Str(metadata={"title": "Comment"})


class HPLCCMetadata(BaseModel):
    buffer_volume = fields.Float(metadata={"title": "volume in ml"})
    buffer_composition = fields.Str(
        metadata={"title": "Formula/ composition of the sample"}
    )
    buffer_name = fields.Str(metadata={"title": "Buffer Name"})
    buffer_ph = fields.Float(metadata={"title": "ph of the Buffer"})
    buffer_port = fields.Str(metadata={"title": "Buffer Port"})
    flow_rate = fields.Float(metadata={"title": "Flow rate mL/min"})
    column_name = fields.Str(metadata={"title": "Column Name"})
    column_dimmenssion_h = fields.Float(metadata={"title": "Height mm"})
    column_dimmenssion_d = fields.Float(metadata={"title": "Width mm"})
    column_bed_volume = fields.Float(metadata={"title": "volume of the column ml"})
    column_temperature = fields.Str(metadata={"title": "Column temperature"})
    column_min_pressure = fields.Float(metadata={"title": "Minimum pressure in MPa"})
    column_max_pressure = fields.Float(metadata={"title": "Maximum pressure in MPa"})
    collect_uv = fields.Boolean(
        required=True, metadata={"title": "Equilibrate with collect UV or Not"}
    )


class HPLCSampleModel(BaseModel):
    samples = fields.List(fields.Nested(HPLCCollectSample.__schema_class__))


class HPLCEquilibrateModel(BaseModel):
    column = fields.Nested(HPLCEquilibrate.__schema_class__)


# ----------------- Test Data ------------------#
class SCSampleBufferModel(BaseModel):
    samples = fields.List(fields.Nested(SCCollectSample.__schema_class__))
    buffers = fields.List(fields.Nested(SCCollectBuffer.__schema_class__))


class HPLCSampleBufferModel(BaseModel):
    samples = fields.List(fields.Nested(HPLCCollectSample.__schema_class__))
    buffers = fields.Nested(HPLCEquilibrate.__schema_class__)


class DataTestArgumentSchema(Schema):
    parameters = fields.Nested(CollectOtherParams.__schema_class__)
    sc_ramdom = fields.Nested(SCSampleBufferModel.__schema_class__)
    hplc_ramdom = fields.Nested(HPLCSampleBufferModel.__schema_class__)
    water_calibration = fields.Nested(SCSampleBufferModel.__schema_class__)
    bsa_calibration = fields.Nested(SCSampleBufferModel.__schema_class__)


# ----------------- End Test Data ------------------#
class SCIspybMetadataArgumentSchema(Schema):
    name = fields.Str(metadata={"title": "Name of the Experiment"})
    comments = fields.Str(metadata={"title": "comments of the Experiment"})
    measurements = fields.List(fields.Nested(SCIspybMetadata.__schema_class__))


# ----------------------- Collect/ Add TO queue  Schema Argument --------------#
class SingleCollectArgumentSchema(Schema):
    parameters = fields.Nested(SingleCollectSchema.__schema_class__)


class ScanCollectArgumentSchema(Schema):
    parameters = fields.Nested(ScanCollectSchema.__schema_class__)


class SCCollectArgumentSchema(Schema):
    parameters = fields.Nested(CollectOtherParams.__schema_class__)
    samples = fields.List(fields.Nested(SCCollectSample.__schema_class__))
    buffers = fields.List(fields.Nested(SCCollectBuffer.__schema_class__))


class HPLCCollectArgumentSchema(Schema):
    parameters = fields.Nested(CollectOtherParams.__schema_class__)
    samples = fields.List(fields.Nested(HPLCCollectSample.__schema_class__))
    metadata = fields.Nested(HPLCCMetadata.__schema_class__)


class HPLCEquilibrateArgumentSchema(Schema):
    parameters = fields.Nested(CollectOtherParams.__schema_class__)
    column = fields.Nested(HPLCEquilibrate.__schema_class__)
    metadata = fields.Nested(HPLCCMetadata.__schema_class__)
