"""
Helper to deal logging
"""
import logging

_CACHE = set()


def log_once(logger: logging.Logger, level: int, msg: str, *args, **kwargs):
    """Helper to log a single time.

    This is useful to log warnings which are potentially often be triggered.

    Arguments:
        logger: The logger to log in
        level: The level of the log (i.e. `logging.WARNING`)
        msg: Message template
        args: Arguments for the message
        kwargs: Extra arguments to pass to the logging
    """
    global _CACHE
    if not logger.isEnabledFor(level):
        # Is ignored so with not be displayed anyway
        return

    # Resolve the message to cache the string without object ref
    msg = msg % args
    if msg in _CACHE:
        # Was already displayed
        return

    _CACHE.add(msg)
    logger.log(level, msg, **kwargs)


class LoggingListener(logging.Handler):
    """Context listening a dedicated logger"""

    def __init__(self, logger: logging.Logger = None):
        if logger is None:
            logger = logging.getLogger()
        elif not isinstance(logger, logging.Logger):
            logger = logging.getLogger(logger)
        self.logger = logger

        self.records = []

        super(LoggingListener, self).__init__()

    def __enter__(self):
        """Setup the state of the logger"""
        self.records = []  # Reset recorded LogRecords
        self.logger.addHandler(self)
        self.logger.propagate = False
        # ensure no log message is ignored
        self.entry_level = self.logger.level * 1
        self.logger.setLevel(logging.DEBUG)
        self.entry_disabled = self.logger.disabled
        self.logger.disabled = False
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Restore the previous state of the logger"""
        self.logger.removeHandler(self)
        self.logger.propagate = True
        self.logger.setLevel(self.entry_level)
        self.logger.disabled = self.entry_disabled

    def emit(self, record):
        """Override `logging.Handler.emit`"""
        self.records.append(record)
