import contextlib
import typing
import logging
from .resource_provider import ResourceProvider
from .resource_provider import ResourceNotAvailable


_logger = logging.getLogger(__name__)


class MultiResourceProvider(ResourceProvider):
    """
    Aggregate resource provider together using priority.

    The first added priority have the priority.
    """

    def __init__(self):
        self.__providers = []

    @property
    def providers(self) -> typing.List[ResourceProvider]:
        """Sub providers exposed by this resource provider"""
        return list(self.__providers)

    def __repr__(self):
        r = ",".join([repr(p) for p in self.__providers])
        return f"<MultiFileResourceProvider {r}>"

    def prepend_provider(self, provider: ResourceProvider):
        """Add a new provider with a higher priority than the ones already
        registered."""
        self.__providers.insert(0, provider)

    def get_resource_path(self, resourcetype: str, resourcename: str) -> str:
        """Get the path of a resource from its name from this resource provider."""
        for p in self.__providers:
            try:
                return p.get_resource_path(resourcetype, resourcename)
            except ResourceNotAvailable:
                pass

        raise ResourceNotAvailable(
            f"Resource {resourcetype}/{resourcename} not provided"
        )

    def list_resource_names(
        self, resourcetype: str, pattern: str = "*"
    ) -> typing.List[str]:
        """List resource names by pattern from the resource provider."""
        for p in self.__providers:
            files = p.list_resource_names(resourcetype, pattern)
            if files:
                return files
        return []

    def _create_resource(self, provider, resource):
        return (provider, resource)

    def resource(self, resourcetype: str, resourcename: str) -> object:
        """Returns a resource descriptor

        Arguments:
            resourcetype: subdirectory
            resourcename: filename
        """
        for p in self.__providers:
            try:
                resource = p.resource(resourcetype, resourcename)
                return self._create_resource(p, resource)
            except ResourceNotAvailable:
                pass

        raise ResourceNotAvailable(
            f"Resource {resourcetype}/{resourcename} not provided"
        )

    @contextlib.contextmanager
    def open_resource(self, resource: object, mode="t"):
        """Context manager with the resource as a stream

        Argument:
            resource: Reference to the resource
        """
        if not isinstance(resource, tuple):
            raise TypeError("Tuple expected")
        relative_provider, relative_resource = resource
        with relative_provider.open_resource(relative_resource, mode=mode) as f:
            yield f
