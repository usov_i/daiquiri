import typing
import contextlib
import glob
import logging
import os.path
from .resource_provider import ResourceProvider
from .resource_provider import ResourceNotAvailable


_logger = logging.getLogger(__name__)


class FileResourceProvider(ResourceProvider):
    """Provide resource from file directory."""

    def __init__(self, root):
        self.__root = os.path.realpath(root)

    @property
    def root(self):
        """Root directory of the resource provider"""
        return self.__root

    def __repr__(self):
        return "<FileResourceProvider {self.__root}>"

    def _get_resource_path(self, resourcetype, resourcename):
        """Get the resource directory."""
        return os.path.join(self.__root, *resourcetype.split("."), resourcename)

    def get_resource_path(self, resourcetype: str, resourcename: str) -> str:
        """Get the path of a resource from its name from this resource provider."""
        if resourcename.startswith("/"):
            raise ValueError(
                "Absolute paths are not supported. Found '%s'" % resourcename
            )
        fpath = self._get_resource_path(resourcetype, resourcename)
        if not os.path.exists(fpath):
            raise ResourceNotAvailable(
                f"Resource {resourcetype}/{resourcename} not provided"
            )
        return fpath

    def list_resource_names(
        self, resourcetype: str, pattern: str = "*"
    ) -> typing.List[str]:
        """List resource names by pattern from the resource provider."""
        files = glob.glob(self._get_resource_path(resourcetype, pattern))
        prefix_size = len(self.__root) + 1 + len(resourcetype) + 1
        files = [f[prefix_size:] for f in files]
        return files

    def resource(self, resourcetype: str, resourcename: str) -> object:
        """Returns a resource descriptor

        Arguments:
            resourcetype: subdirectory
            resourcename: filename
        """
        return self.get_resource_path(resourcetype, resourcename)

    @contextlib.contextmanager
    def open_resource(self, resource: object, mode="t"):
        """Context manager with the resource as a stream

        Argument:
            resource: Reference to the resource
        """
        _logger.info("Using resource '%s'", resource)
        mode = {"t": "rt", "b": "rb", "wt": "wt", "wb": "wb"}[mode]
        with open(resource, mode) as f:
            yield f
