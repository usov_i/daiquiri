import typing
import contextlib
import abc


class ResourceNotAvailable(RuntimeError):
    pass


class ResourceProvider(abc.ABC):
    @abc.abstractmethod
    def get_resource_path(self, resourcetype: str, resourcename: str) -> str:
        """Get the path of a resource from its name from this resource provider.

        Arguments:
            resourcetype: subdirectory
            resourcename: filename

        Raises:
            ResourceNotAvailable: resource does not exist
        """
        raise NotImplementedError

    @abc.abstractmethod
    def list_resource_names(
        self, resourcetype: str, pattern: str = "*"
    ) -> typing.List[str]:
        """List resource names by pattern from the resource provider."""
        raise NotImplementedError

    @abc.abstractmethod
    def resource(self, resourcetype: str, resourcename: str) -> object:
        """Returns a resource descriptor

        Arguments:
            resourcetype: subdirectory
            resourcename: filename
        """
        raise NotImplementedError

    def abs_resource(self, resource_name: str):
        """Returns a resource descriptor from the absolute path.

        Arguments:
            resourcename: filename
        """
        elements = resource_name.split("/", 1)
        if len(elements) == 1:
            return self.resource("", elements[0])
        return self.resource(elements[0], elements[1])

    @contextlib.contextmanager
    @abc.abstractmethod
    def open_resource(self, resource: object, mode="t"):
        """Context manager with the resource as a stream

        Argument:
            resource: Reference to the resource
            mode: 't' or 'b' for text or binary
        """
        raise NotImplementedError
