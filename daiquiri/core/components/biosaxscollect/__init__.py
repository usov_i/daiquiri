#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import os
import sys

from astropy.io import ascii
from astropy.table import Table

from pymemcache.client.base import Client

from marshmallow import fields
from daiquiri.core import marshal, require_control
from daiquiri.core.components import Component, ComponentResource, actor
from daiquiri.core.schema.biosaxscollect.biosaxs import (
    SamplesDoneSchema,
    DataTestArgumentSchema,
    ProcessingParametersSchema,
    SCCollectSample,
    SCCollectBuffer,
    HPLCCollectSample,
    ScanCollectArgumentSchema,
    SingleCollectArgumentSchema,
)

from daiquiri.core.hardware.bliss.session import single_collect, scan_collect

from daiquiri.core.logging import log

from bliss import current_session
from bliss.common import plot
import json

import logging

logger = logging.getLogger(__name__)


def mix_samples_and_buffers(samples, buffers):
    mix = []
    previous_after_buffer_name = None
    for sample in samples:
        _buffer = next((b for b in buffers if b.name == sample.buffer_name), None)

        sample.file_path = "sample_" + sample.name
        sample.do_collect = True
        sample.experiment_type = "sc"

        if sample.buffer_mode in ["Before", "Before & After"]:
            # Only collect the buffer if its different from the
            # last buffer collected. Simply dont collect the same
            # buffer twice in a row
            if previous_after_buffer_name != _buffer.name:
                temp_buffer = copy.deepcopy(_buffer)
                temp_buffer.id = (
                    "bid-" + temp_buffer.id + "-sid-" + sample.id + "-before"
                )
                temp_buffer.file_path = "buffer_before_" + sample.name
                temp_buffer.do_collect = True
                mix.append(temp_buffer)
            else:
                # This buffer is for Substract not to be collect twice
                temp_buffer = copy.deepcopy(_buffer)
                if len(samples) > samples.index(sample) + 1:
                    next_sample = samples[samples.index(sample) + 1]
                elif len(samples) == samples.index(sample) + 1:
                    next_sample = samples[samples.index(sample)]

                temp_buffer.id = (
                    "bid-" + temp_buffer.id + "-sid-" + next_sample.id + "-before"
                )
                temp_buffer.file_path = "buffer_before_" + next_sample.name
                temp_buffer.do_collect = False
                mix.append(temp_buffer)

            previous_after_buffer_name = None

        mix.append(sample)

        if sample.buffer_mode in ["After", "Before & After"]:
            temp_buffer = copy.deepcopy(_buffer)
            temp_buffer.id = "bid-" + temp_buffer.id + "-sid-" + sample.id + "-after"
            temp_buffer.file_path = "buffer_after_" + sample.name
            temp_buffer.do_collect = True
            mix.append(temp_buffer)
            previous_after_buffer_name = _buffer.name

        if (
            sample.buffer_mode in ["Before & After"]
            and len(samples) < samples.index(sample) + 1
        ):
            next_sample = samples[
                samples.index(sample) + 1
            ]  # second conditions is to avoid when there is no next sample
            mix[-1].file_path = (
                "buffer_after_" + sample.name + "_and_buffer_before_" + next_sample.name
            )

    return mix


def add_params_to_hplc_sample(samples):
    to_be_collect = []
    for sample in samples:
        sample.do_collect = True
        sample.experiment_type = "hplc"
        to_be_collect.append(sample)
    return to_be_collect


def add_params_to_hplc_equilibrate(column):
    column.do_collect = True
    column.experiment_type = "equilibrate"
    return column


class AddSCCollectResource(ComponentResource):
    @require_control
    @actor("sccollect", enqueue=True, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = mix_samples_and_buffers(
            kwargs["samples"], kwargs["buffers"]
        )
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_scan_done_cb"] = self._parent.update_sample_scan_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["update_current_sample_cb"] = self._parent.update_current_sample_cb
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class RunSCCollectResource(ComponentResource):
    @require_control
    @actor("sccollect", enqueue=False, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = mix_samples_and_buffers(
            kwargs["samples"], kwargs["buffers"]
        )
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_scan_done_cb"] = self._parent.update_sample_scan_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["update_current_sample_cb"] = self._parent.update_current_sample_cb
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class AddHPLCCollectResource(ComponentResource):
    @require_control
    @actor("hplccollect", enqueue=True, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = add_params_to_hplc_sample(kwargs["samples"])
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_scan_done_cb"] = self._parent.update_sample_scan_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["update_current_sample_cb"] = self._parent.update_current_sample_cb
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class RunHPLCCollectResource(ComponentResource):
    @require_control
    @actor("hplccollect", enqueue=False, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = add_params_to_hplc_sample(kwargs["samples"])
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_scan_done_cb"] = self._parent.update_sample_scan_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["update_current_sample_cb"] = self._parent.update_current_sample_cb
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class AddEquilibrateResource(ComponentResource):
    @require_control
    @actor("hplcequilibrate", enqueue=True, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = add_params_to_hplc_equilibrate(kwargs["column"])
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["update_current_sample_cb"] = self._parent.update_current_sample_cb
        kwargs["save_metadata_to_ascii"] = self._parent.save_metadata_to_ascii
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class RunEquilibrateResource(ComponentResource):
    @require_control
    @actor("hplcequilibrate", enqueue=False, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = add_params_to_hplc_equilibrate(kwargs["column"])
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["save_metadata_to_ascii"] = self._parent.save_metadata_to_ascii
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class AddSingleCollectResource(ComponentResource):
    @require_control
    @actor("singlecollect", enqueue=True, preprocess=True)
    def post(self, *args, **kwargs):
        return kwargs

    def preprocess(self, *args, **kwargs):
        kwargs["collect_parameters"] = kwargs["parameters"]
        kwargs["collect_started_cb"] = self._parent.collect_started_cb
        kwargs["update_sample_done_cb"] = self._parent.update_sample_done_cb
        kwargs["update_sample_scan_done_cb"] = self._parent.update_sample_scan_done_cb
        kwargs["update_sample_failed_cb"] = self._parent.update_sample_failed_cb
        kwargs["update_current_sample_cb"] = self._parent.update_current_sample_cb
        kwargs["update_client_logger_cb"] = self._parent.update_client_logger_cb

        return kwargs


class RunSingleCollectResource(ComponentResource):
    @require_control
    @marshal(inp=SingleCollectArgumentSchema)
    def post(self, *args, **kwargs):
        single_collect.collect(
            kwargs["parameters"],
            callbacks={
                "collect_started_cb": self._parent.collect_started_cb,
                "update_sample_done_cb": self._parent.update_sample_done_cb,
                "update_client_logger_cb": self._parent.update_client_logger_cb,
            },
        )

        return kwargs


class RunScanCollectResource(ComponentResource):
    @require_control
    @marshal(inp=ScanCollectArgumentSchema)
    def post(self, *args, **kwargs):
        if kwargs["parameters"].collect:
            # from daiquiri.core.hardware.bliss.session import scan_collect
            scan_collect.collect(
                kwargs["parameters"],
                callbacks={
                    "collect_started_cb": self._parent.collect_started_cb,
                    "update_client_logger_cb": self._parent.update_client_logger_cb,
                },
            )
        else:
            scan_collect._collect(kwargs["parameters"])
        return kwargs


class SamplesDoneResource(ComponentResource):
    @require_control
    @marshal(out=[[200, SamplesDoneSchema(), "List of sample IDs"]])
    def get(self):
        """
        Get information about Finished & Failed samples, list of ids of the samples
        that have been collected and the id of the current sample
        """
        return self._parent.get_samples_done(), 200


class GetTestDataResource(ComponentResource):
    @require_control
    @marshal(out=[[200, DataTestArgumentSchema(), "Test samples and buffers"]])
    def get(self):
        """
        Get test samples and buffers
        """
        return self._parent.get_test_samples(), 200


class ProcessingParametersResource(ComponentResource):
    @require_control
    @marshal(out=[[200, ProcessingParametersSchema(), "processing  parameters"]])
    def get(self):
        """
        Get processing  parameters
        """
        return self._parent.get_processing_parameters(), 200

    @require_control
    @marshal(inp={"processing_parameters": fields.Nested(ProcessingParametersSchema())})
    def post(self, processing_parameters):
        """Set processing_parameters <processing_parameters>"""
        success = False
        try:
            success = self._parent.set_processing_parameters(processing_parameters)
        except Exception:
            return {"error": "Could not Set processing_parameters"}, 404

        if success:
            return {"Status": "OK"}, 200
        else:
            return {"error": "Could not Set processing_parameters"}, 404


# Update saving proposal after login
#  THis should be move to the sessions page (to be call after login resolved)
class SetCollectDataPathProposalResource(ComponentResource):
    @require_control
    @marshal(
        inp={
            "proposal": fields.Str(required=True, metadata={"title": "login proposal"})
        }
    )
    def post(self, proposal):
        scan_saving = current_session.scan_saving
        try:
            scan_saving.newproposal(proposal)
        except Exception:
            print("Could not change to new Proposal")
        else:
            print("new Proposal change properly")
        return proposal


#  PEtra really wanted to have to possiblity to open FLint from Bsx3 GUI
class OpenFlintResource(ComponentResource):
    @require_control
    def post(self):
        try:
            _flint = plot.get_flint(creation_allowed=False)
            if _flint:
                return {"Status": "OK, BLiss FLint already Opened"}, 200
            else:
                _flint = plot.get_flint()
                return {"Status": "OK, Openning BLiss FLint"}, 200
        except Exception:
            return {"error": "Could not Open Bliss FLint"}, 404


"""
BIOSaxs processing

yml configuration example:
   name: biosaxs_collect
   class: Biosaxscollect
   package: mx.processing
   uri: "tango://nela:20000/DAU/dahu/3"
   template_file_directory: Direcotry from where template files are copied
"""


class Biosaxscollect(Component):
    _actors = ["hplccollect", "sccollect", "hplcequilibrate", "scancollect"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._samples_done = []
        self._samples_failed = []
        self._current_sample = {}
        self._job_output = {}
        self.temp_job_output = []
        self.temp_memchache_data = []
        self.collect_progress = 0
        self.current_exp_name = ""
        self.single_collect_running = True
        self.memcache_client = Client(self._config.get("memcache_client"))
        self.proc_template_file = self._config.get("proc_template_file")
        self.proc_template_file_directory = self._config.get(
            "proc_template_file_directory"
        )

    def setup(self):
        self.register_actor_route(RunSingleCollectResource, "/run-single-collect")

        self.register_actor_route(AddSCCollectResource, "/add-sc-collect")
        self.register_actor_route(RunSCCollectResource, "/run-sc-collect")

        self.register_actor_route(AddHPLCCollectResource, "/add-hplc-collect")
        self.register_actor_route(RunHPLCCollectResource, "/run-hplc-collect")

        self.register_actor_route(AddEquilibrateResource, "/add-hplc-equilibrate")
        self.register_actor_route(RunEquilibrateResource, "/run-hplc-equilibrate")

        self.register_actor_route(SamplesDoneResource, "/samples-done")
        self.register_actor_route(GetTestDataResource, "/get-test-data")

        #  THis should be move to the sessions page (to be call after login resolved)
        self.register_actor_route(
            SetCollectDataPathProposalResource, "/data-path-proposal"
        )

        self.register_actor_route(
            ProcessingParametersResource, "/processing-parameters"
        )

        self.register_actor_route(OpenFlintResource, "/open-flint")

        self.register_actor_route(RunScanCollectResource, "/run-scan-collect")

    def _emit_process_status(self):
        print("SAMPLES DONE %s" % self._samples_done)
        self.emit(
            "sample_done",
            {
                "finished": self._samples_done,
                "failed": self._samples_failed,
                "current": self._current_sample,
                "collect_progress": self.collect_progress,
                "job_output": self._job_output,
                "single_collect": self.single_collect_running,
            },
            namespace="/biosaxscollect",
        )

    def collect_started_cb(self, parameters):
        self.current_exp_name = parameters.exp_name
        self._job_output = {}
        self._job_output["jobs"] = []
        self._job_output["memcached"] = []

        if parameters.experiment_type == "sc":
            self._metadata.create_new_sc_experiment(self.current_exp_name)

        elif parameters.experiment_type == "hplc":
            self._metadata.create_hplc_experiment(self.current_exp_name)

        elif parameters.experiment_type == "single_collect":
            self.single_collect_running = True
        self._emit_process_status()
        print("NEW EXPERIMENT CREATED WITH NAME: %s" % self.current_exp_name)

    def update_sample_scan_done_cb(self, sample, run_number):
        # Save sample information to LIMS (ISPyB)
        measurement_info = {}
        try:
            if isinstance(sample, SCCollectSample) or isinstance(
                sample, SCCollectBuffer
            ):
                measurement_info = self._metadata.store_sc_measurement(
                    sample, run_number
                )
            elif isinstance(sample, HPLCCollectSample):
                measurement_info = self._metadata.store_hplc_frames(run_number)
        except Exception as ex:
            print(f"Error on storing data to Ispyb {ex}")
            return measurement_info

        return measurement_info

    def update_sample_done_cb(self, sample, job_output):
        self._samples_done.append(sample.id)
        if job_output is not None:
            self._job_output["exp_name"] = self.current_exp_name
            if "SCCollect" in type(sample).__name__:
                memchache_data = {}
                self._job_output["type"] = "sc"
                self.temp_job_output.append(job_output)
                if "memcached" in job_output:
                    for key, value in job_output["memcached"].items():
                        print(key, "->", value)
                        memchache_data[key] = self.get_memcache_data(value)
                    memchache_data["frame_name"] = sample.name
                    memchache_data["job_title"] = sample.file_path
                    memchache_data["job_name"] = job_output["job_name"]
                    self.temp_memchache_data.append(memchache_data)

                if job_output["job_name"] == f"Subtract {sample.name}":
                    self._job_output["jobs"].append(self.temp_job_output)
                    self._job_output["memcached"].append(self.temp_memchache_data)
                    self.temp_job_output = []
                    self.temp_memchache_data = []
                    self.collect_progress = self.collect_progress + 5

            elif "HPLCCollect" in type(sample).__name__:
                self._job_output["type"] = "hplc"
                self._job_output["jobs"] = job_output
                self.collect_progress = self.collect_progress + 5

        if job_output is None and self.temp_job_output is not None:
            self._job_output["jobs"].append(self.temp_job_output)
            self._job_output["memcached"].append(self.temp_memchache_data)
            self.temp_job_output = []
            self.temp_memchache_data = []
            self.collect_progress = self.collect_progress + 5

        self.single_collect_running = False
        self._emit_process_status()

    def update_client_logger_cb(self, level, typ, message):
        if level == "info":
            log.get("user").info(message, type=typ)
        elif level == "exception":
            log.get("user").exception(message, type=typ)
        elif level == "warning":
            log.get("user").warning(message, type=typ)

    def get_memcache_data(self, key):
        res = None
        if key and self.memcache_client:
            cash = self.memcache_client.get(key)
            if cash:
                res = json.loads(cash)
        return res

    def update_sample_failed_cb(self, sample, items=[]):
        self.read_uv_state = False
        self._samples_failed.append(sample.id)
        self._emit_process_status()

    def update_current_sample_cb(self, sample, collect_progress, items=[]):
        self._current_sample = sample
        self.collect_progress = collect_progress - 5
        self._emit_process_status()

    def actor_success(self, *args, **kwargs):
        self._samples_done = []
        self._samples_failed = []
        self._current_sample = {}
        self.collect_progress = 0
        self._emit_process_status()

    def actor_started(self, *args, **kwargs):
        # THis need to be Check
        pass

    def get_samples_done(self):
        # self._emit_process_status()
        return {
            "samples": self._samples_done,
            "failed": self._samples_failed,
            "current": self._current_sample,
            "collect_progress": self.collect_progress,
            "job_output": self._job_output,
        }

    def save_metadata_to_ascii(self, root_path, fname, metadata, column, parameters):
        # import pdb; pdb.set_trace()
        try:
            if not os.path.exists(root_path):
                # import pdb; pdb.set_trace()
                sub_root_path = os.path.dirname(root_path)
                if not os.path.exists(sub_root_path):
                    os.mkdir(sub_root_path)
                os.mkdir(root_path)

            data = Table()

            for key, value in metadata.items():
                data[key] = [value]

            for key, value in column.items():
                data[key] = [value]

            for key, value in parameters.items():
                data[key] = [value]

            ascii.write(
                data,
                f"{root_path}/{fname}.dat",
                format="fixed_width",
                bookend=False,
                overwrite=True,
            )
            return True
        except Exception as ex:
            print(ex.reason)
            return False

    def get_test_samples(self):
        data = "{}"
        try:
            with open(
                os.path.join(sys.path[0], self._config.get("data_test")), "r"
            ) as json_file:
                data = json.load(json_file)
        except Exception as ex:
            print(f"Data could not load because of {ex}")
        return data

    def get_processing_parameters(self):
        _data = {}
        log.get("user").info("Getting processing parameters", type="app")
        if os.path.exists(self.proc_template_file):
            try:
                with open(self.proc_template_file) as f:
                    _data = json.load(f)
                    _data["files_root"] = self.proc_template_file_directory
            except Exception as ex:
                print(f"processing parameters could not load because of {ex}")
                log.get("user").exception(
                    f"Error while Getting processing parameters : {ex})", type="app"
                )
        else:
            log.get("user").error(
                f"template file does not existe in {self.proc_template_file_directory}",
                type="app",
            )

        return _data

    def set_processing_parameters(self, processing_parameters):
        poni_file = (
            self.proc_template_file_directory + processing_parameters["poni_file"]
        )
        mask_file = (
            self.proc_template_file_directory + processing_parameters["mask_file"]
        )

        if os.path.exists(poni_file) and os.path.exists(mask_file):
            try:
                with open(self.proc_template_file, "r") as f:
                    _data = json.load(f)
                    _data["poni_file"] = poni_file
                    _data["mask_file"] = mask_file
                    _data["fidelity_abs"] = processing_parameters["fidelity_abs"]
                    _data["fidelity_rel"] = processing_parameters["fidelity_rel"]
                    _data["normalization_factor"] = processing_parameters[
                        "normalization_factor"
                    ]

                with open(self.proc_template_file, "w") as fi:
                    data = json.dumps(_data, indent=4, sort_keys=False)
                    fi.write(data)
                log.get("user").info(
                    "processing parameters changed successfuly", type="app"
                )
            except Exception as ex:
                print(f"processing parameters could not load because of {ex}")
                log.get("user").exception(
                    f"Error while Changing processing parameters : {ex})", type="app"
                )
            return True
        else:
            print(
                f"poni File or Mask file does not existe in {self.proc_template_file_directory}"
            )
            log.get("user").error(
                f"poni File or Mask file does not existe in {self.proc_template_file_directory}",
                type="app",
            )
            return False
