#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
import time

import pexpect

from daiquiri.core.components import Component


import logging

logger = logging.getLogger(__name__)


class SocketFileObject:
    """Mock file object that writes to socket.io"""

    def __init__(self, emit):
        self.emit = emit

    def open(self, *args, **kwargs):
        pass

    def flush(self, *args, **kwargs):
        pass

    def write(self, line):
        self.emit("output", {"output": line})


class Console(Component):
    """Run a shell command and interact with it

    Background:
        https://stackoverflow.com/questions/48359324/controlling-less-with-popen
        https://github.com/cs01/pyxterm.js
        https://pexpect.readthedocs.io/en/stable/index.html
    """

    _namespace = "pty"

    def setup(self):
        self.on("input")(self.input)
        self.on("resize")(self.set_winsize)
        self.start_console()

    def check_user(self):
        self._metadata.get_user()

    def has_control(self):
        sessionid = self._session._session_from_sio("pty")
        return self._session._storage.has_control(sessionid)

    def set_winsize(self, payload):
        self._console.setwinsize(payload["rows"], payload["cols"])

    def start_console(self):
        self._console = pexpect.spawn(
            "/bin/bash", ["-c", self._config["command"]], encoding="utf-8"
        )
        self._console.logfile_read = SocketFileObject(self.emit)
        gevent.spawn(self.read_console)

    def input(self, payload):
        if self.has_control():
            self._console.send(payload["input"])

        else:
            sessionid = self._session._session_from_sio("pty")
            logger.warning(
                f"Session {sessionid} sending console input but does not have control"
            )

        return True

    def read_console(self):
        while True:
            try:
                self._console.read_nonblocking(size=1024, timeout=0.1)
            except pexpect.exceptions.TIMEOUT:
                pass

            time.sleep(0.02)
