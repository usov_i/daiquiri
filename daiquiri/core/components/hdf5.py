#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import h5py
from marshmallow import fields
import numpy

from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.components.hdf5 import RootHdf5Schema, Hdf5Schema
from daiquiri.core.utils import make_json_safe, worker
from daiquiri.core.responses import gzipped

logger = logging.getLogger(__name__)


ids = {
    "autoprocprogramid": fields.Int(
        metadata={"description": "Auto processing program id"}
    ),
    "autoprocprogramattachmentid": fields.Int(
        metadata={"description": "Auto processing program attachment id"}
    ),
    "datacollectionid": fields.Int(metadata={"description": "Data collection id"}),
    "type": fields.Str(metadata={"enum": ["processing"]}),
}


class ContentsResource(ComponentResource):
    @marshal(
        inp=ids,
        out=[
            [200, RootHdf5Schema(), "Get hdf5 file contents"],
            [400, ErrorSchema(), "Could not find hdf5 file"],
        ],
    )
    def get(self, **kwargs):
        """Get the contents of an hdf5 file"""
        contents = self._parent.get_hdf5(**kwargs)
        if contents:

            def gzip():
                return gzipped(contents)

            return worker(gzip)

        return {"error": "Could not find hdf5 file"}, 400


class GroupResource(ComponentResource):
    @marshal(
        inp=ids,
        out=[
            [200, Hdf5Schema(), "Get hdf5 group contents, including data"],
            [400, ErrorSchema(), "Could not find hdf5 group"],
        ],
    )
    def get(self, path, **kwargs):
        """Get a group and its data from a hdf5 file"""
        group = self._parent.get_group(path, **kwargs)
        if group:

            def gzip():
                return gzipped(group)

            return worker(gzip)

        return {"error": "Could not find hdf5 group"}, 400


def hdf5_to_dict(filename: str, path: str = None, load_data: bool = False) -> dict:
    """
    Dump a HDF5 into an arborecent python dict structure

    Arguments:
        filename:Location of the HDF5 file
        path: Path of the HDF5 object to dump from the file
        load_data: If true datasets are dumped with the data, else the data is
                   set to None.

    Returns:
        None: If the file was not reachable, or the path not reachable.
    """

    def _get_data(dataset: h5py.Dataset, load_data: bool):
        dtype = dataset.dtype
        if dtype == "object":
            type_class = dataset.id.get_type().get_class()
            if type_class == h5py.h5t.STRING:
                dtype = "string"
            else:
                raise RuntimeError(
                    "HDF5 dataset type %s unsupposed for now" % type_class
                )

        if not load_data:
            return None, dtype

        # TODO: Optimise for big datasets
        if dtype == "string":
            data = dataset.asstr()[()]
        else:
            data = dataset[()]
        return data, dtype

    def _get_dataset(h5file, path, load_data=False):
        """Retrieve a dataset

        TODO: This will need sensible slicing options in the future

        Args:
            h5file (h5py.File): The h5 file instance
            path (str): uri to the dataset
            load_data (bool): Whether to load the data

        Returns:
            dataset (dict): Dict of the dataset
        """
        dataset = h5file[path]

        data, dtype = _get_data(dataset, load_data)
        return {
            "type": "dataset",
            "data": numpy.nan_to_num(data, posinf=1e200, neginf=-1e200),
            "attrs": {attr: dataset.attrs[attr] for attr in dataset.attrs},
            "shape": dataset.shape,
            "size": dataset.size,
            "ndim": dataset.ndim,
            "dtype": dtype,
            "name": os.path.basename(dataset.name),
        }

    def _get_groups_and_attrs(
        h5file: h5py.File, path: str = "/", load_data: bool = False
    ):
        """Retrieve a group

        Args:
            h5file: The h5 file instance
            path: uri to the dataset
            load_data: Whether to load the data

        Returns:
            group (dict): Dict of the group
        """
        if path not in h5file:
            logger.error("Hdf5 path '%s' not found", path)
            return None
        group = h5file[path]
        if not isinstance(group, h5py.Group):
            logger.error("Hdf5 path '%s' is not a group", path)
            return None

        children = {}
        for node in group:
            node_path = f"{path}/{node}"
            if isinstance(h5file[node_path], h5py.Dataset):
                child = _get_dataset(h5file, node_path, load_data=load_data)
            elif isinstance(h5file[node_path], h5py.Group):
                child = _get_groups_and_attrs(h5file, node_path, load_data=load_data)
            else:
                raise TypeError(f"Unsupported entity at {node_path}")

            children[child["name"]] = child

        return {
            "type": "group",
            "children": children,
            "name": os.path.basename(group.name),
            "uri": group.name,
            "attrs": {**group.attrs},
        }

    try:
        os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
        with h5py.File(filename, mode="r") as h5file:
            content = _get_groups_and_attrs(h5file, path=path, load_data=load_data)
            if path is None and content:
                content["file"] = os.path.basename(filename)
            return make_json_safe(content)
    except OSError:
        logger.error(f"No such file {filename}")
        return None


class Hdf5(Component):
    """Generic HDF5 Component

    A component that can read hdf5 files and return json slices
    of data.

    Currently can get files from an
        autoprocprogramid (first rank file)
        autoprocprogramattachmentid
        datacollectionid

    May have other sources in future
    """

    def setup(self, *args, **kwargs):
        self.register_route(ContentsResource, "")
        self.register_route(GroupResource, "/groups/<path:path>")

    def _file_from_app(self, autoprocprogramid):
        appas = self._metadata.get_autoprocprogram_attachments(
            autoprocprogramid=autoprocprogramid
        )

        rank = 9999
        minr = None
        for app in appas["rows"]:
            app_rank = app["rank"]
            if app_rank is None or app_rank < rank:
                ext = os.path.splitext(app["filename"])[1][1:].strip().lower()
                if app["filetype"] == "Result" and ext in ["h5", "hdf5", "nxs"]:
                    if app_rank is not None:
                        rank = app_rank
                    minr = app

        if minr:
            return os.path.join(minr["filepath"], minr["filename"])

    def _get_file(
        self,
        datacollectionid=None,
        autoprocprogramattachmentid=None,
        autoprocprogramid=None,
        type=None,
        **kwargs,
    ):
        """Find the file relevant for the request"""

        # From autoprocprogramid => lowest rank
        if autoprocprogramid is not None:
            return self._file_from_app(autoprocprogramid)

        #  Directly from autoprocprogramattachmentid
        elif autoprocprogramattachmentid is not None:
            appa = self._metadata.get_autoprocprogram_attachments(
                autoprocprogramattachmentid=autoprocprogramattachmentid
            )
            if appa:
                ext = os.path.splitext(appa["filename"])[1][1:].strip().lower()
                if appa["filetype"] == "Result" and ext in ["h5", "hdf5"]:
                    return appa["filefullpath"]

        # From datacollectionid, taking latest related autoprocprogram and lowest
        # rank attachment
        elif datacollectionid is not None and type == "processing":
            apps = self._metadata.get_autoprocprograms(
                datacollectionid=datacollectionid
            )
            logger.debug("Result: %s", apps)
            if apps["total"]:
                autoprocprogramid = apps["rows"][-1]["autoprocprogramid"]
                return self._file_from_app(autoprocprogramid)

        # Direct datacollection hdf5
        elif datacollectionid is not None:
            dc = self._metadata.get_datacollections(datacollectionid=datacollectionid)
            if dc:
                return os.path.join(dc["imagedirectory"], dc["filetemplate"])

    def get_hdf5(self, **kwargs):
        file = self._get_file(**kwargs)
        if not file:
            return None
        return hdf5_to_dict(file, load_data=False)

    def get_group(self, path, **kwargs):
        file = self._get_file(**kwargs)
        if not file:
            return None
        return hdf5_to_dict(file, path, load_data=True)
