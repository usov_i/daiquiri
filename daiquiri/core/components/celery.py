#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import logging
import os
import time
from typing import List, Dict, Union, Any
import uuid

import gevent

from daiquiri.core import marshal
from daiquiri.core.components import (
    actor,
    Component,
    ComponentResource,
    ComponentActor,
    ComponentActorSchema,
)
from daiquiri.core.schema import ErrorSchema, MessageSchema
from daiquiri.core.utils import get_start_end
from daiquiri.core.schema.celery import (
    CeleryConfigSchema,
    TaskConfigSchema,
    WorkersSchema,
    ExecutedTasksSchema,
)
from daiquiri.core.schema.metadata import paginated

from celery import Celery as CeleryApp
from celery.result import AsyncResult
from kombu import Queue, Connection

try:
    from ewoksjob.events.readers import instantiate_reader
except ImportError:
    instantiate_reader = None

try:
    from blissdata.beacon.files import read_config
except ImportError:
    read_config = None


logger = logging.getLogger(__name__)


class TasksResource(ComponentResource):
    @marshal(out=[[200, paginated(TaskConfigSchema), "List of available tasks"]])
    def get(self):
        """Get a list of available tasks"""
        tasks = self._parent.get_tasks()
        return {"total": len(tasks), "rows": tasks}


class ExecutedTasksResource(ComponentResource):
    @marshal(
        out=[[200, paginated(ExecutedTasksSchema), "List of executed tasks"]],
        paged=True,
    )
    def get(self, **kwargs):
        """Get a list of executed tasks"""
        return self._parent.get_executed_tasks(**kwargs)


class ExecutedTaskResource(ComponentResource):
    @marshal(
        out=[
            [200, ExecutedTasksSchema, "Details of executed task"],
            [404, ErrorSchema, "No such task"],
        ]
    )
    def get(self, job_id):
        """Get details of an executed tasks"""
        task = self._parent.get_task_info(job_id, get_result=True)
        if task:
            return task
        else:
            return {"error": "No such task"}, 404


class WorkersResource(ComponentResource):
    @marshal(
        out=[
            [200, paginated(WorkersSchema), "List of workers status and their tasks"],
        ],
    )
    def get(self):
        """Get a list of workers and their tasks"""
        workers = self._parent.get_workers()
        return {"total": len(workers), "rows": workers}


class RevokeTaskResource(ComponentResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Task successfully revoked"],
            [400, ErrorSchema(), "Could not revoke task"],
        ],
    )
    def delete(self, task_id: str):
        """Revoke and terminate a running task"""
        try:
            self._parent.revoke(task_id)
            return {"message": f"Task id `{task_id}` revoked"}, 200
        except Exception as e:
            return {"error": f"Could not revoke task id `{task_id}`: {str(e)}"}, 400


class Celery(Component):
    """Monitor and execute celery tasks

    https://docs.celeryq.dev/en/master/userguide/workers.html#inspecting-workers
    """

    _config_schema = CeleryConfigSchema()
    _last_emit_time = 0
    _emit_timeout = None

    def setup(self, *args, **kwargs):
        self._tasks = {}

        self.register_route(WorkersResource, "/workers")
        self.register_route(RevokeTaskResource, "/revoke/<string:task_id>")
        self.register_route(TasksResource, "/tasks")
        self.register_route(ExecutedTasksResource, "/tasks/executed")
        self.register_route(ExecutedTaskResource, "/tasks/executed/<string:job_id>")

        self._celery_app = self._create_celery_app(self._config)
        self._default_queue = None
        if self._config.get("broker_dlq"):
            self._default_queue = Queue(
                self._config["broker_queue"],
                queue_arguments={
                    "x-dead-letter-exchange": "dlq",
                    "x-dead-letter-routing-key": f"dlq.{self._config['broker_queue']}",
                },
            )

        for task in self._config["tasks"]:
            if not self._config.get("actors"):
                self._config["actors"] = {}

            self._config["actors"][task["actor"]] = task["actor"]

            if task["actor"] in self._actors:
                continue

            self._actors.append(task["actor"])

            def preprocess(self, *args, **kwargs):
                kwargs["send_task"] = self._parent.send_task
                kwargs["celery_app"] = self._parent._celery_app
                return kwargs

            def post(self, *args, **kwargs):
                pass

            task_res = type(
                task["actor"],
                (ComponentResource,),
                {
                    "post": actor(task["actor"], preprocess=True, synchronous=True)(
                        post
                    ),
                    "preprocess": preprocess,
                },
            )
            self.register_actor_route(task_res, f"/tasks/{task['actor']}")

        if self._config.get("monitor"):
            gevent.spawn(self._enable_events)
            gevent.spawn(self._monitor_events)

        if self._config.get("beamline_queue"):
            gevent.spawn(self._subscribe_to_queue)

        self._ewoks_events_reader = None
        if instantiate_reader:
            if not self._config.get("ewoks_events_backend_url"):
                raise AttributeError(
                    "`ewoks_events_backend_url` must be defined in the celery config to enable event reading"
                )

            logger.info(
                f"Instantiating ewoks events reader on `{self._config['ewoks_events_backend_url']}`"
            )
            self._ewoks_events_reader = instantiate_reader(
                self._config["ewoks_events_backend_url"]
            )

    def _create_celery_app(self, config):
        if "ewoks_config" in config:
            if read_config is None:
                raise RuntimeError(
                    "`ewoks_config` defined but blissdata is not installed in this python env"
                )
            config_url = config["ewoks_config"]
            ewoks_config = read_config(config_url)
            celery_config = ewoks_config.get("celery", None)
            if celery_config is None:
                raise RuntimeError(f"`celery` config not found from `{config_url}`")
            broker = celery_config["broker_url"]
            backend = celery_config["result_backend"]
        else:
            broker = os.environ.get("CELERY_BROKER_URL", config["broker_url"])
            backend = os.environ.get("CELERY_BACKEND_URL", config["backend_url"])

        app = CeleryApp("tasks", broker=broker, backend=backend)
        return app

    def _subscribe_to_queue(self):
        """Subscribe to a beamline specific queue

        Allows daiquiri to receieve notifications from on-going task execution
        """
        with Connection(self._celery_app.conf.broker_url) as conn:
            logger.info(f"Subscribing to queue {self._config['beamline_queue']}")
            queue = Queue(
                f"notification.{self._config['beamline_queue']}",
                exchange="notification",
                durable=False,
            )
            while True:
                with conn.SimpleBuffer(queue) as simple_queue:
                    message = simple_queue.get(block=True)
                    logger.info(f"Received: {message.payload}")
                    self._queue_emit_message(message.headers, message.payload)
                    message.ack()

    def _queue_emit_message(self, headers, message):
        """Debounce event emission

        Try to not spam client
        """
        if self._emit_timeout is not None:
            self._emit_timeout.kill()
            self._emit_timeout = None

        now = time.time()
        if now - self._last_emit_time > 0.2:
            self._emit_message(headers, message)
        else:
            self._emit_timeout = gevent.spawn_later(
                0.2, self._emit_message, headers, message
            )

    def _emit_message(self, headers, message):
        self.emit("message", message)
        self._last_emit_time = time.time()

    def get_tasks(self) -> List[str]:
        return self._config["tasks"]

    def _enable_events(self) -> None:
        logger.info("Starting periodic enabling of celery worker events")
        while True:
            self._celery_app.control.enable_events()
            time.sleep(5)

    def _on_event(self, event: dict) -> None:
        # https://github.com/mher/flower/blob/master/flower/events.py#L65
        if event["type"].startswith("task"):
            if event["uuid"] not in self._tasks:
                self._tasks[event["uuid"]] = {
                    "args": event.get("args"),
                    "kwargs": event.get("kwargs"),
                    "name": event.get("name"),
                }

            if event["type"] == "task-received":
                self._tasks[event["uuid"]]["received"] = datetime.fromtimestamp(
                    event["timestamp"]
                )

            if event["type"] == "task-started":
                self._tasks[event["uuid"]]["started"] = datetime.fromtimestamp(
                    event["timestamp"]
                )

            if event["type"] == "task-succeeded" or event["type"] == "task-failed":
                self._tasks[event["uuid"]]["finished"] = datetime.fromtimestamp(
                    event["timestamp"]
                )

            if event["type"] == "task-failed":
                self._tasks[event["uuid"]]["exception"] = event.get("exception")
                self._tasks[event["uuid"]]["traceback"] = event.get("traceback")

    def _monitor_events(self) -> None:
        with self._celery_app.connection() as connection:
            logger.info("Starting celery event monitor")
            recv = self._celery_app.events.Receiver(
                connection, handlers={"*": self._on_event}
            )
            recv.capture(limit=None, timeout=None, wakeup=True)

    def get_executed_tasks(self, **kwargs) -> Any:
        tasks = list(self._tasks.keys())
        limits = get_start_end(kwargs, points=len(tasks), last=True)
        return {
            "total": len(tasks),
            "rows": [
                self.get_task_info(job_id)
                for job_id in tasks[limits["st"] : limits["en"]]
            ],
        }

    def get_task_info(self, job_id: str, get_result: bool = False) -> Dict[str, Any]:
        if job_id not in self._tasks:
            return

        info = self._tasks[job_id]
        result = AsyncResult(job_id)

        output_uris = {}
        # Resolve output h5 from ewoks jobs
        # TODO: This could go into ewoksjob
        if self._ewoks_events_reader:
            events = list(
                self._ewoks_events_reader.get_events(job_id=job_id, context="node")
            )
            for event in events:
                if event["output_uris"]:
                    for uri in event["output_uris"]:
                        parts = uri["value"].split("?")
                        if parts:
                            stripped_uri = parts[0]
                            if stripped_uri not in output_uris:
                                output_uris[event["task_id"]] = stripped_uri

        return {
            "job_id": job_id,
            "name": info["name"],
            "result": result.result if get_result else None,
            "received": info["received"].timestamp() if info.get("received") else None,
            "started": info["started"].timestamp() if info.get("started") else None,
            "finished": info["finished"].timestamp() if info.get("finished") else None,
            "status": result.status,
            "args": info.get("args"),
            "kwargs": info.get("kwargs"),
            "uris": output_uris,
            "exception": info.get("exception"),
            "traceback": info.get("traceback"),
        }

    def get_workers(self) -> List[Dict]:
        app_inspector = self._celery_app.control.inspect()

        workers_stats = app_inspector.stats()
        running_tasks = app_inspector.active()
        pending_tasks = app_inspector.scheduled()
        reserved_tasks = app_inspector.reserved()

        rows = {}
        if workers_stats:
            for worker, stats in workers_stats.items():
                if worker not in rows:
                    rows[worker] = {
                        "host": worker,
                        "stats": {"uptime": stats["uptime"]},
                        "tasks": [],
                    }

        if reserved_tasks:
            for worker, tasks in reserved_tasks.items():
                for task in tasks:
                    rows[worker]["tasks"].append(
                        {
                            "status": "reserved",
                            "id": task["id"],
                            "name": task["name"],
                            "args": task["args"],
                            "kwargs": task["kwargs"],
                        }
                    )

        if pending_tasks:
            for worker, tasks in pending_tasks.items():
                for task in tasks:
                    rows[worker]["tasks"].append(
                        {
                            "status": "pending",
                            "id": task["id"],
                            "name": task["name"],
                            "args": task["args"],
                            "kwargs": task["kwargs"],
                        }
                    )

        if running_tasks:
            for worker, tasks in running_tasks.items():
                for task in tasks:
                    rows[worker]["tasks"].append(
                        {
                            "status": "running",
                            "id": task["id"],
                            "name": task["name"],
                            "args": task["args"],
                            "kwargs": task["kwargs"],
                            "time_start": task["time_start"],
                        }
                    )

        return rows.values()

    def revoke(self, task_id: str, terminate: bool = True) -> None:
        """Revoke (and kill) a scheduled or running task"""
        # TODO: The manual says explicitly not to do this !
        self._celery_app.control.revoke(task_id, terminate=terminate, signal="SIGKILL")

    def send_task(self, task, *args, with_id=True, **kwargs) -> AsyncResult:
        """Start a celery task"""
        if with_id:
            if "parameters" not in kwargs:
                kwargs["parameters"] = {}
            kwargs["parameters"]["daiquiri_id"] = str(uuid.uuid4())

        if self._default_queue:
            future = self._celery_app.send_task(
                task, args=args, kwargs=kwargs, queue=self._default_queue
            )
        else:
            queue = kwargs.pop("queue", None)
            if queue:
                future = self._celery_app.send_task(
                    task, args=args, kwargs=kwargs, queue=queue
                )
            else:
                future = self._celery_app.send_task(task, args=args, kwargs=kwargs)

        return future

    def send_event(self, datacollectionid: int, event: str) -> str:
        """Send a processing event

        Helper function to send an event to mimas
        """
        if event not in ["start", "end"]:
            raise AttributeError(f"Unknown event {event}")

        task_name = self._config.get("mimas_task", "sidecar.celery.mimas.task.mimas")

        try:
            task = self.send_task(task_name, event, datacollectionid)

            self.emit(
                "message",
                {
                    "type": "event",
                    "event": event,
                    "datacollectionid": datacollectionid,
                    "job_id": task.id,
                },
            )

            return task.id
        except Exception:
            logger.exception("Could not send task to celery")

    def execute_graph(
        self,
        graph: str,
        dataCollectionId: int,
        parameters: Dict[str, any] = {},
        wait: bool = False,
    ) -> Union[Any, str]:
        """Execute an ewoks graph

        Kwargs:
            graph: The graph to execute
            dataCollectionId: The datacollection to run against
            parameters: Any parameters to pass to the ewoks graph
            wait: Block and wait for future to resolve
        """
        task_name = self._config.get(
            "ewoks_task", "sidecar.celery.ewoks.tasks.execute_graph"
        )
        task = self.send_task(
            task_name,
            graph,
            dataCollectionId=dataCollectionId,
            parameters=parameters,
        )

        if wait:
            task.wait()
            return task.result

        return task.id

    def reprocess_graph(
        self,
        processingJobId: int,
        parameters: Dict[str, any] = {},
        overwrite: bool = False,
        wait: bool = False,
    ) -> Union[Any, str]:
        """Reprocess an ewoks graph

        Kwargs:
            processingJobId: The processingJobId to trigger
            parameters: Any parameters to pass to the ewoks graph
            overwrite: Overwrite an existing AutoProcProgram entry and pass this to the task
            wait: Block and wait for future to resolve
        """
        task_name = self._config.get(
            "reprocess_ewoks_task", "sidecar.celery.ewoks.tasks.reprocess_graph"
        )
        task = self.send_task(
            task_name, processingJobId, parameters=parameters, overwrite=overwrite
        )

        if wait:
            task.wait()
            return task.result

        return task.id


class CeleryTaskActor(ComponentActor):
    task = None

    def method(self, **kwargs):
        if self.task is None:
            raise RuntimeError("No task defined")

        send_task = kwargs.pop("send_task")
        kwargs.pop("celery_app")
        future = send_task(
            self.task,
            **kwargs,
        )

        return {"task_id": future.task_id}


class CeleryTaskSchema(ComponentActorSchema):
    pass
