from dataclasses import dataclass
import os
import time
from typing import List, Dict

import numpy
from PIL import Image


def create_scale_matrix(scale_x: float, scale_y: float) -> numpy.ndarray:
    return numpy.array([[scale_x, 0, 0], [0, scale_y, 0], [0, 0, 1]])


def create_translation_matrix(offset_x: float, offset_y: float) -> numpy.ndarray:
    return numpy.array([[1, 0, offset_x], [0, 1, offset_y], [0, 0, 1]])


def calculate_transform_matrix(
    refs: numpy.ndarray, reals: numpy.ndarray
) -> numpy.ndarray:
    A, res, rank, s = numpy.linalg.lstsq(refs, reals, rcond=-1)
    return A.T


@dataclass
class Extremes:
    width: int
    height: int
    min_x: int
    max_x: int
    min_y: int
    max_y: int


def get_extremes(matrix: numpy.ndarray, width: int, height: int) -> Extremes:
    extremes_tl = numpy.dot(matrix, numpy.array([0, 0, 1]))
    extremes_tr = numpy.dot(matrix, numpy.array([width, 0, 1]))
    extremes_bl = numpy.dot(matrix, numpy.array([0, height, 1]))
    extremes_br = numpy.dot(matrix, numpy.array([width, height, 1]))

    min_x = min(extremes_tl[0], extremes_tr[0], extremes_bl[0], extremes_br[0])
    max_x = max(extremes_tl[0], extremes_tr[0], extremes_bl[0], extremes_br[0])

    min_y = min(extremes_tl[1], extremes_tr[1], extremes_bl[1], extremes_br[1])
    max_y = max(extremes_tl[1], extremes_tr[1], extremes_bl[1], extremes_br[1])

    width = max_x - min_x
    height = max_y - min_y
    print("width", width, "height", height, "ratio", width / height)

    return Extremes(width, height, min_x, max_x, min_y, max_y)


@dataclass
class ExportedReference:
    image_path: str
    scale_factor: float
    center_x: int
    center_y: int


def export_reference_to_sampleimage(
    original_path: str,
    snapshot_path: str,
    reference_points: List[List[int]],
    vlm_points: List[List[int]],
    max_size=2000,
    crop: Dict[str, List[int]] = None,
) -> ExportedReference:
    with Image.open(original_path) as original:
        print("snapshot points", reference_points)
        refs = [numpy.array([point[0], point[1], 1]) for point in reference_points]
        vlms = [numpy.array([point[0], point[1], 1]) for point in vlm_points]

        snapshot_dir = os.path.dirname(snapshot_path)
        snapshot_file = os.path.basename(snapshot_path)
        sampleimage_file = (
            f"{snapshot_dir}/transformed_{int(time.time())}_{snapshot_file}"
        )

        # Calculate transform between reference and vlm points
        A_T = calculate_transform_matrix(refs, vlms)

        # Get extremities of new image
        ext = get_extremes(A_T, original.size[0], original.size[1])
        print("extremes", ext)

        # Scale new image (default max 2000 pixels)
        final_scale_factor = max_size / ext.width
        scale_matrix2 = create_scale_matrix(final_scale_factor, final_scale_factor)
        transform_matrix = numpy.dot(scale_matrix2, A_T)

        # Get extremities of scaled image
        ext2 = get_extremes(transform_matrix, original.size[0], original.size[1])
        print("scaled extremes", ext2)

        # Translate the new image into the viewport
        translation_matrix = create_translation_matrix(-ext2.min_x, -ext2.min_y)

        final_matrix = numpy.dot(translation_matrix, transform_matrix)
        # PIL Transform.AFFINE needs the inverted tramsform matrix
        final_matrix_inverted = numpy.linalg.inv(final_matrix)

        original = original.convert("RGBA")
        # Transform the original image
        transformed_image = original.transform(
            (int(round(ext2.width)), int(round(ext2.height))),
            Image.AFFINE,
            data=final_matrix_inverted.flatten()[:6],
            resample=Image.NEAREST,
        )
        transformed_image = transformed_image.convert("RGB")
        transformed_image.save(sampleimage_file)

        # Calculate the new centre of the image (exclude final translate)
        old_center = numpy.array(original.size) / 2
        new_center = numpy.dot(
            transform_matrix, numpy.array([old_center[0], old_center[1], 1])
        )

        # nm -> um
        scale_factor_um = 1 / final_scale_factor * 1e-3
        print("scale factor", scale_factor_um)
        image_centre = new_center[0:2] / final_scale_factor
        print("center", old_center, image_centre)

        return ExportedReference(
            image_path=sampleimage_file,
            scale_factor=scale_factor_um,
            center_x=image_centre[0],
            center_y=image_centre[1],
        )
