#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import gevent
import numpy
from typing import Dict

from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.transform.imageviewer import ImageViewerCanvas
from daiquiri.core.transform.units import get_exponent
from daiquiri.core.utils import timed, dict_nd_to_list

import logging

logger = logging.getLogger(__name__)


class Source:
    """ImageViewer Image Source

    This represents an image source on the imagerviewer, in most cases this is a camera
    which can be mounted on x and y axes (plus fine), and potentially with the ability to
    zoom

    Only one `Source` can be marked as the origin, this is the image from which for example
    the beam is marked relative to

    The offset in the imageviewer of this object is calculated from the axes and converted to
    `self._base_unit` by default nanometers which should give enough precision long term

    """

    _base_unit = "nm"
    _origin = False

    def __init__(self, config, id, hardware, emit, config_file=None):
        self._config = config
        self._config_file = config_file
        self._id = id
        self._hardware = hardware
        self._emit = emit
        self._last_emit_source = 0
        self._emit_source_timeout = None
        self.canvas = None
        self._reference_inverse_matrix = None

        for config_label in ["url", "name", "zoominfo", "scale"]:
            config_value = self._get_from_config(config_label)
            setattr(self, f"_{config_label}", config_value)

        self._device = self._get_hwobj_from_config("device")
        self._device.subscribe("state", self._device_changed)
        self._origin = self._get_from_config(
            "origin", default=False, allow_missing=True
        )

        motors, unitdict = self._motors_from_config()

        zoominfo = self._get_from_config("zoominfo")

        # Use a zoom motor to change zoom level
        if self._get_from_config("zoom", allow_missing=True):
            self._zoom = self._get_hwobj_from_config("zoom")
            self._zoom.subscribe("position", self._zoom_changed)
            self._zoom.subscribe("state", self._zoom_changed)
            zoomlevel = self._zoom.get("position")

        # Otherwise use a fixed configuration
        elif "FIXED" in zoominfo:
            zoomlevel = "FIXED"

        # Neither specified so raise
        else:
            self._raise_config_error(
                "Either a `zoom` motor or a FIXED `zoominfo` key must be provided"
            )

        self._additional = {}
        additional = self._get_from_config("additional", default={}, allow_missing=True)
        err_info = {}
        for name, obj_name in additional.items():
            err_info[name] = obj_name
            hwobj = hardware.get_object(obj_name)
            if hwobj is None:
                self._raise_config_error(
                    "Can't find hardware object", err_info=err_info
                )
            self._additional[name] = hwobj
            hwobj.subscribe("position", self._additional_changed)
            hwobj.subscribe("state", self._additional_changed)

        # Canvas object used for coordinate transformations
        self.canvas = ImageViewerCanvas(
            motors,
            units=self._base_unit,
            unitdict=unitdict,
            sampleoffset=None,  # TODO:
            beamoffset=None,  # TODO:
            beamsize=config.get("beamsize", [0.7e3, 0.3e3, 100e3]),
            beamangle=config.get("beamangle", 0),
            focaloffset=None,  # TODO:
            vlmimageshape=(100, 100),  # can be anything
            zoomlevel=zoomlevel,
            zoominfo=zoominfo,
            downstream=config.get("downstream", False),
        )

    def _motors_from_config(self):
        """Retrieve motor names from config and instantiate the hardware objects"""
        motors = {}
        unitdict = {}
        # Mapping from motor role to configuration field
        # Roles are used buy the canvas
        config_motors = {
            "x": "motor_x",
            "x_fine": "motor_x_fine",
            "y": "motor_y",
            "y_fine": "motor_y_fine",
            "z": "motor_z",
            "z_fine": "motor_z_fine",
        }
        for label, config_label in config_motors.items():
            allow_missing = config_label.endswith("fine")
            motobj = self._get_hwobj_from_config(
                config_label, allow_missing=allow_missing
            )
            if motobj is None:
                continue
            config_label += "_unit"
            unit = self._get_from_config(config_label, allow_missing=allow_missing)
            if not unit:
                unit = motobj.get("unit")
            if not unit:
                self._raise_config_error(f"Missing '{config_label}' key")
            motobj.subscribe("position", self._translate_changed)
            motobj.subscribe("state", self._translate_changed)
            motors[label] = motobj
            unitdict[motobj.id()] = unit
        return motors, unitdict

    def _get_hwobj_from_config(self, config_label, allow_missing=False):
        """Instantiate a hardware object from the config"""
        obj_id = self._get_from_config(
            config_label, default="", allow_missing=allow_missing
        )
        if allow_missing and not obj_id:
            return None
        err_info = {}
        err_info[config_label] = obj_id
        hwobj = self._hardware.get_object(obj_id)
        if not allow_missing and hwobj is None:
            self._raise_config_error("Can't find hardware object", err_info=err_info)
        return hwobj

    def _get_from_config(self, config_label, default=None, allow_missing=False):
        """Get a value for the config"""
        if not allow_missing and config_label not in self._config:
            self._raise_config_error(f"Missing '{config_label}' key")
        return self._config.get(config_label, default)

    def update_config(self, config):
        self._config = config
        self.canvas.zoominfo = self._get_from_config("zoominfo")
        self.canvas.beamsize = self._get_from_config("beamsize")
        self.canvas.downstream = self._get_from_config("downstream")

    def _raise_config_error(self, message, err_info=None):
        err_obj = {"key": "sources", "name": self._config.get("name")}
        if err_info:
            err_obj.update(err_info)
        raise InvalidYAML(
            {"message": message, "file": self._config_file, "obj": err_obj}
        )

    @property
    def name(self):
        return self._name

    @property
    def device(self):
        return self._device

    @property
    def unit(self):
        return get_exponent(self._base_unit)

    @property
    def id(self):
        return self._id

    @property
    def origin(self):
        return self._origin

    @property
    def beamsize(self) -> Dict[str, float]:
        """Return the beamsize (in nm)"""
        bs = self._get_from_config("beamsize")
        return {"x": bs[0], "y": bs[1], "z": bs[2]}

    @timed
    def info(self):
        """Get info from this image source

        This includes name, offsets, scale, limits, url, etc
        """
        info = {
            "sourceid": self._id,
            "name": self._name,
            "type": "video",
            "origin": self._origin,
            "url": self._url,
            "scale": self._scale,
            "additional": self.get_additional(),
        }
        info.update(self.canvas.vlm_image_info)
        info["polylines"] = polylines = {}
        info["markings"] = markings = {}
        polylines["limits"] = self.canvas.reach_polyline.tolist()
        polylines["fine motors"] = self.canvas.fine_polyline.tolist()
        beam_info = self.canvas.beam_info
        beam_info["origin"] = True
        markings["beam"] = beam_info
        info["reference"] = {"beam": self.calculate_reference_beam(beam_info["center"])}

        return dict_nd_to_list(info)

    def _device_changed(self, obj, prop, value):
        self._emit(
            "message",
            {"type": "device", "prop": prop, "value": value},
        )

    def _zoom_changed(self, obj, prop, value):
        """Callback when the zoom changes"""
        if prop == "position" and obj == self._zoom:
            label = self.get_scale_label()
            if label:
                self.canvas.zoomlevel = label
                self._emit(
                    "message",
                    {
                        "type": "source",
                        "info": dict_nd_to_list(
                            {
                                **self.canvas.vlm_image_info,
                                "sourceid": self._id,
                                "markings": {"beam": self.canvas.beam_info},
                            }
                        ),
                    },
                )

    def _additional_changed(self, obj, prop, value):
        """Callback when an additional position changes"""
        for name, ob in self._additional.items():
            if prop == "position" and obj == ob:
                self._emit(
                    "message",
                    {
                        "type": "origin",
                        "id": self._id,
                        "prop": "additional",
                        "object": name,
                        "value": value,
                    },
                )

    def get_additional(self):
        """Get a dict of the additional positions

        Returns:
            positions(dict): A dict of positions and their values
        """
        additional = {}
        for name, obj in self._additional.items():
            additional[name] = obj.get("position")

        return additional

    def move_to_additional(self, additional, wait=True):
        """Move to additional positions"""
        if additional:
            for a, v in additional.items():
                print("moving to additional", a, v)
                if a in self._additional:
                    self._additional[a].move(v)

            if wait:
                for a, v in additional.items():
                    if a in self._additional:
                        self._additional[a].wait()

    def _translate_changed(self, obj, prop, value):
        """Callback when a translation changes"""
        if prop == "position":
            self._queue_emit_source()

    def _queue_emit_source(self):
        now = time.time()
        if now - self._last_emit_source > 0.2:
            self._emit_source()
            self._last_emit_source = now
        else:
            if self._emit_source_timeout:
                self._emit_source_timeout.kill()

            self._emit_source_timeout = gevent.spawn_later(0.5, self._emit_source)

    def _emit_source(self):
        if self.canvas is None:
            return

        beam_info = self.canvas.beam_info
        self._emit(
            "message",
            {
                "type": "source",
                "info": dict_nd_to_list(
                    {
                        **self.canvas.vlm_image_info,
                        "sourceid": self._id,
                        "markings": {"beam": beam_info},
                        "polylines": {"fine motors": self.canvas.fine_polyline},
                        "reference": {
                            "beam": self.calculate_reference_beam(beam_info["center"])
                        },
                    }
                ),
            },
        )

    def set_zoom(self, zoom):
        self._zoom.move(zoom)

    def get_scale_label(self):
        """Get the label for the current scale"""
        label = self._zoom.get("position")
        if label not in self._zoominfo:
            raise RuntimeError(
                "Zoom scale '{}' not in configuration ({})".format(
                    label, list(self._zoominfo.keys())
                )
            )
        return label

    def set_reference_inverse_matrix(self, matrix):
        self._reference_inverse_matrix = matrix

    def calculate_reference_beam(self, center):
        if not isinstance(self._reference_inverse_matrix, numpy.ndarray):
            return

        ref_pos = numpy.dot(
            # Beam center is in inverted y sense compared with reference image
            self._reference_inverse_matrix,
            numpy.array([center[0], -center[1], 1]),
        )
        return (ref_pos[0], ref_pos[1])
