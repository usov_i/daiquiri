#!/usr/bin/env python
# coding: utf-8
import logging
from typing import Optional, Sequence, Tuple, Union

from PIL import Image
import h5py
import numpy


_logger = logging.getLogger(__name__)
Image.MAX_IMAGE_PIXELS = None
UNITS = "pixels"


def blit(
    src: numpy.ndarray, dst: Union[numpy.ndarray, h5py.Dataset], offset: Sequence[int]
) -> Optional[Tuple[int]]:
    """Copy `src` array into `dst` at given `offset` position.

    This functions avoids errors in case `src` is partly of completely outside `dst`.

    Warning: Negative offset are NOT understand as usual.
    Instead they are understand as coordinates in `dst` frame of reference.

    The effectively copied shape is returned.
    """
    assert src.ndim == dst.ndim == len(offset)

    copy_start = numpy.clip(offset, 0, None)
    copy_shape = numpy.clip(
        src.shape + numpy.clip(offset, None, 0), 0, dst.shape - copy_start
    )
    dst[
        tuple(slice(start, start + size) for start, size in zip(copy_start, copy_shape))
    ] = src[tuple(slice(0, size) for size in copy_shape)]
    return tuple(copy_shape) if numpy.all(copy_shape != 0) else None


def binning(image: numpy.ndarray) -> numpy.ndarray:
    """2D 2x2 array binning"""
    assert image.ndim == 2
    return 0.25 * (
        image[:-1:2, :-1:2]
        + image[:-1:2, 1::2]
        + image[1::2, :-1:2]
        + image[1::2, 1::2]
    )


def create_dataset_with_units(group: h5py.Group, path: str, value, units: str) -> None:
    group[path] = value
    group[path].attrs["units"] = units


def prepare_histogram_nxdata(
    ingroup: h5py.Group,
    nbins: int,
    path: str = "histogram",
) -> h5py.Group:
    """Prepare a group `ingroup[path]` for storing a histogram as a NXdata"""
    group = ingroup.create_group(path)
    group.create_dataset("count", shape=(nbins,), dtype=numpy.int32, fillvalue=0)
    group.create_dataset(
        "bin_edges", shape=(nbins + 1,), dtype=numpy.float32, fillvalue=numpy.nan
    )
    group.create_dataset(
        "centers", shape=(nbins,), dtype=numpy.float32, fillvalue=numpy.nan
    )
    group.attrs["NX_class"] = "NXdata"
    group.attrs["signal"] = "count"
    group.attrs["axes"] = "centers"
    group.attrs["title"] = "Histogram"
    return group


def fill_histogram_nxdata(
    group: h5py.Group,
    bin_count: numpy.ndarray,
    bin_edges: numpy.ndarray,
) -> None:
    """Fill a prepared group with given histogram"""
    assert bin_count.ndim == bin_edges.ndim == 1
    nbins = len(bin_count)
    assert nbins + 1 == len(bin_edges)

    count_dataset = group["count"]
    bin_edges_dataset = group["bin_edges"]
    centers_dataset = group["centers"]

    assert len(count_dataset) == nbins
    assert len(bin_edges_dataset) == nbins + 1
    assert len(centers_dataset) == nbins

    count_dataset[()] = bin_count
    bin_edges_dataset[()] = bin_edges
    centers_dataset[()] = 0.5 * (bin_edges[:-1] + bin_edges[1:])

    count_dataset.flush()
    bin_edges_dataset.flush()
    centers_dataset.flush()


class HistogramAccumulator:
    """Histogram accumulator.

    Histogram is re-gridded if needed
    """

    def __init__(self, bins: int):
        self.__bin_edges = None
        self.__count = numpy.zeros((bins,), dtype=int)

    def update(self, data: numpy.ndarray):
        """Accumulate `data` into histogram, extending range if needed"""
        min_, max_ = numpy.nanmin(data), numpy.nanmax(data)
        if self.__bin_edges is None:
            _logger.debug("Initialize bin_edges")
            self.__count, self.__bin_edges = numpy.histogram(
                data, bins=len(self.__count), range=(min_, max_)
            )
            return

        previous_min = self.__bin_edges[0]
        previous_max = self.__bin_edges[-1]
        if min_ < previous_min or max_ > previous_max:
            _logger.debug("Extend bin_edges")
            self.__count, self.__bin_edges = numpy.histogram(
                self.centers,
                weights=self.__count,
                bins=len(self.__count),
                range=(min(min_, previous_min), max(max_, previous_max)),
            )

        self.__count += numpy.histogram(data, self.__bin_edges)[0]

    @property
    def bin_edges(self) -> numpy.ndarray:
        if self.__bin_edges is None:
            return numpy.zeros((len(self.__count),), dtype=numpy.float64)
        else:
            return numpy.array(self.__bin_edges)

    @property
    def centers(self) -> numpy.ndarray:
        edges = self.bin_edges
        return 0.5 * (edges[:-1] + edges[1:])

    @property
    def bin_count(self) -> numpy.ndarray:
        return numpy.array(self.__count)


def compute_nlevels(shape: Sequence[int], atleast_powerof2: int) -> int:
    """Returns number of levels in pyramid of details for given shape and constraint.

    `atleast_powerof2` is the power of 2 of the image size where to cut the pyramid.
    """
    shape_array = numpy.asarray(shape)

    if numpy.any(shape_array <= 0):
        return 1  # Base of the pyramid is empty

    # First, find level in the pyramid of details where the smallest dimension is 1
    max_level = numpy.min(numpy.log2(shape_array).astype(int))
    return (
        max_level
        + 1  # At least one level
        - numpy.clip(  # Cut pyramid if possible based on atleast_powerof2
            atleast_powerof2
            - numpy.ceil(numpy.log2(numpy.max(shape_array // 2**max_level))).astype(
                int
            ),
            0,
            max_level,
        )
    )


def prepare_tiling_dataset(
    group: h5py.Group,
    image: numpy.ndarray,
    pixel_size: float,
    nbins: int,
    dtype=numpy.float32,
):
    _logger.debug("Read scan information")
    y_range = [0, image.shape[1]]
    z_range = [0, image.shape[0]]

    # Get full image shape and corrected coordinate ranges
    full_shape = numpy.array(
        (
            int(numpy.ceil(numpy.abs(z_range[1] - z_range[0]) / pixel_size)),
            int(numpy.ceil(numpy.abs(y_range[1] - y_range[0]) / pixel_size)),
        )
    )
    y_range = y_range[0], y_range[0] + full_shape[1] * pixel_size
    z_range = z_range[0], z_range[0] + full_shape[0] * pixel_size

    _logger.debug(
        f"Save image calibration: pixel size: {pixel_size}{UNITS}, y: {y_range} {UNITS}, z: {z_range} {UNITS}"
    )
    create_dataset_with_units(group, "pixel_size", pixel_size, UNITS)
    create_dataset_with_units(group, "y", y_range, UNITS)
    create_dataset_with_units(group, "z", z_range, UNITS)

    # Get number of levels by cutting levels where images have all dim smaller than 1024
    nlevel = compute_nlevels(full_shape, 9)
    _logger.debug(f"Number of level datasets: {nlevel}")

    datasets = []
    layer_names = []
    for level in range(nlevel):
        name = f"level{level}"
        layer_names.insert(0, name)
        level_shape = tuple(full_shape // (2**level))
        _logger.debug(
            f"Create dataset {name}: shape: {level_shape}, dtype: {numpy.dtype(dtype).name}"
        )
        datasets.append(group.create_dataset(name, shape=level_shape, dtype=dtype))

    group["definition"] = "tiling"
    group.attrs["NX_class"] = "NXentry"
    group["layers"] = layer_names

    prepare_histogram_nxdata(group, nbins)

    return datasets


def fill_tiling_dataset(
    group: h5py.Group,
    image: numpy.ndarray,
    datasets: Sequence[h5py.Dataset],
    nbins: int,
):
    _logger.debug("Read scan information")
    histogram = HistogramAccumulator(bins=nbins)

    nlevel = len(datasets)

    _logger.debug("Process image:")
    # image = numpy.flip(image, 1)

    offset = numpy.array((0, 0))
    _logger.debug(
        f"- Update level 0 at offset {tuple(offset)}, shape: {image.shape}"  # nosec
    )
    shape = blit(image, datasets[0], offset)
    if shape is None:
        _logger.warning("! Image has no overlap: skipping")
        return  # No more processing needed

    datasets[0].flush()  # Notify readers of the update

    if shape != image.shape:
        _logger.warning(f"! Some pixels were not used: copied shape: {shape}")

    _logger.debug("- Update histogram")
    histogram.update(image[: shape[0], : shape[1]])
    fill_histogram_nxdata(group["histogram"], histogram.bin_count, histogram.bin_edges)

    offset = numpy.maximum(offset, 0)  # Clip offset to 0

    # Update other levels of the pyramid of images
    for level in range(1, nlevel):
        # Get copy area with even bounds eventually enlarging it
        end = 2 * numpy.ceil((offset + shape) / 2).astype(int)
        offset = 2 * (offset // 2)
        _logger.debug(
            f"- Binning level {level - 1} from {tuple(offset)} to {tuple(end)}"
        )
        binned_image = binning(
            datasets[level - 1][offset[0] : end[0], offset[1] : end[1]]
        )

        offset //= 2
        _logger.debug(
            f"- Updated level {level} at offset {tuple(offset)}, shape: {binned_image.shape}"
        )
        shape = blit(binned_image, datasets[level], offset)
        datasets[level].flush()  # Notify readers of the update

        if shape is None:
            _logger.warning(f"No overlap for level {level}, offset: {tuple(offset)}")
            break


def generate_pyramid(
    output_filename: str, input_filename: str, nbins: int = 1000, pixel_size: float = 1
):
    _logger.info(f"Write to {output_filename}")
    with h5py.File(output_filename, "w", libver="v110") as h5file:
        _logger.info(f"Read from `{input_filename}`")

        with Image.open(input_filename) as image:
            _logger.info("Prepare HDF5 file for tiling")
            group_name = "sample_registration"
            group = h5file.create_group(group_name)

            print("image", image)
            image = image.convert("L")
            print("image grayscale", image)
            image = numpy.array(image)
            print("image numpy", image.shape)

            datasets = prepare_tiling_dataset(
                group, image, pixel_size, nbins, dtype=numpy.uint8
            )
            fill_tiling_dataset(
                group=group,
                image=image,
                datasets=datasets,
                nbins=nbins,
            )
            _logger.info("Done")


if __name__ == "__main__":
    import argparse

    logging.basicConfig()

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("input_filename", type=str, help="Input image file")
    parser.add_argument("pixel_size", type=float, help="Pixel Size")
    parser.add_argument(
        "output_filename",
        type=str,
        default="output.h5",
        nargs="?",
        help="HDF5 file where to save the result",
    )
    parser.add_argument(
        "--verbose", "-v", action="count", default=0, help="Increase verbosity"
    )
    options = parser.parse_args()

    _logger.setLevel(
        {0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}.get(
            options.verbose, logging.DEBUG
        )
    )

    nbins = 100
    generate_pyramid(
        options.output_filename, options.input_filename, nbins, options.pixel_size
    )
