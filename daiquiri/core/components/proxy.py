#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import logging
from typing import Dict, List

import requests
from marshmallow import fields, Schema
from marshmallow.schema import SchemaMeta
from flask import make_response
from werkzeug.routing import Rule, Map
from urllib.parse import urlparse

from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema.proxy import METHODS, ProxyConfigSchema
from daiquiri.resources.utils import get_resource_provider

logger = logging.getLogger(__name__)

PARAM_MAP = {
    "boolean": fields.Boolean,
    "integer": fields.Int,
    "number": fields.Float,
    "string": fields.Str,
}


class Proxy(Component):
    """Proxy http/s requests from another service"""

    _config_schema = ProxyConfigSchema()

    def _retrieve(self, path, object):
        if len(path):
            node = path.pop(0)
            return self._retrieve(path, object[node])
        else:
            return object

    def _resolve_schema(self, schema, spec):
        path = schema["$ref"].replace("#/", "").split("/")
        node = self._retrieve(path, spec)
        return self._parse_properties(node, spec)

    def _parse_properties(self, node, spec):
        spec_v3 = spec.get("openapi", "").startswith("3.")
        properties = node["properties"]
        params = {}
        try:
            for property_name, property in properties.items():
                if spec_v3:
                    params[property_name] = PARAM_MAP[property["schema"]["type"]]()
                else:
                    if "type" not in property:
                        schema = Schema.from_dict(
                            self._resolve_schema(property, spec),
                            name=f"{property_name}Schema",
                        )
                        params[property_name] = fields.Nested(
                            schema, required=(property_name in node.get("required", []))
                        )
                    elif property["type"] == "array":
                        params[property_name] = fields.List(
                            PARAM_MAP[property["items"]["type"]](
                                required=(property_name in node.get("required", []))
                            )
                        )
                    else:
                        params[property_name] = PARAM_MAP[property["type"]](
                            required=(property_name in node.get("required", []))
                        )
        except Exception:
            logger.error(
                "Error while reading node '%s' param %s:\n%s",
                node,
                property_name,
                property,
                exc_info=True,
            )

        return params

    def create_params_from_schema(
        self,
        url: str,
        method_name: str,
        openapi_spec: dict,
    ) -> Dict[str, fields.Field]:
        """
        "paths": {
            "/api/chat": {
                "get": {
                    "description": "Get the last n chat messages",
                    "parameters": [
                    {
                        "in": "query",
                        "name": "limit",
                        "required": false,
                        "type": "integer"
                    },
                    {
                        "in": "query",
                        "name": "offset",
                        "required": false,
                        "type": "integer"
                    }
                    ],
                }
            }
        }
        """
        params = {}
        nb_errors = 0
        spec_v3 = openapi_spec.get("openapi", "").startswith("3.")
        paths = openapi_spec["paths"]
        for path, path_definition in paths.items():
            if url.endswith(path):
                parameters = path_definition[method_name]["parameters"]
                try:
                    for parameter_id, parameter in enumerate(parameters):
                        if spec_v3:
                            params[parameter["name"]] = PARAM_MAP[
                                parameter["schema"]["type"]
                            ]()
                        else:
                            if "type" not in parameter:
                                fields_dict = self._resolve_schema(
                                    parameter["schema"], openapi_spec
                                )

                                if parameter.get("in") == "body":
                                    params = Schema.from_dict(
                                        fields_dict,
                                        name=f"{parameter['name']}Schema",
                                    )
                                else:
                                    params[parameter["name"]] = fields_dict

                            elif parameter["type"] == "array":
                                params[parameter["name"]] = fields.List(
                                    PARAM_MAP[parameter["items"]["type"]](
                                        required=parameter.get("required")
                                    )
                                )
                            else:
                                params[parameter["name"]] = PARAM_MAP[
                                    parameter["type"]
                                ](required=parameter.get("required"))
                except Exception:
                    logger.error(
                        "Error while reading spec from path '%s' param %s:\n%s",
                        path,
                        parameter_id,
                        parameter,
                        exc_info=True,
                    )
                    nb_errors += 1

        if nb_errors:
            raise RuntimeError("Failed to read proxy api specification")

        return params

    def create_proxy(
        self,
        method_name: str,
        url: str,
        name: str,
        openapi_spec: dict,
        headers=None,
    ) -> callable:
        """Create a proxied route"""

        def proxy(self, *args, **kwargs):
            method = getattr(requests, method_name)
            full_url = url

            if kwargs:
                url_parts = urlparse(url)
                url_rule = Rule(url_parts.path)
                Map([url_rule])
                built_url = url_rule.build(kwargs)
                full_url = f"{url_parts.scheme}://{url_parts.netloc}{built_url[1]}"

            logger.debug(f"Proxying to {full_url}")

            try:
                if method_name == "post":
                    logger.debug("Proxying post request with body: %s", kwargs)
                    response = method(full_url, params=kwargs, headers=headers)
                else:
                    response = method(full_url, headers=headers)
            except Exception as e:
                logger.error("Proxy '%s' failed: %s", full_url, e.args[0])
                return make_response((f"Proxy not available: {e.args[0]}", 404))

            return make_response(
                (response.content, response.status_code, list(response.headers.items()))
            )

        proxy.__name__ = method_name
        proxy.__qualname__ = method_name
        proxy.__doc__ = name

        schema = self.create_params_from_schema(url, method_name, openapi_spec)
        if isinstance(schema, SchemaMeta):
            inp = schema
        else:
            inp = dict(schema)

        return marshal(inp=inp)(proxy)

    def setup(self, *args, **kwargs) -> None:
        for proxy in self._config.get("proxies", []):
            self.setup_proxy(proxy)

    def setup_proxy(self, proxy):
        """Setup a new proxy"""
        openapi_spec = self._read_openapi_spec(proxy["openapi"])
        try:
            routes_description = proxy["routes"]
        except KeyError:  # Load routes from openapi file
            routes_description = self._get_openapi_routes(openapi_spec)

        routes = {}
        for route in routes_description:
            route_name = route["name"].lstrip("/")
            mapping = f"{proxy['name'].strip('/')}/{route_name}"

            for method in route.get("methods", ["get"]):
                routes.setdefault(mapping, {})[method] = self.create_proxy(
                    method,
                    f"{proxy['target'].rstrip('/')}/{route_name}",
                    mapping,
                    openapi_spec,
                    headers=route.get("headers"),
                )

        for path, methods in routes.items():
            methods_ = list(methods.values())
            proxied_route = type(
                f"{methods_[0].__doc__}Proxy",
                (ComponentResource,),
                methods,
            )
            self.register_route(proxied_route, f"/{path}")

    def _read_openapi_spec(self, url: str) -> dict:
        """
        Returns an OpenAPI spec structure from an URI.

        Arguments:
            url: Can use protocol `http[s]://` to read the specification from
                a web service, `file://` to it from the file system. 'res://' to
                read it from Daiquiri resources.

        """
        url_parts = urlparse(url)
        if url_parts.scheme in ("", "file"):
            with open(url_parts.netloc + url_parts.path, "r") as f:
                return json.load(f)

        if url_parts.scheme in ("res"):
            provider = get_resource_provider()
            resource = provider.abs_resource(url_parts.netloc + url_parts.path)
            with provider.open_resource(resource, mode="t") as f:
                return json.load(f)

        response = requests.get(url, timeout=10)
        if response.status_code == 200:
            return response.json()
        else:
            raise RuntimeError(
                f"Could not load OpenAPI spec {url}: {response.status_code}"
            )

    def _get_openapi_routes(self, openapi_spec: dict) -> List[dict]:
        routes = []
        for name, methods_desc in openapi_spec["paths"].items():
            # Path arguments conversion
            for method in methods_desc.values():
                for param in method.get("parameters", ()):
                    if param["in"] == "path":
                        name = name.replace(
                            f"{{{param['name']}}}", f"<{param['type']}:{param['name']}>"
                        )

            methods = [m for m in methods_desc.keys() if m in METHODS]
            routes.append(dict(name=name, methods=methods))
        return routes
