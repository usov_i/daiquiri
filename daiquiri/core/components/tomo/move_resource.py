#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import fields
import pint

from daiquiri.core import marshal
from daiquiri.core.components import ComponentResource
from daiquiri.core.schema import ErrorSchema


logger = logging.getLogger(__name__)


class TomoMoveResource(ComponentResource):
    @marshal(
        inp={
            "sy": fields.Str(metadata={"description": "Quantity to move this motor"}),
            "sz": fields.Str(metadata={"description": "Quantity to move this motor"}),
            "sampx": fields.Str(
                metadata={"description": "Quantity to move of this motor"}
            ),
            "sampy": fields.Str(
                metadata={"description": "Quantity to move of this motor"}
            ),
            "sampu": fields.Str(
                metadata={"description": "Quantity to move of this motor"}
            ),
            "sampv": fields.Str(
                metadata={"description": "Quantity to move of this motor"}
            ),
            "relative": fields.Bool(
                metadata={"description": "If true, motor moves are relative"}
            ),
        },
        out=[
            # [200, ScanDataSchema(), 'Scan data'],
            [404, ErrorSchema(), "Motor not available"]
        ],
    )
    def get(
        self,
        sy=None,
        sz=None,
        sampx=None,
        sampy=None,
        sampu=None,
        sampv=None,
        relative=False,
        **kwargs,
    ):
        """Get a list of all available detectors and their info"""
        if [sy, sz, sampx, sampy, sampu, sampv].count(None) == 6:
            return {"error": "No motors are part of the move request"}, 404

        vsy = None
        vsz = None
        vsampx = None
        vsampy = None
        vsampu = None
        vsampv = None
        try:
            if sy:
                vsy = pint.Quantity(sy)
            if sampx:
                vsampx = pint.Quantity(sampx)
            if sampy:
                vsampy = pint.Quantity(sampy)
            if sz:
                vsz = pint.Quantity(sz)
            if sampu:
                vsampu = pint.Quantity(sampu)
            if sampv:
                vsampv = pint.Quantity(sampv)
        except Exception:
            logger.error(
                "One of requested motor position wrongly formatted (found '%s', '%s', '%s', '%s', '%s', '%s')",
                sy,
                sz,
                vsampx,
                sampy,
                sampu,
                sampv,
                exc_info=True,
            )
            return {"error": "Problem while parsing request"}, 404

        try:
            self._parent.move(
                sy=vsy,
                sz=vsz,
                sampx=vsampx,
                sampy=vsampy,
                sampu=vsampu,
                sampv=vsampv,
                relative=relative,
            )
        except Exception:
            motors = []

            def include_if_value_not_none(name, value):
                if value is not None:
                    motors.append(name)

            include_if_value_not_none("sy", sy)
            include_if_value_not_none("sz", sz)
            include_if_value_not_none("sampx", sampy)
            include_if_value_not_none("sampy", sampy)
            include_if_value_not_none("sampu", sampu)
            include_if_value_not_none("sampv", sampv)
            motors = "/".join(motors)
            logger.error("Error while moving %s motors", motors, exc_info=True)
            return {"error": f"Problem occurred during {motors} move"}, 404
        return "OK", 200
