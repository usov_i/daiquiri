#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import typing
import pint
from ruamel.yaml import YAML

from daiquiri.core.components import Component
from daiquiri.core.components.dcutilsmixin import DCUtilsMixin

try:
    from daiquiri.core.hardware.bliss.scans import BlissScans
except ImportError:
    pass

from .detectors_resource import TomoDetectorsResource
from .move_resource import TomoMoveResource
from .image_resource import TomoImageResource
from .scan_info_resource import TomoScanInfoResource
from .slice_reconstruction_resource import SliceReconstructionResource
from .datatype import DetectorLiveStorage
from .datatype import SampleStageMetadata
from .datatype import ScanInfoStorage
from .scan_listener import TomoScanListener


logger = logging.getLogger(__name__)


TOMOVIS_PROXY_TEMPLATE = """
name: tomovis
target: TO_BE_MODIFIED
openapi: res://openapi/tomovis_v1.json
routes:
  - name: image_tiling/
  - name: image_tiling/<string:resource_id>
    methods: [get, delete]
  - name: h5grove/attr/
  - name: h5grove/data/
  - name: h5grove/meta/
"""


class TomoComponent(Component, DCUtilsMixin):
    """Tomo detector component.

    The tomo detector component watch scans and capture data in order to process
    data correction on the fly.

    For now it captures dark and flat from tomo scans. And provide an API to
    retrieve a flat field correction.
    """

    def __init__(self, *args, **kwargs):
        self._scan_sources = []
        self._detectors: typing.Dict[str, DetectorLiveStorage] = {}
        self._scaninfos: ScanInfoStorage = ScanInfoStorage()
        self._tomo_config = None
        self._exported_config = {}
        self._scanlisteners = []
        self._slice_reconstruction_triggers = None
        self._last_delta_beta = None
        super(TomoComponent, self).__init__(*args, **kwargs)

    def after_all_setup(self, components):
        """Called after the setup of the whole components"""
        self._setup_tomovis_proxy(components)

    def get_export_config(self):
        """Get exported config values from components"""
        return self._exported_config

    def _setup_tomovis_proxy(self, components):
        config = self._config.get("tomovis", {})
        url = config.get("url")
        if url is not None:
            self._exported_config["tomovis_url"] = url
            create_proxy = config.get("create_proxy", False)
            if create_proxy:
                proxy = components.get_component("proxy")
                if proxy is None:
                    raise RuntimeError(
                        "Tomovis setup as proxy but no proxy component configured"
                    )
                self._exported_config["tomovis_proxy_url"] = "/api/proxy/tomovis"
                yaml = YAML()
                description = yaml.load(TOMOVIS_PROXY_TEMPLATE)
                description["target"] = url
                proxy.setup_proxy(description)

    @property
    def slice_reconstruction_triggers(self) -> typing.List[str]:
        return self._slice_reconstruction_triggers

    @property
    def scan_sources(self):
        return self._scan_sources

    @property
    def image_format(self) -> str:
        """Image format to use for the detector data transfer"""
        image_format = self._config.get("image_format", "png")
        if image_format not in ["png", "jpeg"]:
            logger.warning(
                "`image_format` config unsupported. Found '%s'. Fallback with PNG.",
                image_format,
            )
            image_format = "png"
        return image_format.upper()

    @property
    def min_refresh_period(self) -> float:
        """Minimal image refresh period done during scan"""
        return self._config.get("min_refresh_period", 1)

    @property
    def last_delta_beta(self) -> typing.Optional[float]:
        """Last delta beta value used"""
        return self._last_delta_beta

    def set_last_delta_beta(self, delta_beta: float):
        self._last_delta_beta = delta_beta

    @property
    def tomovis_uri(self) -> typing.Optional[str]:
        """Tomovis URI service if defined, else None"""
        config = self._config.get("tomovis", {})
        return config.get("url")

    def get_tomo_config_name(self) -> str:
        """Returns the name of the tomo BLISS object used by the application"""
        return self._tomo_config.name

    def setup(self, *args, **kwargs):
        self._slice_reconstruction_triggers = self._config.get(
            "slice_reconstruction_triggers", None
        )
        if self._slice_reconstruction_triggers is None:
            self._slice_reconstruction_triggers = ["terminated"]

        for source in self._config["sources"]:
            if source["type"] == "bliss":

                def get_bliss_device_from_config(source, name):
                    device_name = source.get(name)
                    if device_name is None:
                        return None

                    if device_name == "ACTIVE_TOMOCONFIG":
                        from tomo.globals import ACTIVE_TOMOCONFIG

                        return ACTIVE_TOMOCONFIG.deref_active_object()

                    from bliss.config.static import get_config

                    config = get_config()
                    return config.get(device_name)

                self._tomo_config = get_bliss_device_from_config(source, "tomo_config")

                tomo_detectors = self._tomo_config.detectors
                for tomo_detector in tomo_detectors.detectors:
                    # Name of the lima device inside BLISS
                    detector_id = tomo_detector.detector.name
                    logger.info("Register BLISS detector: %s", detector_id)
                    self._detectors[detector_id] = DetectorLiveStorage(
                        detector_id=detector_id,
                        tomo_detector_id=tomo_detector.name,
                        detector_node_name=f"{detector_id}:image",
                    )

                src = BlissScans(source["session"], self._config, app=self._app)
                scanlistener = TomoScanListener(self, src)
                src.watch_new_scan(scanlistener.new_scan)
                src.watch_end_scan(scanlistener.end_scan)
                src.watch_new_data(scanlistener.new_data)
                src.watch_new_0d_data(scanlistener.new_0d_data)

                self._scan_sources.append(src)
                self._scanlisteners.append(scanlistener)

                logger.debug("Registered Bliss scan source")
            else:
                raise TypeError("Only bliss source is available for now")

        self.register_route(TomoDetectorsResource, "/detectors")
        self.register_route(TomoScanInfoResource, "/scaninfo")
        self.register_route(TomoImageResource, "/data")
        self.register_route(TomoMoveResource, "/move")
        self.register_route(SliceReconstructionResource, "/slice_reconstruction")

    def get_tomo_config(self):
        """Returns the tomo config object"""
        return self._tomo_config

    def get_sample_stage_meta(self, detector_name) -> SampleStageMetadata:
        """Returns the sample stage state

        This is a workaround for now, it would be much better to retrieve it
        from the scan if possible.

        Which is probably part of positioners for ct
        """

        def quantity_from_motor(motor):
            if motor is None:
                return None
            return pint.Quantity(motor.position, motor.unit)

        pixel_size = None

        def get_active_tomo_detector():
            if self._tomo_config is None:
                return None
            if self._tomo_config.detectors is None:
                return None
            tomo_detector = self._tomo_config.detectors.active_detector
            return tomo_detector

        detector = get_active_tomo_detector()
        if detector and detector.detector.name == detector_name:
            # For now there is no way to reach the pixel size from another
            # detector than the active one
            ps = detector.sample_pixel_size
            if ps:
                pixel_size = pint.Quantity(ps, "um")

        return SampleStageMetadata(
            sy=quantity_from_motor(self._tomo_config.y_axis),
            sz=quantity_from_motor(self._tomo_config.z_axis),
            sampy=quantity_from_motor(self._tomo_config.sample_y_axis),
            somega=quantity_from_motor(self._tomo_config.rotation_axis),
            pixel_size=pixel_size,
        )

    def move(
        self,
        sy: pint.Quantity = None,
        sz: pint.Quantity = None,
        sampx: pint.Quantity = None,
        sampy: pint.Quantity = None,
        sampu: pint.Quantity = None,
        sampv: pint.Quantity = None,
        relative: bool = False,
    ):
        """Move the sample stage motors.

        If multiple moves are requested, all the command are send in parallel

        Arguments:
            relative: If true, the move is relative
        """

        def normalize_value_to_motor_unit(motor, value):
            if value.units == "css_pixel":
                # By default pint provides a conversion from px to metric system
                # TODO this have to be removed
                raise RuntimeError("Can't convert from pixel to length")
            return value.to(motor.unit).magnitude

        trajectory = {
            self._tomo_config.y_axis: sy,
            self._tomo_config.z_axis: sz,
            self._tomo_config.sample_x_axis: sampx,
            self._tomo_config.sample_y_axis: sampy,
            self._tomo_config.sample_u_axis: sampu,
            self._tomo_config.sample_v_axis: sampv,
        }

        trajectory = {
            k: normalize_value_to_motor_unit(k, v)
            for k, v in trajectory.items()
            if v is not None
        }

        if len(trajectory) == 0:
            return

        from bliss.common.motor_group import Group

        group = Group(*trajectory.keys())
        group.move(trajectory, wait=True, relative=relative)

    def get_detector(self, detector_id: str) -> typing.Optional[DetectorLiveStorage]:
        """Get available detectors"""
        return self._detectors.get(detector_id)

    def get_scaninfos(self):
        """Returns the state of the actual scans"""
        return self._scaninfos

    def get_detectors(self) -> typing.List[DetectorLiveStorage]:
        """Get available detectors"""
        return self._detectors.values()
