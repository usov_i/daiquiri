#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from flask import g
from marshmallow import ValidationError

from daiquiri.core import require_control, require_staff, marshal
from daiquiri.core.logging import log
from daiquiri.core.components import (
    Component,
    ComponentResource,
    actor,
    ComponentActorKilled,
)
from daiquiri.core.components.dcutilsmixin import DCUtilsMixin
from daiquiri.core.schema import ErrorSchema, MessageSchema, ValidationErrorSchema
from daiquiri.core.schema.samplescan import ScanConfigSchema, AvailableScansSchema
from daiquiri.core.schema.metadata import paginated

import logging


logger = logging.getLogger(__name__)


class QueueScanResource(ComponentResource):
    @marshal(
        out=[
            [200, MessageSchema(), "Scan successfully queued"],
            [400, ErrorSchema(), "Could not queue scan"],
            [404, ErrorSchema(), "No such data collection plan"],
            [422, ValidationErrorSchema(), "Data collection plan is not valid"],
        ]
    )
    def post(self, datacollectionplanid):
        """Queue a scan from a datacollectionplan"""
        return self._parent.queue_scan(datacollectionplanid)


class AvailableScansResource(ComponentResource):
    @marshal(out=[[200, paginated(AvailableScansSchema), "A list of available scans"]])
    def get(self, **kwargs):
        """Get a list of available scans"""
        return self._parent.get_available_scans()


class Samplescan(Component, DCUtilsMixin):
    _base_url = "samplescan"
    _config_schema = ScanConfigSchema()

    def setup(self):
        self._scan_actors = []
        self.register_route(AvailableScansResource, "")
        self.register_route(QueueScanResource, "/queue/<int:datacollectionplanid>")

        self._generate_scan_actors()

    def reload(self):
        self._generate_scan_actors()

    def preprocess(self, **kwargs):
        sample = self._metadata.get_samples(kwargs["sampleid"])
        if not sample:
            raise AttributeError(f"No such sample {kwargs['sampleid']}")
        kwargs["sampleid"] = sample["sampleid"]
        kwargs["sample"] = sample["name"]
        kwargs["sessionid"] = g.blsession.get("sessionid")
        (
            kwargs["containerqueuesampleid"],
            kwargs["datacollectionplanid"],
        ) = self._metadata.queue_sample(
            kwargs["sampleid"],
            kwargs.get("datacollectionplanid"),
            scanparameters=kwargs,
        )
        kwargs["before_scan_starts"] = self.before_scan_starts
        kwargs["update_datacollection"] = self.update_datacollection
        kwargs["open_attachment"] = self._open_dc_attachment
        kwargs["add_scanqualityindicators"] = self.add_scanqualityindicators
        kwargs["enqueue"] = kwargs.get("enqueue", True)
        return kwargs

    def _generate_scan_actors(self):
        """Dynamically generate scan actor resources"""

        def post(self, **kwargs):
            pass

        for scan in self._config["scans"]:
            name = scan["actor"]

            # populate actors at config root
            if "actors" not in self._config:
                self._config["actors"] = {}
            self._config["actors"][name] = name
            if "actors_config" not in self._config:
                self._config["actors_config"] = {}
            self._config["actors_config"][name] = scan

            if name in self._actors:
                continue

            self._actors.append(name)
            self._scan_actors.append(name)
            fn = require_control(actor(name, preprocess=True)(post))

            if scan.get("require_staff"):
                fn = require_staff(fn)

            act_res = type(
                name,
                (ComponentResource,),
                {"post": fn, "preprocess": self.preprocess},
            )
            self.register_actor_route(act_res, f"/{name}")

    def get_available_scans(self):
        scans = []
        for scan in self._config["scans"]:
            tags = scan.get("tags", [])
            if (not scan.get("require_staff")) or (
                scan.get("require_staff") and g.user.staff()
            ):
                s = {"actor": scan["actor"], "tags": tags}
                scans.append(s)

        return {"total": len(scans), "rows": scans}

    def before_scan_starts(self, actor):
        """Saving directory is created"""
        if actor.get("datacollectionid"):
            self._save_dc_params(actor)

    def actor_started(self, actid, actor):
        """Callback when an actor starts

        For scan actors this will generate a datacollection
        """
        if actor.name in self._scan_actors:
            if actor.metatype is not None:
                self.start_datacollection(actor)
            else:
                self.initialize_saving(actor)

        logger.info(f"Actor '{actor.name}' with id '{actid}' started")

    def actor_success(self, actid, response, actor):
        """Callback when an actor finishes successfully

        For scan actors this update the datacollection with the endtime and
        'success' status
        """
        if actor.name in self._scan_actors:
            if actor.get("datacollectionid"):
                self.update_datacollection(
                    actor, endtime=datetime.now(), runstatus="Successful", emit_end=True
                )
                self._save_dc_log(actor)
            else:
                self.actor_remove(actid, actor)

        logger.info(f"Actor '{actor.name}' with id '{actid}' finished")

    def actor_error(self, actid, exception, actor):
        """Callback when an actor fails

        For scan actors this will update the datacollection with the end time and
        'failed' status
        """
        status = "Aborted" if isinstance(exception, ComponentActorKilled) else "Failed"
        if actor.name in self._scan_actors:
            if actor.get("datacollectionid"):
                self.update_datacollection(
                    actor, endtime=datetime.now(), runstatus=status
                )
                if status == "Failed":
                    self._save_dc_exception(actor, exception)
            else:
                self.actor_remove(actid, actor)
        if status == "Failed":
            logger.error(f"Actor '{actor.name}' with id '{actid}' failed")
        else:
            logger.info(f"Actor '{actor.name}' with id '{actid}' aborted")

    def actor_remove(self, actid, actor):
        """Callback when an actor is removed from the queue

        For scan actors this will remove the item from the database queue
        """
        if actor.name in self._scan_actors:
            self._metadata.unqueue_sample(
                actor["sampleid"],
                containerqueuesampleid=actor["containerqueuesampleid"],
                no_context=True,
            )

    def queue_scan(self, datacollectionplanid):
        """Queue a data collection plan"""
        datacollectionplan = self._metadata.get_datacollectionplans(
            datacollectionplanid=datacollectionplanid
        )
        if not datacollectionplan:
            return {"error": "Data collection plan not found"}, 404

        if datacollectionplan["datacollectionid"]:
            return (
                {
                    "error": f"That data collection plan has already been executed `datacollectionid`: {datacollectionplan['datacollectionid']}"
                },
                400,
            )

        params = {
            "sampleid": datacollectionplan["sampleid"],
            **datacollectionplan["scanparameters"],
        }

        if "actor" not in params:
            return (
                {
                    "error": "No actor specified within the selected data collection plan"
                },
                400,
            )

        actor = params.pop("actor")
        params.pop("component")

        # TODO: This all needs factoring out of `actor_wrapper` to make
        # it reusable
        schema = self.actor_schema(actor)
        schema_inst = schema()
        try:
            schema_inst.load(params)
        except ValidationError as err:
            return {"messages": err.messages}, 422

        params["datacollectionplanid"] = datacollectionplan["datacollectionplanid"]
        params = self.preprocess(**params)

        actkw = {
            "success": self.actor_success,
            "start": self.actor_started,
            "error": self.actor_error,
            "remove": self.actor_remove,
        }
        uuid = self.actor(actor, actargs=params, **actkw)

        if uuid:
            log.get("user").info(f"New actor created '{actor}'", type="actor")
            return {"message": f"Actor created with uuid {uuid}"}
        else:
            return {"error": "Could not create actor"}, 400
