#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os

from daiquiri.core.saving.bliss_basic import Bliss_BasicSavingHandler

logger = logging.getLogger(__name__)


class Bliss_EsrfSavingHandler(Bliss_BasicSavingHandler):
    def _set_filename(self, proposal=None, sample=None, dataset=None):
        self.scan_saving.proposal_name = proposal
        self.scan_saving.collection_name = sample
        self.scan_saving.dataset_name = dataset

    def _set_metadata(self, **metadata):
        self.scan_saving.dataset.all.definition = metadata.get("dataset_definition")
        self.scan_saving.dataset.sample_name = metadata.get("sample_name")
        self.scan_saving.dataset.sample_description = metadata.get("sample_description")

    @property
    def proposal_root_path(self):
        return os.path.join(
            self.scan_saving.base_path,
            self.scan_saving.template.format(
                proposal_dirname=self.scan_saving.proposal,
                beamline=self.scan_saving.beamline,
                proposal_session_name=self.scan_saving.proposal_session_name,
                collection_name="{sample}",
                dataset_name="{dataset}",
            ),
        ).split("{")[0]
