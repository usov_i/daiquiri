#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pathlib
import time
import logging
from bliss.config.static import get_config
from daiquiri.core.saving.base import SavingHandler

logger = logging.getLogger(__name__)


def find_existing(path):
    path_ = pathlib.Path(path)
    if path_.exists():
        logger.info(f"Found existing path: `{path}`")
        return path

    return find_existing(path_.parent.absolute())


class Bliss_BasicSavingHandler(SavingHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.session = get_config().get(self._config["saving_session"])

    @property
    def scan_saving(self):
        return self.session.scan_saving

    def _set_filename(self, base_path=None, template=None, data_filename=None):
        self.scan_saving.base_path = base_path
        self.scan_saving.template = os.path.join(template, data_filename)
        self.scan_saving.data_filename = data_filename

    @property
    def filename(self):
        return self.scan_saving.filename

    def create_root_path(self, wait_exists=False, wait_timeout=360):
        self.scan_saving.create_root_path()

        # If the writer is running on another machine there can be an nfs delay
        # syncing the newly created directory, here we can wait for this
        if wait_exists:
            waited = 0
            if not os.path.exists(self.scan_saving.root_path):
                existing_parent = find_existing(self.scan_saving.root_path)
                while not os.path.exists(self.scan_saving.root_path):
                    # Touching the existing parent can cause nfs to refresh more quickly
                    # -> caveats unknown
                    pathlib.Path(existing_parent).touch()

                    if waited == wait_timeout:
                        logger.warning(
                            f"Timed out waiting for root path: {self.scan_saving.root_path} to be created"
                        )
                        break

                    time.sleep(1)
                    waited += 1

                logger.info(
                    f"Waited {waited}s for `{self.scan_saving.root_path}` to appear"
                )

    def create_path(self, path):
        self.scan_saving.create_path(path)
