#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
import logging
from marshmallow import fields, validate

from bliss.config.static import get_config
from bliss import current_session
from tomo.fulltomo import FullFieldTomo

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)

try:
    from tomo.globals import ACTIVE_TOMOCONFIG
except ImportError:
    ACTIVE_TOMOCONFIG = None


_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class FullfieldscanSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)
    scan_type = OneOf(
        ["Step", "Continuous"],
        dump_default="Step",
        metadata={"title": "Scan type"},
    )
    start_pos = fields.Float(
        dump_default=0,
        metadata={"title": "Start angle", "unit": "deg"},
    )
    end_pos = fields.Float(
        dump_default=360,
        metadata={"title": "End angle", "unit": "deg"},
    )
    tomo_n = fields.Int(
        dump_default=360,
        validate=validate.Range(min=1, max=999999),
        metadata={"title": "Nb steps"},
    )
    n_dark = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb darks"},
    )
    n_flat = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb flats"},
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        dump_default=1,
        metadata={"title": "Exposure time", "unit": "s"},
    )
    use_sinogram = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Sinogram",
            "description": "Generate or not a sinogram during the scan",
        },
    )
    comment = fields.Str(
        dump_default="",
        metadata={
            "title": "Comment",
        },
    )
    enqueue = fields.Bool(title="Queue Scan", dump_default=True)

    class Meta:
        uiorder = [
            "sampleid",
            "scan_type",
            "expo_time",
            "start_pos",
            "end_pos",
            "tomo_n",
            "use_sinogram",
            "n_dark",
            "n_flat",
            "comment",
            "enqueue",
        ]
        uigroups = [
            "sampleid",
            "scan_type",
            {
                "Main": ["expo_time", "start_pos", "end_pos", "tomo_n"],
                "ui:minwidth": 12,
            },
            {"Options": ["use_sinogram", "n_dark", "n_flat"], "ui:minwidth": 12},
            "comment",
            "enqueue",
        ]
        uischema = {
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "comment": {"ui:widget": "textarea", "ui:options": {"rows": 3}},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }


class FullfieldscanActor(ComponentActor):
    schema = FullfieldscanSchema
    name = "[tomo] full tomo scan"

    metatype = "tomo"

    def method(
        self,
        scan_type,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        use_sinogram,
        n_dark,
        n_flat,
        comment,
        before_scan_starts,
        update_datacollection,
        **kwargs,
    ):
        bliss_config = get_config()
        actor_config = self.get_config()

        tomo_config = (
            ACTIVE_TOMOCONFIG is not None and ACTIVE_TOMOCONFIG.deref_active_object()
        )
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")

        fullfield_object_names = actor_config.get("fullfield_object")
        if fullfield_object_names is None:
            raise RuntimeError(
                "'fullfield_name' config field was not setup in this samplescan actor description"
            )
        if isinstance(fullfield_object_names, str):
            fullfield_object_names = [fullfield_object_names]

        for name in fullfield_object_names:
            fulltomo = bliss_config.get(name)
            if not isinstance(fulltomo, FullFieldTomo):
                raise RuntimeError(
                    f"'Object {name} is expected to be a {FullFieldTomo.name}. Found {type(fulltomo)}"
                )
            if fulltomo.tomo_config is tomo_config:
                break
        else:
            raise RuntimeError(
                f"No fullfield object was found for tomo config '{tomo_config.name}'"
            )

        tomocam = tomo_config.detectors.active_detector
        if tomocam is None:
            raise RuntimeError(f"No active detector selected in `{tomo_config.name}`")
        cam = tomocam.detector

        before_scan_starts(self)

        mg = current_session.active_mg
        mg.disable_all()
        try:
            mg.add(cam.image)
        except Exception:  # nosec
            pass
        mg.enable(cam.image.fullname)

        fulltomo.pars.scan_type = scan_type.upper()
        fulltomo.pars.activate_sinogram = use_sinogram
        if use_sinogram:
            # A sinogram is expected so a dark will be acquired
            fulltomo.pars.dark_at_start = True
            fulltomo.pars.flat_at_start = True
        fulltomo.pars.flat_n = n_flat
        fulltomo.pars.dark_n = n_dark
        fulltomo.pars.comment = comment

        scan_info = {
            "daiquiri": {"sampleid": self["sampleid"], "sessionid": self["sessionid"]}
        }
        datacollectionid = self.get("datacollectionid")
        if datacollectionid:
            scan_info["daiquiri"]["datacollectionid"] = datacollectionid

        fulltomo.basic_scan(
            start_pos, end_pos, tomo_n, expo_time, scan_info=scan_info, run=False
        )

        def run():
            fulltomo.run()

        greenlet = gevent.spawn(run)
        try:
            greenlet.join()
        except ComponentActorKilled:
            greenlet.kill()
            raise

        greenlet.get()
