#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
import logging
from marshmallow import fields, validate

from bliss.config.static import get_config
from bliss import current_session
from tomo.zseries import ZSeries

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)

try:
    from tomo.globals import ACTIVE_TOMOCONFIG
except ImportError:
    ACTIVE_TOMOCONFIG = None


_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class ZseriesscanSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)
    scan_type = OneOf(
        ["Step", "Continuous"],
        dump_default="Step",
        metadata={"title": "Scan type"},
    )
    nb_scans = fields.Int(
        dump_default=2,
        validate=validate.Range(min=1, max=999999),
        metadata={"title": "Nb scans"},
    )
    z_step = fields.Float(
        dump_default=0,
        metadata={"title": "Z step", "unit": "mm"},
    )
    start_pos = fields.Float(
        dump_default=0,
        metadata={"title": "Start angle", "unit": "deg"},
    )
    end_pos = fields.Float(
        dump_default=360,
        metadata={"title": "End angle", "unit": "deg"},
    )
    tomo_n = fields.Int(
        dump_default=360,
        validate=validate.Range(min=1, max=999999),
        metadata={"title": "Nb steps"},
    )
    n_dark = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb darks"},
    )
    n_flat = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb flats"},
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        dump_default=1,
        metadata={"title": "Exposure time", "unit": "s"},
    )
    use_sinogram = fields.Bool(
        dump_default=True,
        metadata={
            "title": "Sinogram",
            "description": "Generate or not a sinogram during the scan",
        },
    )

    comment = fields.Str(
        dump_default="",
        metadata={
            "title": "Comment",
        },
    )
    enqueue = fields.Bool(title="Queue Scan", dump_default=True)

    class Meta:
        uiorder = [
            "sampleid",
            "scan_type",
            "expo_time",
            "nb_scans",
            "z_step",
            "start_pos",
            "end_pos",
            "tomo_n",
            "use_sinogram",
            "n_dark",
            "n_flat",
            "comment",
            "enqueue",
        ]
        uigroups = [
            "sampleid",
            "scan_type",
            {
                "Main": ["expo_time", "start_pos", "end_pos", "tomo_n"],
                "ui:minwidth": 12,
            },
            {
                "Z-series": ["nb_scans", "z_step"],
                "ui:minwidth": 6,
            },
            {"Options": ["use_sinogram", "n_dark", "n_flat"], "ui:minwidth": 12},
            "comment",
            "enqueue",
        ]
        uischema = {
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "comment": {"ui:widget": "textarea", "ui:options": {"rows": 3}},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }


class ZseriesscanActor(ComponentActor):
    schema = ZseriesscanSchema
    name = "[tomo] full tomo scan"

    metatype = "tomo"

    def method(
        self,
        scan_type,
        nb_scans,
        z_step,
        start_pos,
        end_pos,
        tomo_n,
        expo_time,
        use_sinogram,
        n_dark,
        n_flat,
        comment,
        before_scan_starts,
        update_datacollection,
        **kwargs,
    ):
        bliss_config = get_config()
        actor_config = self.get_config()

        tomo_config = (
            ACTIVE_TOMOCONFIG is not None and ACTIVE_TOMOCONFIG.deref_active_object()
        )
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")

        zseries_object_names = actor_config.get("zseries_object")
        if zseries_object_names is None:
            raise RuntimeError(
                "'fullfield_name' config field was not setup in this samplescan actor description"
            )
        if isinstance(zseries_object_names, str):
            zseries_object_names = [zseries_object_names]

        for name in zseries_object_names:
            zseries = bliss_config.get(name)
            if not isinstance(zseries, ZSeries):
                raise RuntimeError(
                    f"'Object {name} is expected to be a {ZSeries.name}. Found {type(zseries)}"
                )
            if zseries.tomo_config is tomo_config:
                break
        else:
            raise RuntimeError(
                f"No fullfield object was found for tomo config '{tomo_config.name}'"
            )

        tomocam = tomo_config.detectors.active_detector
        if tomocam is None:
            raise RuntimeError(f"No active detector selected in `{tomo_config.name}`")
        cam = tomocam.detector

        before_scan_starts(self)

        mg = current_session.active_mg
        mg.disable_all()
        try:
            mg.add(cam.image)
        except Exception:  # nosec
            pass
        mg.enable(cam.image.fullname)

        zseries.pars.scan_type = scan_type.upper()
        zseries.pars.activate_sinogram = use_sinogram
        if use_sinogram:
            # A sinogram is expected so a dark will be acquired
            zseries.pars.dark_at_start = True
            zseries.pars.flat_at_start = True
        zseries.pars.flat_n = n_flat
        zseries.pars.dark_n = n_dark
        zseries.pars.comment = comment

        scan_info = {
            "daiquiri": {"sampleid": self["sampleid"], "sessionid": self["sessionid"]}
        }
        datacollectiongroupid = self.get("datacollectiongroupid")
        if datacollectiongroupid is not None:
            scan_info["daiquiri"]["datacollectiongroupid"] = datacollectiongroupid

        zseries.basic_scan(
            delta_pos=z_step,
            nb_scans=nb_scans,
            tomo_start_pos=start_pos,
            tomo_end_pos=end_pos,
            tomo_n=tomo_n,
            expo_time=expo_time,
            step_start_pos=None,
            scan_info=scan_info,
            run=False,
        )

        def run():
            zseries.run()

        greenlet = gevent.spawn(run)
        try:
            greenlet.join()
        except ComponentActorKilled:
            greenlet.kill()
            raise

        greenlet.get()
