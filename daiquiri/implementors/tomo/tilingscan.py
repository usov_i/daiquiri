#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import fields, validate
from daiquiri.core.schema.validators import OneOf

from tomo.sequence.tiling import tiling
from tomo.sequence.tiling import tiling_estimation_time


from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
)

try:
    from tomo.globals import ACTIVE_TOMOCONFIG
except ImportError:
    ACTIVE_TOMOCONFIG = None

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class TilingscanSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)

    # FIXME: Units have to be read from the motors
    # FIXME: Motor names have to be read from the motor
    start_x = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=-250,
        metadata={"title": "Start", "unit": "㎜"},
    )
    end_x = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=250,
        metadata={"title": "Stop", "unit": "㎜"},
    )
    start_y = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=-250,
        metadata={"title": "Start", "unit": "㎜"},
    )
    end_y = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=250,
        metadata={"title": "Stop", "unit": "㎜"},
    )
    start_z = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=550,
        metadata={"title": "Start", "unit": "㎜"},
    )
    end_z = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        dump_default=-550,
        metadata={"title": "Stop", "unit": "㎜"},
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        dump_default=None,
        metadata={"title": "Exposure time", "unit": "s"},
    )
    sleep_time = fields.Float(
        validate=validate.Range(min=0.0, max=10),
        dump_default=0,
        metadata={"title": "Sleep time", "unit": "s"},
    )
    n_dark = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb darks"},
    )
    n_flat = fields.Int(
        dump_default=1,
        validate=validate.Range(min=0, max=999999),
        metadata={"title": "Nb flats"},
    )
    lateral_motor_mode = OneOf(
        ["over_rot", "under_rot"], title="Lateral motor", dump_default="over_rot"
    )
    restore_motor_positions = fields.Bool(
        title="Move back motors",
        metadata={
            "description": "Move back motors at their initial position when the scan is terminated"
        },
        dump_default=True,
    )
    enqueue = fields.Bool(title="Queue Scan", dump_default=True)

    class Meta:
        uiorder = [
            "sampleid",
            "expo_time",
            "sleep_time",
            "n_dark",
            "n_flat",
            "lateral_motor_mode",
            "restore_motor_positions",
            "start_x",
            "end_x",
            "start_y",
            "end_y",
            "start_z",
            "end_z",
            "enqueue",
        ]
        uigroups = [
            "sampleid",
            {
                "Options": [
                    "expo_time",
                    "sleep_time",
                    "n_dark",
                    "n_flat",
                    "lateral_motor_mode",
                    "restore_motor_positions",
                ],
                "ui:minwidth": 12,
            },
            {
                "X-motor": ["start_x", "end_x"],
                "ui:minwidth": 6,
            },
            {
                "Y-motor": ["start_y", "end_y"],
                "ui:minwidth": 6,
            },
            {
                "Z-motor": ["start_z", "end_z"],
                "ui:minwidth": 6,
            },
            "enqueue",
        ]
        uischema = {
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }

    def calculated(self, data, **kwargs):
        """Returns the calculated values

        Arguments:
            data: Dictionary containing the actual parameters of the form
        """
        result = {}

        if data.get("expo_time") is None:
            # Trick to feed the initial expo time based on the tomo imaging device
            tomo_config = (
                ACTIVE_TOMOCONFIG is not None
                and ACTIVE_TOMOCONFIG.deref_active_object()
            )
            if tomo_config is None:
                raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
            imaging = tomo_config.tomo_imaging
            if imaging is not None:
                result["expo_time"] = imaging.exposure_time
            else:
                result["expo_time"] = 1.0

        return result

    def time_estimate(self, data):
        result = self.calculated(data)
        data.update(result)
        try:
            tomo_config = (
                ACTIVE_TOMOCONFIG is not None
                and ACTIVE_TOMOCONFIG.deref_active_object()
            )
            if tomo_config is None:
                raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
            tomo_det = tomo_config.detectors.active_detector
            if tomo_det is not None:
                tomo_cam = tomo_det.detector
            else:
                tomo_cam = None

            if tomo_cam is None:
                raise RuntimeError(
                    f"No active detector selected in `{tomo_config.name}`"
                )

            start_x = data["start_x"]
            end_x = data["end_x"]
            start_y = data["start_y"]
            end_y = data["end_y"]
            start_z = data["start_z"]
            end_z = data["end_z"]

            expo_time = data["expo_time"]
            sleep_time = data.get("sleep_time", 0)  # FIXME: It can be uninitialized...
            n_dark = data["n_dark"]
            n_flat = data["n_flat"]
            lateral_motor_mode = data["lateral_motor_mode"]
            restore_motor_positions = data["restore_motor_positions"]
        except Exception:
            _logger.error("Error while reading data", exc_info=True)
            return 0
        try:
            return tiling_estimation_time(
                start_x,
                end_x,
                start_y,
                end_y,
                start_z,
                end_z,
                tomo_cam,
                expo_time=expo_time,
                sleep_time=sleep_time,
                n_dark=n_dark,
                n_flat=n_flat,
                lateral_motor_mode=lateral_motor_mode,
                restore_motor_positions=restore_motor_positions,
                tomoconfig=tomo_config,
            )
        except Exception:
            _logger.error("Error while computing estimation", exc_info=True)
            return 0


class TilingscanActor(ComponentActor):
    schema = TilingscanSchema
    name = "[tomo] tiling scan"

    metatype = "tomo"

    def method(
        self,
        start_x,
        end_x,
        start_y,
        end_y,
        start_z,
        end_z,
        expo_time,
        sleep_time,
        n_dark,
        n_flat,
        lateral_motor_mode,
        restore_motor_positions,
        before_scan_starts,
        update_datacollection,
        **kwargs,
    ):
        tomo_config = (
            ACTIVE_TOMOCONFIG is not None and ACTIVE_TOMOCONFIG.deref_active_object()
        )
        if tomo_config is None:
            raise RuntimeError("No ACTIVE_TOMOCONFIG selected")
        tomo_det = tomo_config.detectors.active_detector
        if tomo_det is not None:
            tomo_cam = tomo_det.detector
        else:
            tomo_cam = None

        if tomo_cam is None:
            raise RuntimeError(f"No active detector selected in `{tomo_config.name}`")

        tomo_det.sync_hard()
        if not tomo_det.is_online:
            raise RuntimeError(f"{tomo_cam.name} is not online")
        if not tomo_det.check_ready_for_acquisition():
            raise RuntimeError(f"{tomo_cam.name} is not ready")

        tiling(
            start_x,
            end_x,
            start_y,
            end_y,
            start_z,
            end_z,
            tomo_cam,
            expo_time=expo_time,
            sleep_time=sleep_time,
            n_dark=n_dark,
            n_flat=n_flat,
            lateral_motor_mode=lateral_motor_mode,
            restore_motor_positions=restore_motor_positions,
            tomoconfig=tomo_config,
        )
