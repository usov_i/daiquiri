from marshmallow import fields

from daiquiri.core.components.celery import CeleryTaskActor, CeleryTaskSchema


class SleepSchema(CeleryTaskSchema):
    sleep_time = fields.Int(dump_default=60)
    raise_exception = fields.Bool()
    reject = fields.Bool()


class SleepActor(CeleryTaskActor):
    schema = SleepSchema
    task = "sleep"
