import time
import numpy as np
import cv2

from daiquiri.core.components import ComponentActor


class AutofocusActor(ComponentActor):
    name = "autofocus"

    def method(self, *args, **kwargs):
        print("autofocus", self["z_iterations"], self["z_increment"])

        max_contrast = 0
        max_contrast_pos = None

        current = self["z_motor"].get("position")
        start = current - (self["z_iterations"] * self["z_increment"])
        for i in range(self["z_iterations"] * 2):
            position = start + self["z_increment"] * i

            self["z_motor"].move(position)
            self["z_motor"].wait()

            time.sleep(kwargs["camera"].get("exposure") * 1.2)

            contrast = self.get_canny()
            print("point", i, position, contrast)
            if contrast > max_contrast:
                max_contrast = contrast
                max_contrast_pos = position

        print("max contrast", max_contrast, "at", max_contrast_pos)
        if max_contrast_pos is not None:
            self["z_motor"].move(max_contrast_pos)
            self["z_motor"].wait()

    def get_michelson(self):
        """Calculate the Michelson contrast

        https://en.wikipedia.org/wiki/Contrast_(vision)#Michelson_contrast
        """
        frame = self["camera"].frame()
        Y = cv2.cvtColor(frame, cv2.COLOR_RGB2YUV)[:, :, 0]

        min = np.min(Y)
        max = np.max(Y)

        contrast = (max - min) / (max + min)
        print("min", min, "max", max, "contrast", contrast)

        return contrast

    def get_canny(self):
        """Use canny edge detection to detect frame sharpness

        https://medium.com/@sedara/how-the-camera-autofocus-feature-works-in-digital-smartphones-8382d511996c
        https://github.com/opencv/opencv/blob/master/samples/cpp/videocapture_gphoto2_autofocus.cpp
        """
        frame = self["camera"].frame()
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

        gray = cv2.GaussianBlur(gray, (7, 7), 1.5, 1.5)
        edges = cv2.Canny(gray, 0, 30, 3)

        total = np.sum(edges) / (frame.shape[0] * frame.shape[1])
        print("total", total)

        return total
