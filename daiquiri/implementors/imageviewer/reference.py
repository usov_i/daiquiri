#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil

from marshmallow import fields, validates_schema, ValidationError

from PIL import Image

from daiquiri.core.components import ComponentActor, ComponentActorSchema
from daiquiri.core.components.utils.tileimage import generate_pyramid


class ReferenceSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True, metadata={"title": "Sampleid"})
    file_path = fields.Str(required=True, metadata={"title": "File Path"})
    description = fields.Str(required=True, metadata={"title": "Description"})

    @validates_schema
    def schema_validate(self, data, **kwargs):
        if not os.path.exists(data["file_path"]):
            raise ValidationError("Selected file does not exist")

        _, ext = os.path.splitext(data["file_path"])
        if ext.lower()[1:] not in ["jpg", "jpeg", "png"]:
            raise ValidationError("File must be a jpg or png")

    class Meta:
        uischema = {
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }


class ReferenceActor(ComponentActor):
    schema = ReferenceSchema
    name = "reference"
    saving_args = {"data_filename": "{sampleid.name}_reference{sampleactionid}"}

    def method(
        self,
        file_path,
        description,
        **kwargs,
    ):
        file_base = f"reference_{self['sampleactionid']}"
        output_filename = os.path.join(self["base_path"], f"{file_base}.h5")
        snapshot_filename = os.path.join(self["base_path"], f"{file_base}_snapshot.jpg")
        original_filename = os.path.join(self["base_path"], os.path.basename(file_path))

        shutil.copy(file_path, original_filename)

        generate_pyramid(output_filename, file_path, 100, 1.0)

        with Image.open(file_path) as thumb:
            thumb.thumbnail((1024, 1024), Image.LANCZOS)
            thumb.save(snapshot_filename)

        self.metadata.update_sampleaction(
            sampleactionid=self["sampleactionid"],
            no_context=True,
            resultfilepath=output_filename,
            xtalsnapshotafter=snapshot_filename,
            xtalsnapshotbefore=original_filename,
            message=description,
        )
