import time
from daiquiri.core.components import ComponentActor


class Upload_CanvasActor(ComponentActor):
    name = "upload_canvas"

    def method(self, *args, **kwargs):
        """This actor will allow external processing of the current canvas
        view in the client

        Kwargs:
            sample(dict): The current sample dictionary
            image(str): Base64 encoded image

        """
        print(kwargs["sample"])
        time.sleep(1)
