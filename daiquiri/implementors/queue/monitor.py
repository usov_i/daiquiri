from daiquiri.core.components import ComponentActor
from daiquiri.core.hardware.bliss.session import *

from bliss.config.static import get_config


class MonitorActor(ComponentActor):
    def method(self):
        """This actor determines whether the queue is ready to process

        Returns:
            ready (boolean): If the queue is ready to process
        """
        cfg = get_config()
        omega = cfg.get("omega")
        return omega.position != 90
