#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
from marshmallow import fields
import mmh3

from bliss.config.static import get_config
from bliss.scanning.scan import ScanState

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)
from daiquiri.core.hardware.bliss.session import *


class Scan2Schema(ComponentActorSchema):
    sampleid = fields.Int(required=True)
    motor_start = fields.Float(required=True, metadata={"title": "Start Position"})
    motor_end = fields.Float(required=True, metadata={"title": "End Position"})
    detectors = fields.List(
        OneOf(["diode", "simu1", "lima_simulator"]),
        required=True,
        metadata={"title": "Detectors", "uniqueItems": True, "minItems": 1},
    )

    class Meta:
        uischema = {"sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class Scan2Actor(ComponentActor):
    schema = Scan2Schema
    name = "scan2"
    saving_args = {"data_filename": "{sampleid.name}_scan{datacollectionid}"}

    def method(self, **kwargs):
        cfg = get_config()

        kwargs["motor"] = cfg.get("omega")
        kwargs["npoints"] = 5
        kwargs["time"] = 1
        kwargs["detectors"] = [cfg.get(d) for d in kwargs["detectors"]]

        scan = simple_scan(run=False, **kwargs)

        greenlet = gevent.spawn(scan.run)

        scan.wait_state(ScanState.STARTING)
        kwargs["update_datacollection"](
            self,
            datacollectionnumber=mmh3.hash(scan.node.db_name) & 0xFFFFFFFF,
            imagecontainersubpath="1.1/measurement",
            numberofimages=kwargs["npoints"],
            exposuretime=kwargs["time"],
        )

        try:
            greenlet.join()
        except ComponentActorKilled as e:
            greenlet.kill()
            raise e

        greenlet.get()
