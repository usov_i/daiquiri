#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.components.parameteriser import (
    ParameteriserActor,
    ParameteriserActorSchema,
)


class ScircleSchema(ParameteriserActorSchema):
    center_horizontal = fields.Float(
        required=True, metadata={"title": "Center Horizontal"}
    )
    center_vertical = fields.Float(required=True, metadata={"title": "Center Vertical"})
    diameter = fields.Float(required=True, metadata={"title": "Diameter"})


class ScircleActor(ParameteriserActor):
    schema = ScircleSchema
    name = "circle"
    parameter_type = "sample"
