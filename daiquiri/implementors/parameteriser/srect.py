#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import fields

from daiquiri.core.components.parameteriser import (
    ParameteriserActor,
    ParameteriserActorSchema,
)


class SrectSchema(ParameteriserActorSchema):
    center_horizontal = fields.Float(
        required=True, metadata={"title": "Center Horizontal"}
    )
    center_vertical = fields.Float(required=True, metadata={"title": "Center Vertical"})
    size_horizontal = fields.Float(required=True, metadata={"title": "Size Horizontal"})
    size_vertical = fields.Float(required=True, metadata={"title": "Size Vertical"})


class SrectActor(ParameteriserActor):
    schema = SrectSchema
    name = "rect"
    parameter_type = "sample"
