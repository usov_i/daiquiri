# Basic Configuration
All configuration of Daiquiri is done using yaml files

!!! tip
    An example of each configuration file can be found in `daiquiri/resources/example_config`

!!! info
    For general usage configuration files should be stored in the local beamline project i.e. `daiquiri_idxx/daiquiri_idxx/resources/config`. This should be cookiecutter'd from the [https://gitlab.esrf.fr/ui/daiquiri-local](daiquiri-local) project. This project can provide a set a preconfigured files using the `sensible-defaults` branch

## app.yml
This file defines the core configuration of the application. The example can be copied from `daiquiri/resources/example_config` to `daiquiri_idxx/daiquiri_idxx/resources/config/app.yml` and updated accordingly.

### Secrets
Unique secrets should be generated for `iosecret`, `secret`, and `url_secret`, this can be done with:
```bash
cat /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1
```

### SSL
SSL must be enabled and for this to work a local ssl key / cert pair must be available which matche the machine name, then can be defined with:
```yaml
ssl: true
ssl_cert: my_cert.bundle
ssl_key: my_key.key
```

### Authentication
Authentication is modular, currently supporting `ldap`
```yaml
auth_type: ldap
auth_server: ldap.esrf.fr
```

### Metadata
The `metadata` handler should be set to `ispyalchemy` to make use of the mysql database. For worse case scenarios it is possible to drop back to a `mock` `metadata` handler. The `meta_beamline` should match the beamline session information, usually the beamline in upper case. `meta_staff` denotes the permission for which daiquiri deems the current user to be a staff member
```yaml
meta_type: ispyalchemy
meta_user: daiquiri
meta_pass: pass
meta_url: host:3306/daiquiri
meta_beamline: ID21
meta_staff: id21_staff
```

By default sessions are visible one hour either side of their start time. This can be modified for users and staff
independently:

```yaml
meta_session_grace: 2

# A different value for staff members
meta_session_grace_staff: 24
```

### Saving
Two saving types are currently supported `bliss_basic` and `bliss_esrf`. The basic saving makes use of Bliss's standard scan saving, where as the `bliss_esrf` saving plugin requires the bliss esrf saving policy to be enabled and will ingest datasets automatically into ICAT using the metadata manager, where a single actor execution will produce a dataset.

To configure the `bliss_basic` scan saving the follow yml properties are required:
```yaml
saving_type: bliss_basic
saving_session: bliss_session
saving_arguments:
    base_path: "/data/{meta_beamline}/tmp/{sessionid.proposal}"
    template: "{sampleid.name}"
    data_filename: "{datacollectionid}"
```

Using `bliss_esrf` the data policy can be automatically applied by using daiquiri's saving, an example config is as follows:
```yaml
saving_type: bliss_esrf
saving_session: bliss_session
saving_arguments:
    proposal: "{sessionid.proposal}"
    sample: "{sampleid.name}"
    dataset: "{datacollectionid}"
```

Extra metadata can be provided to ICAT for each dataset by providing the `saving_metadata` property. For example:
```yaml
saving_metadata:
    sample_name: "{sampleid.name}"
    sample_description: "{componentid.description}"
    sample_composition: "{componentid.sequence}"
    dataset_definition: "{metadata_definition}"
```

These properties will be passed into ICAT to define details about the sample and the `definition` field

### Controls Session
A controls session can be injected automatically into actors to make available session globals:
```yaml
controls_session_type: bliss
controls_session_name: bliss_session
```

### Implementors
The implementors directive should point to the local project implementors package name. This will be automatically set by the `daiquiri-server-idxx` shell script
```yaml
implementors: daiquiri_id21.implementors
```

## Components
Components can be loaded by creating a yml file with a `component` key in the `resources/config` folder. The `daiquiri/resources` folder comes with a set of sensible defaults. A list of available components can be found in [Components](components/introduction.md) 

For example `config/scans.yml`
```yaml
component: scans
...
some_value: true
...
```

### Swagger-UI
Automatic documentation of the API is provided by Swagger-UI. It is disabled by default but can be enabled via:

```yaml
swagger: true
```

Once enabled it can be accessed at `http://localhost:8080/swagger-ui/`
