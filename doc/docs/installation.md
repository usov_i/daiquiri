# Installation
Check out the repository

```bash
git clone https://gitlab.esrf.fr/ui/daiquiri.git
cd daiquiri
```

Create a conda environment
```bash
conda create -n daiquiri
conda activate daiquiri
conda install --file requirements-conda.txt
pip install -r requirements-pip.txt
pip install --no-deps -e .
```

Or use an existing one and install the Daiquri dependencies
```bash
conda activate current_env
conda install --file requirements-conda.txt
pip install -r requirements-pip.txt
pip install --no-deps -e .
```

### ESRF Specific Installation

At ESRF Bliss is used as the controls system, this needs to be installed into the 
daiquiri environment. The local bliss installation should be used to be consistent
with other beamline operation:
```bash
cd ~/local/bliss.git
conda activate daiquiri
conda install --file requirements-conda.txt
pip install --no-deps -e .
```

## Create a local beamline project

Create a local beamline project from the cookiecutter template, the only required information is the beamline, press enter to all other questions. The `feature/sensible-defaults` branch can be used to create a default app configuration.
```bash
pip install -U cookiecutter
cookiecutter https://gitlab.esrf.fr/ui/daiquiri-local  --checkout feature/sensible-defaults
cd daiquiri_idxx
git init
```

and locally install it
```bash
pip install --no-deps -e .
```

Various parts of the default configuration will need to be edited. See [Configuration](configuration.md)

## Starting the server

Now use the local template script to launch the server. Assuming [Daiquiri UI](https://gitlab.esrf.fr/ui/daiquiri-ui) was installed in a sibling directory daiquiri-ui
```bash
conda activate myenv
daiquiri-server-idxx --static-folder /path/to/daiquiri-ui/build
```
