# Samplescan

Sample Scan is a simple component that allows [Actors](../actors.md) to be
executed against a sample.

The configuration yaml just requires a list of scans.

An example config is provided as follow:

```yaml
scans:
  - actor: scan1
  - actor: scan2
    require_staff: true
```

## Actor source code

By default the actor source code is read from a python module defined inside
the `implementors` package. This is usually defined in the Daiquiri configuration.

For an ESRF beamline it is usually defined as `id00.daiquiri.implementors`.

In this case the following actor, identified as `scan2` is then read from the
python module `id00.daiquiri.implementors.scan2`.

```yaml
  - actor: scan2
```

## Implementor field

An optional `implementor` field can be specified to use a specific python
module instead of the default one.

```yaml
  - actor: scan2
    implementor: id00.daiquiri.foobar
```

In this case the python file `foobar.py` from the package `id00.daiquiri` is
read to define and execute the actor.

## Require staff field

An optional `require_staff` field can be specified to restrict the use of the
actor to the staff members.

```yaml
  - actor: scan2
    require_staff: true
```

## Config field

An optional configuration field can be defined.

This can be used to reuse the same actor implementation multiple times.

```yaml
  - actor: foo1
    implementor: id00.daiquiri.foo
    config:
       stuff_to_use: 1
  - actor: foo2
    implementor: id00.daiquiri.foo
    config:
       stuff_to_use: 2
```

This configuration can be used during the execution of the actor the following
way:

```python
    def method(self, *args, **kwargs):
        actor_config = self.get_config()
        stuff_to_use = actor_config.get("stuff_to_use")
        print("stuff_to_use", stuff_to_use)
```

## Tags field

An optional `tags` field can be defined. It list a set of tag names per scan actors.

This tags are used in the front end to filter the scans according to a
specific use case.

With the following description a specific part of the UI can be configured to
only display the scan actors tagged as `my-scan`, hiding the actor `tomoscan`.
And other part of the UI will still display everything.

```yaml
  - actor: tomoscan     # <- this one will not be displayed
  - actor: tomotiling1
    tags:
      - my-scan
  - actor: tomotiling2
    tags:
      - my-scan
```

The tomo UI defines actually 2 tags. This tags can be used by the configuration
to enhance the interface with extra scan procedures.

- `tomo-scan` for actors available in the tomo detector view.
- `tomo-tiling` for actors available in the tiling view.
  At the creation of this actor, some parameters will be set to define the range of motors
  requested by the user from a 3D box ROI.
