# File Browser

The File Browser component adds endpoints to serve an arboresence of directories and file contents (image data and metadata) to implement OS-like browsing. The starting point of the arboresence is given by the `root` field in the config. The files to be served can be filtered according to their types (`file_types`) or their visibility (`show_hidden`).

The component also defines `container_file_types` to define file types that must be treated as directories that contain multiple file contents (e.g. multiple datasets stored in a single `hdf5` file).

### Example config

```yaml
show_hidden: False
root: /path/to/the/served/dir
container_file_types:
  - hdf5
  - h5

file_types:
  - cbf
  - hdf5
  - h5
  - sh
  - py
  - docx
  - txt
```
