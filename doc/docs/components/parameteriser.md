Parameteriser is a component that allows users to take advantage of daiquiri's form based parameter creation and validation, and use it output simple yaml files containing the corresponding parameters.

A basic config file is as such:

```yaml
component: parameteriser
root: parameteriser
parametertypes:
  sample:
    - actor: scircle
    - actor: srect
  measurement:
    - actor: mexafs
    - actor: mxanes
```

The yaml files will be saved into the current saving directory as so:

```
base_path + root + parametertype + name
```

Actors can be groups into types, for example a series of actors to create different types of samples, and a second group to create parameters for measurements. This allows the UI to serve distinct groups of parameters together, and allows editing them.

The actors themselves simply specify a schema to expect, and the actor itself can inherit from `ParameteriserActor`. For example:

```python
from marshmallow import fields

from daiquiri.core.components.parameteriser import (
    ParameteriserActor,
    ParameteriserActorSchema,
)


class ScircleSchema(ParameteriserActorSchema):
    center_horizontal = fields.Float(
        required=True, metadata={"title": "Center Horizontal"}
    )
    center_vertical = fields.Float(required=True, metadata={"title": "Center Vertical"})
    diameter = fields.Float(required=True, metadata={"title": "Diameter"})


class ScircleActor(ParameteriserActor):
    schema = ScircleSchema
    name = "circle"
    parameter_type = "sample"
```

If the `Meta.uiorder` is overwritten then two extra fields should be added:

- the inherited `name` field
- a hidden field `overwrite`

```python
class ScircleSchema(ParameteriserActorSchema):
    center_horizontal = fields.Float(
        required=True, metadata={"title": "Center Horizontal"}
    )
    center_vertical = fields.Float(required=True, metadata={"title": "Center Vertical"})
    diameter = fields.Float(required=True, metadata={"title": "Diameter"})

    class Meta:
        uiorder = [
            "name",
            "center_horizontal",
            "center_vertical",
            "diameter".
            "overwrite"
        ]
```
