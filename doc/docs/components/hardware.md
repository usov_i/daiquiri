# Hardware

The hardware component allows Daiquiri to interact with arbitrary hardware. Currently there are handlers for:

* Bliss
* Tango
* HardwareRepository
* Daiquiri

## Configuration
The `hardware.yml` configuration file is split into a number of segments:

### Protocols
First the relevant protocol handlers must be loaded. For a beamline using bliss and tango this might be as follows:

```yaml
protocols:
  - id:  bliss
    type: bliss
  - id:  tango
    type: tango
```

The id can then be referred to by the objects later

### Objects
Objects are defined as follows:
```yaml
objects:
  - name: omega 123
    id: omega
    protocol: bliss
    address: omega

  - name: lima
    id: lima_simulator
    protocol: tango
    type: lima
    tango_url: tango://localhost:20000/id00/limaccds/simulator1
```

All objects inherit from the [daiquiri.core.schema.hardware.HOConfigSchema][] which requires:

| Property | Example   | Description                 |
| -------- | --------- | --------------------------- |
| name     | omega 123 | long name of object (optional) |
| id       | omega     | short identifier            |
| protocol | bliss     | the protocol handler to use |
| require_staff | true | if the object requires a staff member. if true anyone can see the object, but only staff can update it |

If name is not provided the value from `id` will be used.

Objects can also be defined with a shorthand `url` notation:
```yaml
objects:
  - url: bliss://omega
  - url: tango://localhost:20000/id00/limaccds/simulator1
    type: lima
```

In this case `protocol` is inferred from the scheme, i.e. the part before ://. `id` and `name` will be defined from the last part of the url so in this case `omega` and `simulator1`, these can still be overriden by providing an `id` property

#### Bliss Objects
For bliss the type can be inferred from introspection, currently available types are defined in [daiquiri.core.hardware.bliss.BlissHandler][]. Bliss objects require in addition the `address`, this is just the identifier used to get the object from Beacon. The schema for bliss objects is defined in [daiquiri.core.hardware.bliss.BlissHOConfigSchema][]

```yaml
objects:
  - name: omega 123
    id: omega
    protocol: bliss
    address: omega
```

| Property | Example   | Description  |
| -------- | --------- | ------------ |
| address | omega | The object id in beacon  |

When defined with the `url` shorthand, `address` is defined by the part after ://
```yaml
objects:
  - url: bliss://omega
```
`address` will be `omega`

Currently available types are:

* motor
* multiposition
* shutter
* beamviewer

#### Tango Objects
For tango the type must be specified as this cannot be inferred from introspection. Tango object types are dynamically imported, available classes can be found in [daiquiri.core.hardware.tango][]. The schema for tango objects is defined in [daiquiri.core.hardware.tango.TangoHOConfig][]

```yaml
  - name: lima
    id: lima_simulator
    protocol: tango
    type: lima
    tango_url: tango://localhost:20000/id00/limaccds/simulator1
```

| Property | Example   | Description  |
| -------- | --------- | ------------ |
| type | lima | the object type  |
| tango_url| id00/limaccds/simulator1 | the tango url, can be fully qualified with tango://host:port/device/property |

Currently available types are:

* lima
* shutter
* frontend
* wago

##### Run Time Schema
For the [Wago][daiquiri.core.hardware.tango.wago] device type, the attributes exported can be defined at run time.

For example say one wanted to export a single attribute from a wago device:
```yaml
- name: tango generic
    id: generic
    protocol: tango
    type: wago
    tango_url: id00/tango/dummy
    attributes:
      - id: position
        name: Position
        type: float
        min: 0
        max: 10
        step: 0.5
        ui_schema:
          ui:widget: range
```
Attributes are defined according to [daiquiri.core.hardware.tango.TangoHOConfigAttribute][]

The `ui_schema` property allows the server to define how the attribute should be rendered in the ui. The default for a float would be a text input box, in this case the attribute is shown as a range slider

#### Daiquiri

!!! info
    The daiquiri protocol handler is automatically loaded so there is no need to define it  manually in protocol handlers

The daiquiri hardware protocol handler provides a simple way to map hardware objects to arbitrary code. It currently provides:

* httpcamera - map an jpeg stream to the daiquiri camera object

```yaml
objects:
  - id: camera
    protocol: daiquiri
    type: httpcamera
    camera_url: http://url.to.camera/image.jpg
```

### Groups
Objects can be grouped together using the groups directive

!!! warning
    Objects in a group are monitored together. This means that if the state of one object changes in the group
    all other objects in that group will be queried for their statuses at the same time. This can be detrimental
    to performance if some objects are sensitive to being polled regularly (serial lines, etc).

```yaml
groups:
  - name: Robot
    groupid: robot
    description: robot motors
    objects:
      - robz
      - roby
```

### Monitor

The monitor section tells the UI what to be made available in the
header of the daiquiri UI.

#### Hardware type

Without `type` property, the type is `hardware`.

In this case the `value` can contain a reference to an hardware name.

The panel shows the state as "good" or "bad" by using a
`comparator` and `comparison`. A secondary parameter can be shown on hover
by defining the `overlay` property.

```yaml
monitor:
  - name: Omega
    value: omega.position
    comparator: 'in'
    comparison: [50, 60]
    overlay: omega.position
  - name: S1D
    value: s1d.state
    comparator: '=='
    comparison: READY
  - name: robz
    value: robz.position
```

Available comparators are `<`, `<=`, `>`, `>=`, `==`, `!=`, `in`: where comparison is an array.

#### Proposal type

A panel can be setup with the active proposal name.

No extra parameters are needed.

```yaml
monitor:
  - name: The proposal
    type: proposal
```

#### Sample type

A panel can be setup with the active sample name.

No extra parameters are needed.

```yaml
monitor:
  - name: The sample
    type: sample
```
