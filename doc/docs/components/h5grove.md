# H5Grove

The h5grove component adds [h5grove](https://github.com/silx-kit/h5grove) endpoints `/attr/`, `/data/` and `/meta/` to daiquiri under the subdomain `/api/h5grove`.

The API is essentially the same as the [h5grove API](https://silx-kit.github.io/h5grove/api.html). The only difference is that, due to daiquiri's handling of files, the `file` query parameter of the request must be replaced by **one** of the following:

- `datacollectionid`: to query the file of a given data collection
- `autoprocprogramid`: to query a processed file
- `autoprocprogramattachmentid`: to query a processed file

Ex: `/attr/?file=<path>&path=/` becomes `/api/h5grove/attr/?datacollectionid=<id>&path=/` in daiquiri
