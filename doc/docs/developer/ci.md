# Continuous Integration

Gitlab CI will run an extensive pipeline upon branch / master commit including:

- checks:

  - flake8 for linting
  - black for style
  - bandit for security

- tests:

  - [coverage](https://ui.gitlab-pages.esrf.fr/daiquiri/coverage/) for master

- build

  - swagger/open api [specification](https://ui.gitlab-pages.esrf.fr/daiquiri/api/spec/spec.json)

- documentation
  - user documentation (these pages)
  - [redoc](https://github.com/Redocly/redoc) api [documentation](https://ui.gitlab-pages.esrf.fr/daiquiri/api/spec/)

## Update BLISS recipe

For BLISS testing the CI use a pinpointed env (except for BLISS `master`)
containing both Daiquiri and BLISS dependencies.

This ensure faster installation, homogeneity, reproductibilyty and stable installation
in the CI machines and in production.

This have to be updated time to time based on the BLISS version.

The BLISS version can be reached from a bliss clone the following way:
```
bliss.git$ git describe --tags --abbrev=0
1.11.0
```

The targetted daiquiri environement also includes test depepndencies to simplify
debug at beamlines.

When your daiquiri environement is properly tested versus this BLISS version,
you can freeze the dependencies in a yaml file the following way:

```
daiquiri.git$ conda env export -n daiquiri-env > deploy/prod-bliss-1.11.0.yml
```

The file finally have to be cleaned up to remove some metadata and some dependencies
like `bliss`/`blissdata`/`tomo`/`fscan`.
