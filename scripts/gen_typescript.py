"""
Execute this script to generate a typescript version of the Marshmallow
hardware objects
"""

import sys
import glob
import importlib
import inspect
import os
from marshmallow import Schema

# Workaround related to https://github.com/adenh93/typemallow/issues/3
from marshmallow import fields

fields.LocalDateTime = None

from daiquiri.core.hardware import abstract

try:
    from typemallow import ts_interface, generate_ts
except ImportError:
    git_hash = "b1ded183b2d3bbaa91e5931497167b2a984e0d8e"
    print("typemallow is not installed")
    print("This can be done the following way:")
    print()
    print("    pip install git+https://github.com/vallsv/typemallow.git@" + git_hash)
    print()
    sys.exit(-1)


def classes_from_package(package):

    current_dir = os.path.join(os.path.dirname(os.path.abspath(package.__file__)))
    current_module_name = os.path.splitext(os.path.basename(current_dir))[0]
    for file in glob.glob(current_dir + "/*.py"):
        name = os.path.splitext(os.path.basename(file))[0]

        # Ignore __ files
        if name.startswith("__"):
            continue
        module = importlib.import_module(
            package.__name__ + "." + name, package=current_module_name
        )

        for member in dir(module):
            handler_class = getattr(module, member)

            if handler_class and inspect.isclass(handler_class):
                yield name, handler_class


classes = []
for module_name, class_obj in classes_from_package(abstract):
    if "PropertiesSchema" in class_obj.__name__:
        print(class_obj)
        if class_obj.__name__.startswith("_"):
            class_obj.__name__ = module_name.capitalize() + "PropertiesSchema"
        classes.append(class_obj)


classes.sort(key=lambda c: c.__name__)
for c in classes:

    for key, value in c._declared_fields.items():
        value.required = True

    class Hardware(Schema):
        properties = fields.Nested(c, required=True)

    Hardware.__name__ = c.__name__.replace("PropertiesSchema", "Schema")
    # ts_interface()(c)
    ts_interface()(Hardware)


filename = "./Hardware.ts"
generate_ts(
    filename, strip_schema_keyword=False, oneof_as_enum=False, nested_inner=True
)
print(f"File '{filename}' generated")
