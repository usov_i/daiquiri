# Mostly stolen from gitlab.esrf.fr/bliss/bliss
default:
  before_script:
    # /dev/random is super slow
    # https://www.tango-controls.org/community/forum/c/platforms/gnu-linux/device-server-gets-stuck-then-works-as-expected/
    # https://stackoverflow.com/questions/26021181/not-enough-entropy-to-support-dev-random-in-docker-containers-running-in-boot2d
    - rm /dev/random
    - ln -s /dev/urandom /dev/random
    # set pip cache to the Docker volume
    - echo ${CI_PROJECT_DIR}
    - export PIP_CACHE_DIR="/opt/cache/pip"
    - export CONDA_ALWAYS_YES=1
    - export MAMBA_NO_BANNER=1
    - /opt/conda/bin/conda init && source /root/.bashrc
    - conda config --env --append channels esrf-bcu
    - conda config --env --append channels tango-controls

stages:
  - style
  - tests
  - build
  - build-doc
  - deploy

services:
  - name: esrfbcu/mimosa-database:main
    alias: mariadb

check_style:
  stage: style
  image: condaforge/mambaforge
  script:
    - mamba install black==22.3
    - LC_ALL=C.UTF-8 black --check --safe .

check_lint:
  stage: style
  image: condaforge/mambaforge
  script:
    - pip install flake8
    - flake8 --extend-exclude=daiquiri/implementors daiquiri/

check_security:
  stage: style
  image: condaforge/mambaforge
  script:
    - pip install bandit
    - bandit --ini .bandit -r daiquiri/
  allow_failure: true

.template_test:
  stage: tests
  image: condaforge/mambaforge
  variables:
    BLISS_HASH: master
  script:
    # install opengl libraries (needed to avoid problem with pyopengl dependency)
    - apt-get update && apt-get -y install libgl1-mesa-glx mariadb-client build-essential
    - git clone https://gitlab.esrf.fr/bliss/bliss bliss.git
    - cd bliss.git
    - git checkout $BLISS_HASH
    - BLISS_VERSION=$(git describe --tags --abbrev=0)
    - cd ..
    - conda config --env --append channels esrf-bcu
    - |
      if [ "$BLISS_HASH" == "master" ]; then
        cd bliss.git
        make dev_env NAME=apienv
        cd ..
        source activate apienv
        mamba install --file requirements-conda.txt
        pip install -r requirements-test.txt
      else
        # Create separate lima simulator environment
        if [[ "$LIMA_SIMULATOR_CONDA_ENV" != "" ]]; then
          mamba env create --name $LIMA_SIMULATOR_CONDA_ENV --file deploy/ci-lima-simulator.yml
        fi
        echo "BLISS_VERSION: $BLISS_VERSION"
        mamba env create --name apienv --file "deploy/prod-bliss-${BLISS_VERSION}.yml"
        source activate apienv
        pip install -e bliss.git --no-deps
      fi
    - pip install .
    - |
      if [[ "$FSCAN_HASH" != "" ]]; then
        pip install git+https://gitlab.esrf.fr/bliss/fscan@${FSCAN_HASH}
      fi
    - |
      if [[ "$TOMO_HASH" != "" ]]; then
        pip install git+https://gitlab.esrf.fr/tomo/ebs-tomo@${TOMO_HASH}
      fi
    - mv daiquiri _daiquiri  # make sure to test the package instead of the source
    - DB_POLICY=INSERT DAIQUIRI_META_URL=mariadb:3306/test pytest -v -s --ignore=bliss.git $PYTEST_ARGS

test_bliss_master:
  extends: .template_test
  variables:
    BLISS_HASH: master
  except:
    - main

test_bliss_1_11_x:
  extends: .template_test
  variables:
    BLISS_HASH: 1.11.x
    LIMA_SIMULATOR_CONDA_ENV: bliss_lima_simulator
  except:
    - main

test_bliss_1_10_x:
  extends: .template_test
  variables:
    BLISS_HASH: 1.10.x
    LIMA_SIMULATOR_CONDA_ENV: bliss_lima_simulator
  except:
    - main

test_bliss_1_11_x_tomo:
  extends: .template_test
  variables:
    BLISS_HASH: 1.11.x
    FSCAN_HASH: "master"
    TOMO_HASH: master
    LIMA_SIMULATOR_CONDA_ENV: bliss_lima_simulator
  except:
    - main

test_cov:
  extends: .template_test
  only:
    - main
  artifacts:
    when: always
    paths:
      - htmlcov
    expire_in: 7 days
  variables:
    PYTEST_ARGS: '--cov daiquiri --cov-report html:htmlcov'
    BLISS_HASH: master

  # Dont stop `main` pipeline if tests fail (so that docs are published)
  allow_failure: true

create_user_doc:
  stage: build-doc
  image: condaforge/mambaforge
  variables:
    TOMO_HASH: master
    FSCAN_HASH: master
  script:
    - apt-get update && apt-get -y install libgl1-mesa-glx build-essential
    - git clone https://gitlab.esrf.fr/bliss/bliss bliss.git
    - cd bliss.git
    - make dev_env NAME=mkdocsenv
    - cd ..
    - source activate mkdocsenv
    - mamba install --file requirements-conda.txt
    - pip install -r requirements-doc.txt
    - pip install .
    - pip install git+https://gitlab.esrf.fr/bliss/fscan@${FSCAN_HASH} --no-deps
    - pip install git+https://gitlab.esrf.fr/tomo/ebs-tomo@${TOMO_HASH} --no-deps
    - cd doc && mkdocs build
  artifacts:
    paths:
      - doc/site
    expire_in: 7 days

create_api_spec:
  extends: .template_test
  stage: build
  variables:
    PYTEST_ARGS: '--apispec tests/api/test_write_spec.py'
    BLISS_HASH: master
  artifacts:
    paths:
      - doc/api
    expire_in: 7 days
  only:
    - tags
    - main

create_api_doc:
  stage: build-doc
  image: node:14.15.0
  before_script:
    - ''
  script:
    - npm install -g redoc-cli
    - redoc-cli bundle -o doc/api/index.html doc/api/spec.json
  artifacts:
    paths:
      - doc/api
    expire_in: 7 days
  only:
    - tags
    - main

pages:
  stage: deploy
  before_script:
    - ''
  script:
    - mv doc/site public/
    - mv doc/api public/api/spec
    - |
      if [ -d "htmlcov" ]; then
        mv htmlcov public/coverage
      fi
  artifacts:
    paths:
      - public
    expire_in: 7 days
  only:
    - tags
    - main
